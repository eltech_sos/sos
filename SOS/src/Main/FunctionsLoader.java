package Main;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import simulation.Parser;

public class FunctionsLoader 
{
	public static ArrayList<String> elementaryFunctionsList;
	public static ArrayList<String> intermediaryFunctionsList;
	
	public static Map< String, Boolean > functionsCharacteristicsMap;
	
	public FunctionsLoader()
	{
		//Initialization
		elementaryFunctionsList = new ArrayList<String>();
		intermediaryFunctionsList = new ArrayList<String>();
		functionsCharacteristicsMap = new HashMap< String, Boolean >();
		
//elementary_functions
		//reading information about elementary_functions
        File docFile = new File("files/elementary_functions.xml");
        Document doc = null;
        try 
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(docFile);
        } 
        catch (java.io.IOException e) 
        {
            System.out.println("Can't find the file");
        } 
        catch (Exception e) 
        {
            System.out.print("Problem parsing the file");
        }
        
        Element root = doc.getDocumentElement();
        
        NodeList elementaryNodeList = root.getElementsByTagName("function");
        Element elementaryFunction;
        if( elementaryNodeList.getLength() > 0 )
        {
        	for( int i=0; i<elementaryNodeList.getLength(); i++ )
            {
        		//add current function to the elementaryFunctionsList
            	elementaryFunction = (Element) elementaryNodeList.item(i);
            	elementaryFunctionsList.add(elementaryFunction.getAttribute("name"));
            	
            	if( elementaryFunction.getAttribute( "participation").equals("true") )
            	{
            		functionsCharacteristicsMap.put( elementaryFunction.getAttribute("name") , true );
            	}
            	else
            	{
            		functionsCharacteristicsMap.put( elementaryFunction.getAttribute("name") , false );
            	}
            	
            	
            }
        }
        
        
//intermediary_functions
		//reading information about intermediary_functions
        docFile = new File("files/intermediary_functions.xml");
        doc = null;
        try 
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(docFile);
        } 
        catch (java.io.IOException e) 
        {
            System.out.println("Can't find the file");
        } 
        catch (Exception e) 
        {
            System.out.print("Problem parsing the file");
        }
        
        Element rootIntermediary = doc.getDocumentElement();
        
        NodeList intermediaryNodeList = rootIntermediary.getElementsByTagName("function");
        Element intermediaryFunction;
        if( intermediaryNodeList.getLength() > 0 )
        {
        	for( int i=0; i<intermediaryNodeList.getLength(); i++ )
            {
        		//add current function to the intermediaryFunctionsList
        		intermediaryFunction = (Element) intermediaryNodeList.item(i);
        		intermediaryFunctionsList.add(intermediaryFunction.getAttribute("name"));
            	
            	if( intermediaryFunction.getAttribute( "participation").equals("true") )
            	{
            		functionsCharacteristicsMap.put( intermediaryFunction.getAttribute("name") , true );
            	}
            	else
            	{
            		functionsCharacteristicsMap.put( intermediaryFunction.getAttribute("name") , false );
            	}
            	
            	
            }
        }
        
	}
	
	public static boolean isParticipate(String functionName)
	{
		return functionsCharacteristicsMap.get( functionName );
		
	}
	
	public static void setParticipateForFuncList( ArrayList<String> functionsNames, boolean participation)
	{
		String functionName = "";
		for( int i=0; i < functionsNames.size(); i++)
		{
			functionName = functionsNames.get( i );
			if( functionsCharacteristicsMap.containsKey( functionName ) )
			{
				functionsCharacteristicsMap.put(functionName, participation);
			}
		}
		
		if( functionsNames.size() > 0 )
		{
			saveCurrentElementaryFunctionsSet();
		}
	}
	
	public static Vector<String> findAllElementaryFunctionsIncludingParametr(String parametr)
	{
		Vector<String> findedFunctions = new Vector<String>();
		String function;
		boolean containsParametr = true;
		int tmpInd;
		//go through all elementary functions
		for(int funcInd=0; funcInd<elementaryFunctionsList.size(); funcInd++)
		{
			function = elementaryFunctionsList.get( funcInd );
			while( function.contains( parametr ) )
			{
				containsParametr = true;
				
				//checking what this parameter did not
				//part of another (like 'kl' part of 'klop')
				tmpInd = function.indexOf( parametr );
				if( tmpInd > 0 )
				{
					if( ( Parser.literalCollection.contains( function.charAt(tmpInd - 1 ) ) )
							|| (Parser.onlyDigitCollection.contains( function.charAt(tmpInd - 1 ) ) ) )
					{
						containsParametr = false;
					}
				}
				if( containsParametr )
				{
					tmpInd = function.indexOf(parametr) + parametr.length();
					if( tmpInd < function.length() )
					{
						if( ( Parser.literalCollection.contains( function.charAt(tmpInd) ) )
								|| (Parser.onlyDigitCollection.contains( function.charAt(tmpInd) ) ) )
						{
							containsParametr = false;
						}
					}
				}
				
				if( containsParametr )
				{
					if( ! findedFunctions.contains( elementaryFunctionsList.get( funcInd ) ) )
					{
						findedFunctions.add( elementaryFunctionsList.get( funcInd ) );
					}	
					function = "";//for checking end
				}
				else
				{
					tmpInd = function.indexOf(parametr);
					function = function.substring( tmpInd, function.length() );
					int i=0;
					for(i=0;i<function.length();i++)
					{
						if( (! Parser.literalCollection.contains( function.charAt(i) ) ) 
								&& (! Parser.onlyDigitCollection.contains( function.charAt(i) ) ) )
						{
							break;
						}
					}
					if( i >= function.length() )
					{
						function = "";
					}
					else
					{
						function = function.substring( i, function.length() );
					}
					
				}
				
			}//end of while
		}//end of for(int funcInd=0; funcInd<elementaryFunctionsList.size(); funcInd++) 
		
		if( findedFunctions.size() > 0 )
		{
			return findedFunctions;
		}
		else
		{
			return null;
		}
	}
	
	public static void replaceParameterNameInAllElementaryFunctions(String oldParamName, String newParamName)
	{
		String checkingFunctionPart, checkedFunctionPart;
		boolean containsParametr = true, functionWasChanged, anyFunctionWasChanged = false;
		int tmpInd;
		anyFunctionWasChanged = false;
		Vector<String> oldFunctionNameList = new Vector<String>();
		Vector<String> newFunctionNameList = new Vector<String>();
		//go through all elementary functions
		for(int funcInd=0; funcInd<elementaryFunctionsList.size(); funcInd++)
		{
			functionWasChanged = false;
			checkingFunctionPart = elementaryFunctionsList.get( funcInd );
			checkedFunctionPart = "";
			while( checkingFunctionPart.contains( oldParamName ) )
			{
				containsParametr = true;
				
				//checking what this parameter did not
				//part of another (like 'kl' part of 'klop')
				tmpInd = checkingFunctionPart.indexOf( oldParamName );
				if( tmpInd > 0 )
				{
					if( ( Parser.literalCollection.contains( checkingFunctionPart.charAt(tmpInd - 1 ) ) )
							|| (Parser.onlyDigitCollection.contains( checkingFunctionPart.charAt(tmpInd - 1 ) ) ) )
					{
						containsParametr = false;
					}
				}
				if( containsParametr )
				{
					tmpInd = checkingFunctionPart.indexOf(oldParamName) + oldParamName.length();
					if( tmpInd < checkingFunctionPart.length() )
					{
						if( ( Parser.literalCollection.contains( checkingFunctionPart.charAt(tmpInd) ) )
								|| (Parser.onlyDigitCollection.contains( checkingFunctionPart.charAt(tmpInd) ) ) )
						{
							containsParametr = false;
						}
					}
				}
				

				if( checkingFunctionPart.indexOf(oldParamName) > 0 )
				{
					checkedFunctionPart = checkedFunctionPart + checkingFunctionPart.substring( 0, checkingFunctionPart.indexOf(oldParamName) );
				}
				if( containsParametr )
				{
					functionWasChanged = true;
					checkedFunctionPart = checkedFunctionPart + newParamName;
					checkingFunctionPart = checkingFunctionPart.substring( checkingFunctionPart.indexOf(oldParamName) + oldParamName.length(), checkingFunctionPart.length() );
				}
				else
				{
					tmpInd = checkingFunctionPart.indexOf( oldParamName );
					checkingFunctionPart = checkingFunctionPart.substring( tmpInd, checkingFunctionPart.length() );
					for(tmpInd=0;tmpInd<checkingFunctionPart.length();tmpInd++)
					{
						if( (! Parser.literalCollection.contains( checkingFunctionPart.charAt(tmpInd) ) ) 
								&& (! Parser.onlyDigitCollection.contains( checkingFunctionPart.charAt(tmpInd) ) ) )
						{
							break;
						}
					}
					if( tmpInd >= checkingFunctionPart.length() )
					{
						checkedFunctionPart = checkedFunctionPart + checkingFunctionPart;
						checkingFunctionPart = "";//for checking end
					}
					else
					{
						checkedFunctionPart = checkedFunctionPart + checkingFunctionPart.substring( 0, tmpInd );
						checkingFunctionPart = checkingFunctionPart.substring( tmpInd, checkingFunctionPart.length() );
					}
				}
				
				
			}//end of while
			if( functionWasChanged )
			{
				anyFunctionWasChanged = true;
				oldFunctionNameList.add( elementaryFunctionsList.get( funcInd ) );
				newFunctionNameList.add( checkedFunctionPart+checkingFunctionPart );
//				changeElementaryFunctionName( elementaryFunctionsList.get( funcInd ), checkedFunctionPart+checkingFunctionPart );
			}
		}//end of for(int funcInd=0; funcInd<elementaryFunctionsList.size(); funcInd++) 
		
		if( anyFunctionWasChanged )
		{
			changeElementaryFunctionsNames(oldFunctionNameList,	newFunctionNameList, oldParamName, newParamName );
		}
	}
	
	public static void changeElementaryFunctionsNames( Vector<String> oldFunctionNameList, Vector<String> newFunctionNameList, String oldParamName, String newParamName )
	{
		boolean participation;
		String libraryFunction;
		int newAndOldFunctionIndex;//index for conformity in oldFunctionNameList and in newFunctionNameList
		for( int i=0; i<elementaryFunctionsList.size(); i++ )
		{
			libraryFunction = elementaryFunctionsList.get( i );
			if( oldFunctionNameList.contains( libraryFunction ) )
			{
				newAndOldFunctionIndex = oldFunctionNameList.indexOf( libraryFunction );
				participation = functionsCharacteristicsMap.get( libraryFunction );
				elementaryFunctionsList.set( elementaryFunctionsList.indexOf( libraryFunction ), newFunctionNameList.elementAt( newAndOldFunctionIndex ) );
				functionsCharacteristicsMap.put( newFunctionNameList.elementAt( newAndOldFunctionIndex ), participation );
			}
		}
		saveCurrentElementaryFunctionsSet();
		TypeLoader.changeElementaryFunctionsNamesInAllTypes( oldFunctionNameList, newFunctionNameList );
		TreeOperations.changeFuncsNamesInAllElementsInTree( oldFunctionNameList, newFunctionNameList, oldParamName, newParamName );
	}
	
	public static void changeElementaryFunctionName(String oldFuncName, String newFuncName)
	{
		boolean participation;
		if( elementaryFunctionsList.contains( oldFuncName ) && functionsCharacteristicsMap.containsKey( oldFuncName ) )
		{
			participation = functionsCharacteristicsMap.get(oldFuncName);
			elementaryFunctionsList.set( elementaryFunctionsList.indexOf( oldFuncName ), newFuncName );
			functionsCharacteristicsMap.remove(oldFuncName);
			functionsCharacteristicsMap.put( newFuncName, participation );
			TypeLoader.changeElementaryFunctionNameInAllTypes( oldFuncName, newFuncName );
			TreeOperations.changeFuncNameInAllElementsInTree( oldFuncName, newFuncName );
		}
	}
	
	
	public static boolean saveCurrentElementaryFunctionsSet()
	{
		File docFile = new File("files/elementary_functions.xml");
		try 
        {
			//Create a Document
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbf.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            
//            doc.setXmlVersion("1.0");
//Creating the XML tree begin
            //create the root element and add it to the document
            Element root = doc.createElement("function_list");
            doc.appendChild(root);
            
            Element funcNode;
            String funcName;
        	String participationString = null;
            for(int paramInd=0; paramInd<elementaryFunctionsList.size(); paramInd++)
            {
            	funcNode = doc.createElement("function");
            	funcName = elementaryFunctionsList.get( paramInd );
            	funcNode.setAttribute( "name", funcName );
            	participationString = "false";
            	if( functionsCharacteristicsMap.get( funcName ) )
            	{
            		participationString = "true";
            	}
            	funcNode.setAttribute( "participation", participationString );
            	root.appendChild(funcNode);
            }
//Creating the XML tree end
            
            //save to file
            String outputURL = docFile.getAbsolutePath();
            
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new FileOutputStream(outputURL));

            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.transform(source, result);

		} 
        catch (Exception e) 
        {
        	JOptionPane.showMessageDialog(null, "������ ��� ���������� ��������� � ���������� �������");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
}
