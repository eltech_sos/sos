package Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.tree.DefaultMutableTreeNode;

import org.jgraph.graph.DefaultGraphCell;

import object.TreeElement;
import simulation.ItselfOrganizationVariables;
import simulation.Parser;

public class TreeOperations
{
	public static TreeElement findElementInStruct(DefaultMutableTreeNode treeNode)
	{
		if( treeNode == null )
		{
			return null;
		}
		TreeElement elementInStruct;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.element.equals(treeNode) )
					{
						return Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					}
				}
			}
		}
		return null;
	}
	
	//used if FunctionOrganization if
	//parameters organization not allowed
	public static Map< String, Double > findAllUsedInSystemParametrsValues()
	{
		Map< String, Double >  usedInSystemParametrsValues;
		usedInSystemParametrsValues = new HashMap< String, Double >();
		if( Main.structTreeElements.size() > 0 )
		{
			TreeElement elementInStruct;
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.type == 3 )
					{
						if( elementInStruct.parametersValuesMap != null )
						{
							Iterator iter = elementInStruct.parametersValuesMap.entrySet().iterator();
							while( iter.hasNext() )
							{
								Entry paramInform = (Entry) iter.next();
					        	String paramName = (String) paramInform.getKey();
					        	if( ! usedInSystemParametrsValues.containsKey(paramName) )
					        	{
					        		usedInSystemParametrsValues.put(paramName, (Double) paramInform.getValue() );
					        	}
							}
						}
					}// end of if( elementInStruct.type == 3 )
				}
			}
		}
		return usedInSystemParametrsValues;	
	}
	
//	public static void setAllElementsNotCounted()
//	{
//		TreeElement elementInStruct;
//		if( Main.structTreeElements.size() > 0 )
//		{
//			for(int level=0; level<Main.structTreeElements.size(); level++)
//			{
//				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
//				{
//					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
////					elementInStruct.itsCounted = false;
//					elementInStruct.elementCount = 0;
//				}
//			}
//		}
//	}
	
	public static double rootCount()
	{
		Main.JCountingCount++;
		if( Main.structTreeElements.size() > 1 )
		{
			TreeElement elementInStruct;
			int level = Main.structTreeElements.size() - 1;
			
			//go through all levels from the last and
			//count all elements in each level
			while( level >= 0 )
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.type == 3)//if element is list
					{
//						listCount( elementInStruct );
						elementInStruct.elementCount = Parser.calculationOfFormula( elementInStruct );
//						elementInStruct.itsCounted = true;
					}
					else//if element is container
					{
						intermediateCount( elementInStruct );
					}
				}
				level--;
			}
			
			//return root value
			elementInStruct = (TreeElement)Main.structTreeElements.elementAt( 0 ).elementAt( 0 );
			return elementInStruct.elementCount;
		}
		
		return 0;
	}
	
	
	
//	public static double listCount(TreeElement element)
//	{
//		element.elementCount = Parser.calculationOfFormula( element );
//		element.itsCounted = true;
//		return element.elementCount;
//	}
	
	public static double intermediateCount(TreeElement element)
	{
		ArrayList<TreeElement> childList = findAllChildrenInStructByIDAndLevel( element.ID, element.Level );
		if( childList.size() > 0)
		{
			TreeElement childElem;
			double elemCount;
			String convolutionFunction;
			convolutionFunction = element.functionName;
			childElem = childList.get( 0 );
			if( convolutionFunction.equals( "sum(Xi)" )
					|| convolutionFunction.equals( "pow(Xi)" ))
			{
				elemCount = childElem.elementCount;
			}
			else if( convolutionFunction.equals( "sum(Xi*Ki)" )
					|| convolutionFunction.equals( "pow(Xi*Ki)" ))
			{
				elemCount = childElem.elementCount * TypeLoader.getFactorForIntermediateFunction( childElem.typeName, convolutionFunction);
			}
			else//( convolutionFunction.equals( "sum(Xi^Ki)" ) )
			{
				elemCount = Math.pow( childElem.elementCount, (double) TypeLoader.getFactorForIntermediateFunction( childElem.typeName, convolutionFunction) );
			}
			
			int childInd = 1; 
			while( childInd < childList.size() )
			{
				childElem = childList.get( childInd );
				if( convolutionFunction.equals( "sum(Xi)" ) )
				{
					elemCount = elemCount + childElem.elementCount;
				}
				else if( convolutionFunction.equals( "sum(Xi*Ki)" ) )
				{
					elemCount = elemCount + childElem.elementCount * TypeLoader.getFactorForIntermediateFunction( childElem.typeName, convolutionFunction);
				}
				else if( convolutionFunction.equals( "sum(Xi^Ki)" ) )
				{
					elemCount = elemCount + Math.pow( childElem.elementCount, (double) TypeLoader.getFactorForIntermediateFunction( childElem.typeName, convolutionFunction) );
				}
				else//if convolutionFunction is pow(Xi) or pow(Xi*Ki)
				{
					elemCount = elemCount * childElem.elementCount * TypeLoader.getFactorForIntermediateFunction( childElem.typeName, convolutionFunction);
				}
				childInd++;
			}
			element.elementCount = elemCount;
		}
		else
		{
			element.elementCount = 0;
		}
//		element.itsCounted = true;
		return element.elementCount;
	}
	
	public static TreeElement findElementInStructByCell(DefaultGraphCell treeCell)
	{
		TreeElement elementInStruct;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.cell == treeCell)
					{
						return Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					}
				}
			}
		}
		return null;
	}
	
	public static TreeElement findElementInStructByID(int ID)
	{
		TreeElement elementInStruct;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.ID == ID)
					{
						return Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					}
				}
			}
		}
		return null;
	}
	
	//must be used ONLY in Modulation and SimulationStep for comparison of element name and ID 
	public static int findElementIDInStructByName(String elemName)
	{
		TreeElement elementInStruct;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.elementName.equals( elemName ) )
					{
						return Main.structTreeElements.elementAt(level).elementAt(elemIndex).ID;
					}
				}
			}
		}
		return -10;
	}
	
//	public static Vector<TreeElement> findAllChildrenInStructByCell(DefaultGraphCell treeCell)
//	{
//		Vector<TreeElement> childrenList = new Vector<TreeElement>();
//		TreeElement elementInStruct;
//		if( Main.structTreeElements.size() > 0 )
//		{
//			for(int level=0; level<Main.structTreeElements.size(); level++)
//			{
//				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
//				{
//					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
//					if( elementInStruct.parentCell == treeCell)
//					{
//						childrenList.add(elementInStruct);
//					}
//				}
//			}
//		}
//		return childrenList;
//	}
	
	public static TreeElement findParentInStructByID(int parentID, int childLevel)
	{
		TreeElement elementInStruct;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt( childLevel-1 ).size(); elemIndex++)
			{
				elementInStruct = (TreeElement)Main.structTreeElements.elementAt( childLevel-1 ).elementAt(elemIndex);
				if( elementInStruct.ID == parentID )
				{
					return Main.structTreeElements.elementAt( childLevel-1 ).elementAt(elemIndex);
				}
			}
		}
		return null;		
	}
	
	public static ArrayList<TreeElement> findAllChildrenInStructByIDAndLevel( int parentID, int parentLevel )
	{
		ArrayList<TreeElement> childrenList = new ArrayList<TreeElement>();
		TreeElement elementInStruct;
		if( Main.structTreeElements.size() > 0 )
		{
			if( Main.structTreeElements.size() > ( parentLevel + 1 ) )
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt( parentLevel+1 ).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt( parentLevel+1 ).elementAt(elemIndex);
					if( elementInStruct.ParentID == parentID)
					{
						childrenList.add(elementInStruct);
					}
				}
			}
			else
			{
				return childrenList;
			}
		}
		return childrenList;
	}
	
//	public static Vector<TreeElement> findAllChildrenInStructByID( int parentID )
//	{
//		Vector<TreeElement> childrenList = new Vector<TreeElement>();
//		TreeElement elementInStruct;
//		if( Main.structTreeElements.size() > 0 )
//		{
//			elementInStruct = findElementInStructByID( parentID );
//			int parentLevel = elementInStruct.Level;
//			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt( parentLevel+1 ).size(); elemIndex++)
//			{
//				elementInStruct = (TreeElement)Main.structTreeElements.elementAt( parentLevel+1 ).elementAt(elemIndex);
//				if( elementInStruct.ParentID == parentID)
//				{
//					childrenList.add(elementInStruct);
//				}
//			}
//		}
//		return childrenList;
//	}
	
	
	public static void changeTypeNameInAllElementsInTree(String oldTypeName, String newTypeName)
	{
		TreeElement elementInStruct;
		boolean structWasChanged = false;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.typeName.equals(oldTypeName) )
					{
						elementInStruct.typeName = newTypeName;
						structWasChanged = true;
					}
				}
			}
			if( structWasChanged )
			{
				Main.panelGraphic.refreshTable();
			}
		}
	}
	
	//this method checking all elements in tree:
	//if element has selected type we checking function of 
	//this element: if this function did not contains in
	//function list of this type we replace this function
	//by the first in list function in current type.
	//This method is useful in situation like then we create 
	//element with type "type_01" and function "x+45", and
	//after that delete function "x+45" from the type "type_01"
	public static void checkAllFunctionsOfChangedType(String typeName)
	{
		TreeElement elementInStruct;
		boolean structWasChanged = false;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.typeName.equals(typeName) )
					{
						if( !TypeLoader.getFunctionList(elementInStruct.typeName).contains(elementInStruct.functionName) )
						{
							elementInStruct.functionName = TypeLoader.getFunctionList(elementInStruct.typeName).get( 0 );
							if( elementInStruct.type == 3 )
							{
								elementInStruct.parametersValuesMap = Main.parser.fillingParametersValuesMap( elementInStruct.functionName );
								fillingParametrsWeightMap( elementInStruct );
							}
							elementInStruct.elementCount = 0;
//							elementInStruct.itsCounted = false;
						}
						structWasChanged = true;
					}
				}
			}
			if( structWasChanged )
			{
				Main.panelGraphic.refreshTable();
			}
		}
	}
	
	//use if we change function name: 
	//a)changed just one function
	//b)parametrsValuesMap and parametrsWeightMap - recounted
	public static void changeFuncNameInAllElementsInTree(String oldFuncName, String newFuncName)
	{
		TreeElement elementInStruct;
		boolean structWasChanged = false;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( oldFuncName.equals( elementInStruct.functionName ) )
					{
						elementInStruct.functionName = newFuncName;
						if( elementInStruct.type == 3 )
						{
							elementInStruct.parametersValuesMap = Main.parser.fillingParametersValuesMap( elementInStruct.functionName );
							fillingParametrsWeightMap( elementInStruct );
						}
						elementInStruct.elementCount = 0;
//						elementInStruct.itsCounted = false;
						structWasChanged = true;
					}
				}
			}
			if( structWasChanged )
			{
				Main.panelGraphic.refreshTable();
			}
		}
	}
	
	//use if we change parameter name: 
	//a)changed several functions
	//b)parametrsValuesMap and parametrsWeightMap - stay as
	//  was but key changed from "oldParamName" to "newParamName"
	public static void changeFuncsNamesInAllElementsInTree(Vector<String> oldFuncNameList, Vector<String> newFuncNameList, String oldParamName, String newParamName)
	{
		TreeElement elementInStruct;
		boolean structWasChanged = false;
		int newAndOldFunctionIndex;//index for conformity in oldFunctionNameList and in newFunctionNameList
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( oldFuncNameList.contains( elementInStruct.functionName ) )
					{
						//change function name
						newAndOldFunctionIndex = oldFuncNameList.indexOf( elementInStruct.functionName );
						elementInStruct.functionName = newFuncNameList.elementAt( newAndOldFunctionIndex );
						
						//change key in parametrsValuesMap and parametrsWeightMap
						if( ( elementInStruct.type == 3 ) &&
								elementInStruct.parametersValuesMap.containsKey( oldParamName ) 
								&& elementInStruct.parametrsWeightMap.containsKey( oldParamName ) )
						{
							Double tpmValue;
							tpmValue = elementInStruct.parametersValuesMap.get(oldParamName);
							elementInStruct.parametersValuesMap.remove(oldParamName);
							elementInStruct.parametersValuesMap.put( newParamName, tpmValue );
							
							tpmValue = elementInStruct.parametrsWeightMap.get(oldParamName);
							elementInStruct.parametrsWeightMap.remove(oldParamName);
							elementInStruct.parametrsWeightMap.put( newParamName, tpmValue );
						}
						
						elementInStruct.elementCount = 0;
//						elementInStruct.itsCounted = false;
						
						structWasChanged = true;
					}
				}
			}
			if( structWasChanged )
			{
				Main.panelGraphic.refreshTable();
			}
		}
	}
	
	public static void checkAllBordersOfChangedParametr(String changedParameter)
	{
		TreeElement elementInStruct;
		if( Main.structTreeElements.size() > 0 )
		{
			boolean structWasChanged = false;
			double currentParamValue;
			double newLeftBorder = ParametersLoader.getLeftBorder( changedParameter );
			double newRightBorder = ParametersLoader.getRightBorder( changedParameter );
			double paramInitValue = ParametersLoader.getInitialValue( changedParameter );
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.type == 3 )
					{
						if( elementInStruct.parametersValuesMap.containsKey( changedParameter ) )
						{
							currentParamValue = elementInStruct.parametersValuesMap.get( changedParameter );
							if( ( currentParamValue < newLeftBorder ) 
									|| ( currentParamValue > newRightBorder ) )
							{
								structWasChanged = true;
								elementInStruct.parametersValuesMap.put(changedParameter, paramInitValue );
							}
						}
					}
				}
			}
		}
	}
	
	public static boolean findAnyElementWithType( String typeName )
	{
		if( Main.structTreeElements.size() > 0 )
		{
			TreeElement elementInStruct;
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.typeName.equals( typeName ) )
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public static boolean findAnyElementWithTypeAndChildrenNumberMoreThen( String typeName, int slotsNumber )
	{
		if( Main.structTreeElements.size() > 0 )
		{
			TreeElement elementInStruct;
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					//we did not check type of object because
					//if typeName is equals it must be intermediate element
					if( elementInStruct.typeName.equals( typeName ) )
					{
						if( elementInStruct.iChildrenCount > slotsNumber )
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public static boolean findAnyElemNotCorrespondingSelectedChildType( String typeName, boolean onlyLists )
	{
		if( Main.structTreeElements.size() > 0 )
		{
			TreeElement elementInStruct;
			ArrayList<TreeElement> childrenList;
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					//we did not check type of object because
					//if typeName is equals it must be intermediate element
					if( elementInStruct.typeName.equals( typeName ) )
					{
						if( elementInStruct.iChildrenCount > 0 )
						{
							childrenList = findAllChildrenInStructByIDAndLevel(elementInStruct.ID, elementInStruct.Level);
							if( onlyLists )
							{
								//if element children must be only 
								//elementary
								for( int i=0; i < childrenList.size(); i++ )
								{
									if( childrenList.get(i).type == 2 )
									{
										return true;
									}
								}
							}
							else
							{
								//if element children must be only 
								//intermediate
								for( int i=0; i < childrenList.size(); i++ )
								{
									if( childrenList.get(i).type == 3 )
									{
										return true;
									}
								}
							}
							
						}
					}
				}
			}
		}
		return false;
	}
	
	public static boolean typeAndFunctionOfElementIsCorrect(TreeElement checkingElem)
	{
		if( checkingElem.type == 3 )//if element is list
		{
			if( TypeLoader.elementaryTypesList.contains( checkingElem.typeName ) )
			{
				if( ! TypeLoader.getFunctionList( checkingElem.typeName ).contains( checkingElem.functionName ) )
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		else//if element is intermediary 
		{
			if( TypeLoader.intermediaryTypesList.contains( checkingElem.typeName ) )
			{
				if( ! TypeLoader.getFunctionList( checkingElem.typeName ).contains( checkingElem.functionName ) )
				{
					return false;
				}
			}
			else
			{
				return false;
			}
		}
		return true;
	}
	
	
//	public static boolean checkAllParametrsValuesInElement(TreeElement checkingElem)
//	{
//		if( checkingElem.type == 3 )
//		{
//			if( ! checkingElem.parametrsValuesMap.isEmpty() )
//			{
//				Iterator iter = checkingElem.parametrsValuesMap.entrySet().iterator();
//				while( iter.hasNext() )
//				{
//					Entry paramInform = (Entry) iter.next();
//		        	String paramName = (String) paramInform.getKey();
//		        	double paramValue = (Double) paramInform.getValue();
//		        	if( ParametrsLoader.parametrsList.contains(paramName) )
//		        	{
//		        		if( ( ParametrsLoader.getLeftBorder( paramName ) > paramValue)
//		        				|| (ParametrsLoader.getRightBorder(paramName) < paramValue ) )
//		        		{
//		        			float initValue;
//		        			double initValueDouble;
//		        			initValue = ParametrsLoader.getInitialValue( paramName );
//		        			initValueDouble = initValue;
//		        			checkingElem.parametrsValuesMap.put( paramName, initValueDouble );
//		        		}
//		        	}
//		        	else
//		        	{
//		        		//if element appears parameter witch 
//		        		//is not described in library
//		        		return false;
//		        	}
//				}
//			}
//		}
//		return true;
//	}
	
	public static void fillingParametrsWeightMap(TreeElement fillingElem)
	{
		double initialWeight = 1;
		fillingElem.parametrsWeightMap = new HashMap<String, Double>();
		if( fillingElem.parametersValuesMap.size() > 0 )
		{
			Iterator iter = fillingElem.parametersValuesMap.entrySet().iterator();
			while( iter.hasNext() )
			{
				Entry paramInform = (Entry) iter.next();
	        	String paramName = (String) paramInform.getKey();
	        	
	        	fillingElem.parametrsWeightMap.put(paramName, initialWeight);
			}
		}
	}
	
	public static ArrayList<TreeElement> findAllListsWithNotParticipateType( )
	{
		ArrayList<TreeElement> rezultList = new ArrayList<TreeElement>();
		TreeElement elementInStruct;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt( level ).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt( level ).elementAt( elemIndex );
					if( elementInStruct.type == 3 )//if element is list
					{
						if( ! TypeLoader.isParticipate( elementInStruct.typeName ) )
						{
							rezultList.add(elementInStruct);
						}
					}
				}
			}
		}
		return rezultList;
	}
	
	public static ArrayList<TreeElement> findAllListsWithNotParticipateFunction( )
	{
		ArrayList<TreeElement> rezultList = new ArrayList<TreeElement>();
		TreeElement elementInStruct;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt( level ).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt( level ).elementAt( elemIndex );
					if( elementInStruct.type == 3 )//if element is list
					{
						if( ! FunctionsLoader.isParticipate( elementInStruct.functionName ) )
						{
							rezultList.add( elementInStruct );
						}
					}
				}
			}
		}
		return rezultList;
	}
	
	public static ArrayList<TreeElement> findAllIntermediateWithNotParticipateType( )
	{
		ArrayList<TreeElement> rezultList = new ArrayList<TreeElement>();
		TreeElement elementInStruct;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt( level ).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt( level ).elementAt( elemIndex );
					if( elementInStruct.type == 2 )//if element is intermediate
					{
						if( ! TypeLoader.isParticipate( elementInStruct.typeName ) )
						{
							rezultList.add(elementInStruct);
						}
					}
				}
			}
		}
		return rezultList;
	}
	
	public static double countCurrentModelCost()
	{
		double modelCost = 0;
		if( Main.structTreeElements.size() > 0 )
		{
			TreeElement elementInStruct;
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					modelCost = modelCost + TypeLoader.getPrice( elementInStruct.typeName );
				}
			}
		}
		return modelCost;
	}
	
	
	//looking in current model parameters witch has 
	//different values in different elements
	public static ArrayList<String> findAllParametrsWithNotSameValues()
	{
		ArrayList<String> rezultList = new ArrayList<String>();
		Map< String, Double > paramTotalMap = new HashMap< String, Double >();
		TreeElement elementInStruct;
		String paramName;
		double paramValue;
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt( level ).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt( level ).elementAt( elemIndex );
					if( elementInStruct.type == 3 )//if element is list
					{
						if( elementInStruct.parametersValuesMap != null )
						{
							Iterator iter = elementInStruct.parametersValuesMap.entrySet().iterator();
							while( iter.hasNext() )
							{
								Entry paramInform = (Entry) iter.next();
					        	paramName = (String) paramInform.getKey();
					        	paramValue = (Double) paramInform.getValue();
					        	
					        	if( paramTotalMap.containsKey(paramName) )
								{
									if( paramTotalMap.get(paramName) != paramValue )
									{
										if( ! rezultList.contains(paramName) )
										{
											rezultList.add( paramName );
										}
									}
								}
					        	else
					        	{
					        		paramTotalMap.put( paramName, paramValue );
					        	}
					        	
							}	
						}
											
					}// end of if( elementInStruct.type == 3 )
				}
			}
		}
		return rezultList;
	}
	
	public static void setParamInAllModelByValues( ArrayList<String> paramNamesList, ArrayList<Double> paramValuesList )
	{
		if( ( Main.structTreeElements.size() > 0 ) && ( paramNamesList.size() > 0 ) )
		{
			
			TreeElement elementInStruct;
			String currentParamName;
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.type == 3 )
					{
						for(int i=0; i < paramNamesList.size(); i++)
						{
							currentParamName = paramNamesList.get( i );
							if( elementInStruct.parametersValuesMap.containsKey( currentParamName ) )
							{
								elementInStruct.parametersValuesMap.put( currentParamName, paramValuesList.get(i) );
							}
						}
					}//end of if( elementInStruct.type == 3 )
				}
			}
			
		}
	}
	
	public static int findMaxUsedSlotsForType( String typeName )
	{
		int maxMaxUsedSlotsCount = 0;
		
		if( Main.structTreeElements.size() > 0 )
		{
			
			TreeElement elementInStruct;
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.typeName.equals( typeName ) )
					{
						if( elementInStruct.iChildrenCount > maxMaxUsedSlotsCount )
						{
							maxMaxUsedSlotsCount = elementInStruct.iChildrenCount;
						}
					}
				}
			}
			
			
		}
		
		return maxMaxUsedSlotsCount;
	}
	
	
	//1:
	//go through all elements in tree from the bottom level
	//and define witch of them is always zero ( for that 
	//elements set .isZeroByStructure = true, for all another
	//elements set .isZeroByStructure = false)
	//element is zero(for intermediate looking only) if:
	//a)its has no lists
	//b)all its child is zero
	//c)one of child is zero and function of element is POW
	//
	//2:
	//go through all elements in tree and for each element
	//2.1)set .itsNeedUpForMax to true if its element must be set
	// to max value for max value in all model
	// or to false if we did not now it must be min or max
	//2.2)set .itsImportantForRootValue = false for elements 
	//witch parents is always zero or witch parents is
	//has .itsImportantForRootValue == false
	//and set .itsImportantForRootValue = true for
	//element witch parents not zero and important for root
	//value
	//3: filling 
	//3.1)ItselfOrganizationVariebles.notUsedInOrgElemFunctionsMeetingInModel and
	//3.2)ItselfOrganizationVariebles.notUsedInOrgElemTypesMeetingInModel and
	//3.3)ItselfOrganizationVariebles.notUsedInOrgInterTypesMeetingInModel
	//
	//return true if in all model did not meeting elements
	//with pow(Xi) or pow(Xi*Ki) functions (its means, that all
	//elements must be set to max for max value in all model)
	//or meeting POW elements has no children
	public static boolean defineNeedsOfUpOrDownForAllElements()
	{
		boolean notPowIntermediateInTree = true;
		ItselfOrganizationVariables.
		notUsedInOrgInterTypesMeetingInModel.clear();
		ItselfOrganizationVariables.notUsedInOrgElemFuncMeetingInModelList.clear();
		ItselfOrganizationVariables.
		notUsedInOrgElemTypesMeetingInModel.clear();
		
		if( Main.structTreeElements.size() > 1 )
		{
			String tmpName;
			TreeElement elementInStruct, elementInStructInNextLevel;
			int level, elemIndex, elemIndexInNextLevel;
			
//1:			
			Map< Integer, Integer > parentOfZeroElemMapPrevious,
			parentOfZeroElemMap;
			parentOfZeroElemMap = new HashMap< Integer, Integer >();
			parentOfZeroElemMapPrevious = new HashMap< Integer, Integer >();
			
			parentOfZeroElemMap.size();
			
			for(level = Main.structTreeElements.size() - 1;
				level >= 0; level--)
			{
				parentOfZeroElemMapPrevious.clear();
				parentOfZeroElemMapPrevious.putAll( parentOfZeroElemMap );
				parentOfZeroElemMap.clear();
				for(elemIndex=0; 
				elemIndex < Main.structTreeElements.elementAt( level ).size(); 
				elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					elementInStruct.isZeroByStructure = false;
					if( elementInStruct.type == 2 )//if element is intermediate
					{
						//3.3:
						//if in this element used not participate in
						//itself - organization type put it in
						//ItselfOrganizationVariebles.notUsedInOrgInterTypesMeetingInModel
						//if it is not already there
						tmpName = elementInStruct.typeName;
						if( ! TypeLoader.isParticipate(tmpName) )
						{
							if( ! ItselfOrganizationVariables.
									notUsedInOrgInterTypesMeetingInModel.
									contains(tmpName) )
							{
								ItselfOrganizationVariables.
								notUsedInOrgInterTypesMeetingInModel
								.add( tmpName );
							}
						}
						
						
						if( elementInStruct.iChildrenCount == 0 )
						{
							
							elementInStruct.isZeroByStructure = true;
							if( parentOfZeroElemMap.containsKey( elementInStruct.ParentID ) )
							{
								parentOfZeroElemMap.put( elementInStruct.ParentID , parentOfZeroElemMap.get(elementInStruct.ParentID)+1 );
							}
							else
							{
								parentOfZeroElemMap.put( elementInStruct.ParentID , 1 );
							}
							
						}
						else if( parentOfZeroElemMapPrevious.containsKey(elementInStruct.ID) )
						{
							if( (elementInStruct.iChildrenCount ==
								parentOfZeroElemMapPrevious.get(elementInStruct.ID) )
								|| elementInStruct.functionName.equals("pow(Xi)") 
								|| elementInStruct.functionName.equals("pow(Xi*Ki)") )
							{

								elementInStruct.isZeroByStructure = true;
								if( parentOfZeroElemMap.containsKey( elementInStruct.ParentID ) )
								{
									parentOfZeroElemMap.put( elementInStruct.ParentID , parentOfZeroElemMap.get(elementInStruct.ParentID)+1 );
								}
								else
								{
									parentOfZeroElemMap.put( elementInStruct.ParentID , 1 );
								}
								
							}
						}
					}//end action if element is intermediate
					else//if element is list
					{
						if( ItselfOrganizationVariables.funcLeafOrgAllowed )
						{
							//3.1:
							//if we use participation for functions
							//if in this element has participate type, but
							//used not participate in
							//itself - organization function put it(function) in
							//ItselfOrganizationVariebles.notUsedInOrgElemFunctionsMeetingInModel
							//if it is not already there
							tmpName = elementInStruct.functionName;
							if( ( ! FunctionsLoader.isParticipate(tmpName) )
									&& ( ItselfOrganizationVariables.useParticipationInFuncOrg ) )
							{
								if( ( TypeLoader.isParticipate( elementInStruct.typeName ) )
										|| ( ! ItselfOrganizationVariables.useParticipationInTypeOrg ) )
								{
									if( ! ItselfOrganizationVariables.notUsedInOrgElemFuncMeetingInModelList.
											contains(tmpName) )
									{
										ItselfOrganizationVariables.notUsedInOrgElemFuncMeetingInModelList
										.add( tmpName );
									}
								}
							}
							
							//3.2:
							//if we use participation for types
							//if in this element used not participate in
							//itself - organization type and participate function,
							//put it(type) in
							//ItselfOrganizationVariebles.notUsedInOrgElemTypesMeetingInModel
							//if it is not already there
							tmpName = elementInStruct.typeName;
							if( ( ! TypeLoader.isParticipate(tmpName) )
									&& ItselfOrganizationVariables.useParticipationInTypeOrg )
							{
								if( FunctionsLoader.isParticipate( elementInStruct.functionName ) 
										|| ( ! ItselfOrganizationVariables.useParticipationInFuncOrg ) )
								{
									if( ! ItselfOrganizationVariables.
											notUsedInOrgElemTypesMeetingInModel.
											contains(tmpName) )
									{
										ItselfOrganizationVariables.
										notUsedInOrgElemTypesMeetingInModel
										.add( tmpName );
									}
								}
							}
						}//end if( ItselfOrganizationVariebles.funcListOrgAllowed )
						else
							//if function organization for lists NOT allowed
							//we must save all used in model functions for we can
							//compare them with max function of selected type(if
							//this type is participate) then we will do type 
							//organization for lists
						{
							if( ( TypeLoader.isParticipate( elementInStruct.typeName ) )
									|| ( ! ItselfOrganizationVariables.useParticipationInTypeOrg ) )
							{
								tmpName = elementInStruct.functionName;
								if( ! ItselfOrganizationVariables.notUsedInOrgElemFuncMeetingInModelList.
										contains(tmpName) )
								{
									ItselfOrganizationVariables.notUsedInOrgElemFuncMeetingInModelList
									.add( tmpName );
								}
							}
						}
						
						
					}//end action if element is list
					
				}//end go through all elements on current level
			}//end go through all levels
			
//2:			
			
			boolean childMustBeMax = true;
			boolean childImportantForRootValue = true;
			
			//set characteristics for root
			elementInStruct = (TreeElement)Main.structTreeElements.elementAt( 0 ).elementAt( 0 );
			elementInStruct.itsNeedUpForMax = true;
			elementInStruct.itsImportantForRootValue = true;
			
			//go through all levels
			for(level = 0; level<Main.structTreeElements.size(); level++)
			{
				//go through all elements on current level
				for(elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					
					//reset .itsImportantForRootValue
					//if its need
					if( elementInStruct.isZeroByStructure )
					{
						elementInStruct.itsImportantForRootValue = false;
					}
					
					if( ( elementInStruct.type == 2 )
							&& ( elementInStruct.iChildrenCount > 0 ) )//if element is intermediate and has children
					{
						
						if( elementInStruct.functionName.equals("sum(Xi)")
								|| elementInStruct.functionName.equals("sum(Xi*Ki)")
								|| elementInStruct.functionName.equals("sum(Xi^Ki)") )
						{
							childMustBeMax = elementInStruct.itsNeedUpForMax;
						}
						else
						{
							notPowIntermediateInTree = false;
							childMustBeMax = false;
						}
						
						childImportantForRootValue = elementInStruct.itsImportantForRootValue;
						
						//go through next level and set .itsNeedUpForMax
						//and preliminary .itsImportantForRootValue(preliminary 
						//because if element at the next level is always zero 
						//then for it .itsImportantForRootValue = false)
						//
						for(elemIndexInNextLevel=0;
						elemIndexInNextLevel < Main.structTreeElements.elementAt( level + 1 ).size() ;
						elemIndexInNextLevel++)
						{
							elementInStructInNextLevel = (TreeElement)Main.structTreeElements.elementAt( level + 1 ).elementAt( elemIndexInNextLevel );
							if( elementInStructInNextLevel.ParentID == elementInStruct.ID )
							{
								elementInStructInNextLevel.itsImportantForRootValue = childImportantForRootValue;
								elementInStructInNextLevel.itsNeedUpForMax = childMustBeMax;
							}
						}
						
						
					}
				}//end go through all elements 
				//on current level
			}//end go through all levels

			
//tmp checking			
//			//go through all levels
//			for(level = 0; level<Main.structTreeElements.size(); level++)
//			{
//				System.out.println("");
//				//go through all elements on current level
//				for(elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
//				{
//					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
//					System.out.print(elementInStruct.elementName+" ( ");
//					if( elementInStruct.isZeroByStructure )
//					{
//						System.out.print("zero, ");
//					}
//					else
//					{
//						System.out.print("not zero, ");
//					}
//					if( elementInStruct.itsImportantForRootValue )
//					{
//						System.out.print("1, ");
//					}
//					else
//					{
//						System.out.print("0, ");
//					}
//					if( elementInStruct.itsNeedUpForMax )
//					{
//						System.out.print("up)  ");
//					}
//					else
//					{
//						System.out.print("down)  ");
//					}
//				}
//			}
				
			
		}
		return notPowIntermediateInTree;
	}
	
	public static boolean setFunctionsVectorToLists( ArrayList<String> funcList )
	{
		ArrayList<TreeElement> currentLeafsList;
		TreeElement list;
		currentLeafsList = ItselfOrganizationVariables.leafsList;
		if( currentLeafsList.size() ==  funcList.size() )
		{
			for( int listInd = 0; 
			listInd < currentLeafsList.size(); listInd++ )
			{
				list = currentLeafsList.get( listInd );
				
				//set new function name
				list.functionName = funcList.get( listInd );
				
				//filling parametrsValuesMap
				list.parametersValuesMap = Parser.fillingParametersValuesMap( list.functionName );
				
				//filling ParametrsWeightMap
				double initialWeight = 1.0;
				list.parametrsWeightMap = new HashMap<String, Double>();
				if( list.parametersValuesMap != null )
				{
					if( list.parametersValuesMap.size() > 0 )
					{
						Iterator iter = list.parametersValuesMap.entrySet().iterator();
						while( iter.hasNext() )
						{
							Entry paramInform = (Entry) iter.next();
				        	String paramName = (String) paramInform.getKey();
				        	
				        	list.parametrsWeightMap.put(paramName, initialWeight);
						}
					}
				}
				else
				{
					list.parametersValuesMap = new HashMap<String, Double> ();
				}
				
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	
	//find in model all elements witch children are lists or
	//not influent on result(zeroByStructure)
	public static ArrayList<TreeElement> findElementsWithOnlyListChildren()
	{
		
		TreeElement elementInStruct;
		ArrayList<TreeElement> rezultList = new ArrayList<TreeElement>();
		ArrayList<TreeElement> currentElemChildList = new ArrayList<TreeElement>();
		boolean allChildrenAreLists;
		
		if( Main.structTreeElements.size() > 0 )
		{
			for(int level=0; level< (Main.structTreeElements.size() - 1 ); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.type == 2 )//element is intermediate
					{
						if( elementInStruct.itsImportantForRootValue )
						{
							if( ( ! ItselfOrganizationVariables.
									useParticipationInTypeOrg )
									|| TypeLoader.isParticipate
									(elementInStruct.typeName) )
							{
								currentElemChildList.clear();
								currentElemChildList = 
									findAllChildrenInStructByIDAndLevel(
										elementInStruct.ID,
										elementInStruct.Level);
								if( currentElemChildList.size() > 0 )
								{
									allChildrenAreLists = true;
									for(int i=0; 
									( i < currentElemChildList.size() ) 
									&& allChildrenAreLists; 
									i++)
									{
										if( currentElemChildList.
												get(i).type == 2 
												&& ( ! currentElemChildList.
												get(i).itsImportantForRootValue ) )
										{
											allChildrenAreLists = false;
										}
									}
									if( allChildrenAreLists )
									{
										rezultList.add
										( elementInStruct );
									}
								}
							}
						}
					}
					
				}
			}
			return rezultList;
		}
		else
		{
			return null;
		}
	}
	
	
	
	
	
	
	
}
