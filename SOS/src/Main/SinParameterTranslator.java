/**
 * 
 */
package Main;


/**
 *  This class implements translation strategy defined by:
 *	Xi = 0.5*(Ai + Bi) + 0.5*(Bi - Ai)*sin(Zi)
 *
 * @author User
 */
final class SinParameterTranslator implements IParameterTranslator {

	/* (non-Javadoc)
	 * @see Main.IParameterTranslator#toLimited(java.lang.String, double)
	 */
	@Override
	public double toLimited(String paramName, double value) {
		//original 
		return 0.5 * (ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(0)/*leftBorder*/ 
				+ ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(1)/*rightBorder*/) 
				+ 0.5 * ( ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(1)/*rightBorder*/ 
						- ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(0)/*leftBorder*/) 
						* Math.sin( value );
	}

	/* (non-Javadoc)
	 * @see Main.IParameterTranslator#toUnlimited(java.lang.String, double)
	 */
	@Override
	public double toUnlimited(String paramName, double value) {
		//original
		value = (value - 0.5 * (ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(0)/*leftBorder*/ 
				+ ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(1)/*rightBorder*/)) 
				/ (0.5 * (ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(1)/*rightBorder*/ 
					- ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(0)/*leftBorder*/));
		return Math.asin(value);
	}

}
