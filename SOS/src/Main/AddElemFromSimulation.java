package Main;

import java.util.Vector;

import javax.swing.tree.DefaultMutableTreeNode;

import object.TreeElement;

public class AddElemFromSimulation 
{
	private DefaultMutableTreeNode newNode;
	
	public  AddElemFromSimulation( TreeElement treeElem, 
			TreeElement parentTreeElem )
	{
	//	DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
		newNode = new DefaultMutableTreeNode( treeElem.elementName );
		
		//create (if necessary)new tree level
		if( parentTreeElem.Level == Main.maxLevelsCount )
		{
			Main.structTreeElements.add(new Vector<TreeElement>());
			Main.maxLevelsCount++;
		}
		//add element to the structTreeElements
		Main.structTreeElements.elementAt( parentTreeElem.Level + 1 ).add(treeElem);
		/*for (int iter = 0; iter < Main.structTreeElements.elementAt(parentTreeElem.Level).size(); iter++){
			if (Main.structTreeElements.elementAt(parentTreeElem.Level).get(iter).ID == parentTreeElem.ID){
				Main.structTreeElements.elementAt(parentTreeElem.Level).get(iter).iChildrenCount++;
				break;
			}
		}*/
		treeElem.Level = parentTreeElem.Level + 1;
		treeElem.cell = Main.panelGraphic.addChildNode( parentTreeElem, treeElem.elementName,  treeElem.ID, treeElem.type);
		treeElem.parentCell = parentTreeElem.cell;
		Main.elementsNamesList.add( treeElem.elementName );
		
		treeElem.element = newNode;					
		Main.count++;
		
		DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) parentTreeElem.element;
		parentNode.add(newNode);
	}
}
