/**
 * 
 */
package Main;

/**
 * For use in ParametersLoader class to define parameter translation strategy
 * 
 * @author User
 *
 */
interface IParameterTranslator {
	
	 double toLimited(String paramName, double value);
	
	 double toUnlimited(String paramName, double value);

}
