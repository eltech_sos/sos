package Main;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import object.TreeElement;

public class AddObjectHandler implements WindowListener{
	
	private DefaultMutableTreeNode newNode;
	public void windowActivated(WindowEvent arg0) {
		
	}
	
	public void actionWindowListener()
	{
		if(Main.user_enter_data_of_adding_object)
		{
		TreePath treePath = Main.tree.getSelectionPath();
		if( treePath == null )
		{
			Main.tree.setSelectionRow(0);
			treePath = Main.tree.getSelectionPath();
		}
		if(treePath.getPathCount()  > 1 || Main.count==0 )
		{

			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
			newNode = new DefaultMutableTreeNode(Main.addingElemName);
			
			TreeElement parentTreeElem = TreeOperations.findElementInStruct(selectedNode);
			TreeElement treeElem;
			if( parentTreeElem != null )
			{
				treeElem = new TreeElement(Main.elemID, parentTreeElem.ID);
			}
			else
			{
				treeElem = new TreeElement(-1, -2);
			}
			
			
			
//add element to the structTreeElements begin
			if( parentTreeElem == null )//if adding element has no parent
			{
				if( Main.count == 0 )//structTreeElements is empty
				{
					if(Main.maxLevelsCount<0)
					{
						Main.structTreeElements.add(new Vector<TreeElement>());
						Main.maxLevelsCount++;
					}	
					Main.structTreeElements.elementAt(0).add(treeElem);//add root
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Error in AddObjectHandler class: /n" +
					"cant find parent element in structTreeElements");
					return;
				}
			}
			else//if adding element has parent
			{
				//create (if necessary)new tree level
				if( parentTreeElem.Level == Main.maxLevelsCount )
				{
					Main.structTreeElements.add(new Vector<TreeElement>());
					Main.maxLevelsCount++;
				}
				//add element to the structTreeElements
				Main.structTreeElements.elementAt( parentTreeElem.Level + 1 ).add(treeElem);
			}
//add element to the structTreeElements end
			
			//fields definition for new treeElem 
			treeElem.elementName = Main.addingElemName;
			Main.elementsNamesList.add( treeElem.elementName );
			treeElem.typeName = Main.addingElemTypeName;
			treeElem.functionName = Main.addingElemFuncName;
			if( Main.addingElemIsElementary )
			{
				treeElem.type = 3;
			}
			treeElem.iChildrenCount = 0;
			if( parentTreeElem != null )//if this is not first element
			{
				treeElem.Level = parentTreeElem.Level + 1;
				
				//filling ParametrsValuesMap
				if( treeElem.type == 3 )//is element is elementary
				{
					treeElem.parametersValuesMap = Main.parser.fillingParametersValuesMap( treeElem.functionName );
					//filling ParametrsWeightMap
					double initialWeight = 1.0;
					treeElem.parametrsWeightMap = new HashMap<String, Double>();
					if( treeElem.parametersValuesMap != null )
					{
						if( treeElem.parametersValuesMap.size() > 0 )
						{
							Iterator iter = treeElem.parametersValuesMap.entrySet().iterator();
							while( iter.hasNext() )
							{
								Entry paramInform = (Entry) iter.next();
					        	String paramName = (String) paramInform.getKey();
					        	
					        	treeElem.parametrsWeightMap.put(paramName, initialWeight);
							}
						}
					}
						 
					
				}
				else//is element is intermediate
				{
//check_me				
				}
				
				treeElem.cell = Main.panelGraphic.addChildNode( parentTreeElem, Main.addingElemName,  treeElem.ID, treeElem.type);
				treeElem.parentCell = parentTreeElem.cell;
				Main.elemID++;
			}
			else//if this is first element
			{
				treeElem.Level = 0;
				treeElem.cell = Main.panelGraphic.addNode( Main.addingElemName );
				treeElem.parentCell = null;
//to_do		
				
			}
			
			treeElem.element = newNode;					

			Main.count++;
			
			
			selectedNode.add(newNode);
			Main.tree.updateUI();
			//check_me  begin		
			//set selection to the new element
			Main.tree.setSelectionPath( new TreePath(treeElem.element.getPath()) );
			Main.panelGraphic.getGraph().setSelectionCell(treeElem.cell);
			Main.panelGraphic.refreshTable();
			//check_me  end
		}
		Main.user_enter_data_of_adding_object=false;
		
		Main.tree.getSelectionPath().getLastPathComponent();
		DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) Main.tree.getSelectionPath().getLastPathComponent();
		TreeElement tmpSelectedElement = TreeOperations.findElementInStruct(selectedNode);
		if( tmpSelectedElement != null )
		{
			Main.changeButtonsEnable(tmpSelectedElement);
		}
		else
		{
			Main.btnAddEP.setDisable(true);
			Main.btnAddEO.setDisable(true);
			Main.btnRemove.setDisable(true);
		}
		}
	}
	
	public void windowClosed(WindowEvent arg0) 
	{
		actionWindowListener();
	}
	public void windowClosing(WindowEvent arg0) {
		
	}
	public void windowDeactivated(WindowEvent arg0) {
		actionWindowListener();
	}
	public void windowDeiconified(WindowEvent arg0) {
		
	}
	public void windowIconified(WindowEvent arg0) {
		
	}
	public void windowOpened(WindowEvent arg0) {
		
	}
	
	public DefaultMutableTreeNode getNewNode()
	{
		return newNode;
	}
	
//	public void fillingParametrsWeightMap(TreeElement elemForFilling)
//	{
//		double initialWeight = 1;
//		elemForFilling.parametrsWeightMap = new HashMap<String, Double>();
//		if( elemForFilling.parametrsValuesMap.size() > 0 )
//		{
//			Iterator iter = elemForFilling.parametrsValuesMap.entrySet().iterator();
//			while( iter.hasNext() )
//			{
//				Entry paramInform = (Entry) iter.next();
//	        	String paramName = (String) paramInform.getKey();
//	        	
//	        	elemForFilling.parametrsWeightMap.put(paramName, initialWeight);
//			}
//		}
//	}

//	
//	public static TreeElement findParentForAddingElement()
//	{
//		return null;
//	}
}
