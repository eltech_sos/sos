package Main;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.event.*;

import object.TreeElement;


public class ParametersTable extends JTable implements TableModelListener 
{
	static float tmp_parametrs_counts_mas[];
	static boolean all_parametrs_correct;
	public static boolean isChanged = false;

	public ParametersTable(int i, int j)
	{
		super(i,j);
		getColumnModel().getColumn(0).setHeaderValue("��� ���������");
		getColumnModel().getColumn(1).setHeaderValue("��������");
		getColumnModel().getColumn(2).setHeaderValue("��� ���������");
	}
	
	

	public void tableChanged(TableModelEvent e) 
	{
		super.tableChanged(e);
		int row = e.getFirstRow();
        int column = e.getColumn();
        TableModel model = (TableModel)e.getSource();
    	RefrechTable(row, column, model);
  
	}

	public static void RefrechTable(int row, int column, TableModel model)
	{
        TreePath treePath = Main.tree.getSelectionPath();
        
        if( column == 0 )
        {
        	return;
        }
        
        if( row != -1 )
        {
        	String parameterName;
        	Object data = model.getValueAt(row, 0);
        	if(data.toString().length() == 0)
         	{
         		return;
         	}
        	parameterName = data.toString();
        	parameterName = parameterName.substring(0, parameterName.indexOf("("));
        	if( ! ParametersLoader.parametrsList.contains(parameterName) )
        	{
        		return;
        	}
        	
        	DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
			TreeElement treeElem = TreeOperations.findElementInStruct(selectedNode);
			if( treeElem != null)
			{
				//if selected element is list
				if( treeElem.type == 3 )
				{
					//change of parameter value
		            if ((row>=0) && (column==1))
		            {
		            	data = model.getValueAt(row, column); 
		             	if(data.toString().length() == 0)
		             	{
		             		Main.tableSystem.setValueAt(treeElem.parametersValuesMap.get(parameterName),row, 1);
		             		return;
		             	}
		             	
		             	all_parametrs_correct = true;
		             	
		             	Double newValue;
		             	try
		             	{
		             		newValue = Double.parseDouble( data.toString() );
		             	}
		             	catch(Exception e)
		             	{
		             		JOptionPane.showMessageDialog(null, "����������, ���������" +
		             				"������������ �������� ������");
		             		Main.tableSystem.setValueAt(treeElem.parametersValuesMap.get(parameterName),row, 1);
		             		return;
		             	}
//		             	JOptionPane.showMessageDialog(null, "parameterName= "+parameterName +"     newValue= "+newValue);
//check_me  begin
		             	//checking: did new value lies in borders
		             	if( ParametersLoader.parametrsList != null )
		             	{
//		             		if(ParametrsLoader.paramLeftBordersMap.containsKey(parameterName))
		             		if( ParametersLoader.parametrsList.contains(parameterName) )
		             		{
		             			try
				             	{
				             		if( newValue < 
				             				ParametersLoader.getLeftBorder(parameterName) )
//				             				ParametrsLoader.paramLeftBordersMap.get(parameterName))
				             		{
				             			all_parametrs_correct = false;
				             		}
				             		if( newValue > 
				             		ParametersLoader.getRightBorder(parameterName) )
//				             		ParametrsLoader.paramRightBordersMap.get(parameterName))
				             		{
				             			all_parametrs_correct = false;
				             		}
				             	}
				             	catch(Exception e)
				             	{
				             		Main.tableSystem.setValueAt(treeElem.parametersValuesMap.get(parameterName),row, 1);
				             		return;
				             	}
		             		}
		             	}
		             	
//check_me  end
		             	
		             	//change parameter value in selected element
						if(all_parametrs_correct)
			            {
//							Double initialWeight=1.0;
							treeElem.parametersValuesMap.put(parameterName, newValue);
//							treeElem.parametrsWeightMap.put(parameterName, initialWeight);
			            }
			            else
			            {
			             	int stringNumber = row+1;
			             	JOptionPane.showMessageDialog(null,	"��������, �� �������� � ������ "+ stringNumber +" �� �������� � �������� ��������");
			         		Main.tableSystem.setValueAt(treeElem.parametersValuesMap.get(parameterName),row, 1);
			            }
						}
		            
		            //change of parameter weight value
		            if ((row>=0) && (column==2))
		            {
		            	data = model.getValueAt(row, column); 
		             	if(data.toString().length() == 0)
		             	{
			         		Main.tableSystem.setValueAt(treeElem.parametrsWeightMap.get(parameterName),row, 2);
		             		return;
		             	}
		             	
		             	Double newValue;
		             	try
		             	{
		             		newValue = Double.parseDouble( data.toString() );
		             	}
		             	catch(Exception e)
		             	{
		             		JOptionPane.showMessageDialog(null, "����������, ���������" +
		             				"������������ �������� ������");
		             		Main.tableSystem.setValueAt(treeElem.parametrsWeightMap.get(parameterName),row, 2);
		             		return;
		             	}
		             	
		             	all_parametrs_correct = true;
		             	
		             	//checking: did new value lies in borders
		             	if( ( newValue > 1 ) || ( newValue < 0 ) )
		             	{
		             		all_parametrs_correct = false;
		             	}
		             	
		             	//change parameter weight in selected element
		             	if( all_parametrs_correct )
		             	{
		             		treeElem.parametrsWeightMap.put(parameterName, newValue);
		             	}
		             	else
		             	{
		             		JOptionPane.showMessageDialog(null, "��� ��������� �����" +
	         				"������������� ������ � �������� �� 0 �� 1");
			         		Main.tableSystem.setValueAt(treeElem.parametrsWeightMap.get(parameterName),row, 2);
			         		return;
		             	}
		            }
				}
				else //if selected element is container or root
				{
//check_me  begin
					return;
//check_me  end
				}
            }
			else
            {
            	
            }
        }
        
        
	}
	
	
	
	
}
	
