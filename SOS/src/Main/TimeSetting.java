package Main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import com.jidesoft.dialog.ButtonPanel;
import com.jidesoft.dialog.StandardDialog;
import com.jidesoft.swing.JideTitledBorder;
import com.jidesoft.swing.PartialEtchedBorder;
import com.jidesoft.swing.PartialSide;

import simulation.ItselfOrganizationVariables;

public class TimeSetting extends StandardDialog
{
	protected JButton _okButton = new JButton("OK");
	protected JButton _cancelButton = new JButton("Cancel");
	
	private JTextField timeStartValue_tf;
	
	public TimeSetting()
	{
		super( (Frame) null,"��������� ���������� ������� ��� �������������", true );
		addCancelAction();
		
		addWindowListener(new WindowAdapter() 
		{
		    public void windowClosing(WindowEvent e) 
		    {
		    	setDialogResult(RESULT_CANCELLED);
		    	setVisible(false);
		    	dispose();
		   }
		});
		
		setToCenter();
		setVisible(true);
	}
	
	public JComponent createBannerPanel() 
	{
		return null;
	}

	public ButtonPanel createButtonPanel()
	{
		ButtonPanel okCancelPanel = new ButtonPanel();
		_okButton.setName(OK);
		_cancelButton.setName(CANCEL);
		okCancelPanel.addButton(_okButton, ButtonPanel.AFFIRMATIVE_BUTTON);
		okCancelPanel.addButton(_cancelButton, ButtonPanel.CANCEL_BUTTON);
		
		_okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				okActionPerformed(e);
			}
		});
		_cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				cancelActionPerformed(e);
			}
		});
		setDefaultCancelAction(_cancelButton.getAction());
		setDefaultAction(_okButton.getAction());
		getRootPane().setDefaultButton(_okButton);
		okCancelPanel.setBorder( createButtonPanelBorder() );
		return okCancelPanel;
	}
	
	public static Border createButtonPanelBorder()
	{
		return BorderFactory.createCompoundBorder(new JideTitledBorder(
				new PartialEtchedBorder(PartialEtchedBorder.LOWERED,
						PartialSide.NORTH), ""), BorderFactory
				.createEmptyBorder(9, 6, 9, 6));
	}
	
	public JComponent createContentPanel() 
	{
		JPanel mainContainer = new JPanel();
		mainContainer.setLayout( new BorderLayout() );
		
		JPanel firstPanel = new JPanel();
		firstPanel.setLayout( new BorderLayout() );
		firstPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
		firstPanel.add( new JLabel(" ��������� �������� �������:     "), BorderLayout.WEST );
		timeStartValue_tf = new JTextField( 10 );
		timeStartValue_tf.setText( String.valueOf( ItselfOrganizationVariables.t ) );
		firstPanel.add( timeStartValue_tf, BorderLayout.CENTER );
		firstPanel.add( new JLabel("     "), BorderLayout.EAST );
		
		mainContainer.add( firstPanel, BorderLayout.NORTH );
		
	    setSize( 370, 140 );
		setResizable(false);
		
		return mainContainer;
	}
	
	public static Border createContentPanelBorder()
	{
		return BorderFactory.createEmptyBorder(10, 10, 0, 10);
	}
	
	protected void okActionPerformed(ActionEvent e)
	{
		if( isEntredDataCorrect() )
		{
			ItselfOrganizationVariables.t = Integer.parseInt( timeStartValue_tf.getText() );
			setVisible(false);
			dispose();
		}
	}
	
	protected boolean isEntredDataCorrect()
	{
		String chekingText;
		
		chekingText = timeStartValue_tf.getText();
		if( (chekingText != null) &&
				( chekingText.length() > 0 ) )
		{
			try
			{
				if(Integer.parseInt(chekingText) <= 0 )
				{
					JOptionPane.showMessageDialog(null, "��������, �� ��������� �������� ������� ������ ���� ������ 0");
					return false;
				}
			}
			catch(Exception e)
			{
				JOptionPane.showMessageDialog(null, "����������, " +
						"��������� ������������ �������� ������" +
						"\n (����� ������ ���� ����� ������, ������ ����)");
				return false;
			}
		}
		else
		{
			JOptionPane.showMessageDialog(null, "����������, ������� ��������� �������� �������");
			return false;
		}
		return true;
	}
	
	protected void addCancelAction()
	{
		Action action = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent ae)
			{
				cancelActionPerformed(ae);
			}
		};

		KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		rootPane.getActionMap().put(action, action);
		rootPane.getInputMap(JComponent.WHEN_FOCUSED).put(stroke, action);
		rootPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(stroke, action);

	}
	
	protected void cancelActionPerformed(ActionEvent e)
	{
		setDialogResult( RESULT_CANCELLED );
		setVisible(false);
		dispose();
	}
	
	public void setToCenter() 
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		screenSize.height = screenSize.height/2;
		screenSize.width = screenSize.width/2;
		int y = screenSize.height - 140/2;
		int x = screenSize.width - 370/2;
		this.setLocation( x, y );
	}
}
