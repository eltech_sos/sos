package Main;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import javafx.stage.Stage;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JTree;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import object.AddObjectFX;
import object.ChangeObject;
import object.TreeElement;

import org.jgraph.JGraph;
import org.jgraph.event.GraphModelEvent;
import org.jgraph.event.GraphModelListener;
import org.jgraph.graph.DefaultCellViewFactory;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.DefaultGraphModel;
import org.jgraph.graph.DefaultPort;
import org.jgraph.graph.GraphConstants;
import org.jgraph.graph.GraphLayoutCache;
import org.jgraph.graph.GraphModel;

import Main.Main.TableElement;




public class GraphicPanel extends JPanel implements MouseListener
{
	GraphModel model;
	public static JTree tree ;
	public static String newTypeOfObjectAfterChange;
	public static String newFuncOfObjectAfterChange;
	public static String newNameOfObjectAfterChange;
	DefaultGraphCell pressedCell = null;
	Rectangle2D  sizePressedCell = null;
	public static int noElemNode = 0;
//	public static int number_of_parametrs_in_current_elem=0;
	GraphLayoutCache view;
	public static JGraph graph;
	ImageIcon softwareImgIcon = new ImageIcon("software.gif");	
//	Vector<?> vtTreeElements;

	JComboBox cbStartElement;
	JComboBox cbEndElement;
	String masOfTypeElem[];
	int maxCountOfTypes=15;
//	public static float borders_of_parametrs[][];
	int  types_count;
	int  func_count;
	static int number_of_strings_in_previus_table;
	float left_border, right_border;
	////��� ������������ ����
	private JRadioButtonMenuItem items[];
    public JPopupMenu popupMenu;	
    public Object table_element;
    public static int l = 0;
    
    //colors
    Color rootColor = new Color(100, 150, 130);
    Color intermediateColor = new Color(130, 175, 130);//ok
    Color elementaryColor = new Color(130, 200, 130);
    
	public GraphicPanel(JComboBox cbStartElement,JComboBox cbEndElement, 
			JTree mainTree)
	{
		super();
		model = new DefaultGraphModel();
		view = new GraphLayoutCache(model,	new DefaultCellViewFactory());
		setGraph(new JGraph(model, view));
		
		this.graph = getGraph();
		this.cbStartElement = cbStartElement;
		this.cbEndElement = cbEndElement;
		this.tree = mainTree;
//		this.vtTreeElements = Main.vtTreeElements;		
		this.setBackground(Color.white);

		add(getGraph());
		setVisible(true);
 //   	Panel.tableSystem.addAncestorListener(this);
		getGraph().addMouseListener(this);	

        tree.addMouseListener(new MouseAdapter() 
        {
            public void mouseClicked(MouseEvent e) 
            {
            	if((e.getButton() == MouseEvent.BUTTON1) && (e.getClickCount() != 2))
    			{
            		actionOnSingleMouseClickedInTree();
//            		TreePath treePath = Main.tree.getPathForRow(0);
//					int countElement = tree.getRowCount();
////temp buttons settings begin
//    				//e��� ����������� ��������� �� ������ ������� ������
//					//� ���� ������ � ������� ��� ����
//    				if((treePath.toString().compareToIgnoreCase(Main.tree.getSelectionPath().toString())) == 0
//    						&& (countElement > 1))
//    				{
//   						Main.addButton.setEnabled(false);
//   						Main.addElementaryButton.setEnabled(false);
//   						Main.removeButton.setEnabled(false);
//   						getGraph().clearSelection();
//     				}
//    				else
//    				{	
//    					if( TypeLoader.intermediaryTypesList.size() > 0 )
//    					{
//    						Main.addButton.setEnabled(true);
//    					}
//    					else
//    					{
//    						Main.addButton.setEnabled(false);
//    					}
//    					
//    					if( (treePath.toString().compareToIgnoreCase(Main.tree.getSelectionPath().toString())) == 0 )
//    					{
//    						Main.addElementaryButton.setEnabled(false);
//       						Main.removeButton.setEnabled(false);
//    					}
//    					else
//    					{
//    						if( TypeLoader.elementaryTypesList.size() > 0 )
//    						{
//    							Main.addElementaryButton.setEnabled(true);
//    						}
//    						else
//    						{
//    							Main.addElementaryButton.setEnabled(false);
//    						}
//    						Main.removeButton.setEnabled(true);
//    					}						
//    				}
////temp buttons settings end
    			}
            	
            	actionOnAnyMouseClickedInTree();
//            	refreshTable();
////            	if(tree.getSelectionRows()[0] != 0)
////            	{
////             		ParametersTable.ChahgeHeader();
////            	}
//            	TreeElement myelem;
//        		Object obj[] = new Object[1];
//        		
//        		DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) Main.tree.getSelectionPath().getLastPathComponent();
//        		myelem = TreeOperations.findElementInStruct(selectedNode);
////check_me  begin     		
//        		if( myelem != null)
//        		{
//        			obj[0] = myelem.cell;
//    				getGraph().setSelectionCells(obj);
//    				Main.changeButtonsEnable(myelem);
//        		}
//        		else
//        		{
//        			
//        		}      		
////check_me  end  
            }  
        }
        
	
       );
        
        tree.getSelectionModel().addTreeSelectionListener(new TreeSelectionListener(){

			public void valueChanged(TreeSelectionEvent arg0) 
			{
				actionOnAnyMouseClickedInTree();
				if(tree.getSelectionRows()!=null &&tree.getSelectionRows().length == 1 && tree.getSelectionRows()[0] == 0)
				{
					Main.btnAddEP.setDisable(true);
					Main.btnAddEO.setDisable(true);
					Main.btnRemove.setDisable(true);
					graph.clearSelection();
				}

				
			}});
        graph.addKeyListener(new KeyAdapter() 
        {
        	
        	public void keyPressed(KeyEvent e)
    		{
        		try
            	{
        			if(e.getKeyCode() == KeyEvent.VK_DELETE )
        			{
        				if( !Main.btnRemove.isDisabled() )
        				{
        					new RemoveSelection(true);
        				}
        			}
            	}
            	catch(Exception ex)
            	{
            		ex.printStackTrace();
            	}	
    		}
        }
	
       );
        
        graph.getModel().addGraphModelListener(new GraphModelListener()
        {
			public void graphChanged(GraphModelEvent arg0) 
			{
				Object[] changedObject = arg0.getChange().getChanged();
				if(changedObject != null && changedObject.length == 1)
				{
					if(((DefaultGraphCell)changedObject[0]).getUserObject() != null)
					{
						String newName = ((DefaultGraphCell)changedObject[0]).getUserObject().toString();
						//������� ����������� ��������� ��� �������������� 
						//�������� ��������
						//JOptionPane.showMessageDialog(null, "�������������� ��������� "+newName);
						TreePath treePath = tree.getSelectionPath();
						DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
						TreeElement treeElem = TreeOperations.findElementInStruct( selectedNode );
						treeElem.elementName = newName;
						((DefaultMutableTreeNode)Main.tree.getSelectionPath().getLastPathComponent()).setUserObject(newName);
						Main.tree.repaint();
					}
				}
				
			}});
		
   	}
	
	public DefaultGraphCell addNode(String nameOfElem)//use for root
	{
		DefaultGraphCell cell= new DefaultGraphCell(nameOfElem);
		GraphConstants.setBounds(cell.getAttributes(), new Rectangle2D.Double(40,40,60,40));
		GraphConstants.setOpaque(cell.getAttributes(), true);				
		Border loweredBevelBorder = BorderFactory.createLoweredBevelBorder();
		GraphConstants.setBorder(cell.getAttributes(), loweredBevelBorder);
		GraphConstants.setBackground(cell.getAttributes(),rootColor);
		GraphConstants.setVerticalAlignment(cell.getAttributes(),SwingConstants.CENTER);
		GraphConstants.setHorizontalAlignment(cell.getAttributes(),SwingConstants.CENTER);		
		DefaultPort port = new DefaultPort();
		cell.add(port);
		getGraph().getGraphLayoutCache().insert(cell);	
		
		
		
		return cell;
	}
	
	public DefaultGraphCell addChildNode(TreeElement tParent, String nameOfElem, int elemID, int elemType)
	{
		
		int countChildOnLevel=0;
		DefaultGraphCell cell= new DefaultGraphCell();
		cell = new DefaultGraphCell(new String(nameOfElem));
		DefaultGraphCell cellParent = tParent.cell;
		
		countChildOnLevel = Main.structTreeElements.elementAt( tParent.Level + 1 ).size();

		if( tParent.Level == 0 )
		{
			GraphConstants.setBounds(cell.getAttributes(), new Rectangle2D.Double( ((countChildOnLevel-1)*70 ),100,60,40));
		}
		else
		{
			GraphConstants.setBounds(cell.getAttributes(), new Rectangle2D.Double( ((countChildOnLevel-1)*70 ),80+80*tParent.Level,60,40));
		}	
		GraphConstants.setOpaque(cell.getAttributes(), true);				
		Border loweredBevelBorder = BorderFactory.createLoweredBevelBorder();
		GraphConstants.setBorder(cell.getAttributes(), loweredBevelBorder);
		if( elemType == 3 )
		{
			GraphConstants.setBackground(cell.getAttributes(),elementaryColor); 
		}
		else
		{
			GraphConstants.setBackground(cell.getAttributes(),intermediateColor);
		}
		GraphConstants.setVerticalAlignment(cell.getAttributes(),SwingConstants.CENTER);
		GraphConstants.setHorizontalAlignment(cell.getAttributes(),SwingConstants.CENTER);
		GraphConstants.setIcon(cell.getAttributes(),softwareImgIcon);
		DefaultPort port = new DefaultPort();
		cell.add(port);
		view.insert(cell);
		
//check_me  begin		
//		TreeElement tempElem = TreeOperations.findElementInStructByCell( cell );

		TreeElement tempElem = TreeOperations.findElementInStructByID(elemID);
//check_me  end
		//		for(int i=0;i<vtTreeElements.size();i++)
//		{
//			tempElem = (TreeElement) vtTreeElements.elementAt(i);			
//			if(tempElem.cell == cell)
//			{
//				break;
//			}			
//		}	
		tempElem.cellLink = createLink( cell, cellParent, 1);
		noElemNode++;
		tParent.iChildrenCount++;
		return cell;
	}
	
	public DefaultGraphCell createLink(DefaultGraphCell cell, DefaultGraphCell cell2, int lineWidth)
	{
		DefaultEdge edge = new DefaultEdge();
		edge.setSource(cell.getChildAt(0));
		edge.setTarget(cell2.getChildAt(0));
		DefaultGraphCell linkingCell= new DefaultGraphCell(); 
		linkingCell = edge;		
		GraphConstants.setLineWidth(edge.getAttributes(),lineWidth);		
		getGraph().getGraphLayoutCache().insert(linkingCell);
//		DefaultPort port = new DefaultPort();
//		JOptionPane.showMessageDialog(null,	linkingCell.toString());
		return linkingCell;
	}
	
	public void setGraph(JGraph graph) 
	{
		this.graph = graph;
	}

	public JGraph getGraph() 
	{
		return graph;
	}
	
	public void mouseClicked(MouseEvent e) 
	{
		DefaultGraphCell clickedCell = (DefaultGraphCell) getGraph().getSelectionCell();
		if( clickedCell == null )
		{
			Main.clearSelection();
			return;
		}
		if( clickedCell.getRoot().toString() == null )
		{
			Main.clearSelection();
			return;
		}
		
		TreeElement tempElem = TreeOperations.findElementInStructByCell( clickedCell );
//		if(tempElem != null)
//		{
//			tree.setSelectionPath(new TreePath(tempElem.element.getPath()));
//			tree.updateUI();
//			Main.changeButtonsEnable(tempElem);
//			refreshTable();
//		}

		
		int x = e.getX(), y = e.getY();
		Object cell = graph.getFirstCellForLocation(x, y);
		if( cell != null )
		{
			tempElem = TreeOperations.findElementInStructByCell( (DefaultGraphCell) cell );
			//if selected element is TreeElement
			if( tempElem != null )
			{
				
				tree.setSelectionPath(new TreePath(tempElem.element.getPath()));
				tree.updateUI();
				Main.changeButtonsEnable(tempElem);
				refreshTable();
				
				if ( e.isMetaDown() )//right button
		    	{ 
		    		////�������, �����������
		    		ItemHandler handler = new ItemHandler();
		    		ItemAdd itemAdd = new ItemAdd(); 
		    		ItemAddElementary itemAddElementary = new ItemAddElementary();
		    		ItemRemove itemRemove = new ItemRemove(); 
		//////////////
		    		
		    		masOfTypeElem=new String[4];
		    		masOfTypeElem[0]="��������";
		    		masOfTypeElem[1]="�������� ������� ���������";
		    		masOfTypeElem[2]="�������� ������������ ������";
		    		masOfTypeElem[3]="������� ����";
		    		//masOfTypeElem[4]="�������������";

		    		ButtonGroup colorGroup = new ButtonGroup();
		    		items = new JRadioButtonMenuItem[4];
		    		items[0] = new JRadioButtonMenuItem(masOfTypeElem[0]);
		    		items[1] = new JRadioButtonMenuItem(masOfTypeElem[1]);
		    		items[2] = new JRadioButtonMenuItem(masOfTypeElem[2]);
		    		items[3] = new JRadioButtonMenuItem(masOfTypeElem[3]);
		    		//items[4] = new JRadioButtonMenuItem(masOfTypeElem[4]);
		    		
		    		popupMenu = new JPopupMenu();
		    		popupMenu.add(items[0]);
		    		if( !Main.btnAddEP.isDisabled() )
		    		{
		    			popupMenu.add(items[1]);
		    		}
		    		if( !Main.btnAddEO.isDisabled() )
		    		{
		    			popupMenu.add(items[2]);
		    		}
		    		popupMenu.add(items[3]);
		    		//popupMenu.add(items[4]);
		    		
					colorGroup.add(items[0]);
					colorGroup.add(items[1]);
					colorGroup.add(items[2]);
					colorGroup.add(items[3]);
					//colorGroup.add(items[4]);
		    		items[0].addActionListener(handler);
		    		items[1].addActionListener(itemAdd);
		    		items[2].addActionListener(itemAddElementary);
		    		items[3].addActionListener(itemRemove);
		    		
		    		popupMenu.show(e.getComponent(),e.getX(),e.getY());
		    		
		    	
		 		
		    	}	            	
		    	else//left button
		    	{		    		
		    		refreshTable();
		    	}
			}
			else//if selected element is NO TreeElement
			{
				Main.clearSelection();
				return;
//				//set selection in tree at the highest node
//				Main.tree.setSelectionPath( Main.tree.getPathForRow(0) );
////				Main.tree.setSelectionRow(0);
//				refreshTable();
			}
    	
    	}
		else//if cell == null
		{
			Main.clearSelection();
			return;
		}
	}

	

	public void mousePressed(MouseEvent e) {
			
	}

	public void mouseReleased(MouseEvent e) {
	}

	public void mouseEntered(MouseEvent e) {
	}

	public void mouseExited(MouseEvent e) {
	}
	
	public void removeCellGraph(DefaultGraphCell cell, DefaultGraphCell link)
	{		
		DefaultGraphCell[] cellsToDelete = new DefaultGraphCell[1];
    	////cellsToDelete[0] = cell;
    	//cellsToDelete[1] = link;	
    	if (link!=null)
    	{
    	 	////view.remove(new Object[] {link});
    		cellsToDelete[0] = link;
    		getGraph().getGraphLayoutCache().remove(cellsToDelete);
    	}
    	cellsToDelete[0] = cell;
		getGraph().getGraphLayoutCache().remove(cellsToDelete);
		getGraph().updateUI();
		
			noElemNode --;
	}

	private class ItemHandler implements ActionListener
	{
		public void actionPerformed (ActionEvent event)
		{
			ChangeObject changeObjectFrame=new ChangeObject();
			ChangeObjectHandler ChangeObjectHandler = new ChangeObjectHandler();
			changeObjectFrame.addWindowListener(ChangeObjectHandler);
			Main.setToCenter(changeObjectFrame);
		}
	}
	
	private class ItemAdd implements ActionListener
	{
		public void actionPerformed (ActionEvent event)
		{
			// ������� ���������� ���� � �������, ��������� �� �������
			Point curLocation = getLocation();
			curLocation.x += 200;
			curLocation.y += 200;
		}
	}
	
	private class ItemAddElementary implements ActionListener
	{
		public void actionPerformed (ActionEvent event)
		{
			// ������� ���������� ���� � �������, ��������� �� �������
			Point curLocation = getLocation();
			curLocation.x += 200;
			curLocation.y += 200;
		}
	}
	
	private class ItemRemove implements ActionListener{
		public void actionPerformed (ActionEvent event)
		{
			new RemoveSelection(true);
		}
	}
	
	private class ItemRename implements ActionListener
	{
		public void actionPerformed (ActionEvent event)
		{
			
		}
	}

	private class ChangeObjectHandler implements WindowListener
	{
		public void windowActivated(WindowEvent arg0) 
		{
		}
		public void windowClosed(WindowEvent arg0) 
		{
			TreePath treePath = tree.getSelectionPath();
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
			TreeElement treeElem = TreeOperations.findElementInStruct( selectedNode );
			
			treeElem.typeName = newTypeOfObjectAfterChange;
			treeElem.functionName = newFuncOfObjectAfterChange;
			//treeElem.elementName = "Changed";//newNameOfObjectAfterChange;
			
			//filling ParametrsValuesMap and ParametrsWeightMap
			if( treeElem.type == 3 )//is element is elementary
			{
				treeElem.parametersValuesMap = Main.parser.fillingParametersValuesMap( treeElem.functionName );
//check_me  begin
				//filling ParametrsWeightMap
				double initialWeight = 1;
				treeElem.parametrsWeightMap = new HashMap<String, Double>();
				treeElem.parametrsWeightMap.clear();
				if( treeElem.parametersValuesMap != null )
				{
					if( treeElem.parametersValuesMap.size() > 0 )
					{
						Iterator iter = treeElem.parametersValuesMap.entrySet().iterator();
						while( iter.hasNext() )
						{
							Entry paramInform = (Entry) iter.next();
				        	String paramName = (String) paramInform.getKey();
				        	
				        	treeElem.parametrsWeightMap.put(paramName, initialWeight);
						}
					}
				}
				
//check_me  end
			}
			else//is element is intermediate
			{
//check_me				
			}
			Main.changeButtonsEnable(treeElem);
			refreshTable();
			
		}
		
		public void windowClosing(WindowEvent arg0) {
		}
		public void windowDeactivated(WindowEvent arg0) {
		}
		public void windowDeiconified(WindowEvent arg0) {
		}
		public void windowIconified(WindowEvent arg0) {
		}
		public void windowOpened(WindowEvent arg0) {
		}
	}
	
	public static void refreshTable()
	{
		refresh();
		Main.typeAndFuncNameInform.setText("");
		if( tree.getSelectionRows() == null || tree.getSelectionRows().length == 0)
		{
			return;
		}
		if( tree.getSelectionRows()[0] == 0 )
    	{
     		return;
    	}
		//������ ��������� treeElem �� ���������� �������
		TreePath treePath = tree.getSelectionPath();
		DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
		TreeElement treeElem = TreeOperations.findElementInStruct( selectedNode );
	    if( treeElem != null )
	    {
	    	 Main.typeAndFuncNameInform.setText("���: "+treeElem.typeName+"  �������:  "+treeElem.functionName);
		     l=0;
		     if( treeElem.type == 3 )//if element is list
		     {
		    	 if( treeElem.parametersValuesMap != null )
		    	 {
		    		 Iterator iter = treeElem.parametersValuesMap.entrySet().iterator();
				        while( iter.hasNext() )
				        {
				        	Entry paramInform = (Entry) iter.next();
				        	String paramName = (String) paramInform.getKey();
				        	Double paramValue = (Double) paramInform.getValue();
				        		 
//				        	Main.tableSystem.setValueAt(paramName+"("+ParametrsLoader.paramLeftBordersMap.get(paramName)+";"+ParametrsLoader.paramRightBordersMap.get(paramName)+")",l, 0);
				        	Main.tableSystem.setValueAt(paramName+"("+ParametersLoader.getLeftBorder( paramName )+";"+ParametersLoader.getRightBorder( paramName )+")",l, 0);
				        	Main.tableSystem.setValueAt(paramValue,l, 1);
					        Main.tableSystem.setValueAt( treeElem.parametrsWeightMap.get( paramName ) ,l, 2);
					        
					        l++;
				        }
		    	 }
		    	 
		     }
		        
		     if( l >= 0 )
		     {
		         while(l<number_of_strings_in_previus_table)
		         {
		        	Main.tableSystem.setValueAt("",l, 0);
			        Main.tableSystem.setValueAt("",l, 1);
			        l++;
			     }
		      }      
			 number_of_strings_in_previus_table=l;
	    }
	       
	}
	
	public static void refresh()
	{
		if(l > 0)
		{
			for(int i = 0; i < l; i++)
			{
				Main.tableSystem.setValueAt("",i, 0);
		   	  	Main.tableSystem.setValueAt("",i, 1);
		   	  	Main.tableSystem.setValueAt("",i, 2);
			}
			 Main.tableSystem.removeRowSelectionInterval(0, l);
		}
	}
	
	public void actionOnSingleMouseClickedInTree()
	{
		TreePath treePath = Main.tree.getPathForRow(0);
		int countElement = tree.getRowCount();

		
		Main.btnAddEP.setDisable(true);
		Main.btnAddEO.setDisable(true);
		Main.btnRemove.setDisable(true);
		
		//if set selection on the first tree element
		//and one root is already exist in system 
		if((treePath.toString().compareToIgnoreCase(Main.tree.getSelectionPath().toString())) == 0
				&& (countElement > 1))
		{	
				getGraph().clearSelection();
		}
		else
		//for the case if set selection on the first
		//tree element, but root is not created
		{
			if( TypeLoader.intermediaryTypesList.size() > 0 )
			{
				Main.btnAddEO.setDisable(false);
			}
		}
		
	}
	
	public void actionOnAnyMouseClickedInTree()
	{
		refreshTable();
    	TreeElement myelem;
		Object obj[] = new Object[1];
		
		if(Main.tree.getSelectionPath() != null)
		{
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) Main.tree.getSelectionPath().getLastPathComponent();
			myelem = TreeOperations.findElementInStruct(selectedNode);
			//check_me  begin     		
			if( myelem != null)
			{
				obj[0] = myelem.cell;
				getGraph().setSelectionCells(obj);
				Main.changeButtonsEnable(myelem);
			}
		}
	}
	
	
	
	
	
	
		
}



