package Main;

import javax.swing.JOptionPane;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBoxTreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.control.cell.CheckBoxTreeCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;
import simulation.ItselfOrganizationVariables;

public class ItselfOrgAllowSettingsFX extends Application {
	private CheckBoxTreeItem<String> rootItem, checkBoxTreeItem, funcOrg, chbList, chbDist, structOrg, changeType, chb�TList, chbCTDist, reConf, addEl;

	@SuppressWarnings("unchecked")
	@Override
	public void start(final Stage stage) throws Exception {
		stage.setTitle("����� ����������� �������� ���������������");
		VBox vBox = new VBox();
    	
		
		
    	rootItem = new CheckBoxTreeItem<String>("��� ���� ���������������");
        rootItem.setExpanded(true);
        
        final TreeView<String> tree = new TreeView<String>(rootItem);  
        tree.setEditable(true);    
        tree.setCellFactory(CheckBoxTreeCell.<String>forTreeView());    
        
        checkBoxTreeItem = new CheckBoxTreeItem<String>("�������������� ���������������");
        checkBoxTreeItem.setSelected(ItselfOrganizationVariables.paramOrgAllowed);
        
       
        funcOrg = new CheckBoxTreeItem<String>("�������������� ���������������");
        funcOrg.setSelected(ItselfOrganizationVariables.funcIntermediateOrgAllowed & ItselfOrganizationVariables.funcLeafOrgAllowed);
       
        
        if(!funcOrg.isSelected()) funcOrg.setIndeterminate(ItselfOrganizationVariables.funcIntermediateOrgAllowed | ItselfOrganizationVariables.funcLeafOrgAllowed);
        rootItem.setExpanded(true);
        chbList = new CheckBoxTreeItem<String>("��� ������");
        chbList.setSelected(ItselfOrganizationVariables.funcLeafOrgAllowed);
        chbDist = new CheckBoxTreeItem<String>("��� �����������");
        chbDist.setSelected(ItselfOrganizationVariables.funcIntermediateOrgAllowed);
        funcOrg.getChildren().addAll(chbList, chbDist);
        
        structOrg = new CheckBoxTreeItem<String>("����������� ���������������");
        structOrg.setSelected(ItselfOrganizationVariables.typeLeafOrgAllowed & ItselfOrganizationVariables.typeIntermediateOrgAllowed & ItselfOrganizationVariables.reconfigurationOrgAllowed & ItselfOrganizationVariables.addNewElementsOrgAllowed);
        structOrg.setSelected(false);
        
        if(!structOrg.isSelected()) structOrg.setIndeterminate(ItselfOrganizationVariables.typeLeafOrgAllowed | ItselfOrganizationVariables.typeIntermediateOrgAllowed 
        		| ItselfOrganizationVariables.reconfigurationOrgAllowed | ItselfOrganizationVariables.addNewElementsOrgAllowed);
        
        //rootItem.setExpanded(true);
        changeType = new CheckBoxTreeItem<String>("����� �����");
        changeType.setSelected(ItselfOrganizationVariables.typeLeafOrgAllowed & ItselfOrganizationVariables.typeIntermediateOrgAllowed);
        if(!changeType.isSelected()) changeType.setIndeterminate(ItselfOrganizationVariables.typeLeafOrgAllowed | ItselfOrganizationVariables.typeIntermediateOrgAllowed);
        rootItem.setExpanded(true);
        chb�TList = new CheckBoxTreeItem<String>("��� ������");
        chb�TList.setSelected(ItselfOrganizationVariables.typeLeafOrgAllowed);
        chbCTDist = new CheckBoxTreeItem<String>("��� �����������");
        chbCTDist.setSelected(ItselfOrganizationVariables.typeIntermediateOrgAllowed);
        changeType.getChildren().addAll(chb�TList,chbCTDist);
        reConf = new CheckBoxTreeItem<String>("��������������");
        reConf.setSelected(ItselfOrganizationVariables.reconfigurationOrgAllowed);
        addEl = new CheckBoxTreeItem<String>("���������� ����� ���������");
        addEl.setSelected(ItselfOrganizationVariables.addNewElementsOrgAllowed);
        structOrg.getChildren().addAll(changeType, reConf, addEl);
        
        rootItem.setSelected(checkBoxTreeItem.isSelected() & funcOrg.isSelected() & structOrg.isSelected());
        if(!rootItem.isSelected()) rootItem.setIndeterminate(checkBoxTreeItem.isSelected() | funcOrg.isSelected() | funcOrg.isIndeterminate() | structOrg.isSelected() | structOrg.isIndeterminate());
        rootItem.getChildren().addAll(checkBoxTreeItem, funcOrg, structOrg);
        
        Button btnOk = new Button("��");
        btnOk.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				okActionPerformed();
				stage.close();
			}});
        Button btnCancel = new Button("������");
        btnCancel.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				stage.close();
			}});
        
        HBox hBox = new HBox();
        hBox.setSpacing(5);
        hBox.setPadding(new Insets(5, 0, 0, 230));
        hBox.getChildren().addAll(btnOk, btnCancel);
        
        tree.setMaxHeight(260);
        tree.setMinWidth(380);
        tree.setRoot(rootItem);
        tree.setShowRoot(true);
        
        vBox.getChildren().addAll(tree, hBox);
        vBox.setSpacing(5);
        vBox.setPadding(new Insets(10, 10, 10, 10));        		
		Scene scene = new Scene(vBox);	
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
    	stage.setScene(scene);
    	stage.show();
	}

	protected void okActionPerformed()
	{
		ItselfOrganizationVariables.paramOrgAllowed = checkBoxTreeItem.isSelected();
		ItselfOrganizationVariables.funcLeafOrgAllowed = chbList.isSelected();
		ItselfOrganizationVariables.funcIntermediateOrgAllowed = chbDist.isSelected();
		ItselfOrganizationVariables.typeLeafOrgAllowed = chbList.isSelected();
		ItselfOrganizationVariables.typeIntermediateOrgAllowed = chbCTDist.isSelected();
		ItselfOrganizationVariables.reconfigurationOrgAllowed = reConf.isSelected();
		ItselfOrganizationVariables.addNewElementsOrgAllowed = addEl.isSelected();
		
	}
}
