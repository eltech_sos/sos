package Main;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import simulation.ItselfOrganizationVariables;

public class AdditionalSettingsFX extends Application{
	private CheckBox chbParam, chbFunction, chbType, chbMaxPrice;
	private ComboBox<String> cbAsUse;
	
	@Override
	public void start(final Stage stage) throws Exception {
		stage.setTitle("������� �������������� �������� �������������");
		VBox vBox = new VBox();
    	
    	GridPane grid = new GridPane();
    	grid.getColumnConstraints().add(new ColumnConstraints(110));
    	grid.getColumnConstraints().add(new ColumnConstraints(110));
    	grid.getColumnConstraints().add(new ColumnConstraints(110));
    	grid.setHgap(10);
        grid.setVgap(15);
        grid.setPadding(new Insets(10, 10, 20, 70));                
        
        Text rootValue = new Text("��������� �������� \"��������� � ���������������\" ���");
        rootValue.setFont(Font.font("Arial",FontWeight.BOLD, 12));
        rootValue.setFill(Color.color(0.3, 0.3, 0.3));// Dark grey
        vBox.setAlignment(Pos.CENTER);
        
        Text txtParam = new Text("����������");
        txtParam.setFont(Font.font("Arial",FontWeight.BOLD, 12));
        txtParam.setFill(Color.color(0.3, 0.3, 0.3));// Dark grey
        GridPane.setHalignment(txtParam, HPos.CENTER);
        grid.add(txtParam, 0, 0);
        Text txtFunction = new Text("�������");
        txtFunction.setFont(Font.font("Arial",FontWeight.BOLD, 12));
        txtFunction.setFill(Color.color(0.3, 0.3, 0.3));// Dark grey
        GridPane.setHalignment(txtFunction, HPos.CENTER);
        grid.add(txtFunction, 1, 0);
        Text txtType = new Text("�����");
        txtType.setFont(Font.font("Arial",FontWeight.BOLD, 12));
        txtType.setFill(Color.color(0.3, 0.3, 0.3));// Dark grey
        GridPane.setHalignment(txtType, HPos.CENTER);
        grid.add(txtType, 2, 0);
        
        chbParam = new CheckBox();
        GridPane.setHalignment(chbParam, HPos.CENTER);
        chbParam.setSelected(ItselfOrganizationVariables.useParticipationInParamOrg);
        grid.add(chbParam, 0, 1);
        chbFunction = new CheckBox();
        GridPane.setHalignment(chbFunction, HPos.CENTER);
        chbFunction.setSelected(ItselfOrganizationVariables.useParticipationInFuncOrg);
        grid.add(chbFunction, 1, 1);
        chbType = new CheckBox();
        GridPane.setHalignment(chbType, HPos.CENTER);
        chbType.setSelected(ItselfOrganizationVariables.useParticipationInTypeOrg);
        grid.add(chbType, 2, 1);
        
        GridPane gridCenter = new GridPane();
        gridCenter.setHgap(10);
        gridCenter.setVgap(15);
        gridCenter.setPadding(new Insets(0, 10, 0, 10));  
        
        Text txtAsUse = new Text("��� ������������ ����������� ����");
        txtAsUse.setFont(Font.font("Arial",FontWeight.BOLD, 12));
        txtAsUse.setFill(Color.color(0.3, 0.3, 0.3));// Dark grey
        gridCenter.add(txtAsUse, 0, 0);
        Text txtMaxPrice = new Text("��������� ������������ ��������� ������");
        txtMaxPrice.setFont(Font.font("Arial",FontWeight.BOLD, 12));
        txtMaxPrice.setFill(Color.color(0.3, 0.3, 0.3));// Dark grey
        gridCenter.add(txtMaxPrice, 0, 1);
        
        cbAsUse = new ComboBox<String>();
        cbAsUse.getItems().addAll(new String[] {"�� ������������", "������������ ��������", "�������� ����������"});
        cbAsUse.getSelectionModel().select(ItselfOrganizationVariables.UseIntelligentSOTypeChoosing ? 1 : ItselfOrganizationVariables.UseNeuralSOTypeChoosing ? 2 : 0);
        gridCenter.add(cbAsUse, 1, 0);
        chbMaxPrice = new CheckBox();
        GridPane.setHalignment(chbMaxPrice, HPos.CENTER);
        chbMaxPrice.setSelected(ItselfOrganizationVariables.useMaximumPrice);
        gridCenter.add(chbMaxPrice, 1, 1);
        
        Button btnOk = new Button("��");
        btnOk.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				ItselfOrganizationVariables.useParticipationInParamOrg = chbParam.isSelected();
				ItselfOrganizationVariables.useParticipationInFuncOrg = chbFunction.isSelected();
				ItselfOrganizationVariables.useParticipationInTypeOrg = chbType.isSelected();
				ItselfOrganizationVariables.useMaximumPrice = chbMaxPrice.isSelected();
				ItselfOrganizationVariables.UseIntelligentSOTypeChoosing = (cbAsUse.getSelectionModel().getSelectedIndex() == 1);
				ItselfOrganizationVariables.UseNeuralSOTypeChoosing = (cbAsUse.getSelectionModel().getSelectedIndex() == 2);
				stage.close();
			}});
        Button btnCancel = new Button("������");
        btnCancel.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				stage.close();
			}});
        
        HBox hBox = new HBox();
        hBox.setSpacing(5);
        hBox.setPadding(new Insets(10, 0, 0, 318));
        hBox.getChildren().addAll(btnOk, btnCancel);
                
        vBox.getChildren().addAll(rootValue, grid, gridCenter, hBox);
        vBox.setSpacing(5);
        vBox.setPadding(new Insets(10, 0, 10, 10));        		
		Scene scene = new Scene(vBox);	
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
    	stage.setScene(scene);
		stage.show();
	}
}
