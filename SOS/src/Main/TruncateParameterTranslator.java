/**
 * 
 */
package Main;

/**
 * This parameter translator simply truncate parameters to their borders
 * 
 * @author User
 *
 */
final class TruncateParameterTranslator implements IParameterTranslator {

	/* (non-Javadoc)
	 * @see Main.IParameterTranslator#toLimited(java.lang.String, double)
	 */
	@Override
	public double toLimited(String paramName, double value) {
		
		double leftBorder = ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(0);
		if(value < leftBorder)
			return leftBorder;
		
		double rightBorder = ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(1);
		if(value > rightBorder)
			return rightBorder;
		
		return value;
	}

	/* (non-Javadoc)
	 * @see Main.IParameterTranslator#toUnlimited(java.lang.String, double)
	 */
	@Override
	public double toUnlimited(String paramName, double value) {

		return toLimited(paramName, value);
	}

}
