package Main;

import tabs.FXDialog;
import tabs.Message;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import simulation.ItselfOrganizationVariables;

public class ModelRequirementsFX extends Application{
	private TextField textRootValue, textMaxDev, textMaxPrice;
	
	@Override
	public void start(final Stage stage) throws Exception {
		stage.setTitle("����������� ���������� � ������");
		GridPane grid = new GridPane();
    	
    	grid.setHgap(10);
        grid.setVgap(15);
        grid.setPadding(new Insets(0, 10, 0, 10));                
        
        // Category in column 2, row 1
        Text rootValue = new Text("����������� �������� � �����:");
        rootValue.setFont(Font.font("Arial",FontWeight.BOLD, 12));
        rootValue.setFill(Color.color(0.3, 0.3, 0.3));// Dark grey
        grid.add(rootValue, 0, 1); 

        textRootValue = new TextField();
        textRootValue.setText(Double.toString(ItselfOrganizationVariables.rootMinimumCount));
        grid.add(textRootValue, 1, 1); 

        Text maxDev = new Text("����������� ���������� ����������:");
        maxDev.setFont(Font.font("Arial",FontWeight.BOLD, 12));
        maxDev.setFill(Color.color(0.3, 0.3, 0.3));// Dark grey
        grid.add(maxDev, 0, 2); 

        textMaxDev = new TextField();
        textMaxDev.setText(Double.toString(ItselfOrganizationVariables.maximumDevialation));
        grid.add(textMaxDev, 1, 2); 

        Text maxPrice = new Text("����������� ���������� ���������:");
        maxPrice.setFont(Font.font("Arial",FontWeight.BOLD, 12));
        maxPrice.setFill(Color.color(0.3, 0.3, 0.3));// Dark grey
        grid.add(maxPrice, 0, 3); 
    	
        textMaxPrice = new TextField();
        textMaxPrice.setText(Double.toString(ItselfOrganizationVariables.maximumPrice));
        grid.add(textMaxPrice, 1, 3); 
       
        Button btnOk = new Button("��");
        btnOk.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				if( isEntredDataCorrect() )
				{
					ItselfOrganizationVariables.rootMinimumCount = Double.parseDouble( textRootValue.getText() );
					ItselfOrganizationVariables.maximumDevialation = Double.parseDouble( textMaxDev.getText() );
					ItselfOrganizationVariables.maximumPrice = Double.parseDouble( textMaxPrice.getText() );
					stage.close();
				}
			}});
        Button btnCancel = new Button("������");
        btnCancel.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				stage.close();
			}});
        
        HBox hBox = new HBox();
        hBox.setSpacing(5);
        hBox.getChildren().addAll(btnOk, btnCancel);
        grid.add(hBox, 1, 4);
        
        grid.add(new Text(), 1, 5);
        
		Scene scene = new Scene(grid);	
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
    	stage.setScene(scene);
		stage.show();
	}

	protected boolean isEntredDataCorrect()
	{
		String chekingText;
		double chekingValue;
		double currentMinRootValue = 10;
		
		chekingText = textRootValue.getText();
		if( (chekingText != null) &&
				( chekingText.length() > 0 ) )
		{
			try
			{
				currentMinRootValue = Double.parseDouble( chekingText );
			}
			catch(Exception e)
			{
				FXDialog.showMessageDialog("����������, ��������� ������������ ���������� �����", "������ ����������", Message.WARNING);
				return false;
			}
		}
		else
		{
			FXDialog.showMessageDialog("����������, ������� ����������� �������� ��� �����", "������ ����������", Message.WARNING);
			return false;
		}
		
		chekingText = textMaxDev.getText();
		if( (chekingText != null) &&
				( chekingText.length() > 0 ) )
		{
			try
			{
				chekingValue = Double.parseDouble( chekingText );
				if( (chekingValue < 0) || ( Math.abs(chekingValue) >= Math.abs(currentMinRootValue) ) )
				{
					FXDialog.showMessageDialog("��������, �� ����������� ���������� ���������� ������ ���� " +
							"\n ������������� ������ �� ������ ������� ��� ����������� �������� ��� �����", "������ ����������", Message.WARNING);
					return false;
				}
			}
			catch(Exception e)
			{
				FXDialog.showMessageDialog("����������, ��������� ������������ ���������� �����", "������ ����������", Message.WARNING);
				return false;
			}
		}
		else
		{
			FXDialog.showMessageDialog("����������, ������� ����������� ���������� ����������", "������ ����������", Message.WARNING);
			return false;
		}
		
		chekingText = textMaxPrice.getText();
		if( (chekingText != null) &&
				( chekingText.length() > 0 ) )
		{
			try
			{
				chekingValue = Double.parseDouble( chekingText );
				if( chekingValue <= 0 )
				{
					FXDialog.showMessageDialog("��������, �� ����������� ���������� ��������� ������ ���� ������ ����", "������ ����������", Message.WARNING);
					return false;
				}
			}
			catch(Exception e)
			{
				FXDialog.showMessageDialog("����������, ��������� ������������ ���������� �����", "������ ����������", Message.WARNING);
				return false;
			}
		}
		else
		{
			FXDialog.showMessageDialog("����������, ������� ����������� ���������� ���������", "������ ����������", Message.WARNING);
			return false;
		}

		
		return true;
	}
	
}
