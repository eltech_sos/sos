package Main;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import simulation.ItselfOrganizationVariables;



public class TypeLoader 
{
	public static ArrayList<String> elementaryTypesList;
	public static ArrayList<String> intermediaryTypesList;
	
	//structure:
	//key - type name
	//Vector value: function list 
	//ATTENTION! each element in function list has next structure: 
	//function_name|function_priority 
	public static Map< String, ArrayList<String> > typesMapForFunctions;
	
	//structure:
	//key - type name
	//Vector value:
	//element 0 - factor in sum(Xi*Ki) function
	//element 1 - factor in sum(Xi^Ki) function
	//element 2 - factor in pow(Xi*Ki) function
	//element 3 - slots(for Intermediary Types)
	//element 4 - price
	//element 5 - participation
	//element 6 - type of possible children 
	//             ( 3 - list or intermediate, 
	//               2 - only intermediate
	//               1 - only list)
	public static Map< String, ArrayList<Float> > typesParametersMap;
	
	private static double averageElementaryPrice = 0.0;
	private static double averageElementaryMaxCount = 0.0;
	private static double participateElemTypesCount = 0.0;
	
	private static double averageInterPrice = 0.0;
//	private static double averageInterMaxCount = 0.0;
	private static double averageSlotsNumber = 0.0;
	private static double participateInterTypesCount = 0.0;
	
	public TypeLoader()
	{
		//Initialization
		elementaryTypesList = new ArrayList<String>();
		intermediaryTypesList = new ArrayList<String>();
		typesMapForFunctions = new HashMap< String, ArrayList<String> >();
		typesParametersMap = new HashMap< String, ArrayList<Float> >();
		
//elementary_types
		//reading information about elementary_types
        File docFile = new File("files/elementary_types.xml");
        Document doc = null;
        try 
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(docFile);
        } 
        catch (java.io.IOException e) 
        {
            System.out.println("Can't find the file");
        } 
        catch (Exception e) 
        {
            System.out.print("Problem parsing the file");
        }
        
        Element root = doc.getDocumentElement();
        
        NodeList elementaryTypesNodeList = root.getElementsByTagName("type");
        Element elementaryType;
        if( elementaryTypesNodeList.getLength() > 0 )
        {
        	for( int i=0; i<elementaryTypesNodeList.getLength(); i++ )
            {
        		//add current type to the elementaryTypesList
            	elementaryType = (Element) elementaryTypesNodeList.item(i);
            	elementaryTypesList.add(elementaryType.getAttribute("name"));
            	
            	//create characteristics list for current type and put it to typesParametersMap
            	try
            	{
            		ArrayList<Float> typeCharacteristicsList = new ArrayList<Float>();
            		typeCharacteristicsList.add( Float.parseFloat( elementaryType.getAttribute("sum_mult") ) );
            		typeCharacteristicsList.add( Float.parseFloat( elementaryType.getAttribute("sum_pow") ) );
            		typeCharacteristicsList.add( Float.parseFloat( elementaryType.getAttribute( "pow_mult") ) );
            		typeCharacteristicsList.add( 0.0f );//slots
            		typeCharacteristicsList.add( Float.parseFloat( elementaryType.getAttribute("price") ) );
                	typeCharacteristicsList.add( Float.parseFloat( elementaryType.getAttribute( "participation") ) );
                	typeCharacteristicsList.add( 0.0f );//children type
                	typesParametersMap.put(elementaryType.getAttribute("name"), typeCharacteristicsList);
            	}
            	catch (NumberFormatException e) 
            	{
                    e.printStackTrace();
                }
            	
            	//create function list for current type
            	NodeList funcNodeList = elementaryType.getElementsByTagName("func");
            	if( funcNodeList.getLength() >0 )
            	{
            		Element function;
            		ArrayList<String> funcElementaryList = new ArrayList<String>();
                    for( int j=0; j<funcNodeList.getLength(); j++ )
                    {
                    	function = (Element) funcNodeList.item(j);
                    	funcElementaryList.add( function.getAttribute("value") + "|" + function.getAttribute("priority") );
                    }
                    
                    typesMapForFunctions.put( elementaryType.getAttribute("name") , funcElementaryList);
            	}
            	
            }
        }
        
//intermediary_types
        //reading information about intermediary_types
        File docFileInter = new File("files/inter_types.xml");
        Document docInter = null;
        try 
        {
            DocumentBuilderFactory dbfInter = DocumentBuilderFactory.newInstance();
            DocumentBuilder dbInter = dbfInter.newDocumentBuilder();
            docInter = dbInter.parse(docFileInter);
        } 
        catch (java.io.IOException e) 
        {
            System.out.println("Can't find the file");
        } 
        catch (Exception e) 
        {
            System.out.print("Problem parsing the file");
        }
        
        Element rootInter = docInter.getDocumentElement();
        
        NodeList intermediaryTypesNodeList = rootInter.getElementsByTagName("type");
        Element intermediaryType;
        if( intermediaryTypesNodeList.getLength() > 0 )
        {
        	for( int i=0; i<intermediaryTypesNodeList.getLength(); i++ )
            {
        		//add current type to the intermediaryTypesList
        		intermediaryType = (Element) intermediaryTypesNodeList.item(i);
        		intermediaryTypesList.add(intermediaryType.getAttribute("name"));
        		
        		//create characteristics list for current type and put it to typesParametersMap
        		ArrayList<Float> typeCharacteristicsList = new ArrayList<Float>();
            	typeCharacteristicsList.add( Float.parseFloat( intermediaryType.getAttribute("sum_mult") ) );
            	typeCharacteristicsList.add( Float.parseFloat( intermediaryType.getAttribute("sum_pow") ) );
            	typeCharacteristicsList.add( Float.parseFloat( intermediaryType.getAttribute( "pow_mult") ) );
            	typeCharacteristicsList.add( Float.parseFloat( intermediaryType.getAttribute("slots") ) );
            	typeCharacteristicsList.add( Float.parseFloat( intermediaryType.getAttribute("price") ) );
            	typeCharacteristicsList.add( Float.parseFloat( intermediaryType.getAttribute("participation") ) );
            	typeCharacteristicsList.add( Float.parseFloat( intermediaryType.getAttribute("children_type") ) );

            	typesParametersMap.put(intermediaryType.getAttribute("name"), typeCharacteristicsList);
        		
        		//create function list for current type
            	NodeList funcNodeListInter = intermediaryType.getElementsByTagName("func");
            	if( funcNodeListInter.getLength() >0 )
            	{
            		Element function;
            		ArrayList<String> funcIntermediaryList = new ArrayList<String>();
                    for( int j=0; j<funcNodeListInter.getLength(); j++ )
                    {
                    	function = (Element) funcNodeListInter.item(j);
                    	funcIntermediaryList.add( function.getAttribute("value") + "|" + function.getAttribute("priority") );
                    }
                    
                    typesMapForFunctions.put( intermediaryType.getAttribute("name") , funcIntermediaryList);
            	}
            }
        } 
        
        
	}
	
//	public static void sortFuncInTypeByPriority( String typeName )
//	{
//		int funcCount;
//		ArrayList< String > funcList = new ArrayList<String>();
//		ArrayList< Integer > funcPriorityList = new ArrayList<Integer>();
//		ArrayList<String> loadedList = new ArrayList<String>();
//		if( typesMapForFunctions.containsKey( typeName ) )
//		{
//			loadedList = typesMapForFunctions.get(typeName);
//		}
//		funcCount = loadedList.size();
//		if( funcCount > 0 )
//		{
//			String tmpString;
//			System.out.println("WAS");
//			for(int i=0; i<loadedList.size(); i++)
//			{
//				
//				tmpString = loadedList.get(i);
//				System.out.println(tmpString);
//				funcList.add( tmpString.substring( 0, tmpString.indexOf("|") ) );
//				funcPriorityList.add( Integer.parseInt( tmpString.substring( tmpString.indexOf("|") + 1 , tmpString.length() ) ) );
//				
//			}
//			
//			int tmpPrior;
//			for( int j=0; j < funcCount - 1; j++ )
//			{
//				for( int i=0; i < funcCount - 1 - j; i++ )
//				{
//					if( funcPriorityList.get( i ) > 
//							funcPriorityList.get( i + 1 ) )
//					{
//						tmpString = funcList.get( i );
//						funcList.set( i, funcList.get( i + 1 ) );
//						funcList.set( i + 1, tmpString );
//						
//						tmpPrior = funcPriorityList.get( i );
//						funcPriorityList.set( i, funcPriorityList.get( i + 1 ) );
//						funcPriorityList.set( i + 1, tmpPrior );
//					}
//				}
//			}
////			typesMapForFunctions.put(typeName, funcList);
//			
//			System.out.println("Became:");
////			if( typesMapForFunctions.containsKey( typeName ) )
////			{
////				loadedList = typesMapForFunctions.get(typeName);
//				for(int i=0; i<funcList.size(); i++)
//				{
//					tmpString = funcList.get(i);
//					System.out.println(tmpString);
//				}
////			}
//		}
//	}
	
	
	
	public static void setAverageCharacteristicsForLeafs()
	{
		String typeName;
		int typeInd;
		
		participateElemTypesCount = 0.0;
		averageElementaryPrice = 0.0;
		averageElementaryMaxCount = 0.0;
		
		for( int i=0; i < elementaryTypesList.size(); i++ )
		{
			typeName = elementaryTypesList.get( i );
			
			typeInd = ItselfOrganizationVariables.
			usedInOrganizationElementaryTypesList.
			indexOf( typeName );
			
			if( typeInd > -1 )
			{
				participateElemTypesCount++;
				averageElementaryPrice = averageElementaryPrice 
				+ getPrice( typeName );
				averageElementaryMaxCount = averageElementaryMaxCount
				+ ItselfOrganizationVariables.
				typesMaxValuesList.get( typeInd );
			}
		}
		
		if( participateElemTypesCount > 0 )
		{
			averageElementaryPrice = averageElementaryPrice / 
			participateElemTypesCount;
			averageElementaryMaxCount = averageElementaryMaxCount / 
			participateElemTypesCount;
			if( averageElementaryMaxCount == 0 )
			{
				averageElementaryMaxCount = 1.0;
			}
		}
	}
	
	public static double getAveragePriceForLeafs()
	{
		return averageElementaryPrice;
	}
	
	public static double getAverageCountForLeafs()
	{
		return averageElementaryMaxCount;
	}
	
	//used ONLY for sum(Xi), sum(Xi*Ki) and sum(Xi^Ki)
	//for case if in participate type selected 
	//not participate function
	public static double getListMainCaracteristic(double listCount,
			String listType,
			String parentFunc)
	{
		double mainCaracteristic = 0.0;
		double typePrice;
		
		if( parentFunc.equals("sum(Xi)") )
		{
			mainCaracteristic = listCount;
		}
		else if( parentFunc.equals("sum(Xi*Ki)") )
		{
			mainCaracteristic = listCount * 
			getFactorForSumMultFunction( listType );
		}
		else if( parentFunc.equals("sum(Xi^Ki)") )
		{
			mainCaracteristic = Math.pow( listCount, 
					getFactorForSumPowFunction( listType ) );
		}
		
		if( averageElementaryMaxCount != 0 )
		{
			mainCaracteristic = 
				( (mainCaracteristic -
						averageElementaryMaxCount)
						* 100.0 ) / 
						averageElementaryMaxCount;
		}
		
		if( ItselfOrganizationVariables
				.useMaximumPrice )
		{
			typePrice = getPrice( listType );
			typePrice = 
				( (typePrice - averageElementaryPrice)
					* 100.0 ) / averageElementaryPrice;
			
			mainCaracteristic = mainCaracteristic - typePrice;
		}
		
		return mainCaracteristic;
	}
	
	public static void setAverageCharacteristicsForIntermediate()
	{
		String typeName;

		averageInterPrice = 0.0;
//		averageInterMaxCount = 0.0;
		averageSlotsNumber = 0.0;
		participateInterTypesCount = 0.0;
		
		for( int i=0; i < intermediaryTypesList.size(); i++ )
		{
			typeName = intermediaryTypesList.get( i );
			
			if( ( ! ItselfOrganizationVariables.useParticipationInTypeOrg)
					|| isParticipate(typeName) )
			{
				participateInterTypesCount++;
				averageInterPrice = averageInterPrice 
				+ getPrice( typeName );
				averageSlotsNumber = averageSlotsNumber
				+ getSlotsNumber( typeName );
			}
		}
		
		if( participateInterTypesCount > 0 )
		{
			averageInterPrice = averageInterPrice / 
			participateInterTypesCount;
			averageSlotsNumber = averageSlotsNumber / 
			participateInterTypesCount;
		}
	}
	
	public static double getAveragePriceForInter()
	{
		return averageInterPrice;
	}
	
	public static double getAverageSlotsNumberForInter()
	{
		return averageSlotsNumber;
	}
	
	//filling
	//participateInterTypeMainCharacteristics
	//interTypesContainsSumFunc
	//interTypesContainsSumMultFunc
	//interTypesContainsSumPowFunc
	//in ItselfOrganizationVariebles
	public static void fillingInterTypesContainingFuncLists()
	{
		ItselfOrganizationVariables.
		participateInterTypeMainCharacteristics.clear();
		ItselfOrganizationVariables.
		interTypesContainsSumFunc.clear();
		ItselfOrganizationVariables.
		interTypesContainsSumMultFunc.clear();
		ItselfOrganizationVariables.
		interTypesContainsSumPowFunc.clear();
		
		String typeName;
		
		double mainCharacteristic;
//		ArrayList<String> participateInterTypesList = 
//			new ArrayList<String>();
		double currentSlotsNumber;
		double currentPrice;
		
		ArrayList<String> funcList = new ArrayList<String>();

//create tmp structures witch at the end this function will
//be copy to the corresponding variables in ItselfOrganizationVariebles
		//key - intermediate type witch participate in itself - 
		//organization
		//value - this type main characteristic
		Map< String, Double  >  participateInterTypeMainCharacteristics = new HashMap< String, Double  >();
		//interTypesContains...Func - contains list of types for
		//witch include ... function, in the order of decrease them
		//general characteristic 
		ArrayList<String> interTypesContainsSumFunc = new ArrayList<String>();
		ArrayList<String> interTypesContainsSumMultFunc = new ArrayList<String>();
		ArrayList<String> interTypesContainsSumPowFunc = new ArrayList<String>();
		
///filling
//participateInterTypeMainCharacteristics
//interTypesContainsSumFunc
//interTypesContainsSumMultFunc
//interTypesContainsSumPowFunc
		if( ItselfOrganizationVariables.useParticipationInTypeOrg )
		{
			for(int typeInd=0; typeInd < intermediaryTypesList.size(); typeInd++ )
			{
				typeName = intermediaryTypesList.get( typeInd );
				if( isParticipate(typeName) )
				{
					
					//filling
					//interTypesContainsSumFunc
					//interTypesContainsSumMultFunc
					//interTypesContainsSumPowFunc
					funcList.clear();
					if( ItselfOrganizationVariables.funcIntermediateOrgAllowed )
					{
						funcList.addAll( getAllIntermediateFunctions(typeName) );
					}
					else
					{
						funcList.add( getFirstIntermediateFunc(typeName) );
					}
					
					for (int itr = 0; itr < funcList.size(); itr++)
					{
						if (funcList.get(itr).contains("|"))
							funcList.set(itr, funcList.get(itr).substring(0, funcList.get(itr).indexOf("|")));
					}
					
					if( funcList.contains("sum(Xi)") )
					{
						interTypesContainsSumFunc.add( typeName );
					}
					if( funcList.contains("sum(Xi*Ki)") )
					{
						interTypesContainsSumMultFunc.add( typeName );
					}
					if( funcList.contains("sum(Xi^Ki)") )
					{
						interTypesContainsSumPowFunc.add( typeName );
					}
					
//					participateInterTypesList.add( typeName );
					if( ItselfOrganizationVariables.useMaximumPrice &&
							( ItselfOrganizationVariables.
									priceInflusionOnMainInterChar != 0 ) )
					{
						//mainCharacteristic =
						// ( 1 - "ItselfOrganizationVariebles.
						//priceInflusionOnMainInterChar" ) *
						//"percent of a gain of current type 
						//slots number rather its average value for
						//all participate intermediate types"  - 
						//( "percent of a gain of current type 
						//price  rather its average value for
						//all participate intermediate types" *
						//"ItselfOrganizationVariebles.
						//priceInflusionOnMainInterChar" )
						currentSlotsNumber = getSlotsNumber( typeName );
						currentPrice = getPrice( typeName );
						
						currentSlotsNumber = 
							( currentSlotsNumber - averageSlotsNumber ) 
							* 100 / averageSlotsNumber;
						
						currentPrice =
							( currentPrice - averageInterPrice)
							* 100 / averageInterPrice;
						
						mainCharacteristic = 
							( 1 - ItselfOrganizationVariables.
									priceInflusionOnMainInterChar) *
									currentSlotsNumber - 
									ItselfOrganizationVariables.
									priceInflusionOnMainInterChar * 
									currentPrice;
									
					}
					else
					{
						mainCharacteristic = getSlotsNumber( typeName );
					}
					participateInterTypeMainCharacteristics.
					put( typeName, mainCharacteristic );
				}
			}
		}
		else
		{
//			participateInterTypesList.addAll( intermediaryTypesList );
			for(int typeInd=0; typeInd < intermediaryTypesList.size(); typeInd++ )
			{
				typeName = intermediaryTypesList.get( typeInd );
				
				//filling
				//interTypesContainsSumFunc
				//interTypesContainsSumMultFunc
				//interTypesContainsSumPowFunc
				funcList.clear();
				if( ItselfOrganizationVariables.funcIntermediateOrgAllowed )
				{
					funcList.addAll( getAllIntermediateFunctions(typeName) );
				}
				else
				{
					funcList.add( getFirstIntermediateFunc(typeName) );
				}
				if( funcList.contains("sum(Xi)") )
				{
					interTypesContainsSumFunc.add( typeName );
				}
				if( funcList.contains("sum(Xi*Ki)") )
				{
					interTypesContainsSumMultFunc.add( typeName );
				}
				if( funcList.contains("sum(Xi^Ki)") )
				{
					interTypesContainsSumPowFunc.add( typeName );
				}
				
				if( ItselfOrganizationVariables.useMaximumPrice &&
						( ItselfOrganizationVariables.
								priceInflusionOnMainInterChar != 0 ) )
				{
					//mainCharacteristic =
					// ( 1 - "ItselfOrganizationVariebles.
					//priceInflusionOnMainInterChar" ) *
					//"percent of a gain of current type 
					//slots number rather its average value for
					//all participate intermediate types"  - 
					//( "percent of a gain of current type 
					//price  rather its average value for
					//all participate intermediate types" *
					//"ItselfOrganizationVariebles.
					//priceInflusionOnMainInterChar" )
					currentSlotsNumber = getSlotsNumber( typeName );
					currentPrice = getPrice( typeName );
					
					currentSlotsNumber = 
						( currentSlotsNumber - averageSlotsNumber ) 
						* 100 / averageSlotsNumber;
					
					currentPrice =
						( currentPrice - averageInterPrice)
						* 100 / averageInterPrice;
					
					mainCharacteristic = 
						( 1 - ItselfOrganizationVariables.
								priceInflusionOnMainInterChar) *
								currentSlotsNumber - 
								ItselfOrganizationVariables.
								priceInflusionOnMainInterChar * 
								currentPrice;
								
				}
				else
				{
					mainCharacteristic = getSlotsNumber( typeName );
				}
				participateInterTypeMainCharacteristics.
				put( typeName, mainCharacteristic );
				
			}
		}
		
//sorting 
//interTypesContainsSumFunc
//interTypesContainsSumMultFunc
//interTypesContainsSumPowFunc	
		double a, b;
		String typeA, typeB;
		//sorting interTypesContainsSumFunc
		for( int i=0; i < interTypesContainsSumFunc.size() - 1 ; i++ )
		{
			for( int j=0; 
			j < interTypesContainsSumFunc.size() - 1 - i; j++ )
			{
				typeA = interTypesContainsSumFunc.get(j);
				a = participateInterTypeMainCharacteristics
				.get( typeA );
				typeB = interTypesContainsSumFunc.get( j + 1 );
				b = participateInterTypeMainCharacteristics
				.get( typeB );
				if(  a <= b )
				{
					
					if( a == b)
					{
						//if main characteristics are equals we look witch type
						//has more functions
						if( ( typesMapForFunctions.get(typeB) ).size() >
						( typesMapForFunctions.get(typeA) ).size() )
						{
							interTypesContainsSumFunc.set( j, typeB );
							interTypesContainsSumFunc.set( j + 1, typeA );
						}
					}
					else
					{
						interTypesContainsSumFunc.set( j, typeB );
						interTypesContainsSumFunc.set( j + 1, typeA );
					}
					
				}
			}
		}
		
		//sorting interTypesContainsSumMultFunc
		for( int i=0; i < interTypesContainsSumMultFunc.size() - 1 ; i++ )
		{
			for( int j=0; 
			j < interTypesContainsSumMultFunc.size() - 1 - i; j++ )
			{
				typeA = interTypesContainsSumMultFunc.get(j);
				a = participateInterTypeMainCharacteristics
				.get( typeA );
				typeB = interTypesContainsSumMultFunc.get( j + 1 );
				b = participateInterTypeMainCharacteristics
				.get( typeB );
				if(  a <= b )
				{
					
					if( a == b)
					{
						//if main characteristics are equals we look witch type
						//has more functions
						if( ( typesMapForFunctions.get(typeB) ).size() >
						( typesMapForFunctions.get(typeA) ).size() )
						{
							interTypesContainsSumMultFunc.set( j, typeB );
							interTypesContainsSumMultFunc.set( j + 1, typeA );
						}
					}
					else
					{
						interTypesContainsSumMultFunc.set( j, typeB );
						interTypesContainsSumMultFunc.set( j + 1, typeA );
					}
					
				}
			}
		}
		
		//sorting interTypesContainsSumPowFunc
		for( int i=0; i < interTypesContainsSumPowFunc.size() - 1 ; i++ )
		{
			for( int j=0; 
			j < interTypesContainsSumPowFunc.size() - 1 - i; j++ )
			{
				typeA = interTypesContainsSumPowFunc.get(j);
				a = participateInterTypeMainCharacteristics
				.get( typeA );
				typeB = interTypesContainsSumPowFunc.get( j + 1 );
				b = participateInterTypeMainCharacteristics
				.get( typeB );
				if(  a <= b )
				{
					
					if( a == b)
					{
						//if main characteristics are equals we look witch type
						//has more functions
						if( ( typesMapForFunctions.get(typeB) ).size() >
						( typesMapForFunctions.get(typeA) ).size() )
						{
							interTypesContainsSumPowFunc.set( j, typeB );
							interTypesContainsSumPowFunc.set( j + 1, typeA );
						}
					}
					else
					{
						interTypesContainsSumPowFunc.set( j, typeB );
						interTypesContainsSumPowFunc.set( j + 1, typeA );
					}
					
				}
			}
		}
//copy tmp structures to the corresponding variables in
//ItselfOrganizationVariebles
		ItselfOrganizationVariables.
		participateInterTypeMainCharacteristics.
		putAll(participateInterTypeMainCharacteristics);
		ItselfOrganizationVariables.
		interTypesContainsSumFunc.
		addAll(interTypesContainsSumFunc);
		ItselfOrganizationVariables.
		interTypesContainsSumMultFunc.
		addAll(interTypesContainsSumMultFunc);
		ItselfOrganizationVariables.
		interTypesContainsSumPowFunc.
		addAll(interTypesContainsSumPowFunc);
		
		
	}
	
	
	
	
	
	public static float getPrice(String typeName)
	{
		if( typesParametersMap.containsKey( typeName ) )
		{
			return typesParametersMap.get(typeName).get(4);
		}
		return -1f;
	}
	
	public static boolean isParticipate(String typeName)
	{
		if( typesParametersMap.containsKey( typeName ) )
		{
			if( typesParametersMap.get(typeName).get(5) == 1 )
			{
				return true;
			}
		}
		return false;
	}
	
	public static void setParticipateForTypeList( ArrayList<String> typesNames, boolean participation)
	{
		String typeName = "";
		float tmpVar;
		for( int i=0; i < typesNames.size(); i++)
		{
			typeName = typesNames.get( i );
			if( typesParametersMap.containsKey( typeName ) )
			{
				if( participation )
				{
					tmpVar = 1;
				}
				else
				{
					tmpVar = 0;
				}
				typesParametersMap.get(typeName).set( 5, tmpVar );
			}
		}
		
		if( typesNames.size() > 0 )
		{
			if( elementaryTypesList.contains( typesNames.get(0) ) )
			{
				saveCurrentElementaryTypeSet();
			}
			else
			{
				saveCurrentIntermediaryTypeSet();
			}
		}
	}
	
	public static int getSlotsNumber(String typeName)
	{
		if( typesParametersMap.containsKey( typeName ) )
		{
			return typesParametersMap.get(typeName).get(3).intValue();
		}
		return -1;
	}
	
	public static int getChildrenType(String typeName)
	{
		if( typesParametersMap.containsKey( typeName ) )
		{
			return typesParametersMap.get(typeName).get(6).intValue();
		}
		return -1;
	}
	
	public static ArrayList<String> getFunctionList(String typeName)
	{
		ArrayList<String> loadedList = new ArrayList<String>();
		ArrayList<String> tmpList = new ArrayList<String>();
		if( typesMapForFunctions.containsKey( typeName ) )
		{
			loadedList = typesMapForFunctions.get(typeName);
		}
		if( loadedList.size() > 0 )
		{
			for(int i=0; i<loadedList.size(); i++)
			{
				tmpList.add( loadedList.get(i).substring( 0, loadedList.get(i).indexOf("|") ) );
			}
			return tmpList;
		}
		return null;
	}
	
	public static Map<String,Integer> getFunctionPriorityMap(String typeName)
	{
		ArrayList<String> loadedList = new ArrayList<String>();
		Map<String,Integer> tmpMap = new HashMap< String, Integer >();
		if( typesMapForFunctions.containsKey( typeName ) )
		{
			loadedList = typesMapForFunctions.get(typeName);
		}
		if( loadedList.size() > 0 )
		{
			String tmpString;
			for(int i=0; i<loadedList.size(); i++)
			{
				tmpString = loadedList.get(i);
				tmpMap.put( tmpString.substring( 0, tmpString.indexOf("|") ) , Integer.parseInt( tmpString.substring( tmpString.indexOf("|") + 1 , tmpString.length() ) ) );
			}
			return tmpMap;
		}
		return null;
	}
	
	public static float getFactorForSumMultFunction( String typeName )
	{
		if( typesParametersMap.containsKey( typeName ) )
		{
			return typesParametersMap.get(typeName).get(0);
		}
		return -1.0f;
	}
	
	public static float getFactorForSumPowFunction( String typeName )
	{
		if( typesParametersMap.containsKey( typeName ) )
		{
			return typesParametersMap.get(typeName).get(1);
		}
		return -1.0f;
	}
	
	public static float getFactorForPowMultFunction( String typeName )
	{
		if( typesParametersMap.containsKey( typeName ) )
		{
			return typesParametersMap.get(typeName).get(2);
		}
		return -1.0f;
	}
	
	public static float getFactorForIntermediateFunction( String typeName, String funcName )
	{
		if( typesParametersMap.containsKey( typeName ) )
		{
			if( funcName.equals("sum(Xi*Ki)") )
			{
				return typesParametersMap.get(typeName).get(0);
			}
			else if( funcName.equals("sum(Xi^Ki)") )
			{
				return typesParametersMap.get(typeName).get(1);
			}
			else if( funcName.equals("pow(Xi*Ki)") )
			{
				return typesParametersMap.get(typeName).get(2);
			}
		}
		return 1.0f;
	}

	public static boolean saveCurrentElementaryTypeSet() 
	{
		File docFile = new File("files/elementary_types.xml");
		try 
        {
			//Create a Document
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbf.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            
//            doc.setXmlVersion("1.0");
//Creating the XML tree begin
            //create the root element and add it to the document
            Element root = doc.createElement("type_list");
            doc.appendChild(root);
            
            Element typeNode;
            Element funcNode;
            String funcString;
            String typeName;
            for(int typeInd=0; typeInd<elementaryTypesList.size(); typeInd++)
            {
            	typeNode = doc.createElement("type");
            	typeName = elementaryTypesList.get( typeInd );
            	typeNode.setAttribute( "name", typeName );
            	typeNode.setAttribute( "sum_mult", String.valueOf( typesParametersMap.get(typeName).get(0) ) );
            	typeNode.setAttribute( "sum_pow", String.valueOf( typesParametersMap.get(typeName).get(1) ) );
            	typeNode.setAttribute( "pow_mult", String.valueOf( typesParametersMap.get(typeName).get(2) ) );
            	typeNode.setAttribute( "slots", String.valueOf( typesParametersMap.get(typeName).get(3) ) );
            	typeNode.setAttribute( "price", String.valueOf( typesParametersMap.get(typeName).get(4) ) );
            	typeNode.setAttribute( "participation", String.valueOf( typesParametersMap.get(typeName).get(5) ) );
            	typeNode.setAttribute( "children_type", String.valueOf( typesParametersMap.get(typeName).get(6) ) );
            	
            	for(int funcInTypeInd=0;funcInTypeInd<typesMapForFunctions.get( typeName ).size();funcInTypeInd++)
            	{
            		funcNode = doc.createElement("func");
            		funcString = typesMapForFunctions.get( typeName ).get( funcInTypeInd );
            		funcNode.setAttribute( "value",  funcString.substring( 0, funcString.indexOf("|") ) );
            		funcNode.setAttribute( "priority",  funcString.substring( funcString.indexOf("|")+1 , funcString.length() ) );
            		typeNode.appendChild( funcNode );
            	}
            		 
            	root.appendChild(typeNode);
            }
//Creating the XML tree end
            
            //save to file
            String outputURL = docFile.getAbsolutePath();
            
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new FileOutputStream(outputURL));

            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.transform(source, result);

		} 
        catch (Exception e) 
        {
        	JOptionPane.showMessageDialog(null, "������ ��� ���������� ��������� � ���������� �����");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean saveCurrentIntermediaryTypeSet() 
	{
		File docFile = new File("files/inter_types.xml");
		try 
        {
			//Create a Document
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbf.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            
//            doc.setXmlVersion("1.0");
//Creating the XML tree begin
            //create the root element and add it to the document
            Element root = doc.createElement("type_list");
            doc.appendChild(root);
            
            Element typeNode;
            Element funcNode;
            String funcString;
            String typeName;
            for(int typeInd=0; typeInd<intermediaryTypesList.size(); typeInd++)
            {
            	typeNode = doc.createElement("type");
            	typeName = intermediaryTypesList.get( typeInd );
            	typeNode.setAttribute( "name", typeName );
            	typeNode.setAttribute( "sum_mult", String.valueOf( typesParametersMap.get(typeName).get(0) ) );
            	typeNode.setAttribute( "sum_pow", String.valueOf( typesParametersMap.get(typeName).get(1) ) );
            	typeNode.setAttribute( "pow_mult", String.valueOf( typesParametersMap.get(typeName).get(2) ) );
            	typeNode.setAttribute( "slots", String.valueOf( typesParametersMap.get(typeName).get(3) ) );
            	typeNode.setAttribute( "price", String.valueOf( typesParametersMap.get(typeName).get(4) ) );
            	typeNode.setAttribute( "participation", String.valueOf( typesParametersMap.get(typeName).get(5) ) );
            	typeNode.setAttribute( "children_type", String.valueOf( typesParametersMap.get(typeName).get(6) ) );
            	
            	for(int funcInTypeInd=0;funcInTypeInd<typesMapForFunctions.get( typeName ).size();funcInTypeInd++)
            	{
            		funcNode = doc.createElement("func");
            		funcString = typesMapForFunctions.get( typeName ).get( funcInTypeInd );
            		funcNode.setAttribute( "value",  funcString.substring( 0, funcString.indexOf("|") ) );
            		funcNode.setAttribute( "priority",  funcString.substring( funcString.indexOf("|")+1 , funcString.length() ) );
            		typeNode.appendChild( funcNode );
            	}
            		 
            	root.appendChild(typeNode);
            }
//Creating the XML tree end
            
            //save to file
            String outputURL = docFile.getAbsolutePath();
            
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new FileOutputStream(outputURL));

            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.transform(source, result);

		} 
        catch (Exception e) 
        {
        	JOptionPane.showMessageDialog(null, "������ ��� ���������� ��������� � ���������� �����");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void changeElementaryFunctionNameInAllTypes(String oldFunctionName, String newFunctionName)
	{
		String typeName, currentFunctionInTypeTotalInf, currentFunctionInTypeName,
		       currentFunctionInTypePriority;
		ArrayList<String> typeFunctionTotalInfList;
		boolean typeWasChanged, anyTypeWasChanged = false;
		//go through all elementary types
		for(int typeInd=0; typeInd<elementaryTypesList.size(); typeInd++)
		{
			typeName = elementaryTypesList.get( typeInd );
			
			//get function list for current type
			//each element in this list has next structure: 
		    //function_name|function_priority 
			typeFunctionTotalInfList = typesMapForFunctions.get( typeName );
			typeWasChanged = false;
			
			//go through all functions in current type
			for(int funcInTypeInd=0; funcInTypeInd < typeFunctionTotalInfList.size(); funcInTypeInd++ )
			{
				currentFunctionInTypeTotalInf = typeFunctionTotalInfList.get( funcInTypeInd );
				currentFunctionInTypeName = currentFunctionInTypeTotalInf.substring( 0, currentFunctionInTypeTotalInf.indexOf("|") );
				currentFunctionInTypePriority = currentFunctionInTypeTotalInf.substring( currentFunctionInTypeTotalInf.indexOf("|") + 1 , currentFunctionInTypeTotalInf.length() );
				if( oldFunctionName.equals( currentFunctionInTypeName ) )
				{
					typeWasChanged = true;
					typeFunctionTotalInfList.set( funcInTypeInd, newFunctionName + "|" + currentFunctionInTypePriority );
				}
			}
			
			if( typeWasChanged )
			{
				anyTypeWasChanged = true;
				typesMapForFunctions.put( typeName, typeFunctionTotalInfList );
			}
		}
		
		if( anyTypeWasChanged )
		{
			saveCurrentElementaryTypeSet();
		}
	}
	
	public static void changeElementaryFunctionsNamesInAllTypes(Vector<String> oldFunctionNameList, Vector<String> newFunctionNameList)
	{
		String typeName, currentFunctionInTypeTotalInf, currentFunctionInTypeName,
	       currentFunctionInTypePriority;
		ArrayList<String> typeFunctionTotalInfList;
		boolean typeWasChanged, anyTypeWasChanged = false;
		int oldAndNewFunctionInd;//index for conformity in oldFunctionNameList and in newFunctionNameList
		//go through all elementary types
		for(int typeInd=0; typeInd<elementaryTypesList.size(); typeInd++)
		{
			typeName = elementaryTypesList.get( typeInd );
		
			//get function list for current type
			//each element in this list has next structure: 
			//function_name|function_priority 
			typeFunctionTotalInfList = typesMapForFunctions.get( typeName );
			typeWasChanged = false;
		
			//go through all functions in current type
			for(int funcInTypeInd=0; funcInTypeInd < typeFunctionTotalInfList.size(); funcInTypeInd++ )
			{
				currentFunctionInTypeTotalInf = typeFunctionTotalInfList.get( funcInTypeInd );
				currentFunctionInTypeName = currentFunctionInTypeTotalInf.substring( 0, currentFunctionInTypeTotalInf.indexOf("|") );
				currentFunctionInTypePriority = currentFunctionInTypeTotalInf.substring( currentFunctionInTypeTotalInf.indexOf("|") + 1 , currentFunctionInTypeTotalInf.length() );
				if( oldFunctionNameList.contains( currentFunctionInTypeName ) )
				{
					typeWasChanged = true;
					oldAndNewFunctionInd = oldFunctionNameList.indexOf( currentFunctionInTypeName );
					typeFunctionTotalInfList.set( funcInTypeInd, newFunctionNameList.elementAt(oldAndNewFunctionInd) + "|" + currentFunctionInTypePriority );
				}
			}
			
			if( typeWasChanged )
			{
				anyTypeWasChanged = true;
				typesMapForFunctions.put( typeName, typeFunctionTotalInfList );
			}
		}
	
		if( anyTypeWasChanged )
		{
			saveCurrentElementaryTypeSet();
		}
	}
	
	public static boolean isAnyTypeContainsElementaryFunction( String functinName )
	{
		if( elementaryTypesList.size() > 0 )
		{
			String typeName;
			ArrayList<String> typesFunctionsList;
			for( int typeInd=0; typeInd < elementaryTypesList.size(); typeInd++ )
			{
				typeName = elementaryTypesList.get( typeInd );
				typesFunctionsList = getFunctionList(typeName);
				for( int funcInd=0; funcInd < typesFunctionsList.size(); funcInd++ )
				{
					if( typesFunctionsList.get(funcInd).equals(functinName) )
					{
						return true;
					}
				}
			}
		}
		return false;
	}
	
	public static boolean isExistParticipateTypeWithSlotsNumberMoreThen( int minSlotsNumber )
	{
		String typeName;
		for( int i=0; i < intermediaryTypesList.size(); i++ )
		{
			typeName = intermediaryTypesList.get( i );
			if( typesParametersMap.get(typeName).get(5) == 1 )
			{
				if( typesParametersMap.get(typeName).get(4) > minSlotsNumber )
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public static ArrayList<String> getAllParticipateListFunctions(String typeName)
	{
		ArrayList<String> allTypeFunctionsList;
		if( elementaryTypesList.contains(typeName) )
		{
			allTypeFunctionsList = typesMapForFunctions.get(typeName);
			if( allTypeFunctionsList != null )
			{
				ArrayList<String> rezultList = new ArrayList<String>();
				String funcName;
				for( int i=0; i < allTypeFunctionsList.size(); i++ )
				{
					funcName = allTypeFunctionsList.get( i );
					funcName = funcName.substring( 0, funcName.indexOf("|") );
					if( FunctionsLoader.isParticipate( funcName ) 
							|| ( ! ItselfOrganizationVariables.useParticipationInFuncOrg ) )
					{
						rezultList.add( funcName );
					}
				}
				return  rezultList;
			}
			else
			{
				return null;
			}
		}
		else
		{
			return null;
		}
	}
	
	public static ArrayList<String> getAllIntermediateFunctions(String typeName)
	{
		ArrayList<String> allTypeFunctionsList;
		if( intermediaryTypesList.contains(typeName) )
		{
			allTypeFunctionsList = typesMapForFunctions.get(typeName);
			if( ( allTypeFunctionsList != null )
					&& ( allTypeFunctionsList.size() > 0 ) )
			{
				ArrayList<String> rezultList = new ArrayList<String>();
				String funcName;
				for( int i=0; i < allTypeFunctionsList.size(); i++ )
				{
					funcName = allTypeFunctionsList.get( i );
					funcName = funcName.substring( 0, funcName.indexOf("|") );
					rezultList.add( funcName );
				}
				return  rezultList;
			}
			else
			{
//to_do
				return new ArrayList<String>();//m.b. null??
			}
		}
		else
		{
			return new ArrayList<String>();//m.b. null??
		}
	}
	
	public static String getFirstIntermediateFunc(String typeName)
	{
		ArrayList<String> allTypeFunctionsList;
		if( intermediaryTypesList.contains(typeName) )
		{
			allTypeFunctionsList = typesMapForFunctions.get(typeName);
			if( ( allTypeFunctionsList != null )
					&& ( allTypeFunctionsList.size() > 0 ) )
			{
				ArrayList<String> rezultList = new ArrayList<String>();
				String funcName = allTypeFunctionsList.get( 0 );
				return  funcName;
			}
			else
			{
//to_do
				return "";//m.b. null??
			}
		}
		else
		{
			return "";//m.b. null??
		}
	}
	
	public static ArrayList<String> findInterTypesMayHasLeafsChild()
	{
		ArrayList<String> interTypesMayHasLeafsChild;
		interTypesMayHasLeafsChild = new ArrayList<String>();
		
		String typeName;
		int childrenType;
		if( ItselfOrganizationVariables.useParticipationInTypeOrg )
		{
			for( int i=0; i < intermediaryTypesList.size() ; i++ )
			{
				typeName = intermediaryTypesList.get( i );
				if( isParticipate(typeName) )
				{
					childrenType = getChildrenType( typeName );
					if( childrenType == 1 || childrenType == 2 )
					{
						interTypesMayHasLeafsChild.add( typeName );
					}
				}
			}
		}
		else
		{
			for( int i=0; i < intermediaryTypesList.size() ; i++ )
			{
				typeName = intermediaryTypesList.get( i );
				childrenType = getChildrenType( typeName );
				if( childrenType == 1 || childrenType == 2 )
				{
					interTypesMayHasLeafsChild.add( typeName );
				}
			}
		}
		
		
		return interTypesMayHasLeafsChild;
	}
	
}
