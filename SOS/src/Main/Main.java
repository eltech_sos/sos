package Main;

import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;
import java.util.Map.Entry;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.event.TreeModelEvent;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jgraph.graph.DefaultGraphCell;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import Librares.FunctionsLibraryFX;
import Librares.ParametrsLibraryFX;
import Librares.TypesLibraryFX;
import object.AddElementaryObjectFX;
import object.AddObjectFX;
import object.TreeElement;
import simulation.ItselfOrganizationVariables;
import simulation.SimulationFX;
import simulation.parameter.ParametersOrganizer;
import simulation.parameter.ParametersOrganizerStrategy;
import simulation.Parser;
import tabs.ConfirmationType;
import tabs.FXDialog;
import tabs.Message;

public class Main extends JFrame {
	// program version for checking file type
	final JFXPanel fxPanelMenu;
	final JFXPanel fxPanelTreeView;
	final JFXPanel fxPanelTableView;
	public static Label btnAddEP = null;
	public static Label btnAddEO = null;
	public static Label btnRemove = null;
	
	
	private String version = "00002";
    public static int JCountingCount;
	private DefaultMutableTreeNode newNode;
	
	public static JTree tree; // tree at left panel
	DefaultMutableTreeNode dmtRoot;// first tree element(always exist)

	static ParametersTable tableSystem; // table for element parameters
	static JTextArea typeAndFuncNameInform;
	static int maxCountOfParams = 15;

	JLabel lbEmulatedSystem;

	// Graphic Panel
	public static GraphicPanel panelGraphic;
	JComboBox cbStartElement;
	JComboBox cbEndElement;

	// for adding elements
	public static String addingElemName;
	public static String addingElemTypeName;
	public static String addingElemFuncName;
	public static boolean addingElemIsElementary = false;
	public static boolean user_enter_data_of_adding_object = false;

	// total tree parameters
	public static int count = 0;// count of elements in tree
	public static int elemID = 0;
	public static Vector<Vector<TreeElement>> structTreeElements = null;
	public static int maxLevelsCount = -1;// number of created sublists(0=>1;
	// 1=>2 i td)
	public static Vector<String> elementsNamesList = null;

	// public static TypeLoader typeLoader;
	// public static ParametrsLoader parametrsLoader;
	public static Parser parser;

	// for simulation synchronization
	public static boolean allow_simulation = true;
	// public static boolean allow_tree_correction = true;
	public static boolean simulation_is_on = false;
	public static double maximumDeviation = 0;

	// for only one library can be opened at the same moment
	LibraryHandler libraryHandler;

	static JMenu libraryMenu;
	private static JMenuItem simulationBeginMenuItem;

	public static Main mainFrame;

	private static JMenuItem newMenuItem, openMenuItem, paramSameMenuItem,
			paramNotSameMenuItem;

	
	public void CreateMenu(final JFXPanel fxPanel)
	{
		Group root = new Group();
        Scene scene = new Scene(root);
       
        HBox hBoxSubMenu = new HBox();
        hBoxSubMenu.setPadding(new Insets(10, 0, 10, 0));
        
        VBox vBoxIVNew = new VBox();
        ImageView ivNew = new ImageView(new Image(getClass().getResourceAsStream("Resource/New64.png")));
        vBoxIVNew.getStyleClass().add("tree-view-menu");
        Label lblNew = new Label("�����");
        lblNew.setAlignment(Pos.CENTER);
        lblNew.setMinWidth(64);
        vBoxIVNew.getChildren().addAll(ivNew, lblNew);
        hBoxSubMenu.getChildren().add(vBoxIVNew);
        vBoxIVNew.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				newModel();
			}
        });
        
        
        VBox vBoxIVOpen = new VBox();
        ImageView ivOpen = new ImageView(new Image(getClass().getResourceAsStream("Resource/Open64.png")));
        vBoxIVOpen.getStyleClass().add("tree-view-menu");
        Label lblOpen = new Label("�������");
        lblOpen.setAlignment(Pos.CENTER);
        lblOpen.setMinWidth(64);
        vBoxIVOpen.getChildren().addAll(ivOpen, lblOpen);
        hBoxSubMenu.getChildren().add(vBoxIVOpen);
        vBoxIVOpen.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				openModel();
			}
        });
        
        
        VBox vBoxIVSave = new VBox();
        ImageView ivSave = new ImageView(new Image(getClass().getResourceAsStream("Resource/Save64.png")));
        vBoxIVSave.getStyleClass().add("tree-view-menu");
        Label lblSave = new Label("���������");
        lblSave.setAlignment(Pos.CENTER);
        lblSave.setMinWidth(64);
        vBoxIVSave.getChildren().addAll(ivSave, lblSave);
        hBoxSubMenu.getChildren().add(vBoxIVSave);
        vBoxIVSave.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				saveModel();
			}
        });

        VBox vBoxIVExit = new VBox();
        ImageView ivExit = new ImageView(new Image(getClass().getResourceAsStream("Resource/Exit64.png")));
        vBoxIVExit.getStyleClass().add("tree-view-menu");
        Label lblExit = new Label("�����");
        lblExit.setAlignment(Pos.CENTER);
        lblExit.setMinWidth(64);
        vBoxIVExit.getChildren().addAll(ivExit, lblExit);
        vBoxIVExit.setPadding(new Insets(0,5,0,0));
        hBoxSubMenu.getChildren().add(vBoxIVExit);
        vBoxIVExit.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				System.exit(0);
			}
        });
        
        VBox vBoxLibraries = new VBox();
        Label lblElementaryObject = new Label("���� ������������ ��������");
        lblElementaryObject.getStyleClass().add("tree-view-menu");
        Label lblDistributedObject = new Label("���� �����������");
        lblDistributedObject.getStyleClass().add("tree-view-menu");
        Label lblParameters = new Label("���������");
        lblParameters.getStyleClass().add("tree-view-menu");
        Label lblFunctions = new Label("�������");
        lblFunctions.getStyleClass().add("tree-view-menu");
        vBoxLibraries.getChildren().addAll(lblElementaryObject, lblDistributedObject, lblParameters, lblFunctions);
        lblElementaryObject.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				Stage stage = new Stage();
				try {
					(new TypesLibraryFX("���������� ����� ������������ ��������", true)).start(stage);
				} catch (Exception e) {
					e.printStackTrace();
				}
				stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
					public void handle(javafx.stage.WindowEvent arg0) {
						libraryMenu.setEnabled(true);
						allow_simulation = true;
						if (!simulation_is_on) {
							simulationBeginMenuItem.setEnabled(true);
						}
					}
				});
				stage.setOnShowing(new EventHandler<javafx.stage.WindowEvent>(){
					public void handle(javafx.stage.WindowEvent arg0) {
						allow_simulation = false;
						libraryMenu.setEnabled(false);
						simulationBeginMenuItem.setEnabled(false);
					}
				});
			}
        });
        lblDistributedObject.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				Stage stage = new Stage();
				try {
					(new TypesLibraryFX("���������� ����� �����������", false)).start(stage);
				} catch (Exception e) {
					e.printStackTrace();
				}
				stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
					public void handle(javafx.stage.WindowEvent arg0) {
						libraryMenu.setEnabled(true);
						allow_simulation = true;
						if (!simulation_is_on) {
							simulationBeginMenuItem.setEnabled(true);
						}
					}
				});
				stage.setOnShowing(new EventHandler<javafx.stage.WindowEvent>(){
					public void handle(javafx.stage.WindowEvent arg0) {
						allow_simulation = false;
						libraryMenu.setEnabled(false);
						simulationBeginMenuItem.setEnabled(false);
					}
				});
			}
        });
        lblParameters.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				Stage stage = new Stage();
				try {
					(new ParametrsLibraryFX()).start(stage);
				} catch (Exception e) {
					e.printStackTrace();
				}
				stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
					public void handle(javafx.stage.WindowEvent arg0) {
						libraryMenu.setEnabled(true);
						allow_simulation = true;
						if (!simulation_is_on) {
							simulationBeginMenuItem.setEnabled(true);
						}
					}
				});
				stage.setOnShowing(new EventHandler<javafx.stage.WindowEvent>(){
					public void handle(javafx.stage.WindowEvent arg0) {
						allow_simulation = false;
						libraryMenu.setEnabled(false);
						simulationBeginMenuItem.setEnabled(false);
					}
				});
			}
        });
        lblFunctions.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				Stage stage = new Stage();
				try {
					(new FunctionsLibraryFX()).start(stage);
				} catch (Exception e) {
					e.printStackTrace();
				}
				stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
					public void handle(javafx.stage.WindowEvent arg0) {
						libraryMenu.setEnabled(true);
						allow_simulation = true;
						if (!simulation_is_on) {
							simulationBeginMenuItem.setEnabled(true);
						}
					}
				});
				stage.setOnShowing(new EventHandler<javafx.stage.WindowEvent>(){
					public void handle(javafx.stage.WindowEvent arg0) {
						allow_simulation = false;
						libraryMenu.setEnabled(false);
						simulationBeginMenuItem.setEnabled(false);
					}
				});
			}
        });
        
        
        
        VBox vBoxModeling = new VBox();
        //vBoxModeling.getStyleClass().add("tree-view-menu");
        Label lblModeling = new Label("������ �������������");
        lblModeling.getStyleClass().add("tree-view-menu");
        vBoxModeling.getChildren().add(lblModeling);
        lblModeling.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				if (count < 2) {
				
					FXDialog.showMessageDialog("��� ���������������� ������� ��������� \n��� ������� ��� ��������", "�������������", Message.ERROR);
					return;
				}
				try {
					(new SimulationFX()).start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
        });
               
        
        VBox vBoxSettings = new VBox();
        Label lblIsSelfOrg = new Label("���������� ���������������");
        lblIsSelfOrg.getStyleClass().add("tree-view-menu");
        Label lblRequirements = new Label("���������� � ������");
        lblRequirements.getStyleClass().add("tree-view-menu");
        Label lblAddition = new Label("�������������� ���������");
        lblAddition.getStyleClass().add("tree-view-menu");
        
        //=======================================================================
        MenuBar menuBarSettings = new MenuBar();
		Menu menuParametricSO = new Menu("��������������� ���������������   >");
		MenuItem menuItemStepCount = new MenuItem("����� ����� �����");
		menuItemStepCount.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
			@Override
			public void handle(javafx.event.ActionEvent arg0) {
				try {
					(new StepsCountSettingsFX()).start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
        });
		
		
		Menu menuOptimizationMethod = new Menu("����� ������ �����������");
		ToggleGroup tGroupOptimizationMethod = new ToggleGroup();
		RadioMenuItem rmItemCicleDown = new RadioMenuItem("����������� �������������� �����");
		rmItemCicleDown.setToggleGroup(tGroupOptimizationMethod);
		rmItemCicleDown.setSelected(true);
		rmItemCicleDown.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
			@Override
			public void handle(javafx.event.ActionEvent arg0) {
				ItselfOrganizationVariables.metodRelax = false;
				//TODO refactor metodRelax selecting logic later
				//ItselfOrganizationVariables.isFirstParamOrg = true;
				//new
				// use old field ItselfOrganizationVariables.paramOrgStrategy to choose strategy
				ParametersOrganizerStrategy strategy;
				switch(ItselfOrganizationVariables.paramOrgStrategy) {
				case 1 : strategy = ParametersOrganizerStrategy.WEIGHTED_CCS;
					break;
				case 2 : strategy = ParametersOrganizerStrategy.CHANGE_COMPARSION_CCS;
					break;
				case 3 : strategy = ParametersOrganizerStrategy.CHANGE_OPPORTUNITY_CCS;
					break;
				case 4 : strategy = ParametersOrganizerStrategy.SAVE_STEP_VECTOR_CCS;
					break;
				default : strategy = ParametersOrganizerStrategy.SAVE_STEP_VECTOR_CCS;
					break;
				}
				ItselfOrganizationVariables.getInstance()
					.setParametersModelOrganizerStrategy(strategy);
			}
        });
		RadioMenuItem rmItemExponentRelax = new RadioMenuItem("����� ���������������� ����������");
		rmItemExponentRelax.setToggleGroup(tGroupOptimizationMethod);
		menuOptimizationMethod.getItems().addAll(rmItemCicleDown, rmItemExponentRelax);
		rmItemExponentRelax.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
			@Override
			public void handle(javafx.event.ActionEvent arg0) {
				//TODO refactor metodRelax selecting logic later
				ItselfOrganizationVariables.metodRelax = true;
				
				//ItselfOrganizationVariables.isFirstParamOrg = true;
				//New
				ItselfOrganizationVariables.getInstance()
					.setParametersModelOrganizerStrategy(ParametersOrganizerStrategy.EXPONENTIAL_RELAX);
			}
        });
		
		
		Menu menuStrategy = new Menu("����� ���������");
		ToggleGroup tGroupStrategy = new ToggleGroup();
		RadioMenuItem rmItemCompWeight = new RadioMenuItem("��������� ����������� �����");
		rmItemCompWeight.setToggleGroup(tGroupStrategy);
		rmItemCompWeight.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
			@Override
			public void handle(javafx.event.ActionEvent arg0) {
				//TODO remove after tests
				//ItselfOrganizationVariables.isFirstParamOrg = true;
				ItselfOrganizationVariables.paramOrgStrategy = 1;
				//new
				ItselfOrganizationVariables.getInstance()
					.setParametersModelOrganizerStrategy(ParametersOrganizerStrategy.WEIGHTED_CCS);
			}
        });
		RadioMenuItem rmItemCompChange = new RadioMenuItem("��������� ��������� ���������");
		rmItemCompChange.setToggleGroup(tGroupStrategy);
		rmItemCompChange.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
			@Override
			public void handle(javafx.event.ActionEvent arg0) {
				//TODO remove after tests
				//ItselfOrganizationVariables.isFirstParamOrg = true;
				ItselfOrganizationVariables.paramOrgStrategy = 2;
				//new
				ItselfOrganizationVariables.getInstance()
					.setParametersModelOrganizerStrategy(ParametersOrganizerStrategy.CHANGE_COMPARSION_CCS);
			}
        });
		RadioMenuItem rmItemOpportChange = new RadioMenuItem("��������� ��������� ������ ����������� ���������");
		rmItemOpportChange.setToggleGroup(tGroupStrategy);
		rmItemOpportChange.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
			@Override
			public void handle(javafx.event.ActionEvent arg0) {
				//TODO remove after tests
				//ItselfOrganizationVariables.isFirstParamOrg = true;
				ItselfOrganizationVariables.paramOrgStrategy = 3;
				//new
				ItselfOrganizationVariables.getInstance()
					.setParametersModelOrganizerStrategy(ParametersOrganizerStrategy.CHANGE_OPPORTUNITY_CCS);
			}
        });
		RadioMenuItem rmItemSaveVector = new RadioMenuItem("��������� ���������� ������� �����");
		rmItemSaveVector.setSelected(true);
		rmItemSaveVector.setToggleGroup(tGroupStrategy);
		rmItemSaveVector.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
			@Override
			public void handle(javafx.event.ActionEvent arg0) {
				//TODO remove after tests
				//ItselfOrganizationVariables.isFirstParamOrg = true;
				ItselfOrganizationVariables.paramOrgStrategy = 4;
				//new
				ItselfOrganizationVariables.getInstance()
					.setParametersModelOrganizerStrategy(ParametersOrganizerStrategy.SAVE_STEP_VECTOR_CCS);
			}
        });
		menuStrategy.getItems().addAll(rmItemCompWeight, rmItemCompChange, rmItemOpportChange, rmItemSaveVector);
		
		
		Menu menuParameterValue = new Menu("�������� ����������");
	    ToggleGroup tGroupParameterValue = new ToggleGroup();
		RadioMenuItem rmItemAllSame = new RadioMenuItem("��������� �� ���� ���������");
		rmItemAllSame.setToggleGroup(tGroupParameterValue);
		rmItemAllSame.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
			@Override
			public void handle(javafx.event.ActionEvent arg0) {
				//TODO try to clean this up later
				//ItselfOrganizationVariables.isFirstParamOrg = true;
				ItselfOrganizationVariables.sameParametrsInParamOrg = true;
				//new
				ItselfOrganizationVariables.getInstance()
					.setParametersModelOrganizer(ParametersOrganizer.GLOBAL_PARAMETERS_ORGANIZER);
			}
        });
		RadioMenuItem rmItemAllDifferent = new RadioMenuItem("�������� �� ���� ���������");
		rmItemAllDifferent.setToggleGroup(tGroupParameterValue);
		rmItemAllDifferent.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
			@Override
			public void handle(javafx.event.ActionEvent arg0) {
				//TODO try to clean this up later
				//ItselfOrganizationVariables.isFirstParamOrg = true;
				ItselfOrganizationVariables.sameParametrsInParamOrg = false;
				//new
				ItselfOrganizationVariables.getInstance()
					.setParametersModelOrganizer(ParametersOrganizer.LOCAL_PARAMETERS_ORGANIZER);
			}
        });
		menuParameterValue.getItems().addAll(rmItemAllSame, rmItemAllDifferent);
		if( ItselfOrganizationVariables.sameParametrsInParamOrg )
		{
			rmItemAllSame.setSelected( true );
		}
		else
		{
			rmItemAllDifferent.setSelected( true );
		}
		
		
		menuParametricSO.getItems().addAll(menuItemStepCount, menuOptimizationMethod, menuStrategy, menuParameterValue);
		menuBarSettings.getMenus().add(menuParametricSO);
        //=======================================================================
        
        vBoxSettings.getChildren().addAll(lblIsSelfOrg, lblRequirements, menuBarSettings, lblAddition);
        lblIsSelfOrg.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				try {
					(new ItselfOrgAllowSettingsFX()).start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
        });
        lblRequirements.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				try {
					(new ModelRequirementsFX()).start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
        });
        lblAddition.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				try {
					(new AdditionalSettingsFX()).start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
        });
        
        
        hBoxSubMenu.getChildren().addAll(vBoxModeling, vBoxLibraries, vBoxSettings);
       
/*        TabPane tabPane = new TabPane();
        tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);
        BorderPane mainPane = new BorderPane();
       
        //Create Tabs
        Tab tabA = new Tab();
        tabA.setText("  ����  ");
        HBox hBoxFile = new HBox();
        hBoxFile.setPadding(new Insets(5,10,5,10));
        hBoxFile.setSpacing(5);
        hBoxFile.getChildren().add(GetCustomMenuItem("Ctrl+N", "Resource/newPage.png", "�����"));
        hBoxFile.getChildren().add(GetCustomMenuItem("Ctrl+O", "Resource/openPage.png", "�������"));
        hBoxFile.getChildren().add(GetCustomMenuItem("Ctrl+S", "Resource/savePage.png", "���������"));
        
        Node nodeNewpage = GetCustomMenuItem("Ctrl+E", "Resource/Exit.png", "�����");
        nodeNewpage.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				fxPanel.setVisible(false);
				//System.exit(0);
			}
        });
        hBoxFile.getChildren().add(nodeNewpage);
        tabA.setContent(hBoxFile);
        tabPane.getTabs().add(tabA);
       
        Tab tabB = new Tab();
        tabB.setText("  �������������  ");
        //Add something in Tab
        StackPane tabB_stack = new StackPane();
        tabB_stack.setAlignment(Pos.CENTER);
        tabB_stack.getChildren().add(new Label("Label@Tab B"));
        tabB.setContent(tabB_stack);
        tabPane.getTabs().add(tabB);
       
        Tab tabC = new Tab();
        tabC.setText("  ����������  ");
        //Add something in Tab
        VBox tabC_vBox = new VBox();
        tabC_vBox.getChildren().addAll(
                new Button("Button 1@Tab C"),
                new Button("Button 2@Tab C"),
                new Button("Button 3@Tab C"),
                new Button("Button 4@Tab C"));
        tabC.setContent(tabC_vBox);
        tabPane.getTabs().add(tabC);
       
        Tab tabSettings = new Tab();
        tabSettings.setText("  ���������  ");
        tabSettings.setContent(tabC_vBox);
        tabPane.getTabs().add(tabSettings);
        
        
        Tab tabHelp = new Tab();
        tabHelp.setText("  ������  ");
        tabHelp.setContent(tabC_vBox);
        tabPane.getTabs().add(tabHelp);
        
        mainPane.setCenter(tabPane);
        mainPane.prefHeightProperty().bind(scene.heightProperty());
        mainPane.prefWidthProperty().bind(scene.widthProperty());
       */
        
        vBoxLibraries.setPadding(new Insets(0, 10, 0, 10));
        vBoxLibraries.setSpacing(3);
        vBoxModeling.setPadding(new Insets(0, 10, 0, 10));
        vBoxModeling.getStyleClass().add("custom-menu-separator");
        vBoxModeling.setAlignment(Pos.CENTER);
        vBoxSettings.setPadding(new Insets(0, 10, 0, 10));
        menuBarSettings.getStyleClass().add("custom-menu-item");
        root.getChildren().add(hBoxSubMenu);
        scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
        scene.setFill(new LinearGradient(0,0,0,0.7,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(230,230,230)), 
				new Stop(1.0, Color.rgb(200,200,200))));
        fxPanel.setScene(scene);
	}
	
	public void CreateTreeView(JFXPanel fxPanel)
	{
		final HBox hMainBox = new HBox();
		
		final VBox vBox = new VBox();
		final HBox hBox = new HBox();
		hBox.setAlignment(Pos.CENTER);
		hBox.setSpacing(15);
		hBox.setPadding(new Insets(10, 0, 10, 0));
		btnAddEO = new Label(null, new ImageView(new Image(getClass().getResourceAsStream("Resource/AddRoot.png"))));
		btnAddEP = new Label(null, new ImageView(new Image(getClass().getResourceAsStream("Resource/AddList.png"))));
		btnRemove = new Label(null, new ImageView(new Image(getClass().getResourceAsStream("Resource/Delete.png"))));
		btnAddEO.getStyleClass().add("tree-view-menu");
		btnAddEO.setTooltip(new Tooltip("�������� ������/����������"));
		btnAddEP.getStyleClass().add("tree-view-menu");
		btnAddEP.setTooltip(new Tooltip("�������� ������������ ������"));
		btnRemove.getStyleClass().add("tree-view-menu");
		btnRemove.setTooltip(new Tooltip("������� �������"));
		btnAddEO.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			@Override
			public void handle(MouseEvent arg0) {
				Stage stage = new Stage();
				try {
					(new AddObjectFX()).start(stage);
				} catch (Exception e) {
					e.printStackTrace();
				}
				stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
					public void handle(javafx.stage.WindowEvent event) {
						actionWindowListener();
						btnAddEP.setDisable(false);
						btnRemove.setDisable(false);
					}
        		});
			}
		});
		
		btnAddEP.setDisable(true);
		btnAddEP.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			@Override
			public void handle(MouseEvent arg0) {
				Stage stage = new Stage();
				try {
					(new AddElementaryObjectFX()).start(stage);
				} catch (Exception e) {
					e.printStackTrace();
				}
				stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
					public void handle(javafx.stage.WindowEvent event) {
						actionWindowListener();
					}
        		});
			}
		});
		
		
		btnRemove.setDisable(true);
		btnRemove.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			@Override
			public void handle(MouseEvent arg0) {
				new RemoveSelection(true);
			}
		});

		
		hBox.getChildren().addAll(btnAddEO, btnAddEP, btnRemove);
		vBox.getChildren().addAll(hBox);

		hMainBox.getChildren().addAll(vBox);

		hMainBox.setAlignment(Pos.CENTER);
		final Scene scene = new Scene(hMainBox);
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
		scene.setFill(new LinearGradient(0,0,0,0.7,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(150,150,150)), 
				new Stop(1.0, Color.rgb(120,120,120))));
        fxPanel.setScene(scene);
	}
	
	public void CreateTableView(JFXPanel fxPanel)
	{
		VBox vBox = new VBox();
		Scene scene = new Scene(vBox);
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
        fxPanel.setScene(scene);
	}
	
	public Main()
	{
		super("�������� ������������������ ������");
		
		fxPanelMenu = new JFXPanel();
		fxPanelTreeView = new JFXPanel();
		fxPanelTableView = new JFXPanel();
		Platform.runLater(new Runnable() {
            public void run() {
            	CreateMenu(fxPanelMenu);
            	CreateTreeView(fxPanelTreeView);
            	CreateTableView(fxPanelTableView);
            }
         });
		setLayout(new BorderLayout());
		
		add(fxPanelMenu, BorderLayout.NORTH);
		JPanel panelForTreeView = new JPanel();
		panelForTreeView.setLayout(new BoxLayout(panelForTreeView,BoxLayout.Y_AXIS));
		fxPanelTreeView.setPreferredSize(new Dimension(200, 50));
		fxPanelTreeView.setMaximumSize(new Dimension(200, 50));
		fxPanelTreeView.setMinimumSize(new Dimension(200, 50));
		panelForTreeView.add(fxPanelTreeView);
	    add(panelForTreeView, BorderLayout.WEST);
		
		
		//add(fxPanelTableView, BorderLayout.SOUTH);
//		float k = Float.MAX_VALUE;
//		float min= Float.MIN_VALUE;
//		float my=-10000000;
//		float my=-2147480000;
//		JOptionPane.showMessageDialog(null,k+"\n"+min+"\n"+my);
		//initialization
		structTreeElements = new Vector< Vector<TreeElement> >();
		elementsNamesList = new Vector< String >();
		/*typeLoader = */new TypeLoader();
		/*parametrsLoader = */new ParametersLoader();
		new FunctionsLoader();
		//TODO FOR DEAR GOD, FOR WHAT? new ItselfOrganizationVariables();
		
		parser = new Parser();
		
		libraryHandler = new LibraryHandler();
		
		//for deleting element by "Delete" button
		 KeyListener treeNodeListenerKey = new PopupTreeNodeListenerKey();
		
//Menu begin
		
		MenuHandler menuHandler = new MenuHandler();
		
		//file
		JMenu fileMenu = new JMenu("����");
		newMenuItem = new JMenuItem();
		newMenuItem.setText("�����");
		fileMenu.add(newMenuItem);
		newMenuItem.addActionListener(menuHandler);
		newMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, KeyEvent.CTRL_MASK));
		openMenuItem = new JMenuItem();
		openMenuItem.setText("�������");
		fileMenu.add(openMenuItem);
		openMenuItem.addActionListener(menuHandler);
		openMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_MASK));
		JMenuItem saveMenuItem = new JMenuItem();
		saveMenuItem.setText("���������");
		fileMenu.add(saveMenuItem);
		saveMenuItem.addActionListener(menuHandler);
		saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_MASK));
		JMenuItem exitMenuItem = new JMenuItem();
		fileMenu.add(exitMenuItem);
		exitMenuItem.setText("�����");
		ExitItem exitItem = new ExitItem();
		exitMenuItem.addActionListener(exitItem);
		exitMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_MASK));
		
		//simulation
		JMenu simulationMenu = new JMenu();
		simulationMenu.setText("�������������");
		simulationBeginMenuItem = new JMenuItem();
		simulationBeginMenuItem.setText("������ �������������");
		simulationMenu.add(simulationBeginMenuItem);
		simulationBeginMenuItem.addActionListener(menuHandler);
		simulationBeginMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, KeyEvent.CTRL_MASK));
		
		//customise
		JMenu customiseMenu = new JMenu();
		customiseMenu.setText("���������");
		JMenuItem itselfOrgAllowMenuItem = new JMenuItem();
		itselfOrgAllowMenuItem.setText("���������� ���������������");
		customiseMenu.add(itselfOrgAllowMenuItem);
		itselfOrgAllowMenuItem.addActionListener(menuHandler);
//		itselfOrgAllowMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_MASK));
		JMenuItem modelReguirementsMenuItem = new JMenuItem();
		modelReguirementsMenuItem.setText("���������� � ������");
		customiseMenu.add(modelReguirementsMenuItem);
		modelReguirementsMenuItem.addActionListener(menuHandler);
//		customiseMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, KeyEvent.CTRL_MASK));
		JMenuItem customiseMenuItem = new JMenuItem();
		customiseMenuItem.setText("��������� �������");
//!!!		customiseMenu.add(customiseMenuItem);    !!!!!!!!!!!! deleted by Ruslan and Julia due to request from Andrew Kochetkov
		customiseMenuItem.addActionListener(menuHandler);
//		customiseMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, KeyEvent.CTRL_MASK));
		JMenuItem paramSamoorgazationMenuItem = new JMenu();
		paramSamoorgazationMenuItem.setText("��������������� ���������������");
		customiseMenu.add(paramSamoorgazationMenuItem);
		paramSamoorgazationMenuItem.addActionListener(menuHandler);
		/*
		JMenuItem customiseStepCountMenuItem = new JMenuItem();
		customiseStepCountMenuItem.setText("����� ���������� ����");
		paramSamoorgazationMenuItem.add(customiseStepCountMenuItem);
		customiseStepCountMenuItem.addActionListener(menuHandler);
		*/
		JMenuItem customiseStepNumberMenuItem = new JMenuItem();
		customiseStepNumberMenuItem.setText("����� ����� �����");
		paramSamoorgazationMenuItem.add(customiseStepNumberMenuItem);
		customiseStepNumberMenuItem.addActionListener(menuHandler);
		
		JMenuItem paramOptimizationChooseMenuItem = new JMenu();
		paramOptimizationChooseMenuItem.setText("����� ������ �����������");
		paramSamoorgazationMenuItem.add(paramOptimizationChooseMenuItem);
		paramOptimizationChooseMenuItem.addActionListener(menuHandler);
		
		ButtonGroup groupStrategy = new ButtonGroup();
		JMenuItem paramOptimizationCZ1MenuItem = new JRadioButtonMenuItem();
		paramOptimizationCZ1MenuItem.setText("����������� �������������� �����");
		paramOptimizationChooseMenuItem.add(paramOptimizationCZ1MenuItem);
		paramOptimizationCZ1MenuItem.addActionListener(menuHandler);
		paramOptimizationCZ1MenuItem.setSelected(true);
		groupStrategy.add(paramOptimizationCZ1MenuItem);
		JMenuItem paramOptimizationRELEXMenuItem = new JRadioButtonMenuItem();
		paramOptimizationRELEXMenuItem.setText("����� ���������������� ����������");
		paramOptimizationChooseMenuItem.add(paramOptimizationRELEXMenuItem);
		paramOptimizationRELEXMenuItem.addActionListener(menuHandler);
		groupStrategy.add(paramOptimizationRELEXMenuItem);
		
		JMenuItem paramOptimizationMenuItem = new JMenu();
		paramOptimizationMenuItem.setText("����� ���������");
		paramSamoorgazationMenuItem.add(paramOptimizationMenuItem);
		paramOptimizationMenuItem.addActionListener(menuHandler);
		
		ButtonGroup group = new ButtonGroup();
		JMenuItem paramOptimization1MenuItem = new JRadioButtonMenuItem();
		paramOptimization1MenuItem.setText("���������  ����������� �����");
		paramOptimizationMenuItem.add(paramOptimization1MenuItem);
		paramOptimization1MenuItem.addActionListener(menuHandler);
		group.add(paramOptimization1MenuItem);
		JMenuItem paramOptimization2MenuItem = new JRadioButtonMenuItem();
		paramOptimization2MenuItem.setText("��������� ��������� ���������");
		paramOptimizationMenuItem.add(paramOptimization2MenuItem);
		paramOptimization2MenuItem.addActionListener(menuHandler);
		group.add(paramOptimization2MenuItem);	
		JMenuItem paramOptimization3MenuItem = new JRadioButtonMenuItem();
		paramOptimization3MenuItem.setText("��������� ��������� ������ ����������� ���������");
		paramOptimizationMenuItem.add(paramOptimization3MenuItem);
		paramOptimization3MenuItem.addActionListener(menuHandler);
		group.add(paramOptimization3MenuItem);	
		JMenuItem paramOptimization4MenuItem = new JRadioButtonMenuItem();
		paramOptimization4MenuItem.setText("��������� ���������� ������� �����");
		paramOptimizationMenuItem.add(paramOptimization4MenuItem);
		paramOptimization4MenuItem.addActionListener(menuHandler);
		paramOptimization4MenuItem.setSelected(true);
		group.add(paramOptimization4MenuItem);	
		
		JMenuItem paramOptimizationSameMenuItem = new JMenu();
		paramOptimizationSameMenuItem.setText("�������� ����������");
		paramSamoorgazationMenuItem.add(paramOptimizationSameMenuItem);
		paramOptimizationSameMenuItem.addActionListener(menuHandler);
		ButtonGroup sameMenuGroup = new ButtonGroup();
		paramSameMenuItem = new JRadioButtonMenuItem();
		paramSameMenuItem.setText("��������� �� ���� ���������");
//		paramSameMenuItem.setSelected(true);
		paramOptimizationSameMenuItem.add(paramSameMenuItem);
		paramSameMenuItem.addActionListener(menuHandler);
		sameMenuGroup.add(paramSameMenuItem);
		paramNotSameMenuItem = new JRadioButtonMenuItem();
		paramNotSameMenuItem.setText("�������� � ������ ���������");
		
		if( ItselfOrganizationVariables.sameParametrsInParamOrg )
		{
			paramSameMenuItem.setSelected( true );
		}
		else
		{
			paramNotSameMenuItem.setSelected( true );
		}
		
		paramOptimizationSameMenuItem.add(paramNotSameMenuItem);
		paramNotSameMenuItem.addActionListener(menuHandler);
		sameMenuGroup.add(paramNotSameMenuItem);	
		
		JMenuItem dopSettingsMenuItem = new JMenuItem();
		dopSettingsMenuItem.setText("�������������� ���������");
		customiseMenu.add(dopSettingsMenuItem);
		dopSettingsMenuItem.addActionListener(menuHandler);
		
		//libraries
		/*JMenu */libraryMenu = new JMenu();
		libraryMenu.setText("����������");
		final JMenuItem classcorrMenuItem = new JMenuItem();
		classcorrMenuItem.setText("���������� ����� ������������ ��������");
		libraryMenu.add(classcorrMenuItem);
		classcorrMenuItem.addActionListener(menuHandler);
		classcorrMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, KeyEvent.SHIFT_MASK));
		final JMenuItem classposrMenuItem = new JMenuItem();
		classposrMenuItem.setText("���������� ����� �����������");
		libraryMenu.add(classposrMenuItem);
		classposrMenuItem.addActionListener(menuHandler);
		classposrMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, KeyEvent.SHIFT_MASK));
		
		final JMenuItem parametrslibraryMenuItem = new JMenuItem();
		parametrslibraryMenuItem.setText("���������� ����������");
		libraryMenu.add(parametrslibraryMenuItem);
		parametrslibraryMenuItem.addActionListener(menuHandler);
		parametrslibraryMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.SHIFT_MASK));
		final JMenuItem functionslibraryMenuItem = new JMenuItem();
		functionslibraryMenuItem.setText("���������� �������");
		libraryMenu.add(functionslibraryMenuItem);
		functionslibraryMenuItem.addActionListener(menuHandler);
		functionslibraryMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.SHIFT_MASK));
	
		//help
		JMenu helpMenu = new JMenu();
		helpMenu.setText("������");
		JMenuItem aboutMenuItem = new JMenuItem();
		aboutMenuItem.setText("� ���������");
		helpMenu.add(aboutMenuItem);
		aboutMenuItem.addActionListener(menuHandler);
		aboutMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, KeyEvent.CTRL_MASK));
		JMenuItem functionsHelpMenuItem = new JMenuItem();
		functionsHelpMenuItem.setText("���� �������");
		helpMenu.add(functionsHelpMenuItem);
		functionsHelpMenuItem.addActionListener(menuHandler);

		//adding menu to frame
		JMenuBar menuBar = new JMenuBar();
		//setJMenuBar(menuBar);	
		menuBar.add(fileMenu);
		menuBar.add(simulationMenu);
		menuBar.add(libraryMenu);
		menuBar.add(customiseMenu);
		menuBar.add(helpMenu);
//Menu end
		
		

		JPanel panelTree = new JPanel(new BorderLayout());
		
// add, addAO, del Button Panel begin
		JPanel panelButtons = new JPanel();
		panelButtons.setBorder(new BevelBorder(BevelBorder.RAISED));
		panelButtons.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		panelButtons.setLayout(new GridLayout(1,3, 15, 5));
		panelButtons.setPreferredSize(new Dimension(50, 27));
		panelTree.add(panelButtons,BorderLayout.NORTH);
		
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setPreferredSize(new Dimension(200, 100));
		//panelTree.add(scrollPane,BorderLayout.CENTER);
		
		
//tree at left panel begin
		dmtRoot = new DefaultMutableTreeNode("��������� �������");
		tree = new JTree(dmtRoot);
		tree.setSelectionRow(0);
		tree.setEditable(true);
		tree.addKeyListener(treeNodeListenerKey );//for deleting element by "Delete" button
		scrollPane.setViewportView(tree);
		scrollPane.setBorder(null);
		tree.setBackground(java.awt.Color.GRAY);
//		DefaultTreeCellRenderer renderer = (DefaultTreeCellRenderer) tree.getCellRenderer();
//        renderer.setLeafIcon(new ImageIcon(getClass().getResource("Resource/Root.png")));
//        renderer.setClosedIcon(new ImageIcon(getClass().getResource("Resource/Root.png")));
//        renderer.setOpenIcon(new ImageIcon(getClass().getResource("Resource/Root.png")));
		
        
		
		panelForTreeView.add(scrollPane);
//tree at left panel end	
		
		JScrollPane scrollPane_1 = new JScrollPane();//ScrollPane for Graphic Panel
		scrollPane_1.setPreferredSize(new Dimension(200, 100));
		
//properties load begin
		Properties props=new Properties();
		try {
			props.load(new FileInputStream("properties.txt"));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
//properties load end	
				
		JPanel pane_6 = new JPanel(new BorderLayout());
		lbEmulatedSystem = new JLabel("�������� ���������� ��������");		
		lbEmulatedSystem.setHorizontalAlignment(SwingConstants.CENTER);
		JScrollPane scrollPane_6 = new JScrollPane();
		scrollPane_6.setPreferredSize(new Dimension(200, 200));
		
		tableSystem = new ParametersTable(maxCountOfParams,3);
		tableSystem.getModel().addTableModelListener(tableSystem);
			
		scrollPane_6.setViewportView(tableSystem);
		
		typeAndFuncNameInform = new JTextArea();
		typeAndFuncNameInform.setEditable(false);
		pane_6.add(typeAndFuncNameInform,BorderLayout.NORTH);
		pane_6.add(scrollPane_6,BorderLayout.CENTER);	

		cbStartElement = new JComboBox();
		cbEndElement = new JComboBox();
		panelGraphic = new GraphicPanel(cbStartElement, cbEndElement, tree);
		
		
		JSplitPane splitPane_1 = new JSplitPane();		
		splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);		
		splitPane_1.setBottomComponent(pane_6);	
		scrollPane_1.setViewportView(panelGraphic);
		splitPane_1.setTopComponent(scrollPane_1);
		splitPane_1.setDividerLocation(357);
		

		JPanel panelForTableView = new JPanel();
		panelForTableView.setLayout(new BoxLayout(panelForTableView,BoxLayout.Y_AXIS));
		panelForTableView.add(panelGraphic);
		pane_6.setPreferredSize(new Dimension(2200, 150));
		pane_6.setMaximumSize(new Dimension(2200, 150));
		pane_6.setMinimumSize(new Dimension(2200, 150));
		panelForTableView.add(pane_6);
		add(panelForTableView, BorderLayout.CENTER);
		
		//tabbedPane.addTab("���������� � �������", null, splitPane_1, null);
		
//		getContentPane().setEnabled(false);
//		getContentPane().setFocusable(false);
		setSize(1000, 700);
		setVisible(true);
		
		tree.getModel().addTreeModelListener(new javax.swing.event.TreeModelListener() {

			@Override
			public void treeNodesChanged(TreeModelEvent e) {
				DefaultMutableTreeNode DMTN;
		        DMTN = (DefaultMutableTreeNode)
		                 (e.getTreePath().getLastPathComponent());
		        try {
		            int index = e.getChildIndices()[0];
		            DMTN = (DefaultMutableTreeNode)
		                   (DMTN.getChildAt(index));
		        } catch (NullPointerException exc) {}
		        TreeElement myelem = TreeOperations.findElementInStruct(DMTN);
		        String newName = DMTN.getUserObject().toString();
		        if( myelem != null){
		        	myelem.cell.setUserObject(newName);
		        	myelem.elementName = newName;
		        	//((DefaultMutableTreeNode)tree.getSelectionPath().getLastPathComponent()).setUserObject(newName);
		        	panelGraphic.repaint();
		        }
			}

			@Override
			public void treeNodesInserted(TreeModelEvent e) {}

			@Override
			public void treeNodesRemoved(TreeModelEvent e) {}

			@Override
			public void treeStructureChanged(TreeModelEvent e) {}
		});

	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater( new Runnable(){ 
	    	  public void run() {}});
		mainFrame = new Main();
		setToCenter(mainFrame);
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void setToCenter(JFrame frame) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		Dimension size = frame.getSize();
		screenSize.height = screenSize.height / 2;
		screenSize.width = screenSize.width / 2;
		size.height = size.height / 2;
		size.width = size.width / 2;
		int y = screenSize.height - size.height;
		int x = screenSize.width - size.width;
		frame.setLocation(x, y);
	}

	// begin(not restart after stop from same time) of simulation
	public static void simulationBegin() {
		simulation_is_on = true;
		mainFrame.setEnabled(false);
		simulationBeginMenuItem.setEnabled(false);
		newMenuItem.setEnabled(false);
		openMenuItem.setEnabled(false);
	}

	// END(not stop) simulation
	public static void simulationEnd() {
		simulation_is_on = false;
		mainFrame.setEnabled(true);
		simulationBeginMenuItem.setEnabled(true);
		newMenuItem.setEnabled(true);
		openMenuItem.setEnabled(true);
	}

	private class LibraryHandler implements WindowListener {
		public void windowActivated(WindowEvent e) {

		}

		public void windowClosed(WindowEvent e) {
			// anyLibraryIsOpened = false;
			libraryMenu.setEnabled(true);
			allow_simulation = true;
			if (!simulation_is_on) {
				simulationBeginMenuItem.setEnabled(true);
			}
		}

		public void windowClosing(WindowEvent e) {
			// anyLibraryIsOpened = false;
			libraryMenu.setEnabled(true);
			allow_simulation = true;
			if (!simulation_is_on) {
				simulationBeginMenuItem.setEnabled(true);
			}
		}

		public void windowDeactivated(WindowEvent e) {

		}

		public void windowDeiconified(WindowEvent e) {

		}

		public void windowIconified(WindowEvent e) {

		}

		public void windowOpened(WindowEvent e) {
			// anyLibraryIsOpened = true;
			allow_simulation = false;
			libraryMenu.setEnabled(false);
			simulationBeginMenuItem.setEnabled(false);
		}

	}

	private class ExitItem implements ActionListener {
		public void actionPerformed(ActionEvent event) {
			// ���������� ������ �� ��������
			// to do
			TreePath treePath = tree.getPathForRow(1);
			if (treePath != null) {
				int dialog = JOptionPane
						.showConfirmDialog(
								null,
								"�� ������ ��������� ������� ������\n ����� ������� �� ���������?",
								"�����", JOptionPane.YES_NO_CANCEL_OPTION);

				if (dialog == 0 || dialog == 1) {
					if (dialog == 0) {
						saveModel();
					}
				}
				if (dialog == 2 || dialog == -1) {
					return;
				}
			}
			dispose();
			System.exit(0);
		}
	}

	// to do
	private boolean saveModel() {
		if (count < 1) {
			FXDialog.showMessageDialog("��� ������ ��� ����������", "����������", Message.WARNING);
		} else {
			File createdFile = null;
			boolean correctFileCoosed = false;
			while (!correctFileCoosed) {
				FileChooser fileChooser = new FileChooser();
				FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
	            fileChooser.getExtensionFilters().add(extFilter);
	            createdFile = fileChooser.showSaveDialog(null);

				if (createdFile != null) {
					
					String fileName = createdFile.getName();
					correctFileCoosed = true;
					for (int i = 0; i < fileName.length(); i++) {
						if ((!(Parser.onlyDigitCollection.contains(fileName
								.charAt(i))))
								&& (!(Parser.literalCollection
										.contains(fileName.charAt(i))))) {
							if ((i == (fileName.length() - 4)) && (fileName.charAt(i) == '.'))
							{
								if ((fileName.substring(fileName.length() - 3,
										fileName.length())).toLowerCase()
										.equals("xml")) {

								} else {
									FXDialog.showMessageDialog("��������, �� ��� ����� ��������� ������ "
											+ "\n ����� ����������� ��������, ����� � ���� ������� �������������", "����������", Message.WARNING);
									i = fileName.length();
									correctFileCoosed = false;
								}
							} else {
								FXDialog.showMessageDialog("��������, �� ��� ����� ��������� ������ "
										+ "\n ����� ����������� ��������, ����� � ���� ������� �������������", "����������", Message.WARNING);
								i = fileName.length();
								correctFileCoosed = false;
							}
						}
					}
				} else {
					return false;
				}
			}

			try {
				// Create a Document
				DocumentBuilderFactory dbf = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder docBuilder = dbf.newDocumentBuilder();
				Document doc = docBuilder.newDocument();

				// doc.setXmlVersion("1.0");
				// Creating the XML tree begin
				// create the root element and add it to the document
				Element root = doc.createElement("model");
				doc.appendChild(root);

				// create version node
				Element node = doc.createElement("version");
				node.setAttribute("number", version);
				root.appendChild(node);

				Element levelNode;
				Element elemNode;
				TreeElement elementInStruct;
				if (Main.structTreeElements.size() > 0) {
					for (int level = 0; level < Main.structTreeElements.size(); level++) {
						if (Main.structTreeElements.elementAt(level).size() > 0) {
							levelNode = doc.createElement("level");
							levelNode.setAttribute("number", String
									.valueOf(level));
							for (int elemIndex = 0; elemIndex < Main.structTreeElements
									.elementAt(level).size(); elemIndex++) {
								elementInStruct = (TreeElement) Main.structTreeElements
										.elementAt(level).elementAt(elemIndex);
								elemNode = createNodeFromTreeElem(
										elementInStruct, doc/*
															 * .createElement("elem"
															 * )
															 */);
								levelNode.appendChild(elemNode);
							}
							root.appendChild(levelNode);
						}
					}
				}

				// Creating the XML tree end

				// save to file
				String outputURL = createdFile.getAbsolutePath();
				if (!outputURL.contains(".")) {
					outputURL = outputURL + ".xml";
				}

				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new FileOutputStream(
						outputURL));

				TransformerFactory transFactory = TransformerFactory
						.newInstance();
				Transformer transformer = transFactory.newTransformer();

				transformer.transform(source, result);

			} catch (Exception e) {
				FXDialog.showMessageDialog("������ ��� ���������� ������", "����������", Message.WARNING);
				e.printStackTrace();
				return false;
			}
			FXDialog.showMessageDialog("������ ���������", "����������", Message.INFORMATION);
			// }
		}
		return true;
	}

	private boolean openModel() {
		// save before open new model
		TreePath treePath = tree.getPathForRow(1);
		if (treePath != null) {
			boolean dialog = FXDialog.showConfirmDialog("C�������� ������� ������ ����� ��������� �������?", "����������", ConfirmationType.YES_NO_OPTION);

			if (dialog) {
				saveModel();
			}
		}

		NodeList nodeList = null;
		Element root = null;
		Element node = null;

		boolean correctFileCoosed = false;

		while (!correctFileCoosed) {
			// open model
			FileChooser fileChooser = new FileChooser();
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XML files (*.xml)", "*.xml");
            fileChooser.getExtensionFilters().add(extFilter);
            File openedFile = fileChooser.showOpenDialog(null);

			if (openedFile != null) {
				//File openedFile = fc.getSelectedFile();
				// create DOM document
				Document doc = null;
				try {
					DocumentBuilderFactory dbf = DocumentBuilderFactory
							.newInstance();
					DocumentBuilder db = dbf.newDocumentBuilder();
					doc = db.parse(openedFile);
				} catch (java.io.IOException e) {
					System.out.println("Can't find the file");
				} catch (Exception e) {
					System.out.print("Problem parsing the file");
				}

				root = doc.getDocumentElement();

				// checking file version
				nodeList = root.getElementsByTagName("version");
				if (nodeList.getLength() > 0) {
					node = (Element) nodeList.item(0);
					String fileVersion = node.getAttribute("number");
					// to_do
					if (fileVersion.equals(version)) {
						correctFileCoosed = true;
					} else {
						FXDialog.showMessageDialog("������ ���������� ����� ("
								+ fileVersion + ") �� ��������� � ������� ������� ��������� ("
								+ version + ")", "������ �����", Message.ERROR);
					}
				} else {
					FXDialog.showMessageDialog("� ���������, ��������� ���� ����"
							+ " �� �������� ����������� ��������� ����������"
							+ "\n ����������, ��������� � ������������ ������ �����", "������ �����", Message.ERROR);
				}

			} else {
				return false;
			}
		}

		// Clean project
		cleanProject();

		NodeList elementsNodeList = null;
		Element elementNode = null;
		TreeElement openedElement = null;
		TreeElement parentElement = null;
		// Building structure begin
		try {
			nodeList = root.getElementsByTagName("level");
			// checking: did opened model contains any levels
			if (nodeList.getLength() > 0) {
				// go through all levels in model
				for (int level = 0; level < nodeList.getLength(); level++) {
					node = (Element) nodeList.item(level);// level node
					// checking: did levels go in right order (0,1,2,3,4,5...)
					if (level == Integer.parseInt(node.getAttribute("number"))) {
						elementsNodeList = node.getElementsByTagName("elem");
						// checking: did level contains any elements
						if (elementsNodeList.getLength() > 0) {

							// variant: structure building in order as it was
							// created
							// go through all elements in current level
							for (int nodeNumber = 0; nodeNumber < elementsNodeList
									.getLength(); nodeNumber++) {
								elementNode = (Element) elementsNodeList
										.item(nodeNumber);
								openedElement = createTreeElemFromNode(elementNode);
								if (!TreeOperations
										.typeAndFunctionOfElementIsCorrect(openedElement)) {
									FXDialog.showMessageDialog("��������, �� ������ ������ �� ����� ���� ������� \n "
											+ "��� ��� � ��� ������������ �������� � ����������������� ������� ������� ��������� ������", "������ �����", Message.ERROR);
									if (level != 0)// if we already add some
									// elements
									{
										cleanProject();
									}
									return false;
								}
								if (level == 0)// if this is root element
								{
									new AddParametrizedObject(openedElement);
								} else// if this is NO root element
								{
									// find parent element
									parentElement = TreeOperations
											.findParentInStructByID(
													openedElement.ParentID,
													openedElement.Level);
									// set selection to the parent element
									Main.tree.setSelectionPath(new TreePath(
											parentElement.element.getPath()));
									Main.panelGraphic.getGraph()
											.setSelectionCell(
													parentElement.cell);
									new AddParametrizedObject(openedElement);
								}
							}

						} else {
							FXDialog.showMessageDialog("������ � ����������� ������ (������ �������)", "������ �����", Message.ERROR);
							return false;
						}
					} else {
						FXDialog.showMessageDialog("��������� ���� ��� ������ ������ \n "
								+ "����������, ���������� � ������������", "������ �����", Message.ERROR);
						return false;
					}
				}
			} else {
				FXDialog.showMessageDialog("����������� ������ �����", "������ �����", Message.ERROR);
				return false;
			}
		} catch (Exception e) {
			FXDialog.showMessageDialog("������ ��� �������� ������", "������ �����", Message.ERROR);
			e.printStackTrace();
			return false;
		}
		// Building structure end
		if (count > 1) {
			Main.elemID++;
		}

		// set selection to the root
		tree.setSelectionRow(1);
		Main.tree.getSelectionPath().getLastPathComponent();
		DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) Main.tree
				.getSelectionPath().getLastPathComponent();
		TreeElement tmpSelectedElement = TreeOperations
				.findElementInStruct(selectedNode);
		Main.panelGraphic.getGraph().setSelectionCell(tmpSelectedElement.cell);
		Main.panelGraphic.refreshTable();
		if (tmpSelectedElement != null) {
			changeButtonsEnable(tmpSelectedElement);
		}

		return true;

	}

	private boolean newModel() {
		// save before clear model
		TreePath treePath = tree.getPathForRow(1);
		if (treePath != null) {
			boolean dialog = FXDialog.showConfirmDialog("C�������� ������� ������ ����� ��������� ������?", "����������", ConfirmationType.YES_NO_OPTION);

			if (dialog) {
				saveModel();
			}
		}

		cleanProject();

		return true;
	}

	// to do
	private class MenuHandler implements ActionListener {
		public void actionPerformed(ActionEvent event) {

		}
	}

	private class PopupTreeNodeListenerKey extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			if (e.getKeyCode() == KeyEvent.VK_DELETE) {
				if (!btnRemove.isDisabled()) {
					new RemoveSelection(true);
				}
			}
		}
	}

	// Object remove
	public static void Remove(DefaultGraphCell cell) {

		TreeElement treeElem = TreeOperations.findElementInStructByCell(cell);

		if (treeElem != null) {
			if (elementsNamesList.contains(treeElem.elementName)) {
				elementsNamesList.remove(treeElem.elementName);
			}
			// deleting elements children
			ArrayList<TreeElement> childrenList = new ArrayList<TreeElement>();
			if ((treeElem.type == 2) && (treeElem.iChildrenCount > 0)) {
				childrenList = TreeOperations
						.findAllChildrenInStructByIDAndLevel(treeElem.ID,
								treeElem.Level);
			}
			if (childrenList.size() > 0) {
				for (int i = 0; i < childrenList.size(); i++) {
					Remove(childrenList.get(i).cell);
				}
			}

			if (simulation_is_on) {
				// if element was displayed in simulation did not
				// display it more
				// if( simulationCount.visibilityOfElementsMap.size() > 0 )
				// {
				// if( simulationCount.visibilityOfElementsMap.containsKey(
				// treeElem.elementName ) )
				// {
				// simulationCount.visibilityOfElementsMap.put(
				// treeElem.elementName, false );
				// }
				// }
			}
			structTreeElements.get(treeElem.Level).remove(treeElem);
			panelGraphic.removeCellGraph(treeElem.cell, treeElem.cellLink);
			count--;
		}
	}

	// Object remove
	public static void listRemove(TreeElement removedList) {

		if (removedList != null) {
			if (elementsNamesList.contains(removedList.elementName)) {
				elementsNamesList.remove(removedList.elementName);
			}

			// if(simulation_is_on)
			// {
			// if element was displayed in simulation did not
			// display it more
			// if( simulationCount.visibilityOfElementsMap.size() > 0 )
			// {
			// if( simulationCount.visibilityOfElementsMap.containsKey(
			// treeElem.elementName ) )
			// {
			// simulationCount.visibilityOfElementsMap.put(
			// treeElem.elementName, false );
			// }
			// }
			// }
			structTreeElements.get(removedList.Level).remove(removedList);
			panelGraphic
					.removeCellGraph(removedList.cell, removedList.cellLink);
			count--;
		}
	}

	class FileXMLFilter extends javax.swing.filechooser.FileFilter {
		public boolean accept(File file) {
			if (!file.isDirectory()) {
				String filename = file.getName();
				return filename.endsWith(".xml");
			} else {
				return true;
			}
		}

		public String getDescription() {
			return "*.xml";
		}
	}

	private void cleanProject() {
		// set selection to the root and delete it
		tree.setSelectionRow(1);
		new RemoveSelection(false);
		
		ItselfOrganizationVariables.QControl.init();
		ItselfOrganizationVariables.History.init();

		elemID = 0;
		if (count != 0) {
			JOptionPane
					.showMessageDialog(
							null,
							"�������� ��������� ���� ��� ������� ������ (count = " + count + ")"
									+ "\n��� �������� �������� ������ ������������� �������"
									+ "\n� ������ ������� ��������");
		}

		// clearing variables for simulation synchronization
		allow_simulation = true;
		// allow_tree_correction = true;
		simulation_is_on = false;
		maximumDeviation = 0;

		// to_do!!!!
	}

	// used if selection was lost
	public static void clearSelection() {
		if (tree.getSelectionRows() != null) {
			tree.removeSelectionRow(tree.getSelectionRows()[0]);
			tree.setSelectionPath(Main.tree.getPathForRow(0));
		}
		btnAddEP.setDisable(true);
		btnAddEO.setDisable(true);
		btnRemove.setDisable(true);
		panelGraphic.getGraph().clearSelection();
		panelGraphic.refresh();
		// clearing text about selected element type and function
		typeAndFuncNameInform.setText("");
	}

	public static void changeButtonsEnable(TreeElement currentTreeElement) {
		int children_type;
		btnRemove.setDisable(false);
		btnAddEP.setDisable(true);
		btnAddEO.setDisable(true);
		if (currentTreeElement.type == 2)// element is container
		{
			// check slots number, it must be more then children count
			// and check children type
			if (currentTreeElement.iChildrenCount < TypeLoader
					.getSlotsNumber(currentTreeElement.typeName)) {
				children_type = TypeLoader
						.getChildrenType(currentTreeElement.typeName);
				if (TypeLoader.intermediaryTypesList.size() > 0
						&& (children_type != 1)) {
					btnAddEP.setDisable(false);
				}
				if (TypeLoader.elementaryTypesList.size() > 0
						&& (children_type != 2)) {
					btnAddEO.setDisable(false);
				}
			}
		}
	}

	public static void changeButtonsEnableForCurrentSelection() {
		int children_type;
		btnRemove.setDisable(false);
		btnAddEP.setDisable(true);
		btnAddEO.setDisable(true);
		TreeElement currentTreeElement;
		DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) tree
				.getSelectionPath().getLastPathComponent();
		currentTreeElement = TreeOperations.findElementInStruct(selectedNode);

		if (currentTreeElement != null) {
			if (currentTreeElement.type == 2)// element is container
			{
				// check slots number, it must be more then children count
				// and check children type
				if (currentTreeElement.iChildrenCount < TypeLoader
						.getSlotsNumber(currentTreeElement.typeName)) {
					children_type = TypeLoader
							.getChildrenType(currentTreeElement.typeName);
					if (TypeLoader.intermediaryTypesList.size() > 0
							&& (children_type != 1)) {
						btnAddEP.setDisable(false);
					}
					if (TypeLoader.elementaryTypesList.size() > 0
							&& (children_type != 2)) {
						btnAddEO.setDisable(false);
					}
				}
			}
		} else {
			btnRemove.setDisable(true);
			TreePath treePath = Main.tree.getPathForRow(0);
			int countElement = tree.getRowCount();
			if ((treePath.toString().compareToIgnoreCase(Main.tree
					.getSelectionPath().toString())) == 0
					&& (countElement <= 1)) {
				if (TypeLoader.intermediaryTypesList.size() > 0) {
					btnAddEP.setDisable(false);
				}
			}

		}

	}

	private TreeElement createTreeElemFromNode(Element node) {
		TreeElement newTreeElement;
		try {
			newTreeElement = new TreeElement(Integer.parseInt(node
					.getAttribute("id")), Integer.parseInt(node
					.getAttribute("parent_id")));
			newTreeElement.elementName = node.getAttribute("name");
			newTreeElement.typeName = node.getAttribute("type_name");
			newTreeElement.functionName = node.getAttribute("func_name");
			newTreeElement.type = Integer.parseInt(node.getAttribute("type"));
			newTreeElement.iChildrenCount = 0;
			newTreeElement.Level = Integer.parseInt(node.getAttribute("level"));

			if (newTreeElement.type == 3) {

				NodeList mapElementNodeList = null;
				Element mapNode;
				Element paramInMapNode;

				// filling parametrsValuesMap
				mapElementNodeList = node
						.getElementsByTagName("parametrsValuesMap");
				if (mapElementNodeList.getLength() > 0) {
					mapNode = (Element) mapElementNodeList.item(0);
					newTreeElement.parametersValuesMap = new HashMap<String, Double>();
					mapElementNodeList = mapNode
							.getElementsByTagName("mapElem");
					for (int nodeNumber = 0; nodeNumber < mapElementNodeList
							.getLength(); nodeNumber++) {
						paramInMapNode = (Element) mapElementNodeList
								.item(nodeNumber);
						newTreeElement.parametersValuesMap.put(paramInMapNode
								.getAttribute("name"), Double
								.valueOf(paramInMapNode.getAttribute("value")));
					}
				}

				// filling parametrsWeightMap
				mapElementNodeList = node
						.getElementsByTagName("parametrsWeightMap");
				if (mapElementNodeList.getLength() > 0) {
					mapNode = (Element) mapElementNodeList.item(0);
					newTreeElement.parametrsWeightMap = new HashMap<String, Double>();
					mapElementNodeList = mapNode
							.getElementsByTagName("mapElem");
					for (int nodeNumber = 0; nodeNumber < mapElementNodeList
							.getLength(); nodeNumber++) {
						paramInMapNode = (Element) mapElementNodeList
								.item(nodeNumber);
						newTreeElement.parametrsWeightMap.put(paramInMapNode
								.getAttribute("name"), Double
								.valueOf(paramInMapNode.getAttribute("value")));
					}
				}
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"������ ��� ��������� ������ �� ��������");
			e.printStackTrace();
			return null;
		}
		return newTreeElement;

	}

	private Element createNodeFromTreeElem(TreeElement savedTreeElement,
			Document doc/* Element elem */) {
		// Element newNode = elem;
		Element newNode = doc.createElement("elem");
		Element childNode;
		Element mapNode;
		try {
			newNode.setAttribute("id", String.valueOf(savedTreeElement.ID));
			newNode.setAttribute("parent_id", String
					.valueOf(savedTreeElement.ParentID));
			newNode.setAttribute("name", savedTreeElement.elementName);
			newNode.setAttribute("type_name", savedTreeElement.typeName);
			newNode.setAttribute("func_name", savedTreeElement.functionName);
			newNode.setAttribute("type", String.valueOf(savedTreeElement.type));
			newNode.setAttribute("children_count", String
					.valueOf(savedTreeElement.iChildrenCount));
			newNode.setAttribute("level", String
					.valueOf(savedTreeElement.Level));

			Iterator iter;

			// if element is list
			if (savedTreeElement.type == 3) {
				// save parametrsValuesMap
				childNode = doc.createElement("parametrsValuesMap");
				if (savedTreeElement.parametersValuesMap != null) {
					iter = savedTreeElement.parametersValuesMap.entrySet()
							.iterator();
					while (iter.hasNext()) {
						mapNode = doc.createElement("mapElem");
						Entry parametrsInform = (Entry) iter.next();
						String paramName = (String) parametrsInform.getKey();
						String paramValue = ((Double) parametrsInform
								.getValue()).toString();
						mapNode.setAttribute("name", paramName);
						mapNode.setAttribute("value", paramValue);

						childNode.appendChild(mapNode);
					}
				}
				newNode.appendChild(childNode);

				// save parametrsValuesMap
				childNode = doc.createElement("parametrsWeightMap");
				if (savedTreeElement.parametrsWeightMap != null) {
					iter = savedTreeElement.parametrsWeightMap.entrySet()
							.iterator();
					while (iter.hasNext()) {
						mapNode = doc.createElement("mapElem");
						Entry parametrsInform = (Entry) iter.next();
						String paramName = (String) parametrsInform.getKey();
						String paramValue = ((Double) parametrsInform
								.getValue()).toString();
						mapNode.setAttribute("name", paramName);
						mapNode.setAttribute("value", paramValue);

						childNode.appendChild(mapNode);
					}
				}
				newNode.appendChild(childNode);

			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"������ ��� ���������� ������ �� ��������");
			e.printStackTrace();
			return null;
		}
		return newNode;

	}

	public static void setSameParametrsInParamOrg(boolean newValue) {
		//TODO cleanup this method (setSameParametrsInParamOrg)
		ItselfOrganizationVariables.sameParametrsInParamOrg = newValue;
		if (ItselfOrganizationVariables.sameParametrsInParamOrg) {
			paramSameMenuItem.setSelected(true);
			//new
			ItselfOrganizationVariables.getInstance()
				.setParametersModelOrganizer(ParametersOrganizer.GLOBAL_PARAMETERS_ORGANIZER);
		} else {
			paramNotSameMenuItem.setSelected(true);
			//new
			ItselfOrganizationVariables.getInstance()
				.setParametersModelOrganizer(ParametersOrganizer.LOCAL_PARAMETERS_ORGANIZER);
		}
	}

	public void actionWindowListener()
	{
		if(Main.user_enter_data_of_adding_object)
		{			
			TreePath treePath = Main.tree.getSelectionPath();
			if( treePath == null )
			{
				Main.tree.setSelectionRow(0);
				treePath = Main.tree.getSelectionPath();
			}
			if(treePath.getPathCount()  > 1 || Main.count==0 )
			{
	
				DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
				
				newNode = new DefaultMutableTreeNode(Main.addingElemName);
				
				TreeElement parentTreeElem = TreeOperations.findElementInStruct(selectedNode);
				TreeElement treeElem;
				if( parentTreeElem != null )
				{
					treeElem = new TreeElement(Main.elemID, parentTreeElem.ID);
				}
				else
				{
					treeElem = new TreeElement(-1, -2);
				}
				
				
				
				//add element to the structTreeElements begin
				if( parentTreeElem == null )//if adding element has no parent
				{
					if( Main.count == 0 )//structTreeElements is empty
					{
						if(Main.maxLevelsCount<0)
						{
							Main.structTreeElements.add(new Vector<TreeElement>());
							Main.maxLevelsCount++;
						}	
						Main.structTreeElements.elementAt(0).add(treeElem);//add root
					}
					else
					{
						JOptionPane.showMessageDialog(null, "Error in AddObjectHandler class: /n" +
						"cant find parent element in structTreeElements");
						return;
					}
				}
				else//if adding element has parent
				{
					//create (if necessary)new tree level
					if( parentTreeElem.Level == Main.maxLevelsCount )
					{
						Main.structTreeElements.add(new Vector<TreeElement>());
						Main.maxLevelsCount++;
					}
					//add element to the structTreeElements
					Main.structTreeElements.elementAt( parentTreeElem.Level + 1 ).add(treeElem);
				}
				//add element to the structTreeElements end
				
				//fields definition for new treeElem 
				treeElem.elementName = Main.addingElemName;
				Main.elementsNamesList.add( treeElem.elementName );
				treeElem.typeName = Main.addingElemTypeName;
				treeElem.functionName = Main.addingElemFuncName;
				if( Main.addingElemIsElementary )
				{
					treeElem.type = 3;
				}
				treeElem.iChildrenCount = 0;
				if( parentTreeElem != null )//if this is not first element
				{
					treeElem.Level = parentTreeElem.Level + 1;
					
					//filling ParametrsValuesMap
					if( treeElem.type == 3 )//is element is elementary
					{
						treeElem.parametersValuesMap = Main.parser.fillingParametersValuesMap( treeElem.functionName );
						//filling ParametrsWeightMap
						double initialWeight = 1.0;
						treeElem.parametrsWeightMap = new HashMap<String, Double>();
						if( treeElem.parametersValuesMap != null )
						{
							if( treeElem.parametersValuesMap.size() > 0 )
							{
								Iterator iter = treeElem.parametersValuesMap.entrySet().iterator();
								while( iter.hasNext() )
								{
									Entry paramInform = (Entry) iter.next();
						        	String paramName = (String) paramInform.getKey();
						        	
						        	treeElem.parametrsWeightMap.put(paramName, initialWeight);
								}
							}
						}
							 
						
					}
					else//is element is intermediate
					{
						//check_me				
					}
					
					treeElem.cell = Main.panelGraphic.addChildNode( parentTreeElem, Main.addingElemName,  treeElem.ID, treeElem.type);
					treeElem.parentCell = parentTreeElem.cell;
					Main.elemID++;
				}
				else//if this is first element
				{
					treeElem.Level = 0;
					treeElem.cell = Main.panelGraphic.addNode( Main.addingElemName );
					treeElem.parentCell = null;
					//to_do		
					
				}
				
				treeElem.element = newNode;					
	
				Main.count++;
				
				
				selectedNode.add(newNode);
				Main.tree.updateUI();
				//check_me  begin		
				//set selection to the new element
				Main.tree.setSelectionPath( new TreePath(treeElem.element.getPath()) );
				Main.panelGraphic.getGraph().setSelectionCell(treeElem.cell);
				Main.panelGraphic.refreshTable();
				//check_me  end
			}
			Main.user_enter_data_of_adding_object=false;
			
			//check_me  begin	
			Main.tree.getSelectionPath().getLastPathComponent();
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) Main.tree.getSelectionPath().getLastPathComponent();
			TreeElement tmpSelectedElement = TreeOperations.findElementInStruct(selectedNode);
			if( tmpSelectedElement != null )
			{
				Main.changeButtonsEnable(tmpSelectedElement);
			}
			else
			{
				btnAddEP.setDisable(true);
				btnAddEO.setDisable(true);
				btnRemove.setDisable(true);
			}
		}
	}
	
	public class TableElement{
		SimpleStringProperty Name;
		SimpleStringProperty Value;
		SimpleStringProperty Weight;
		
		public TableElement(String _name, String _value, String _weight)
		{
			this.Name = new SimpleStringProperty(_name);
			this.Value = new SimpleStringProperty(_value);
			this.Weight =  new SimpleStringProperty(_weight);
		}
		
		public String GetName()	{
			return this.Name.get();
		}
		
		public String GetValue()	{
			return this.Value.get();
		}
		
		public String GetWeight()	{
			return this.Weight.get();
		}
		
		public void SetName(String _name)	{
			this.Name.set(_name);
		}
		
		public void SetValue(String _value)	{
			this.Value.set(_value);
		}
		
		public void SetWeight(String _weight)	{
			this.Weight.set(_weight);
		}
	}
	
}
