/**
 * 
 */
package Main;

/**
 *  This class implements translation strategy defined by:
 *	Xi = Bi + (Ai - Bi)*sin^2(Zi)
 *
 * @author User
 */
final class SinSquareParameterTranslator implements IParameterTranslator {

	/* (non-Javadoc)
	 * @see Main.IParameterTranslator#toLimited(java.lang.String, double)
	 */
	@Override
	public double toLimited(String paramName, double value) {
		//version 2
		return ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(1)/*rightBorder*/ 
				+	(ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(0)/*leftBorder*/ 
					- ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(1)/*rightBorder*/)
					*Math.pow(Math.sin(value), 2);
	}

	/* (non-Javadoc)
	 * @see Main.IParameterTranslator#toUnlimited(java.lang.String, double)
	 */
	@Override
	public double toUnlimited(String paramName, double value) {
		//version 2
		value = (value - ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(1)/*rightBorder*/)
				/ (ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(0)/*leftBorder*/ 
				- ParametersLoader.parametrsCharacteristicsMap.get(paramName).get(1)/*rightBorder*/);
		return Math.asin(Math.pow(value, 0.5));
	}

}
