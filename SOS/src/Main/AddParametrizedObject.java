package Main;

import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import object.TreeElement;

public class AddParametrizedObject 
{
	private DefaultMutableTreeNode newNode;
	
	public AddParametrizedObject(TreeElement treeElem)
	{
		TreePath treePath = Main.tree.getSelectionPath();
		if( treePath == null )
		{
			Main.tree.setSelectionRow(0);
			treePath = Main.tree.getSelectionPath();
		}
		if( ( treePath.getPathCount()  > 1 ) || ( Main.count == 0 ) )
		{
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
			newNode = new DefaultMutableTreeNode( treeElem.elementName );
			TreeElement parentTreeElem = TreeOperations.findElementInStruct(selectedNode);
			
//add element to the structTreeElements begin
			if( parentTreeElem == null )//if adding element has no parent
			{
				if( Main.count == 0 )//structTreeElements is empty
				{
					if(Main.maxLevelsCount<0)
					{
						Main.structTreeElements.add(new Vector<TreeElement>());
						Main.maxLevelsCount++;
					}	
					Main.structTreeElements.elementAt(0).add(treeElem);//add root
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Error in AddObjectHandler class: /n" +
					"cant find parent element in structTreeElements");
					return;
				}
			}
			else//if adding element has parent
			{
				//create (if necessary)new tree level
				if( parentTreeElem.Level == Main.maxLevelsCount )
				{
					Main.structTreeElements.add(new Vector<TreeElement>());
					Main.maxLevelsCount++;
				}
				//add element to the structTreeElements
				Main.structTreeElements.elementAt( parentTreeElem.Level + 1 ).add(treeElem);
			}
//add element to the structTreeElements end
			
			
			if( parentTreeElem != null )//if this is not first element
			{
				treeElem.Level = parentTreeElem.Level + 1;
				
//				//filling ParametrsValuesMap
//				if( treeElem.type == 3 )//is element is elementary
//				{
//					treeElem.parametrsValuesMap = Main.parser.fillingParametersValuesMap( treeElem.functionName );
//					//filling ParametrsWeightMap
//					double initialWeight = 1;
//					treeElem.parametrsWeightMap = new HashMap<String, Double>();
//					if( treeElem.parametrsValuesMap.size() > 0 )
//					{
//						Iterator iter = treeElem.parametrsValuesMap.entrySet().iterator();
//						while( iter.hasNext() )
//						{
//							Entry paramInform = (Entry) iter.next();
//				        	String paramName = (String) paramInform.getKey();
//				        	
//				        	treeElem.parametrsWeightMap.put(paramName, initialWeight);
//						}
//					}
//				}
//				else//is element is intermediate
//				{
////check_me				
//				}
				
				treeElem.cell = Main.panelGraphic.addChildNode( parentTreeElem, treeElem.elementName,  treeElem.ID, treeElem.type);
				treeElem.parentCell = parentTreeElem.cell;
//check_me	begin
				if( Main.elemID < treeElem.ID )
				{
					Main.elemID = treeElem.ID;
				}
//check_me	end
			}
			else//if this is first element
			{
				treeElem.Level = 0;
				treeElem.cell = Main.panelGraphic.addNode( treeElem.elementName );
				treeElem.parentCell = null;	
			}
			Main.elementsNamesList.add( treeElem.elementName );
	
			treeElem.element = newNode;					
			Main.count++;
			selectedNode.add(newNode);
			Main.tree.updateUI();
			
			Main.tree.setSelectionPath( new TreePath(treeElem.element.getPath()) );
			Main.panelGraphic.getGraph().setSelectionCell(treeElem.cell);
			Main.panelGraphic.refreshTable();
			
		}
	}
	
}
