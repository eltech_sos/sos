package Main;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import object.TreeElement;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ParametersLoader 
{
	public static ArrayList<String> parametrsList;
	
	//structure:
	//key - parameter name
	//Vector value:
	//element 0 - left_border, element 1 - right_border, 
	//element 2 - init_value, element 3 - participation
	public static Map< String, ArrayList<Float> > parametrsCharacteristicsMap;
	
	/**
	 * This will store concrete parameter translator to define the translation strategy
	 */
	private static IParameterTranslator	parameterTranslator;
	
	static {
		// define the parameter translation strategy
		parameterTranslator = new SinSquareParameterTranslator();
		//parameterTranslator = new SinParameterTranslator();//original
		//parameterTranslator = new TruncateParameterTranslator();
	}
	
	public ParametersLoader()
	{
		//Initialization
		parametrsList = new ArrayList<String>();
		parametrsCharacteristicsMap = new HashMap< String, ArrayList<Float> >();
		//reading information about parameters
        File docFile = new File("files/parameters.xml");
        Document doc = null;
        try 
        {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(docFile);
        } 
        catch (java.io.IOException e) 
        {
            System.out.println("Can't find the file");
        } 
        catch (Exception e) 
        {
            System.out.print("Problem parsing the file");
        }
		
        Element root = doc.getDocumentElement();
        NodeList parametersNodeList = root.getElementsByTagName("param");
        Element param;
        if( parametersNodeList.getLength() > 0 )
        {
        	for( int i=0; i<parametersNodeList.getLength(); i++ )
        	{
        		//add current parameter to the list
        		param = (Element) parametersNodeList.item(i);
        		parametrsList.add(param.getAttribute("name"));
        		
        		ArrayList<Float> parametrsCharacteristicsList = new ArrayList<Float>();
        		//filling parametrsCharacteristicsMap
        		parametrsCharacteristicsList.add( Float.valueOf( param.getAttribute("left_border") ) );
        		parametrsCharacteristicsList.add( Float.valueOf( param.getAttribute("right_border") ) );
        		parametrsCharacteristicsList.add( Float.valueOf( param.getAttribute("init_value") ) );
        		parametrsCharacteristicsList.add( Float.valueOf( param.getAttribute("participation") ) );
        		parametrsCharacteristicsMap.put( param.getAttribute("name"), parametrsCharacteristicsList );
        		
        	}
        }
        
	}
	
	public static float getLeftBorder(String typeName)
	{
		if( parametrsCharacteristicsMap.containsKey( typeName ) )
		{
			return parametrsCharacteristicsMap.get(typeName).get(0);
		}
		return -1;
	}
	
	public static float getRightBorder(String typeName)
	{
		if( parametrsCharacteristicsMap.containsKey( typeName ) )
		{
			return parametrsCharacteristicsMap.get(typeName).get(1);
		}
		return -1;
	}
	
	public static float getInitialValue(String typeName)
	{
		if( parametrsCharacteristicsMap.containsKey( typeName ) )
		{
			return parametrsCharacteristicsMap.get(typeName).get(2);
		}
		return -1;
	}
	
	public static boolean isParticipate(String typeName)
	{
		if( parametrsCharacteristicsMap.containsKey( typeName ) )
		{
			if( parametrsCharacteristicsMap.get(typeName).get(3) == 1 )
			{
				return true;
			}
		}
		return false;
	}
	
	public static boolean saveCurrentParametrsSet()
	{
		File docFile = new File("files/parameters.xml");
		try 
        {
			//Create a Document
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbf.newDocumentBuilder();
            Document doc = docBuilder.newDocument();
            
//            doc.setXmlVersion("1.0");
//Creating the XML tree begin
            //create the root element and add it to the document
            Element root = doc.createElement("parameters_list");
            doc.appendChild(root);
            
            Element paramNode;
            String paramName;
            for(int paramInd=0; paramInd<parametrsList.size(); paramInd++)
            {
            	paramNode = doc.createElement("param");
            	paramName = parametrsList.get( paramInd );
            	paramNode.setAttribute( "name", paramName );
            	paramNode.setAttribute( "left_border", String.valueOf( parametrsCharacteristicsMap.get(paramName).get(0) ) );
            	paramNode.setAttribute( "right_border", String.valueOf( parametrsCharacteristicsMap.get(paramName).get(1) ) );
            	paramNode.setAttribute( "init_value", String.valueOf( parametrsCharacteristicsMap.get(paramName).get(2) ) );
            	paramNode.setAttribute( "participation", String.valueOf( parametrsCharacteristicsMap.get(paramName).get(3) ) );
            	root.appendChild(paramNode);
            }
//Creating the XML tree end
            
            //save to file
            String outputURL = docFile.getAbsolutePath();
            
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new FileOutputStream(outputURL));

            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.transform(source, result);

		} 
        catch (Exception e) 
        {
        	JOptionPane.showMessageDialog(null, "������ ��� ���������� ��������� � ���������� ����������");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	

	//function for translation parameter limited value to unlimited value
	public static double valueTranslationToUnlimitedFunction(String paramName, double value)
	{
////TMP!!!!
//		if( paramName.equals("x") || paramName.equals("y") )
//		{
//			return value;
//		}
////TMP!!!!
		return parameterTranslator.toUnlimited(paramName, value);
	}
	
	//function for translation parameter unlimited value to limited value
	public static double valueTranslationToLimitedFunction(String paramName, double value)
	{
////TMP!!!!
//		if( paramName.equals("x") || paramName.equals("y") )
//		{
//			return value;
//		}
////TMP!!!!
		return parameterTranslator.toLimited(paramName, value);
	}
	
	//counted for LIMITED parameter value
	public static double getOpportunityOfChangeForCurrentValue( String paramName, double currentParamValue )
	{
		double leftBorder = parametrsCharacteristicsMap.get(paramName).get(0)/*leftBorder*/;
		double rightBorder = parametrsCharacteristicsMap.get(paramName).get(1)/*rightBorder*/;
		return Math.min( currentParamValue - leftBorder , rightBorder - currentParamValue ) / ( rightBorder - leftBorder );
	}
	
	
//	//function for translation parameter limited value to unlimited value
//	public static double valueTranslationToUnlimitedFunction(double leftBorder,
//			double rightBorder, double x)
//	{
//		x = (x - 0.5 * (leftBorder + rightBorder)) / (0.5 * (rightBorder - leftBorder));
//		return Math.asin(x);
//	}
//
//	//function for translation parameter unlimited value to limited value
//	public static double valueTranslationToLimitedFunction(double leftBorder, double rightBorder,
//			double x)
//	{
//		return 0.5 * (leftBorder + rightBorder) + 0.5 * (rightBorder - leftBorder) * Math.sin(x);
//	}
	
}
