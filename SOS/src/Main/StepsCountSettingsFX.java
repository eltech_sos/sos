package Main;

import tabs.FXDialog;
import tabs.Message;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import simulation.ItselfOrganizationVariables;

public class StepsCountSettingsFX extends Application{
	private TextField tfMaxStep;
	
	@Override
	public void start(final Stage stage) throws Exception {
		stage.setTitle("������� ������������� ����� �����");
		VBox vBox = new VBox();
    	
    	GridPane hBoxStep = new GridPane();
    	Text txtMaxStep = new Text("������������ ����� �����     ");
    	txtMaxStep.setFont(Font.font("Arial",FontWeight.BOLD, 12));
    	txtMaxStep.setFill(Color.color(0.3, 0.3, 0.3));// Dark grey
    	hBoxStep.add(txtMaxStep, 0, 1);
    	tfMaxStep = new TextField();
    	tfMaxStep.setText(String.valueOf( ItselfOrganizationVariables.maxNumerOfCounts ));
    	hBoxStep.add(tfMaxStep, 1, 1);
    	
    	Button btnOk = new Button("��");
        btnOk.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				if( isEntredDataCorrect() )
				{
					ItselfOrganizationVariables.maxNumerOfCounts = Integer.parseInt( tfMaxStep.getText() );
					stage.close();
				}
			}});
        Button btnCancel = new Button("������");
        btnCancel.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				stage.close();
			}});
        
        HBox hBox = new HBox();
        hBox.setSpacing(5);
        hBox.setPadding(new Insets(5, 0, 0, 179));
        hBox.getChildren().addAll(btnOk, btnCancel);
        
        vBox.getChildren().addAll(hBoxStep, hBox);
        vBox.setSpacing(5);
        vBox.setPadding(new Insets(10, 10, 10, 10));       		
		Scene scene = new Scene(vBox);	
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
    	stage.setScene(scene);
		stage.show();
	}

	protected boolean isEntredDataCorrect()
	{
		String chekingText;
		
		chekingText = tfMaxStep.getText();
		if( (chekingText != null) &&
				( chekingText.length() > 0 ) )
		{
			try
			{
				if(Integer.parseInt(chekingText) <= 0 )
				{
					FXDialog.showMessageDialog("��������, �� ��������� �������� ������� ������ ���� ������ 0", "������ ����������", Message.WARNING);
					return false;
				}
			}
			catch(Exception e)
			{
				FXDialog.showMessageDialog("����������, " +
						"��������� ������������ �������� ������" +
						"\n(����� ������ ���� ����� ������, ������ ����)", "������ ����������", Message.WARNING);
				return false;
			}
		}
		else
		{
			FXDialog.showMessageDialog("����������, ������� ��������� �������� �������", "������ ����������", Message.WARNING);
			return false;
		}
		return true;
	}
	
}
