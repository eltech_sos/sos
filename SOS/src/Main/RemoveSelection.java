package Main;

import javax.swing.JOptionPane;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import object.TreeElement;
import simulation.SimulationStep;

import org.jgraph.graph.DefaultGraphCell;

public class RemoveSelection 
{
	public RemoveSelection( boolean confirmReguest )
	{
		TreePath treePath = Main.tree.getSelectionPath();
		
		if( treePath == null )
		{
			return;
		}
		int countElement = Main.tree.getRowCount();
		if(countElement < 2)
		{
			return;
		}
		
		
		
		TreePath treePathSelected = Main.tree.getSelectionPath().getParentPath();
		if(treePathSelected == null)
		{
			treePathSelected = Main.tree.getSelectionPath();
		}
//		Main.allow_tree_correction = SimulationStep.allow_tree_correction;
//	     if(Main.allow_tree_correction)
//	     {
	    	
	    	
//	    	 Main.allow_modulation=false;
	
	//		DefaultGraphCell clickedCell = (DefaultGraphCell) panelGraphic.getGraph().getSelectionCell();		
	//		JOptionPane.showMessageDialog(null,	clickedCell.toString());
			//tree.addSelectionPath(treePath);
			TreeElement treeElem =null;
			DefaultGraphCell cell= null;
			DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
			DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) selectedNode.getParent();
			
			//Reduction of number of children at the parent of a deleted element
			treeElem = TreeOperations.findElementInStruct(parentNode);
			if( treeElem != null )
			{
				treeElem.iChildrenCount--;
			}
			
			treeElem = TreeOperations.findElementInStruct(selectedNode);
			
			//confirm dialog
			if( confirmReguest )
			{
				int dialog = JOptionPane.showConfirmDialog(null, "�� ������� ��� ������ ������� ������� " + 
						treeElem.elementName + " ?", 
						"������������� ��������",
						JOptionPane.YES_NO_CANCEL_OPTION);
				if( dialog == 2 || dialog == -1 || dialog ==1 )
				{
					return;
				}
			}
			
			
			cell = treeElem.cell;
			
			if( Main.elementsNamesList.contains(treeElem.elementName) )
			{
				Main.elementsNamesList.remove( treeElem.elementName );
			}
			
			if(Main.simulation_is_on)
			{
				//if element was displayed in simulation did not 
				//display it more
//				if( ModulationCount.visibilityOfElementsMap.size() > 0 )
//				{
//					if( ModulationCount.visibilityOfElementsMap.containsKey( treeElem.elementName ) )
//					{
//						ModulationCount.visibilityOfElementsMap.put( treeElem.elementName, false );
//					}
//				}
			}
			
			//check: is deleting element is root
			if( treeElem.ID == -1 )
			{
				if( TypeLoader.intermediaryTypesList.size() > 0 )
				{
					Main.btnAddEP.setDisable(false);
				}
				Main.btnAddEO.setDisable(true);;
				Main.btnRemove.setDisable(true);
				Main.panelGraphic.refresh();
				//clearing text about selected element type and function
				Main.typeAndFuncNameInform.setText("");
			}
			
				
		//	if (treeElem.Level!=0){
			if( ! selectedNode.isRoot() )
			{
				parentNode.remove(selectedNode);
				Main.Remove(cell);
//				Main.vtTreeElements.remove(treeElem);
				Main.panelGraphic.removeCellGraph(treeElem.cell,treeElem.cellLink );
			}
				
		//	}
			Main.tree.updateUI();
//			Main.allow_modulation=true;
//	     }
	     try
	     {
	    	 Main.tree.setSelectionPath(treePathSelected);
	    	 int countElement1 = Main.tree.getRowCount();
	    	 if( countElement1>1 )
	    	 {
	    		 TreePath treeNewPath = Main.tree.getSelectionPath();
	    		 TreeElement treeElemForSelection = TreeOperations.findElementInStruct( (DefaultMutableTreeNode) treeNewPath.getLastPathComponent() );
	    		 if( treeElemForSelection != null )
	    		 {
	    			 Main.panelGraphic.getGraph().setSelectionCell(treeElemForSelection.cell);
	    			 Main.changeButtonsEnableForCurrentSelection();
	    		 }
	    		 else
		    	 {
		    		 Main.panelGraphic.getGraph().clearSelection();
		    	 }
	    	 }
	    	 else
	    	 {
	    		 Main.panelGraphic.getGraph().clearSelection();
	    	 }
	     }
	     catch(Exception exept)
	     {
	    	 Main.tree.setSelectionRow(0);
	     }
	     
	}
}
