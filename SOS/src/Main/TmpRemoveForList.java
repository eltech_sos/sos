package Main;

import javax.swing.tree.DefaultMutableTreeNode;

import org.jgraph.graph.DefaultGraphCell;

import object.TreeElement;

public class TmpRemoveForList 
{
	public TmpRemoveForList(TreeElement deletedElem)
	{
		DefaultGraphCell cell= null;
		cell = deletedElem.cell;
		
		DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) deletedElem.element;
		DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) selectedNode.getParent();
		
		if( cell == null
				|| selectedNode == null
				|| parentNode == null)
		{
			return;
		}
		
		//Reduction of number of children at the parent of a deleted element
		TreeElement parentElem;
		parentElem = TreeOperations.findElementInStruct(parentNode);
		if( parentElem != null )
		{
			parentElem.iChildrenCount--;
		}
		
		if( Main.elementsNamesList.contains(deletedElem.elementName) )
		{
			Main.elementsNamesList.remove( deletedElem.elementName );
		}
		
		parentNode.remove( selectedNode );
		Main.listRemove( deletedElem );
		Main.panelGraphic.removeCellGraph( deletedElem.cell,deletedElem.cellLink );
		
	}
}
