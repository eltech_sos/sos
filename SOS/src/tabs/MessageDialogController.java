package tabs;

import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;

public class MessageDialogController {
    
    @FXML protected HBox headerPane;
    @FXML protected ImageView icon;
    @FXML protected Label lblHeader;
    @FXML protected Label lblMsg;
    
    @FXML private void ok(ActionEvent evt) {
        FXDialog.getInstance().primaryStage.close();
    }
}
