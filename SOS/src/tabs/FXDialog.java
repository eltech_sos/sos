/*
Copyright (c) 2013, Alvin Cris Tabontabon
All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
 
1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/


package tabs;

import java.io.*;
import java.util.logging.*;
import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.image.*;
import javafx.scene.input.*;
import javafx.scene.paint.*;
import javafx.stage.*;

public class FXDialog {

    private double initX;       // X-Coordinate location of the dialog
    private double initY;       // Y-Coordinate location of the dialog
    
    private Parent root;
    private Response response;
    private static FXDialog main;
    
    private static Object Controller;

    protected Stage primaryStage;

    public FXDialog() {
        primaryStage = new Stage();
        primaryStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
    }

    protected static FXDialog getInstance() {
        if(main == null) {
            main = new FXDialog();
        }
        return main;
    }
    
    /*
     * The purpose of this method is to change the scene depending on the speciied 
     * type of the dialog will be shown.
     * 
     * @param dialogType
     */
    private void replaceScene(DialogType dialogType) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(dialogType.getFXML()));
            // TODO check if this load process works or make it more JavaFX correct
            //fxmlLoader.setRoot(root);
            fxmlLoader.load();
            root = fxmlLoader.getRoot();
            Controller = fxmlLoader.getController();
            Scene scene = new Scene(root, Color.TRANSPARENT);
            primaryStage.setScene(scene);
            primaryStage.centerOnScreen();

            root.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent me) {
                    initX = me.getScreenX() - primaryStage.getX();
                    initY = me.getScreenY() - primaryStage.getY();
                }
            });

            root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent me) {
                    primaryStage.setX(me.getScreenX() - initX);
                    primaryStage.setY(me.getScreenY() - initY);
                }
            });
        } catch (IOException ex) {
            Logger.getLogger(FXDialog.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            System.gc();
        }
    }
    
    /*
     * The purpose of this method is to retain the selected action in the confirmation
     * dialog.
     * 
     * @param response;
     */
    protected void setReponse(Response response) { this.response = response; }

    public static void showMessageDialog(String message, String title, Message messageType) {
        getInstance().replaceScene(DialogType.MESSAGE);

        if (messageType == Message.ERROR) {
            ((MessageDialogController)Controller).icon.setImage(new Image("/tabs/icons/" + messageType.getIcon()));
            ((MessageDialogController)Controller).headerPane.setStyle("-fx-background-color: red;");
        } else if (messageType == Message.INFORMATION) {
            ((MessageDialogController)Controller).icon.setImage(new Image("/tabs/icons/" + messageType.getIcon()));
            ((MessageDialogController)Controller).headerPane.setStyle("-fx-background-color: blue;");

        } else if (messageType == Message.WARNING) {
            ((MessageDialogController)Controller).icon.setImage(new Image("/tabs/icons/" + messageType.getIcon()));
            ((MessageDialogController)Controller).headerPane.setStyle("-fx-background-color: orange;");
        }
 
        ((MessageDialogController)Controller).lblHeader.setText(title);
        ((MessageDialogController)Controller).lblMsg.setText(message);
        
        getInstance().primaryStage.setTitle(messageType.toString());
        getInstance().primaryStage.showAndWait();
    }

    public static boolean showConfirmDialog(String caption, String title, ConfirmationType confirmType) {
        getInstance().replaceScene(DialogType.CONFIRMATION);
        
        if(confirmType == ConfirmationType.DELETE_OPTION) {
            ((ConfirmationDialogController)Controller).btnAccept.setText("Delete");
            ((ConfirmationDialogController)Controller).btnDecline.setDefaultButton(true);
            ((ConfirmationDialogController)Controller).btnDecline.requestFocus();
            ((ConfirmationDialogController)Controller).btnDecline.setText("Don't Delete");  
        }
        else if(confirmType == ConfirmationType.YES_NO_OPTION) {
            ((ConfirmationDialogController)Controller).btnAccept.setText("Yes");
            ((ConfirmationDialogController)Controller).btnAccept.setDefaultButton(true);
            ((ConfirmationDialogController)Controller).btnDecline.setText("No");  
        }
        else if(confirmType == ConfirmationType.ACCEPT_DECLINE_OPTION) {
            ((ConfirmationDialogController)Controller).btnAccept.setText("Accept");
            ((ConfirmationDialogController)Controller).btnAccept.setDefaultButton(true);
            ((ConfirmationDialogController)Controller).btnDecline.setText("Decline");  
        }
        
        ((ConfirmationDialogController)Controller).lblHeader.setText(title);
        ((ConfirmationDialogController)Controller).lblMsg.setText(caption);
        
        getInstance().primaryStage.setTitle("CONFIRMATION");
        getInstance().primaryStage.showAndWait();
        
        return (getInstance().response.getValue() ? true : false);
    }
    
    public static String showInputDialog(String caption, String title) {
        getInstance().replaceScene(DialogType.INPUT);
        
        ((InputDialogController)Controller).lblHeader.setText(title);
        ((InputDialogController)Controller).lblMsg.setText(caption);
        
        getInstance().primaryStage.showAndWait();
     
        return (getInstance().response.getValue() ? ((InputDialogController)Controller).inputField.getText().trim() : null);
    }
    
    protected enum Response {

        APPROVE(true),
        DECLINE(false);
        private boolean val;

        private Response(boolean val) {
            this.val = val;
        }

        public boolean getValue() {
            return val;
        }
    }
}
