
package tabs;

import javafx.event.*;
import javafx.fxml.*;
import javafx.scene.control.*;

public class ConfirmationDialogController extends MessageDialogController {

    @FXML protected Button btnAccept;
    @FXML protected Button btnDecline;
    
    @FXML protected void accept(ActionEvent evt) {
        FXDialog.getInstance().setReponse(FXDialog.Response.APPROVE);
        FXDialog.getInstance().primaryStage.close();
    }
    
    @FXML protected void decline(ActionEvent evt) {
        FXDialog.getInstance().setReponse(FXDialog.Response.DECLINE);
        FXDialog.getInstance().primaryStage.close();
    }
}
