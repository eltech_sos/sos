package simulation;

/******************************************************************
 * 
 * ���� �������� �� ���������� ������ ���� �������������� � ������
 * �� ������������� ��������������� - ��� � ������� ����������� ��
 * ������, ��� � � �������������� ���������� ��������� �������� ��
 * 
 ******************************************************************/

import javafx.beans.property.SimpleStringProperty;
import simulation.FindMaxTypesValues;
import simulation.FuncOrgForLists;
import simulation.FunctionOrganization;
import simulation.HistoryOfSelforganizations;
import simulation.ItselfOrganizationVariables;
import simulation.SimulationCountFX;
import simulation.Restructurize;
import simulation.StructAddElement;
import simulation.TypeOrgForLeafs;
import simulation.TypeOrganization;

import javax.swing.JOptionPane;

import Main.Main;
import Main.TreeOperations;
import Main.TypeLoader;
import object.TreeElement;

public class SimulationStep 
{
	private int tableColumnInd=0; // ������ ������� � �������-������
	
	public static double rootMinCount = 10;  // �������������
	public static double maxDevialation = 0; // ��������
	public static double maxPrice = 1000;    // ����������
	
	// �����, ���������� �� ��������� ���� ���������� ��
	private boolean wasItselfOrganizationAtThisStep;
	private boolean wasFunctionsOrganizationAtThisStep;
	private boolean wasStructuralOrganizationAtThisStep;
	private String typeOfLastSSO = "";
	
	// ������� ���������� �� ��� ����������� � �������-������
	private HistoryOfSelforganizations History = new HistoryOfSelforganizations();
	
	public SimulationStep()
	{
		// �������� ���������� ��������� ��������� �����������
		Main.JCountingCount = 0;
		// ����� �������� �������� ����������
		rootMinCount = ItselfOrganizationVariables.rootMinimumCount;
		maxDevialation = ItselfOrganizationVariables.maximumDevialation;
		maxPrice = ItselfOrganizationVariables.maximumPrice;
		wasItselfOrganizationAtThisStep = false;
		wasFunctionsOrganizationAtThisStep = false;
		wasStructuralOrganizationAtThisStep = false;
		
		// ���������, ������������� �� ������� ���������� � ������������ �������� � �����
		ItselfOrganizationVariables.rootValueNotValid = (TreeOperations.rootCount() < rootMinCount)? true : false;
		
		if( ItselfOrganizationVariables.rootValueNotValid )
		{
			// ������ ������ ���� ������-��
			ItselfOrganizationVariables.leafsList.clear();
			if( Main.structTreeElements.size() > 0 )
			{ // ���� � ������� ���� ���� �� ���� �������� (�� ��������) �������
				TreeElement elementInStruct;
				for(int level=0; level<Main.structTreeElements.size(); level++)
				{ // ��� ������� ������ �������
					for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
					{ // �������� �� ���� ��������� ����� ������
						elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
						if( elementInStruct.type == 3 )
						{ // ���� ������� ������� - ��, �������� ��� � ������
							ItselfOrganizationVariables.leafsList.add( elementInStruct );
						}
					}
				}
			}
		}
/**************************************************************************			
����� ������� ����������� ������������� ���������� ��.
���� ������������ ������� ���� �� ������� ������������� ������������ �����,
�� ��� �� ����� ���������� �� ���������������� ���������.
***************************************************************************/
		if (!ItselfOrganizationVariables.UseIntelligentSOTypeChoosing && !ItselfOrganizationVariables.UseNeuralSOTypeChoosing)
		{ // ���� ������������ �� ������� ����� ������������� �����
			double  OldJ = TreeOperations.rootCount(), // �������� � �����
					NewJ = OldJ;	// �������� � ����� ����� ��
			if( ItselfOrganizationVariables.paramOrgAllowed && ItselfOrganizationVariables.rootValueNotValid )
			{ // ���� ��������� �� � ��������������� �� ���������
				if( ItselfOrganizationVariables.leafsList.size() > 0 )
				{ // ���� � ������� ������� ���� �� ���� ����-��
					History.addRecord(SimulationCountFX.t); // ����� ������ � �������
					History.getForParams(false); // ���������� ��������� ������� �� ���
					
					//TODO parameter organization call was replaced
					//new SingleParamOrganization( rootMinCount ); // �������� ���
					ItselfOrganizationVariables.getInstance().runPSO(rootMinCount);
					
					History.getForParams(true); // ���������� ��������� ������� ����� ���
					NewJ = TreeOperations.rootCount(); // ��������� �������� � �����
					// ����������� ���� ���; ������� ��������������� �������������� ����������
					ItselfOrganizationVariables.QControl.add(1, SimulationCountFX.t, NewJ - OldJ, 0);
					OldJ = NewJ; // ��� ���������. �������� "�������� � ����� �� ��"
					wasItselfOrganizationAtThisStep = true; // ���� � ���������� ���
				}
				if ( NewJ < rootMinCount) { // ���� ������� �� ��� �� ���������, ������ ����
					ItselfOrganizationVariables.rootValueNotValid = true;
				}
				else // ���� ��� �������������� �������, ������� ���� ��������������
					ItselfOrganizationVariables.rootValueNotValid = false;
			} // ����� ��������� ���

			if( ItselfOrganizationVariables.rootValueNotValid )
			{ // ���� ������� �����������
				wasItselfOrganizationAtThisStep = true; // ���� � ���������� ��
				// ���� ���� �� �����-������ ��� �� �� �������� � ������
				if( ItselfOrganizationVariables.funcLeafOrgAllowed ||
						ItselfOrganizationVariables.funcIntermediateOrgAllowed
						|| ItselfOrganizationVariables.typeLeafOrgAllowed 
						|| ItselfOrganizationVariables.typeIntermediateOrgAllowed 
						|| ItselfOrganizationVariables.reconfigurationOrgAllowed
						|| ItselfOrganizationVariables.addNewElementsOrgAllowed)
				{ // ������� ������������ �������� ��������� ������� ����
					new FindMaxTypesValues(); 
				}

				// ��� ��� ������-��		
				if( ItselfOrganizationVariables.funcLeafOrgAllowed  )
				{ // ���� ��� ��� ������ ��������� � �������
					History.addRecord(SimulationCountFX.t); // ����� ������ � �������
					History.getForFuncs(false); // ��������� ������� �� ���
					new FuncOrgForLists(); // ��������� ���
					wasFunctionsOrganizationAtThisStep = true; // ���� � ���������� ���
					NewJ = TreeOperations.rootCount(); // �������� � ����� ����� ���
					if ( NewJ < rootMinCount)
						ItselfOrganizationVariables.rootValueNotValid = true;
					else // ���� ������� ���������, ������� ���� � � �������������.
						ItselfOrganizationVariables.rootValueNotValid = false;
					if(!( ItselfOrganizationVariables.rootValueNotValid && ItselfOrganizationVariables.funcIntermediateOrgAllowed ))
					{ // ���� ������� ��������� ��� ��� ��� �� ��������� (�.�. ���� ��� ��� �� �� ����� �������)
						History.getForFuncs(true); // ��������� ������� ����� ���
						// ����������� ���� ���; ������� �������������� �������������� ����������
						ItselfOrganizationVariables.QControl.add(2, SimulationCountFX.t, NewJ - OldJ, 0);
						OldJ = NewJ; // ��������� �������� �������� � �����
					}
				}
				// ��� ��� ���������-�����������			
				if( ItselfOrganizationVariables.rootValueNotValid && ItselfOrganizationVariables.funcIntermediateOrgAllowed )
				{ // ���� ��������� �� � ��� ��� �� ���������
					if(!ItselfOrganizationVariables.funcLeafOrgAllowed)
					{ // ���� ��� ��� �� �� ����������, ������ ������ � �������
						History.addRecord(SimulationCountFX.t);
						History.getForFuncs(false);
					}
					new FunctionOrganization(); // ������ ���
					History.getForFuncs(true); // ������ ��������� ������� ����� ���
					NewJ = TreeOperations.rootCount();
					// ����������� ���� ���; ������� �������������� �������������� ����������
					ItselfOrganizationVariables.QControl.add(2, SimulationCountFX.t, NewJ - OldJ, 0);
					OldJ = NewJ;
					wasFunctionsOrganizationAtThisStep = true; // ���� � ���������� ���
					if ( NewJ < rootMinCount)
						ItselfOrganizationVariables.rootValueNotValid = true;
					else // ���� ������� �����������������, ������� ���� ��������������
						ItselfOrganizationVariables.rootValueNotValid = false;
				} // ����� ��������� ���

				if( ItselfOrganizationVariables.rootValueNotValid )
				{ // ���� ������� ��-�������� �� �����������������
					if(ItselfOrganizationVariables.typeLeafOrgAllowed 
							|| ItselfOrganizationVariables.typeIntermediateOrgAllowed 
							|| ItselfOrganizationVariables.reconfigurationOrgAllowed
							|| ItselfOrganizationVariables.addNewElementsOrgAllowed)
					{ // ���� �������� ���� �� ���� ��� ����������� ��
						// ������������� �������� ������� ��������� ��� ������
						if( ItselfOrganizationVariables.useMaximumPrice )
							TypeLoader.setAverageCharacteristicsForLeafs();

						// ������� ��� ������� ���� �� ������ ���� �����-��
						ItselfOrganizationVariables.findBestElementaryChildren();
						// ���������� ������������ ���� ��� ������ �� ������� ������
						ItselfOrganizationVariables.setConvolutionFuncPriorForLeafs();
						// ������������� �������� ������� ��������� ��� ��
						TypeLoader.setAverageCharacteristicsForIntermediate();
						// ������� ��� ��� ��������������� � ������� ������� ��
						TypeLoader.fillingInterTypesContainingFuncLists(); 
						OldJ = TreeOperations.rootCount();  
						// ������� �������� � �����; ����������, ��������� �� �������
						if ( OldJ < rootMinCount)
							ItselfOrganizationVariables.rootValueNotValid = true;
						else
							ItselfOrganizationVariables.rootValueNotValid = false;
						wasStructuralOrganizationAtThisStep = true; // ���� ���
						typeOfLastSSO = "�����������"; // ���������� ��� ���
					}
				} // ����� ��������� ����������� ��� ���

				// ������� �� ��� ������
				if( ItselfOrganizationVariables.rootValueNotValid && ItselfOrganizationVariables.typeLeafOrgAllowed )
				{
					// ��������� ������ � ��������� ������� �� ��� � �������
					History.addRecord(SimulationCountFX.t);
					History.getForTypes(false);
					new TypeOrgForLeafs(); // ��������� ���
					wasStructuralOrganizationAtThisStep = true; // ���� �� ���
					typeOfLastSSO = "������� ��� ������"; // ��� ���
					if ( TreeOperations.rootCount() < rootMinCount)
					{ // ���� ������� �� ���������
						ItselfOrganizationVariables.rootValueNotValid = true; // ���� ��������������
						if (!ItselfOrganizationVariables.typeIntermediateOrgAllowed){
							// ���� ��� ��� �� ���������� �� ����� 
							History.getForTypes(true);	// ������ � ��������� ������� ����� ���
							NewJ = TreeOperations.rootCount();
							// ���������� ����� ���; ������� ������� �������������� ����������
							ItselfOrganizationVariables.QControl.add(3, SimulationCountFX.t, NewJ - OldJ, 0);
							OldJ = NewJ;
						}
					}
					else
					{ // ������� ���������, ������ ��� ��� �� ���������� �� �����
						ItselfOrganizationVariables.rootValueNotValid = false; // ������� ���� ��������������
						History.getForTypes(true); // ������ � ��������� ������� ����� ���
						NewJ = TreeOperations.rootCount();
						// ���������� ����� ���; ������� ������� �������������� ����������
						ItselfOrganizationVariables.QControl.add(3, SimulationCountFX.t, NewJ - OldJ, 0);
						OldJ = NewJ;
					}
				} // ����� ��������� ��� ��� ��

				// ��� ��� ��
				if( ItselfOrganizationVariables.rootValueNotValid && ItselfOrganizationVariables.typeIntermediateOrgAllowed )
				{ // ���� ��� ���������� � ���������
					if (!ItselfOrganizationVariables.typeLeafOrgAllowed)
					{ // ���� ��� ��� �� �� �����������, ��������� ������ � �������
						History.addRecord(SimulationCountFX.t);
						History.getForTypes(false); // ��������� ������� �� ���
					}
					new TypeOrganization(); // ������ ���
					wasStructuralOrganizationAtThisStep = true; // ���� �� ���
					typeOfLastSSO = "�������"; // ���������� ��� ���
					History.getForTypes(true); // ��������� ������� ����� ���
					NewJ = TreeOperations.rootCount();
					// ���������� ����� ���; ������� ������� �������������� ����������
					ItselfOrganizationVariables.QControl.add(3, SimulationCountFX.t, NewJ - OldJ, 0);
					OldJ = NewJ;
					if ( NewJ < rootMinCount)
						ItselfOrganizationVariables.rootValueNotValid = true;
					else // ������� ���� �������������� �������, ���� ��� ����������
						ItselfOrganizationVariables.rootValueNotValid = false;
				} // ����� ��������� ��� ��� ��

				if( ItselfOrganizationVariables.rootValueNotValid && ( ItselfOrganizationVariables.reconfigurationOrgAllowed || ItselfOrganizationVariables.addNewElementsOrgAllowed ) )
				{ // ���� ����� �� � �������� ���������������� ��� ����������
					// ��������� ������ ����� ��, � ������� ����� ���� ����-������
					ItselfOrganizationVariables.interTypesMayHaveLeafChild.clear();
					ItselfOrganizationVariables.interTypesMayHaveLeafChild.addAll( TypeLoader.findInterTypesMayHasLeafsChild() );
				}

				if( ItselfOrganizationVariables.rootValueNotValid && ItselfOrganizationVariables.reconfigurationOrgAllowed )
				{ // ���� ���������������� ���������� � ���������
					wasStructuralOrganizationAtThisStep = true; // ���� �� ���
					typeOfLastSSO = "��������������"; // ���������� ��� ���
					History.addRecord(SimulationCountFX.t); // ����� ������ ������� �� ��
					History.getForRests(false); // ��������� ������� �� ����������������
					new Restructurize(); // ��������� ���������������� 
					Main.tree.updateUI(); // ��������� ���
					NewJ = TreeOperations.rootCount(); // �������� �������� � �����
					// ����������� ���� ��������������; ������� �������������� ����������
					ItselfOrganizationVariables.QControl.add(4, SimulationCountFX.t, NewJ - OldJ, 0);
					OldJ = NewJ;
					if ( NewJ < rootMinCount)
						ItselfOrganizationVariables.rootValueNotValid = true;
					else // �� ������������� ���������� ���� �������������� �������
						ItselfOrganizationVariables.rootValueNotValid = false;
					History.getForRests(true); // ������ � ��������� ������� ����� ���
				}

				if( ItselfOrganizationVariables.rootValueNotValid && ItselfOrganizationVariables.addNewElementsOrgAllowed )
				{ // ���� ���������� � ��������� ���������� ����� ���������
					wasStructuralOrganizationAtThisStep = true; // ���� ���
					typeOfLastSSO = "����������"; // ���������� ��� ���
					History.addRecord(SimulationCountFX.t); // ����� ������ �������
					History.getForAdds(false); // ������ ��������� ������� �� ���
					new StructAddElement(); // ��������� ���������� ���������
					Main.tree.updateUI(); // ���������� ���
					if ( TreeOperations.rootCount() < rootMinCount)
						ItselfOrganizationVariables.rootValueNotValid = true;
					else // �� ������������� ������� ���� �������������� �������
						ItselfOrganizationVariables.rootValueNotValid = false;
					History.getForAdds(true); // ������ ��������� ������� ����� ���
				}
			}
		} // ����� ��������� ��������, ����� ��������� ������������ ����������� ����
		else 
		{ 	// ���� ������������ �������� ������������� ������ �� ����������
			// ������ ���� �� �� ������ ������������ �����
			boolean EOS = false; // ���� ����� ������� ���������������
			while (ItselfOrganizationVariables.rootValueNotValid && !EOS) {
				// ���� ����������� �� ��� ���, ���� ������� �� ���������������
				// ��������� ��� �������� ����������� ���� �� �� ������������
				// �����, ������� ������ ������������: ������������ ��� ������������
				int level = (ItselfOrganizationVariables.UseNeuralSOTypeChoosing)? ItselfOrganizationVariables.QControl.GetBest_neural(SimulationCountFX.t) : ItselfOrganizationVariables.QControl.GetBest_emperical(SimulationCountFX.t);// ������� ����������� ��� ��
				// ������� �������� � ����� �� ���������� ��
				double OldJ = TreeOperations.rootCount(), NewJ = OldJ;
				switch(level)
				{ // ������ ���� ���� ���������������, ������� ��������� ��������
				case 1:{ // ���
					if( ItselfOrganizationVariables.paramOrgAllowed )
					{ // ���� ��� �� ��������� � �������
						if( ItselfOrganizationVariables.leafsList.size() > 0 )
						{ // ���� � ������� ���� ���� �� ���� ����
							History.addRecord(SimulationCountFX.t); // ����� ������ � �������
							History.getForParams(false); // ��������� ������� �� ��� 
							
							//TODO parameter organization call was replaced
							//new SingleParamOrganization( rootMinCount ); // ������ ���
							ItselfOrganizationVariables.getInstance().runPSO(rootMinCount);
							
							History.getForParams(true); // ��������� ������� ����� ���
							NewJ = TreeOperations.rootCount(); // ������� �����������
							// ���������� �����; ������� ��������������� �������������� ����������
							ItselfOrganizationVariables.QControl.add(1, SimulationCountFX.t, NewJ - OldJ, 0);
							wasItselfOrganizationAtThisStep = true; // ���� � ���
						}				
					}
					break;
				}
				case 2:{ // ��� 
					wasItselfOrganizationAtThisStep = true; // ���� ���
					// ���� ���� �� ���� �� ����� �� (����� ���) ��������
					if( ItselfOrganizationVariables.funcLeafOrgAllowed ||
							ItselfOrganizationVariables.funcIntermediateOrgAllowed
							|| ItselfOrganizationVariables.typeLeafOrgAllowed 
							|| ItselfOrganizationVariables.typeIntermediateOrgAllowed 
							|| ItselfOrganizationVariables.reconfigurationOrgAllowed
							|| ItselfOrganizationVariables.addNewElementsOrgAllowed)
					{ // ������� ������ ���� �������� �� ��� ������� ���� ��
						new FindMaxTypesValues();
					}

					// ��� ��� ������			
					if( ItselfOrganizationVariables.funcLeafOrgAllowed  )
					{ // ���� ��� ��� �� ���������, ��
						History.addRecord(SimulationCountFX.t); // ������ � �������
						History.getForFuncs(false); // ��������� ������� �� ���
						new FuncOrgForLists(); // ������ ��� ��� ������
						wasFunctionsOrganizationAtThisStep = true; // ���� ���
						NewJ = TreeOperations.rootCount(); // �������� �������� � �����
						if ( NewJ < rootMinCount)
							ItselfOrganizationVariables.rootValueNotValid = true;
						else // �� ������������� ������� ���� �������������� �������
							ItselfOrganizationVariables.rootValueNotValid = false;
						// ���������� ��������� ������� ����� ��� ���� ��� ��� ��
						// ���������� �� ����� (��� ��������� ��� ������� ���������) 
						if(!( ItselfOrganizationVariables.rootValueNotValid && ItselfOrganizationVariables.funcIntermediateOrgAllowed ))
							History.getForFuncs(true);
					}
					// ��� ��� ��			
					if( ItselfOrganizationVariables.rootValueNotValid && ItselfOrganizationVariables.funcIntermediateOrgAllowed )
					{ // ���� ������� ��� �� ��������� � ��� ��� �� ���������
						if(!ItselfOrganizationVariables.funcLeafOrgAllowed)
						{ // ���� ��� ��� �� �� ����������, ��������� ������ � �������
							History.addRecord(SimulationCountFX.t);
							History.getForFuncs(false); // ��������� ������� �� ���
						}
						new FunctionOrganization(); // ��� ��� ��
						History.getForFuncs(true); // ��������� ������� ����� ���
						wasFunctionsOrganizationAtThisStep = true; // ���� ���
						NewJ = TreeOperations.rootCount(); // �������� �����������
						if ( NewJ < rootMinCount)
							ItselfOrganizationVariables.rootValueNotValid = true;
						else // ��� ������������� ������� ���� �������������� �������
							ItselfOrganizationVariables.rootValueNotValid = false;
					}
					// ����������� ����; ������� ������������� ��������������� ����������
					ItselfOrganizationVariables.QControl.add(2, SimulationCountFX.t, NewJ - OldJ, 0);
					break;
				}
				case 3:{ // ������� ��
					if(ItselfOrganizationVariables.typeLeafOrgAllowed 
							|| ItselfOrganizationVariables.typeIntermediateOrgAllowed 
							|| ItselfOrganizationVariables.reconfigurationOrgAllowed
							|| ItselfOrganizationVariables.addNewElementsOrgAllowed)
					{ // ���� �������� ���� �� ���� �� ����� ���
						if( ItselfOrganizationVariables.useMaximumPrice )
						{ // ��������� ������� �������� �������� ��� ������
							TypeLoader.setAverageCharacteristicsForLeafs();
						}
						// ���������� ������ ��� �� ��� ������� �� ����� ��
						ItselfOrganizationVariables.findBestElementaryChildren();
						// ������������ ���������� ������� ������ ��� ������
						ItselfOrganizationVariables.setConvolutionFuncPriorForLeafs();
						// ���������� ������� ������� ��� ��� ����� ��
						TypeLoader.setAverageCharacteristicsForIntermediate();
						// ������� ��� ������������ � ������� ������� ��
						TypeLoader.fillingInterTypesContainingFuncLists();
						// �������� ����������� � NewJ: ������� ��� �.�. ����������
						NewJ = TreeOperations.rootCount();
						if ( NewJ < rootMinCount)
							ItselfOrganizationVariables.rootValueNotValid = true;
						else // �� ������������� ���������� ���� ��������������
							ItselfOrganizationVariables.rootValueNotValid = false;
						wasStructuralOrganizationAtThisStep = true; // ���� ���
						typeOfLastSSO = "�����������"; // ���������� ��� ���
					}
					// ������� �� ��� ������
					if( ItselfOrganizationVariables.rootValueNotValid && ItselfOrganizationVariables.typeLeafOrgAllowed )
					{ // ���� ��� ��� ������� ���������� � �� ���������
						History.addRecord(SimulationCountFX.t); // ������ � �������
						History.getForTypes(false); // ��������� ������� �� ���
						new TypeOrgForLeafs(); // ������ ��� ��� �������
						wasStructuralOrganizationAtThisStep = true; // ���� ���
						typeOfLastSSO = "������� ��� �������"; // ���������� ��� ���
						NewJ = TreeOperations.rootCount(); // �������� �����������
						// ���� ��� ��� �� ���������� �� �����, ���������� ��������� �������
						if ( NewJ < rootMinCount)
						{ 
							ItselfOrganizationVariables.rootValueNotValid = true;
							if (!ItselfOrganizationVariables.typeIntermediateOrgAllowed)
								History.getForTypes(true);	
						}
						else
						{ // �� ������������� ���������� ���� ��������������
							ItselfOrganizationVariables.rootValueNotValid = false;
							History.getForTypes(true);
						}
					}

					// ��� ��� ��						
					if( ItselfOrganizationVariables.rootValueNotValid && ItselfOrganizationVariables.typeIntermediateOrgAllowed )
					{ // ���� ��� ��� �� ��������� � ����������
						if (!ItselfOrganizationVariables.typeLeafOrgAllowed)
						{ // ���� ��� ��� �� �� �����������, ��������� ������ � �������
							History.addRecord(SimulationCountFX.t);
							History.getForTypes(false); // ��������� ������� �� ���
						}
						new TypeOrganization(); // ��������� ��� ��� ��
						wasStructuralOrganizationAtThisStep = true; // ������ ���� ���
						typeOfLastSSO = "�������"; // ���������� ��� ���
						History.getForTypes(true); // ��������� ������� ����� ���
						NewJ = TreeOperations.rootCount(); // �������� �����������
						if ( NewJ < rootMinCount)
							ItselfOrganizationVariables.rootValueNotValid = true;
						else // ��� ������������� ������� ���� ��������������
							ItselfOrganizationVariables.rootValueNotValid = false;
					}
					// ����������� ���� ���; ������� ��������� ����� ��������� ����������
					ItselfOrganizationVariables.QControl.add(3, SimulationCountFX.t, NewJ - OldJ, 0);
					break;
				}
				case 4:{ // ����������������
					if( ItselfOrganizationVariables.reconfigurationOrgAllowed )
					{ // ���� ���������������� ���������
						wasStructuralOrganizationAtThisStep = true; // ���� ���
						typeOfLastSSO = "��������������"; // ���������� ��� ���
						History.addRecord(SimulationCountFX.t); // ������ � �������
						History.getForRests(false); // ��������� ������� �� ���
						new Restructurize(); // ������ ����������������
						Main.tree.updateUI(); // ���������� ��� 
						NewJ = TreeOperations.rootCount(); // �������� �����������
						if ( NewJ < rootMinCount)
							ItselfOrganizationVariables.rootValueNotValid = true;
						else // �� ������������� ������� ���� ��������������
							ItselfOrganizationVariables.rootValueNotValid = false;
						History.getForRests(true);
					}
					// ����������� ���� ��; ������� ���������������� ����������
					ItselfOrganizationVariables.QControl.add(4, SimulationCountFX.t, NewJ - OldJ, 0);
					break;
				}
				case 6: // ���� ���� ���������� ���������� ��������� ���� �������������
				case 5:{ // ��������� ����� ��������
					if( ItselfOrganizationVariables.addNewElementsOrgAllowed )
					{ // ���� ���������� ����� ��������� �� ���������
						wasStructuralOrganizationAtThisStep = true; // ���� ���
						typeOfLastSSO = "����������"; // ������ ���� ���
						History.addRecord(SimulationCountFX.t); // ������ � �������
						History.getForAdds(false); // ��������� ������� �� ��
						new StructAddElement(); // ���������� ����� ���������
						Main.tree.updateUI(); // ���������� ���
						if ( TreeOperations.rootCount() < rootMinCount){
							ItselfOrganizationVariables.rootValueNotValid = true;
							EOS = true; // ���������� ����� ��������� ����������
						}
						else // ��� ������������� ���������� ���� ��������������
							ItselfOrganizationVariables.rootValueNotValid = false;
						History.getForAdds(true); // ��������� ������� ����� ��
					}
					break;
				}
				default:{
					EOS = true;// ��� ���� �� ���������!
				}
				}
			}
		}
		
		// ����� � ������� ���������� � ��������� ������� � ������ ������
		// ��� ������������� �������� ���� ��������� ������������� ����������
		TreeOperations.rootCount();
		// �������� �� ���� ��������� � ��������� ������� � ������� � �������
		// �������� ��� �� ���, ��� ������� ������������ �������� ���� ������
		TreeElement elementInStruct;
		for(int level=0; level<Main.structTreeElements.size(); level++)
		{ // ���� �� ������� ������
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
			{ // ���� �� ��������� ������� ������
				elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
				if( SimulationCountFX.visibleElementsIDList.contains( elementInStruct.ID ) )
				{ // ���� ���������, ������� � ������� ���������� � �������� ��������
					SimulationCountFX.tempProperty[SimulationCountFX.visibleElementsIDList.indexOf(elementInStruct.ID) + 1] = new SimpleStringProperty(Double.toString(elementInStruct.elementCount));
				}
			}
		}
		if(wasStructuralOrganizationAtThisStep == true) {
			// ���� �� ���� ���� ���� ���, ������� � ������� �������� � ����
			SimulationCountFX.tempProperty[SimulationCountFX.tableView.getColumns().size() - 1] = new SimpleStringProperty("��� " + typeOfLastSSO + ": " + Integer.toString(Main.JCountingCount));

		} else if (wasFunctionsOrganizationAtThisStep == true){
			// ���� ���� ���, ������� "���" � ���������� ��������� �����������
			SimulationCountFX.tempProperty[SimulationCountFX.tableView.getColumns().size() - 1] = new SimpleStringProperty("���: " + Integer.toString(Main.JCountingCount));
		} else if (wasItselfOrganizationAtThisStep == true) {
			// ���� ���� ���, ������� "���" � ���������� ��������� �����������
			SimulationCountFX.tempProperty[SimulationCountFX.tableView.getColumns().size() - 1] = new SimpleStringProperty("���: " + Integer.toString(Main.JCountingCount));
		}
		Main.tree.updateUI(); // ��������� ���
		// ��������� �������� ������������ �������
		if( ItselfOrganizationVariables.rootValueNotValid )
		{ 	// ���� ������� �� ��� �����������, ������ � �������� �����������
			// � ��������������� ����������. ������� ������������ ��� ����������
			JOptionPane.showMessageDialog(null, "� ���������, �� ������� �������� �������" +
					"\n � ���������� ���������. ���������� �������� ���������.");
			SimulationCountFX.stopsimulation = true; // ������������� ��������������
		}
	}
}
