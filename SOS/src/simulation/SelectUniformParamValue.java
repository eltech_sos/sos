package simulation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import object.TreeElement;

import Main.Main;
import Main.TreeOperations;
import Main.TypeLoader;
import simulation.UserChioceDialog;

import com.jidesoft.dialog.ButtonPanel;
import com.jidesoft.dialog.StandardDialog;
import com.jidesoft.swing.JideTitledBorder;
import com.jidesoft.swing.PartialEtchedBorder;
import com.jidesoft.swing.PartialSide;

public class SelectUniformParamValue extends StandardDialog
{
	protected JButton _okButton = new JButton("OK");
	protected JButton _cancelButton = new JButton("Cancel");
	
	private JComboBox parmNames_cb, parmValues_cb;
	
	ArrayList<String> parametersList;
	
	//key - name of parameter
	//value - list of values of this parameter meeting in model
	public static Map< String, ArrayList<Double> > parametrsValuesMap;
	
	
	ArrayList<Integer> selecedValuesIndexList;
	
	boolean mayChangeIndInParmValues_cb = true;
	
	public SelectUniformParamValue( ArrayList<String> paramList )
	{
		super( (Frame) null, "����� ������ �������� ��� ����������", true);
		
//		elementsList = new ArrayList<TreeElement>();
		parametersList = paramList;
		
		//creating for each parameter in parametersList
		//list of possible values and write it to parametrsValuesMap
		parametrsValuesMap = new HashMap< String, ArrayList<Double> >();
		ArrayList<Double> singleParamValuesList;// = new ArrayList<Double>();
		selecedValuesIndexList = new ArrayList<Integer>();
		TreeElement elementInStruct;
		String parameterName;
		double parameterValue;
		for( int i=0; i < parametersList.size(); i++)
		{
			parameterName = parametersList.get(i);
			singleParamValuesList = new ArrayList<Double>();
			for(int level=0; level<Main.structTreeElements.size(); level++)
			{
				for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
				{
					elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
					if( elementInStruct.type == 3 )//if element is list
					{
						if( elementInStruct.parametersValuesMap.containsKey( parameterName ) )
						{
							parameterValue = elementInStruct.parametersValuesMap.get( parameterName );
							if( ! singleParamValuesList.contains( parameterValue ) )
							{
								singleParamValuesList.add( parameterValue );
							}
						}
					}
					
				}
			}
			parametrsValuesMap.put( parameterName, singleParamValuesList );
			selecedValuesIndexList.add( 0 );
		}
		
		addCancelAction();
		
		addWindowListener(new WindowAdapter() 
		{
		    public void windowClosing(WindowEvent e) 
		    {
		    	UserChioceDialog.childDialogWasCanceled = true;
		    	setDialogResult(RESULT_CANCELLED);
		    	setVisible(false);
		    	dispose();
		   }
		});

		
		setToCenter();
		
		setVisible(true);
	}
	
	public JComponent createBannerPanel() 
	{
		return null;
	}

	public ButtonPanel createButtonPanel()
	{
		ButtonPanel okCancelPanel = new ButtonPanel();
		_okButton.setName(OK);
		_cancelButton.setName(CANCEL);
		okCancelPanel.addButton(_okButton, ButtonPanel.AFFIRMATIVE_BUTTON);
		okCancelPanel.addButton(_cancelButton, ButtonPanel.CANCEL_BUTTON);
		
		_okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				okActionPerformed(e);
			}
		});
		_cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				cancelActionPerformed(e);
			}
		});
		setDefaultCancelAction(_cancelButton.getAction());
		setDefaultAction(_okButton.getAction());
		getRootPane().setDefaultButton(_okButton);
		okCancelPanel.setBorder( createButtonPanelBorder() );
		return okCancelPanel;
	}
	
	public static Border createButtonPanelBorder()
	{
		return BorderFactory.createCompoundBorder(new JideTitledBorder(
				new PartialEtchedBorder(PartialEtchedBorder.LOWERED,
						PartialSide.NORTH), ""), BorderFactory
				.createEmptyBorder(9, 6, 9, 6));
	}
	
	public JComponent createContentPanel() 
	{
		JPanel mainContainer = new JPanel();
		mainContainer.setLayout( new BorderLayout() );
		
		JLabel paramName_lb = new JLabel("  ��������  ");
		JLabel paramValue_lb = new JLabel("  ��������  ");

		parmNames_cb = new JComboBox();
        try
		{
        	parmNames_cb.removeAllItems();
 			for(int i=0; i< parametersList.size(); i++)
 			{
 				parmNames_cb.addItem( parametersList.get(i) );
 			}
	    } 
		catch (Exception ef) 
		{	
			ef.printStackTrace();	
		}
		
		parmNames_cb.addItemListener(new ItemListener()
		{
				public void itemStateChanged(ItemEvent event) {
						try
						{
							mayChangeIndInParmValues_cb = false;
							String currentParamName;
							parmValues_cb.removeAllItems();
							currentParamName = parmNames_cb.getSelectedItem().toString();
							ArrayList<Double> tmpList = parametrsValuesMap.get( currentParamName );						 
							for(int i=0; i<tmpList.size(); i++)
							{
								parmValues_cb.addItem( tmpList.get(i) );
							}
							parmValues_cb.setSelectedIndex( selecedValuesIndexList.get( parametersList.indexOf(currentParamName) ) );
							mayChangeIndInParmValues_cb = true;
						} 
						catch (Exception ef) 
						{	
							ef.printStackTrace();	
						}
			
				}
		});
		
		parmValues_cb = new JComboBox();
        try
		{
        	parmValues_cb.removeAllItems();
        	ArrayList<Double> tmpList = parametrsValuesMap.get( parmNames_cb.getSelectedItem().toString() );
			for(int i=0; i<tmpList.size(); i++)
			{
				parmValues_cb.addItem( tmpList.get(i) );
			}    		
	   }
        catch (Exception ef) 
    	{	
    		ef.printStackTrace();	
    	}
        
        parmValues_cb.addItemListener(new ItemListener()
		{
				public void itemStateChanged(ItemEvent event) {
						try
						{
							String currentParameterName;
							currentParameterName = parmNames_cb.getSelectedItem().toString();
//							double selectedValue = Double.parseDouble( parmValues_cb.getSelectedItem().toString() );
							int selectedInd = parmValues_cb.getSelectedIndex();
							if( ( selectedInd >= 0 ) && mayChangeIndInParmValues_cb )
							{
								selecedValuesIndexList.set( parametersList.indexOf(currentParameterName), selectedInd );
							}
							
						} 
						catch (Exception ef) 
						{	
							ef.printStackTrace();	
						}
			
				}
		});
        
        JPanel firstPanel = new JPanel();
		firstPanel.setLayout( new BorderLayout() );
		firstPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
		firstPanel.add( paramName_lb, BorderLayout.WEST );
		firstPanel.add( parmNames_cb, BorderLayout.CENTER );
		firstPanel.add( new JLabel("                                             "), BorderLayout.EAST );
		
		JPanel secondPanel = new JPanel();
		secondPanel.setLayout( new BorderLayout() );
		secondPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
		secondPanel.add( paramValue_lb, BorderLayout.WEST );
		secondPanel.add( parmValues_cb, BorderLayout.CENTER );
		secondPanel.add( new JLabel("                                             "), BorderLayout.EAST );
		secondPanel.add( new JLabel("                                                                                         "), BorderLayout.SOUTH );
		
		JPanel lev2_secondPanel = new JPanel();	
		lev2_secondPanel.setLayout( new BorderLayout() );
		lev2_secondPanel.add( firstPanel, BorderLayout.NORTH );
		lev2_secondPanel.add( secondPanel, BorderLayout.SOUTH );
		
		JPanel lev2_firstPanel = new JPanel();	
		lev2_firstPanel.setLayout( new BorderLayout() );
		lev2_firstPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
		lev2_firstPanel.add( new JLabel("  �������� ������� �������� ��� ������� ���������:"), BorderLayout.SOUTH );
		
		mainContainer.add( lev2_firstPanel, BorderLayout.NORTH );
		mainContainer.add( lev2_secondPanel, BorderLayout.SOUTH );
		
		
	    setSize( 500, 230 );
		setResizable(false);
		
		return mainContainer;
	}
	
	public static Border createContentPanelBorder()
	{
		return BorderFactory.createEmptyBorder(10, 10, 0, 10);
	}
	
	
	protected void okActionPerformed(ActionEvent e)
	{
		ArrayList<Double> paramValuesList = new ArrayList<Double>();
		for(int i=0; i < parametersList.size(); i++)
		{
			paramValuesList.add( parametrsValuesMap.get( parametersList.get(i) ).get( selecedValuesIndexList.get(i) ) );
		}
		TreeOperations.setParamInAllModelByValues( parametersList, paramValuesList );
		
		setDialogResult(RESULT_AFFIRMED);
		setVisible(false);
		dispose();
		
	}
	
	protected void addCancelAction()
	{
		Action action = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent ae)
			{
				cancelActionPerformed(ae);
			}
		};

		KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		rootPane.getActionMap().put(action, action);
		rootPane.getInputMap(JComponent.WHEN_FOCUSED).put(stroke, action);
		rootPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(stroke, action);

	}
	
	protected void cancelActionPerformed(ActionEvent e)
	{
		UserChioceDialog.childDialogWasCanceled = true;
		setDialogResult(RESULT_CANCELLED);
		setVisible(false);
		dispose();
	}
	
	public void setToCenter() 
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		screenSize.height = screenSize.height/2;
		screenSize.width = screenSize.width/2;
		int y = screenSize.height - 300/2;
		int x = screenSize.width - 500/2;
		this.setLocation( x, y );
	}
}
