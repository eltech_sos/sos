package simulation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import object.TreeElement;

import Main.Main;
import Main.TreeOperations;
import simulation.UserChioceDialog;

import com.jidesoft.dialog.ButtonPanel;
import com.jidesoft.dialog.StandardDialog;
import com.jidesoft.swing.JideTitledBorder;
import com.jidesoft.swing.PartialEtchedBorder;
import com.jidesoft.swing.PartialSide;

public class ChooseNewTypes extends StandardDialog
{
	protected JButton _okButton = new JButton("OK");
	protected JButton _cancelButton = new JButton("Cancel");
	
	private JComboBox typeNames_cb, funcList_cb;
	
	public ChooseNewTypes( ArrayList<String> oldTypeList, ArrayList<String> oldFuncList, ArrayList<Integer> typeSlotsMinNumberList )
	{
		super( (Frame) null, "����� ������ �������� ��� ����������", true);
				
		addCancelAction();
		
		addWindowListener(new WindowAdapter() 
		{
		    public void windowClosing(WindowEvent e) 
		    {
		    	UserChioceDialog.childDialogWasCanceled = true;
		    	setDialogResult(RESULT_CANCELLED);
		    	setVisible(false);
		    	dispose();
		   }
		});

		
		setToCenter();
		
		setVisible(true);
	}
	
	public JComponent createBannerPanel() 
	{
		return null;
	}

	public ButtonPanel createButtonPanel()
	{
		ButtonPanel okCancelPanel = new ButtonPanel();
		_okButton.setName(OK);
		_cancelButton.setName(CANCEL);
		okCancelPanel.addButton(_okButton, ButtonPanel.AFFIRMATIVE_BUTTON);
		okCancelPanel.addButton(_cancelButton, ButtonPanel.CANCEL_BUTTON);
		
		_okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				okActionPerformed(e);
			}
		});
		_cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				cancelActionPerformed(e);
			}
		});
		setDefaultCancelAction(_cancelButton.getAction());
		setDefaultAction(_okButton.getAction());
		getRootPane().setDefaultButton(_okButton);
		okCancelPanel.setBorder( createButtonPanelBorder() );
		return okCancelPanel;
	}
	
	public static Border createButtonPanelBorder()
	{
		return BorderFactory.createCompoundBorder(new JideTitledBorder(
				new PartialEtchedBorder(PartialEtchedBorder.LOWERED,
						PartialSide.NORTH), ""), BorderFactory
				.createEmptyBorder(9, 6, 9, 6));
	}
	
	public JComponent createContentPanel() 
	{
		JPanel mainContainer = new JPanel();
		mainContainer.setLayout( new BorderLayout() );
		
		JLabel typeName_lb = new JLabel("  ���      ");
		JLabel funcList_lb = new JLabel("  �������  ");

		
		
		
        JPanel firstPanel = new JPanel();
		firstPanel.setLayout( new BorderLayout() );
		firstPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
		firstPanel.add( typeName_lb, BorderLayout.WEST );
		firstPanel.add( typeNames_cb, BorderLayout.CENTER );
		firstPanel.add( new JLabel("                                             "), BorderLayout.EAST );
		
		JPanel secondPanel = new JPanel();
		secondPanel.setLayout( new BorderLayout() );
		secondPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
		secondPanel.add( funcList_lb, BorderLayout.WEST );
		secondPanel.add( funcList_lb, BorderLayout.CENTER );
		secondPanel.add( new JLabel("                                             "), BorderLayout.EAST );
		secondPanel.add( new JLabel("                                                                                         "), BorderLayout.SOUTH );
		
		JPanel lev2_secondPanel = new JPanel();	
		lev2_secondPanel.setLayout( new BorderLayout() );
		lev2_secondPanel.add( firstPanel, BorderLayout.NORTH );
		lev2_secondPanel.add( secondPanel, BorderLayout.SOUTH );
		
		JPanel lev2_firstPanel = new JPanel();	
		lev2_firstPanel.setLayout( new BorderLayout() );
		lev2_firstPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
		lev2_firstPanel.add( new JLabel("  �������� ����� ����:"), BorderLayout.SOUTH );
		
		mainContainer.add( lev2_firstPanel, BorderLayout.NORTH );
		mainContainer.add( lev2_secondPanel, BorderLayout.SOUTH );
		
		
	    setSize( 500, 230 );
		setResizable(false);
		
		return mainContainer;
	}
	
	public static Border createContentPanelBorder()
	{
		return BorderFactory.createEmptyBorder(10, 10, 0, 10);
	}
	
	
	protected void okActionPerformed(ActionEvent e)
	{
		
		setDialogResult(RESULT_AFFIRMED);
		setVisible(false);
		dispose();
		
	}
	
	protected void addCancelAction()
	{
		Action action = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent ae)
			{
				cancelActionPerformed(ae);
			}
		};

		KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		rootPane.getActionMap().put(action, action);
		rootPane.getInputMap(JComponent.WHEN_FOCUSED).put(stroke, action);
		rootPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(stroke, action);

	}
	
	protected void cancelActionPerformed(ActionEvent e)
	{
		UserChioceDialog.childDialogWasCanceled = true;
		setDialogResult(RESULT_CANCELLED);
		setVisible(false);
		dispose();
	}
	
	public void setToCenter() 
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		screenSize.height = screenSize.height/2;
		screenSize.width = screenSize.width/2;
		int y = screenSize.height - 300/2;
		int x = screenSize.width - 500/2;
		this.setLocation( x, y );
	}

}
