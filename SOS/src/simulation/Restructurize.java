//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// �������� ���������������� �������������� ������
// � ���� ���������������� ��������� �������������� ������������ 
// ��������� ������� ����� �������, ����� ��������� �������� � ��������
// �����������
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
package simulation;

import java.util.Collections;
import java.util.Vector;
import javax.swing.tree.TreePath;
import object.TreeElement;
import Main.AddElemFromSimulation;
import Main.Main;
import Main.RemoveSelection;
import Main.TreeOperations;
import Main.TypeLoader;
import simulation.FuncOrgForLists;
import simulation.FunctionOrganization;
import simulation.ItselfOrganizationVariables;

public class Restructurize 
{
	// �����, ����������� ������������ �������
	public class Parent
	{
		public double dJ = Double.NEGATIVE_INFINITY; // ������� �����������
		public int level = -1,  // ������� ��������
		index = -1, // ������ �������� �� ������
		ID = -1; // ���������� ������������� ��������
	}
	
	public Vector<Parent> BestParents; // ������ ������ ���������
	public Vector<Parent> WorstParents; // ������ ������ ���������
	public Vector<String> TypesList;  // ������ �����
	public double J; // �������� � �������� �����������
	public TreeElement CurElem; // ������� ������� 
	public TreeElement ChildElem; // ������� �������� �������
	public int TypeIndex; // ������ ����
	public int LevelIndex; // ������ ������
	public int ElemIndex; // ������ ��������
	public int ChildIndex; // ������ ��������� ��������
	public Vector<Vector<TreeElement>> Backup; // ����� ������ �������
	public Vector<TreeElement> LevelList; // ������ ��������� �� ������
	public Vector<TreeElement> WorstLevel; // ������� ������ ������ (��� ��-��)
	public double realMinRootValue; // ����������� �������� �����������
	public int TypesCount;  // ����� ���������� ����� (��+��)
	public int BestType; // ������ ���
	public int realCount; // ���������� ��������� � ������ (�����)
	public int realLevelCount; // ����� ���������� ������� (��� ��������������)

	//---------------------------------------------------------------------------
	// �������-�����������
	//---------------------------------------------------------------------------
	public Restructurize() 
	{
		do 
		{
			//��������� ����� �������
			J = TreeOperations.rootCount(); //������� �������� ��������� �����������
			ChildElem = null; //������� �������� �������
			//��������� ���������� ���������� �������� � �����
			realMinRootValue = ItselfOrganizationVariables.rootMinimumCount; 
			realCount = Main.count; //��������� ���������� ��������� � ������
			realLevelCount = Main.maxLevelsCount; //��������� ���������� �������
			//�������� ������� ����������� �� �������� � �����
			ItselfOrganizationVariables.rootMinimumCount = Double.POSITIVE_INFINITY;
			Backup = new Vector<Vector<TreeElement>>(); //��������������
			//�������� �� ���� �������
			for (int i = 0; i < Main.structTreeElements.size(); i++) 
			{
				//������� ����� ��������
				Backup.add(new Vector<TreeElement>());
				//�������� �� ���� ��������� ������
				for (int j = 0; j < Main.structTreeElements.elementAt(i).size(); j++)
				{
					//�������� �������
					Backup.lastElement().add((TreeElement) 
							Main.structTreeElements.elementAt(i).get(j).clone());
				}
			}
			
			//��������� ������ �����
			TypesList = new Vector<String>();
			//��������� ���� ��
			TypesList.addAll(TypeLoader.elementaryTypesList);
			//��������� ���� ��
			TypesList.addAll(TypeLoader.intermediaryTypesList);
			//������������ ������ �������
			TypesCount = TypesList.size();
			
			//�������������� ������ ������ � ������ ���������
			BestParents = new Vector<Parent>(TypesCount);//������ ��������
			WorstParents = new Vector<Parent>(TypesCount); //������ ��������
			LevelList = new Vector<TreeElement>(); //������ ��������� �� ������
			WorstLevel = new Vector<TreeElement>(); //������� ������� ��������

			// ����������� ��������� ��������� � �������
			//�������� �� ���� �����
			for (TypeIndex = 0; TypeIndex < TypesCount; TypeIndex++) 
			{
				//��������� ���������
				WorstParents.add(TypeIndex, new Parent());
				//������������� ������� ����������� � -�������������
				WorstParents.lastElement().dJ = Double.POSITIVE_INFINITY;
				//�������� �� ���� �������
				for (LevelIndex = 0; 
					LevelIndex < Main.structTreeElements.size() - 1; LevelIndex++) 
				{
					//�������� �������� ������
					LevelList.addAll(Main.structTreeElements.get(LevelIndex));
					//�������� ��� �������� �������� � "������ �������"
					WorstLevel.addAll(Main.structTreeElements.get(LevelIndex + 1));
					//�������� �� ���� ��������� ������� ������
					for (ElemIndex = 0; ElemIndex < LevelList.size(); ElemIndex++) 
					{
						//������� �������
						CurElem = LevelList.get(ElemIndex);
						// ���� ��� ��
						if (TypeLoader.intermediaryTypesList.contains(CurElem.typeName)) 
						{
							//�������� �� ���� �������� ���������
							for (ChildIndex = 0; ChildIndex < WorstLevel.size(); ChildIndex++) 
							{
								//������� �������� �������
								ChildElem = WorstLevel.get(ChildIndex);
								//���� ��� ��������� � ������� � ��� ���� (��� �����)
								if (ChildElem.typeName.equals(TypesList.get(TypeIndex)) && 
										ChildElem.iChildrenCount < 1) 
								{
									tmpDelElem(LevelIndex + 1, ChildIndex); // �������
																			// ����
																			// �������
									Optimize_R(); // ��������� ���
									double dJ = J - TreeOperations.rootCount(); // ���������
																				// ��������
																				// �
																				// �����
									//���� ���� ������� ���� ������� �������
									if (WorstParents.get(TypeIndex).dJ > dJ) 
									{
										WorstParents.get(TypeIndex).dJ = dJ; //�������������� �������
										WorstParents.get(TypeIndex).level = LevelIndex;//������� ��������
										WorstParents.get(TypeIndex).index = ElemIndex;//������ ��������
										WorstParents.get(TypeIndex).ID = CurElem.ID;//�������. ��������
									}
									// ��������������� �������� ������� �� �����
									restoreSystem();
									break; // ��������� ������� ���� ������
											// �����. ������� �� �����.
								}
							}// for �� NextLevel
						}
					}// for �� ElemIndex
					LevelList.clear(); //�������
					WorstLevel.clear(); //�������
				}// for �� LevelIndex
			}// for �� TypeIndex
			LevelList.clear(); //�������
			WorstLevel.clear(); //�������
			// ����������� ��������� ��������� � �������
			//�������� �� ���� �����
			for (TypeIndex = 0; TypeIndex < TypesCount; TypeIndex++) 
			{
				//������� ���������
				BestParents.add(TypeIndex, new Parent());
				//���� ������ ��������� ��� - �������
				if (WorstParents.get(TypeIndex).level == -1)
					continue;
				//�������� �� ���� ������� �������
				for (LevelIndex = 0; 
					LevelIndex < Main.structTreeElements.size(); LevelIndex++) 
				{
					//�������� ������� �������
					LevelList.addAll(Main.structTreeElements.get(LevelIndex));
					//������� ������� ����� ������� �������� ������� ����
					WorstLevel.addAll(Main.structTreeElements.get(WorstParents.get(TypeIndex).level + 1));
					//�������� �� ���� ��������� ������
					for (ElemIndex = 0; ElemIndex < LevelList.size(); ElemIndex++) 
					{
						//������� �������
						CurElem = LevelList.get(ElemIndex);
						// ����������,������� �� ���� ������� ��
						if (TypeLoader.intermediaryTypesList.contains(CurElem.typeName)) 
						{
							// ���� � ����� �� ������ ������������ �� ��� ��
							// ��������������, ���������� ���.
							if (TypeLoader.getChildrenType(CurElem.typeName) == 1 
									&& TypeIndex >= TypeLoader.elementaryTypesList.size())
								continue;
							if (TypeLoader.getChildrenType(CurElem.typeName) == 2 
									&& TypeIndex < TypeLoader.elementaryTypesList.size())
								continue;
							//���� ���� ��������� �����
							if (CurElem.iChildrenCount < TypeLoader.getSlotsNumber(CurElem.typeName)) 
							{
								//�������� �� ��������� ������ ����� ������� �������� ��� ������� ����
								for (ChildIndex = 0; ChildIndex < WorstLevel.size(); ChildIndex++) {
									//������� �������� �������
									ChildElem = WorstLevel.get(ChildIndex);
									//���� ��������� ��� � ��� ���� � �������� - ������
									if (ChildElem.typeName.equals(TypesList.get(TypeIndex)) 
											&& ChildElem.iChildrenCount < 1 && 
											ChildElem.ParentID == WorstParents.get(TypeIndex).ID) {
										tmpAdd(LevelIndex, ElemIndex, TypeIndex); // ���������
									    // � �������� �������� ��� ���� ��������� ����
										tmpDelElem(WorstParents.get(TypeIndex).level + 1,ChildIndex); 
										// ������� ��������� �������
										break; // ��������� ������� ���� ������
												// �����. ������� �� �����.
									}
								}// 
								Optimize_R(); // ��������� ���
								double dJ = TreeOperations.rootCount() - J; 
								// ��������� �������� � �����
								//���� ���������� �����
								if (BestParents.get(TypeIndex).dJ < dJ) 
								{
									BestParents.get(TypeIndex).dJ = dJ; //�������������� �������
									BestParents.get(TypeIndex).level = LevelIndex; //������� ��������
									BestParents.get(TypeIndex).index = ElemIndex; //������
									BestParents.get(TypeIndex).ID = CurElem.ID; //�������������
								}
								// ��������������� �������� ������� �� �����
								restoreSystem();
							}
						}
					}// for �� ElemIndex
					LevelList.clear();//�������
					WorstLevel.clear(); //�������
				}// for �� LevelIndex
			}// for �� TypeIndex
			// ��������� ������ ������� ����������� ������ �����
			//������ ���
			BestType = 0;
			//�������� �� ���� �����
			for (TypeIndex = 1; TypeIndex < TypesCount; TypeIndex++)
				//���� ������� ��������� ����������� �����
				if (BestParents.get(BestType).dJ < BestParents.get(TypeIndex).dJ)
					// �������������� ������ ���
					BestType = TypeIndex;
			//��������������� �����������
			ItselfOrganizationVariables.rootMinimumCount = realMinRootValue;
			//���� �������� � ��������� �������� �������
			if (BestParents.get(BestType).ID == WorstParents.get(BestType).ID)
			{
				restoreSystem(); //��������������� ��������
				return;
			}
			move(); // ������������ ���������� �������
			Optimize_R(); // ��������� ���
		} while (TreeOperations.rootCount() < realMinRootValue);
	}//����������� Restructurize(double);
	
	//---------------------------------------------------------------------------
	// ��������� �������� ��������
	//---------------------------------------------------------------------------
	public void tmpDelElem(int lvl, int ind)
	{
		// ��� ����, ����� ������� �� ���������� ��� �������� ��������� �����������,
		// ���������� ������ ������� ��� �� ������� structTreeElements
		//������������� ��������
		int PID = Main.structTreeElements.get(lvl).get(ind).ParentID;
		//������� �������
		Main.structTreeElements.get(lvl).removeElementAt(ind);
		//���� ��� ��� ��������� ������� ������
		if (Main.structTreeElements.get(lvl).size() < 1) 
		{
			Main.structTreeElements.removeElementAt(lvl); //������� �������
			Main.maxLevelsCount--; //��������� ���������� �������
		}
		//��������� ����� ���������� �������
		Main.count--; //���������� ��������� � �������
		Vector<TreeElement> tmpV;
		//������������ �������
		int i = lvl - 1;
		//�������� ������������ �������
		tmpV = Main.structTreeElements.get(i);
		for (int j = 0; j < tmpV.size(); j++){
			//��������� ���������� ����� � �������� ���������� ��������
			if (tmpV.get(j).ID == PID){
				tmpV.get(j).iChildrenCount--;
				return; //�������
			}
		}
	}
	
	//---------------------------------------------------------------------------
	// �������������� �������
	//---------------------------------------------------------------------------
	public void restoreSystem()
	{
		Main.count = realCount; //���������� ��������� � �������
		Main.maxLevelsCount = realLevelCount; //���������� �������	
		Main.structTreeElements.clear(); //������� ������
		//�������� �� �������
		for (int i = 0; i < Backup.size(); i++)
		{
			//��������� �������
			Main.structTreeElements.add(new Vector<TreeElement>());
			//�������� �� ���� ��������� ������
			for (int j = 0; j < Backup.elementAt(i).size(); j++){
				//��������� �������
				Main.structTreeElements.lastElement().add((TreeElement) Backup.elementAt(i).get(j).clone());
			}
		}
		//������� ����������, ������� ���������� ���������� ��������������� ��
		ItselfOrganizationVariables.getInstance().modelChanged(null);
		//TODO cleanup
		//ItselfOrganizationVariables.isFirstParamOrg = true;
		//������� ������ ������
		ItselfOrganizationVariables.leafsList.clear();
		TreeElement elementInStruct;
		//�������� �� ���� �������
		for(int level=0; level<Main.structTreeElements.size(); level++)
		{
			//�������� �� ���� ��������� ������
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
			{
				elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
				//���� ��� ����
				if( elementInStruct.type == 3 )
				{
					//��������� ��� � ������ ������
					ItselfOrganizationVariables.leafsList.add( elementInStruct );
				}
			}
		}
	}
	
	//---------------------------------------------------------------------------
	// ��������� ���������� ��������
	//---------------------------------------------------------------------------
	public void tmpAdd(int lvl, int ind, int type)
	{
		//���������� ������������� ������ ��������
		int ID = -1;
		Vector<TreeElement> tmpV;
		//�������� �� ���� �������
		for (int i = 0; i < Main.structTreeElements.size(); i++)
		{
			//�������� �� ���� ��������� ������
			tmpV = Main.structTreeElements.get(i);
			for (int j = 0; j < tmpV.size(); j++)
			{
				//���� ID ������
				if (tmpV.get(j).ID > ID)
					ID = tmpV.get(j).ID; //���������� ������������ id
			}
		}
		//���� ��� ��������� �������
		if (Main.structTreeElements.size() -1 == lvl)
		{
			//������� ����� �������
			Main.structTreeElements.add(new Vector<TreeElement>());
			Main.maxLevelsCount++; //����������� ���������� �������
		}
		//������� ����� �������
		TreeElement tmp = new TreeElement(ID + 1, 
				Main.structTreeElements.get(lvl).get(ind).ID, ChildElem.type);
		tmp.elementName = "Temporary Added"; //��� ��������
		tmp.elementCount = ChildElem.elementCount; //�������� � ��������
		tmp.functionName = ChildElem.functionName; //������� ��������
		tmp.iChildrenCount = ChildElem.iChildrenCount; //���������� �����
		tmp.Level = lvl + 1; //������� ��������
		tmp.parametersValuesMap = ChildElem.parametersValuesMap; //����� ����������
		tmp.parametrsWeightMap = ChildElem.parametrsWeightMap;//����� �����
		tmp.typeName = ChildElem.typeName; //��� ��������
		Main.structTreeElements.get(lvl + 1).add(tmp);
		//��������� �������
		Main.structTreeElements.get(lvl).get(ind).iChildrenCount++;
		//��������� ����� ���������� �������
		Main.count++; //���������� ��������� � �������
		//Main.elemID++; //������ ��������� �������������
	}
	
	//---------------------------------------------------------------------------
	// ������� ��������
	//---------------------------------------------------------------------------
	public void move()
	{
		TreeElement NewElem = null; //������� ����� �������
		LevelList.clear(); //������� �������� �������� ������
		//������� ���������� ��������
		LevelIndex = WorstParents.get(BestType).level + 1;
		//for (LevelIndex = 0; LevelIndex < Main.structTreeElements.size() - 1; LevelIndex++){
		//������� ���� ������� ���������� ��������	
		LevelList.addAll(Main.structTreeElements.get(LevelIndex));
		//�������� �� ���� ��������� ������	
		for (ElemIndex = 0; ElemIndex < LevelList.size(); ElemIndex++)
		{
			//������� �������	
			CurElem = LevelList.get(ElemIndex);
			//���� ����� ��������� �������	
			if (CurElem.ParentID == WorstParents.get(BestType).ID && 
					CurElem.typeName.equals(TypesList.get(BestType)))
			{
				//������� ����� ������� � ��. � ����� ����������, ����������� � ������� ��������	
				NewElem = new TreeElement(CurElem.ID, BestParents.get(BestType).ID, CurElem.type);
				NewElem.elementName = CurElem.elementName; //�������� ���
				NewElem.elementCount = CurElem.elementCount; //��������
				NewElem.functionName = CurElem.functionName; //�������
				NewElem.iChildrenCount = CurElem.iChildrenCount; //���������� �����
				NewElem.Level = BestParents.get(BestType).level + 1; //�������=������� ������� ��������+1
				NewElem.parametersValuesMap = CurElem.parametersValuesMap;//����� ����������
				NewElem.parametrsWeightMap = CurElem.parametrsWeightMap;//����� �����
				NewElem.typeName = CurElem.typeName; //���
				ItselfOrganizationVariables.leafsList.remove(CurElem); //������� ��������� �������
				//�������� ������� (� �.�. � ���������� ����������)
				new AddElemFromSimulation(NewElem, Main.structTreeElements.get
						(BestParents.get(BestType).level).get(BestParents.get(BestType).index));
				//���������� ��������� � ���������� ������
				Main.tree.setSelectionPath(new TreePath(Main.structTreeElements.get(LevelIndex).get(ElemIndex).element.getPath()));
				//Main.structTreeElements.get(WorstParents.get(BestType).level).get(WorstParents.get(BestType).index).iChildrenCount--;
				new RemoveSelection(false);
				//�������� ������� � ������ ������
				ItselfOrganizationVariables.leafsList.add(NewElem);
				ElemIndex = LevelList.size(); //������
				}
			}// for �� ElemIndex
			LevelList.clear();
		
	}
	
	//---------------------------------------------------------------------------
	// ���������� � ��� � ����� ���
	//---------------------------------------------------------------------------
	public void Optimize_R()
	{
		//������� ����������, ������� ���������� ���������� ���
		ItselfOrganizationVariables.getInstance().modelChanged(null);
		//TODO cleanup
		//ItselfOrganizationVariables.isFirstParamOrg = true;
		//������� ������ ������ �������
		ItselfOrganizationVariables.leafsList.clear();
		TreeElement elementInStruct;
		//�������� �� ���� �������
		for(int level=0; level<Main.structTreeElements.size(); level++)
		{
			//�������� �� ���� ��������� ������� ������
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
			{
				//���� ��� ��
				elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
				if( elementInStruct.type == 3 )
				{
					//��������� ��� � ������ ������
					ItselfOrganizationVariables.leafsList.add( elementInStruct );
				}
			}
		}
		//���� �������������� �� ��� �� ���������
		if (ItselfOrganizationVariables.funcIntermediateOrgAllowed) {
			//��������� ��� ��� ��
			new FunctionOrganization();
			//��� ��� ��
			new FuncOrgForLists();
			//���
			//TODO call to parameter organization replaced
			//new SimpleParamOrg(realMinRootValue);
			ItselfOrganizationVariables.getInstance().runPSO(realMinRootValue);
		} else { 
			//���� ��������� ��� ��� ��
			if (ItselfOrganizationVariables.funcLeafOrgAllowed) {
				//��� ��� ��
				new FuncOrgForLists();
				//���
				//TODO call to parameter organization replaced
				//new SimpleParamOrg(realMinRootValue);
				ItselfOrganizationVariables.getInstance().runPSO(realMinRootValue);
			} else if (ItselfOrganizationVariables.paramOrgAllowed) {
				//���� ��������� ���
				//���
				//TODO call to parameter organization replaced
				//new SimpleParamOrg(realMinRootValue);
				ItselfOrganizationVariables.getInstance().runPSO(realMinRootValue);
			}
		}
	}
}
