package simulation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import javax.swing.JOptionPane;

import Main.ParametersLoader;
import simulation.SimulationCountFX;
import object.TreeElement;

public class Parser 
{
	private  char[] beginningSymbols={'!','-','('};
	private  char[] digitSymbols={'0','1','2','3','4','5','6','7','8','9','.'};
	private  char[] literalSymbols={'_','a','A','b','B','c','C','d','D','e','E','f','F',
			'g','G','h','H','i','I','j','J','k','K','l','L','m','M','n','N','o','O',
			'p','P','q','Q','r','R','s','S','t','T','u','U','v','V','w','W','x','X','y','Y','z','Z'};
	private  char[] operationSymbols={'+','-','*','/'};
	
	private static Map<String, Double> variablesMap = new HashMap<String, Double>();
	
	private static List<Character> beginningOfTheFormulaCollection;
	public static  List<Character> digitCollection;
	public static  List<Character> onlyDigitCollection;
	public static  List<Character> literalCollection;
	public static  List<Character> operationCollection;
	public static  List<Character> allowedSymbolsCollection;
	
	
	//flag for formula analyze (must be set to true 
	//before analisisOfTheFormula(String) will be called)
	private static boolean currentFormulaIsCorrect = true;
	
	private static int additionalDisplacement=0;
	
	public Parser()
	{
		beginningOfTheFormulaCollection = new ArrayList<Character>();
		digitCollection = new ArrayList<Character>();
		onlyDigitCollection = new ArrayList<Character>();
		literalCollection = new ArrayList<Character>();
		operationCollection = new ArrayList<Character>();
		allowedSymbolsCollection = new ArrayList<Character>();
		int i;
		for(i=0;i<beginningSymbols.length;i++)
		{
			beginningOfTheFormulaCollection.add(beginningSymbols[i]);
		}
		for(i=0;i<digitSymbols.length;i++)
		{
			digitCollection.add(digitSymbols[i]);
		}
		for(i=0; i< ( digitSymbols.length - 1 ); i++)
		{
			onlyDigitCollection.add(digitSymbols[i]);
		}
		for(i=0;i<literalSymbols.length;i++)
		{
			literalCollection.add(literalSymbols[i]);
		}
		for(i=0;i<operationSymbols.length;i++)
		{
			operationCollection.add(operationSymbols[i]);
		}
		beginningOfTheFormulaCollection.addAll(digitCollection);
		beginningOfTheFormulaCollection.addAll(literalCollection);
		
		allowedSymbolsCollection.addAll(beginningOfTheFormulaCollection);
		allowedSymbolsCollection.addAll(operationCollection);
		allowedSymbolsCollection.add(')');
		allowedSymbolsCollection.add('<');
		allowedSymbolsCollection.add('>');
		allowedSymbolsCollection.add(',');
		//!!!!!!!!!!!?????????????????
//		variablesMap.put("x", 10.0);
//		variablesMap.put("t", 5.0);
//		variablesMap.put("a", 1.0);
//		variablesMap.put("b", 2.0);
//		variablesMap.put("abc", 5.0);
	}
	
	public static double calculationOfFormula(TreeElement treeElem)
	{
		String formula="x+t";
		formula = treeElem.functionName;
//check_me  begin		
		variablesMap.clear();
		if( treeElem.parametersValuesMap != null )
		{
			variablesMap.putAll( treeElem.parametersValuesMap );
		}
		
//check_me  end		
		if((double)SimulationCountFX.t < treeElem.TimeOfCreation)
			treeElem.TimeOfCreation = 0;
		double tmpForTime = (double)SimulationCountFX.t - treeElem.TimeOfCreation;
		variablesMap.put("t", tmpForTime);
//		fillingVariablesMap(treeElem, formula);

		return analisisOfTheFormula(formula, false);
	}
	
	//used ONLY in FunctionOrganization 
	public static double calculationOfFunction( 
			String function, 
			ArrayList<String> paramNamesList, 
			ArrayList<Double> paramValueList, 
			ArrayList<String> notVaryingParamNamesList, 
			ArrayList<Double> notVaryingParamValueList, 
			boolean needTranslationToLimitedFunction )
	{
//check_me  begin		
		variablesMap.clear();
		
		if( needTranslationToLimitedFunction )
		{
			for( int i=0; i < paramNamesList.size(); i++)
			{
				variablesMap.put( paramNamesList.get( i ), ParametersLoader.valueTranslationToLimitedFunction( paramNamesList.get( i ), paramValueList.get( i ) ) );
			}
		}
		else
		{
			for( int i=0; i < paramNamesList.size(); i++)
			{
				variablesMap.put( paramNamesList.get( i ), paramValueList.get( i ) );
			}
		}		
		
		for( int i=0; i < notVaryingParamNamesList.size(); i++)
		{
			variablesMap.put( notVaryingParamNamesList.get( i ), notVaryingParamValueList.get( i ) );
		}
//check_me  end		
		double tmpForTime = SimulationCountFX.t;
		variablesMap.put("t", tmpForTime);
//		fillingVariablesMap(treeElem, formula);

		return analisisOfTheFormula(function, false);
	}
	
	public static Map<String, Double> fillingParametersValuesMap(String formula)
	{
		Map<String, Double> parametrsValuesMap = new HashMap<String, Double>();
		int currentPositionInFormula=0;
		char currentChar=formula.charAt(0);
		String currentVariableName;
		double currentVariableValue;
		
		for(currentPositionInFormula=0;
		currentPositionInFormula<formula.length();
		currentPositionInFormula++)
		{
			currentChar=formula.charAt(currentPositionInFormula);
			if( currentChar=='!' )
			{
				int addedPosition;
				addedPosition=( formula.substring( currentPositionInFormula , formula.length() ) ).indexOf('<');
				currentPositionInFormula = currentPositionInFormula + addedPosition;
			}
			if(literalCollection.contains(currentChar))
			{
				currentVariableName="";
				while( ( !operationCollection.contains(currentChar) ) 
						&& ( currentPositionInFormula<formula.length() ) 
						&& ( !( currentChar==')' )) 
						&& ( !( currentChar=='>' ))
						&& ( !( currentChar==',' )))
				{
					currentVariableName=currentVariableName+currentChar;
					currentPositionInFormula++;
					if(currentPositionInFormula<formula.length())
					{
						currentChar=formula.charAt(currentPositionInFormula);
					}
					else
					{
						currentChar='0';
					}
				}
				if( ( !parametrsValuesMap.containsKey(currentVariableName) ) && (!currentVariableName.equals("t")) )
				{
//					currentVariableValue = ParametrsLoader.paramLeftBordersMap.get(currentVariableName) ;
					currentVariableValue = ParametersLoader.getInitialValue(currentVariableName) ;
					parametrsValuesMap.put( currentVariableName, currentVariableValue );
					//JOptionPane.showMessageDialog(null, "variablesMap.containsKey(currentVariableName)", "", JOptionPane.INFORMATION_MESSAGE);
				}
				//JOptionPane.showMessageDialog(null, "currentVariableName="+currentVariableName, "", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		return parametrsValuesMap;
	}
	
	public static ArrayList<String> findAllParametrsInFunction(String formula)
	{
		int currentPositionInFormula=0;
		char currentChar=formula.charAt(0);
		String currentVariableName;
		double currentVariableValue;
		ArrayList<String> parametrsList = new ArrayList<String>();
		
		for(currentPositionInFormula=0;
		currentPositionInFormula<formula.length();
		currentPositionInFormula++)
		{
			currentChar=formula.charAt(currentPositionInFormula);
			if( currentChar=='!' )
			{
				int addedPosition;
				addedPosition=( formula.substring( currentPositionInFormula , formula.length() ) ).indexOf('<');
				currentPositionInFormula = currentPositionInFormula + addedPosition;
			}
			if(literalCollection.contains(currentChar))
			{
				currentVariableName="";
				while( ( !operationCollection.contains(currentChar) ) 
						&& ( currentPositionInFormula<formula.length() ) 
						&& ( !( currentChar==')' )) 
						&& ( !( currentChar=='>' ))
						&& ( !( currentChar==',' )))
				{
					currentVariableName=currentVariableName+currentChar;
					currentPositionInFormula++;
					if(currentPositionInFormula<formula.length())
					{
						currentChar=formula.charAt(currentPositionInFormula);
					}
					else
					{
						currentChar='0';
					}
				}
				if( ( !parametrsList.contains(currentVariableName) ) && (!currentVariableName.equals("t")) )
				{
					parametrsList.add( currentVariableName );
				}
				//JOptionPane.showMessageDialog(null, "currentVariableName="+currentVariableName, "", JOptionPane.INFORMATION_MESSAGE);
			}
		}
		return parametrsList;
	}
	
//	public static void fillingVariablesMap(TreeElement treeElem, String formula)
//	{
//		int currentPositionInFormula=0;
//		char currentChar=formula.charAt(0);
//		String currentVariableName;
//		int currentIndexOfVariableValue=0;
//		double currentVariableValue=simulation.t;
//		
//		variablesMap.put("t", currentVariableValue);
//		
//		for(currentPositionInFormula=0;
//		currentPositionInFormula<formula.length();
//		currentPositionInFormula++)
//		{
//			currentChar=formula.charAt(currentPositionInFormula);
//			if( currentChar=='!' )
//			{
//				int addedPosition;
//				addedPosition=( formula.substring( currentPositionInFormula , formula.length() ) ).indexOf('<');
//				currentPositionInFormula = currentPositionInFormula + addedPosition;
//			}
//			if(literalCollection.contains(currentChar))
//			{
//				currentVariableName="";
//				while( ( !operationCollection.contains(currentChar) ) 
//						&& ( currentPositionInFormula<formula.length() ) 
//						&& ( !( currentChar==')' )) 
//						&& ( !( currentChar=='>' ))
//						&& ( !( currentChar==',' )))
//				{
//					currentVariableName=currentVariableName+currentChar;
//					currentPositionInFormula++;
//					if(currentPositionInFormula<formula.length())
//					{
//						currentChar=formula.charAt(currentPositionInFormula);
//					}
//					else
//					{
//						currentChar='0';
//					}
//				}
//				if( ( !variablesMap.containsKey(currentVariableName) ) && (!currentVariableName.equals("t")) )
//				{
//					currentVariableValue = treeElem.parametrsValuesMap.get(currentVariableName);
//					variablesMap.put( currentVariableName, currentVariableValue );
//					currentIndexOfVariableValue++;
//					//JOptionPane.showMessageDialog(null, "variablesMap.containsKey(currentVariableName)", "", JOptionPane.INFORMATION_MESSAGE);
//				}
//				//JOptionPane.showMessageDialog(null, "currentVariableName="+currentVariableName, "", JOptionPane.INFORMATION_MESSAGE);
//			}
//		}
//	}
	
	
	
	public static double analisisOfTheFormula(String formulaForTheAnalysis, boolean itsOnlyChecking)
	{
		char currentChar=formulaForTheAnalysis.charAt(0);
		double currentValue=0;
		double firstOperand=0;
		double secondOperand=0;
		double valueOfTheFormula=0;
		char currentOperation=' ';
		char signBeforeContinuousOperation=' ';
		int currentPositionInFormula=0;
		boolean firstOperandInFormula=true;
		boolean firstSymbolIsMinus=false;
		
		if(formulaForTheAnalysis.charAt(0)=='-')
		{
			firstSymbolIsMinus=true;
			formulaForTheAnalysis=formulaForTheAnalysis.substring(1, formulaForTheAnalysis.length());
		}
		
		for(currentPositionInFormula=0;
			currentPositionInFormula<formulaForTheAnalysis.length();
			currentPositionInFormula++)
		{
			currentChar=formulaForTheAnalysis.charAt(currentPositionInFormula);
			if(operationCollection.contains(currentChar))
			{
				if(signBeforeContinuousOperation!=' ')/////!!!!!
				{
					if( (currentChar=='+') || (currentChar=='-') )
					{
						if(signBeforeContinuousOperation=='+')
						{
							valueOfTheFormula=valueOfTheFormula + currentValue;
						}
						else
						{
							valueOfTheFormula=valueOfTheFormula - currentValue;
						}
					}
				}
				
				if(firstOperandInFormula)
				{
					if( (currentChar=='*') || (currentChar=='/') )///!!!!!
					{
///						
					}
					else
					{
						if(firstSymbolIsMinus)
						{
							valueOfTheFormula=-currentValue;
						}
						else
						{
							valueOfTheFormula=currentValue;
						}
						firstOperandInFormula=false;
					}
				}
				currentOperation=currentChar;
				if( (currentChar=='+') || (currentChar=='-') )/////////!!!!!!!!!!!
				{
					signBeforeContinuousOperation=currentChar;
				}
			}
			else
			{
				if( (currentOperation=='*') || (currentOperation=='/') )
				{
					firstOperand=currentValue;
				}
				
				//We read out a current operand in a variable "currentValue":
				if(digitCollection.contains(currentChar))
				{
					String contaningNumberString=formulaForTheAnalysis.substring(currentPositionInFormula, formulaForTheAnalysis.length());
					int endOfNumberPosition=0;
					for(endOfNumberPosition=0;endOfNumberPosition<contaningNumberString.length();endOfNumberPosition++)
					{
						if(  !(digitCollection.contains( contaningNumberString.charAt(endOfNumberPosition) ) ) )
						{
							break;
						}
					}
					try
					{
						currentValue = Double.parseDouble( contaningNumberString.substring(0, endOfNumberPosition) );
					}
					catch(Exception e)
					{
						currentFormulaIsCorrect = false;
						JOptionPane.showMessageDialog(null, "������ � �������� �����", "Mistake window", JOptionPane.ERROR_MESSAGE);
						return 0;
					}
					currentPositionInFormula=currentPositionInFormula+(endOfNumberPosition-1);
//					if(endOfNumberPosition==(contaningNumberString.length()-1))
//					{
//						currentValue = Double.parseDouble( contaningNumberString.substring(0, endOfNumberPosition) );
//					}
				}
				else
				{
					if(literalCollection.contains(currentChar))
					{
						String contaningVariableString=formulaForTheAnalysis.substring(currentPositionInFormula, formulaForTheAnalysis.length());
						int endOfVariablePosition=0;
						for(endOfVariablePosition=0;endOfVariablePosition<contaningVariableString.length();endOfVariablePosition++)
						{
							if(  ( !(literalCollection.contains( contaningVariableString.charAt(endOfVariablePosition) ) ) ) 
									&& ( !(onlyDigitCollection.contains( contaningVariableString.charAt(endOfVariablePosition) ) )  ) )
							{
								break;
							}
						}
						if( itsOnlyChecking )
						{
							//if we check formula, we must set parameter value at its left border
							if( ParametersLoader.parametrsList.contains( contaningVariableString.substring(0, endOfVariablePosition) ) 
									|| contaningVariableString.substring(0, endOfVariablePosition).equals("t") )
							{
								if( contaningVariableString.substring(0, endOfVariablePosition).equals("t") )
								{
									currentValue = 1;
								}
								else
								{
									currentValue = ParametersLoader.parametrsCharacteristicsMap.get( contaningVariableString.substring(0, endOfVariablePosition) ).get(0);
								}								
							}
							else
							{
								currentFormulaIsCorrect = false;
								JOptionPane.showMessageDialog(null, "Incorrect variable name: "+contaningVariableString.substring(0, endOfVariablePosition), "Mistake window", JOptionPane.ERROR_MESSAGE);
								currentValue = 0;
							}	
						}
						else
						{
							if(variablesMap.containsKey(contaningVariableString.substring(0, endOfVariablePosition)))
							{
								currentValue = variablesMap.get(contaningVariableString.substring(0, endOfVariablePosition));
							}
							else
							{
								currentFormulaIsCorrect = false;
								JOptionPane.showMessageDialog(null, "Incorrect variable name: "+contaningVariableString.substring(0, endOfVariablePosition), "Mistake window", JOptionPane.ERROR_MESSAGE);
								currentValue = 0;
							}	
						}	
//						currentValue=variableReading(contaningVariableString.substring(0, endOfVariablePosition));
						currentPositionInFormula=currentPositionInFormula+(endOfVariablePosition-1);
						
						
					}
					else
					{
						if( currentChar=='!' )
						{
							currentValue=specificOperationReading(formulaForTheAnalysis.substring( currentPositionInFormula, formulaForTheAnalysis.length()) , itsOnlyChecking );
							currentPositionInFormula= currentPositionInFormula + additionalDisplacement;
						}
						else
						{
							if( currentChar != '(' )
							{
								currentFormulaIsCorrect = false;
								JOptionPane.showMessageDialog(null, "������������ ������ � ������� " + currentPositionInFormula, "Mistake window", JOptionPane.ERROR_MESSAGE);
								return 0;
							}
							//The beginning of expression in brackets:
							int leftBorder=formulaForTheAnalysis.indexOf('(');
							int rightBorder=formulaForTheAnalysis.lastIndexOf(')');
							if( rightBorder < 0 )
							{
								currentFormulaIsCorrect = false;
								JOptionPane.showMessageDialog(null, "����������� ������ �� ����� ��������������� �����������", "Mistake window", JOptionPane.ERROR_MESSAGE);
								return 0;
							}
							String newFormula=null;
							newFormula=formulaForTheAnalysis.substring(leftBorder+1,rightBorder);
//							JOptionPane.showMessageDialog(null, "newFormula= " + newFormula);
							currentValue=analisisOfTheFormula(newFormula, itsOnlyChecking);
							currentPositionInFormula= currentPositionInFormula + newFormula.length()+ 1;
							//JOptionPane.showMessageDialog(null, "Mistake!!!", "Mistake window", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
								
				if( (currentOperation=='*') || (currentOperation=='/') )
				{
					secondOperand=currentValue;
				}
				
				if( currentOperation!=' ' )
				{
					switch (currentOperation)
					{
					case '+': 
						if( (currentPositionInFormula+1) < formulaForTheAnalysis.length() )////!!!!
						{
							if( (formulaForTheAnalysis.charAt(currentPositionInFormula+1)=='*') || 
									(formulaForTheAnalysis.charAt(currentPositionInFormula+1)=='/'))
							{
								int l=0;
								//////////!!!!!!!!
							}
							else
							{
								valueOfTheFormula=
									valueOfTheFormula + currentValue;
								signBeforeContinuousOperation=' ';
							}
						}
						else
						{
							valueOfTheFormula=
								valueOfTheFormula + currentValue;
							signBeforeContinuousOperation=' ';
						}
						
						//JOptionPane.showMessageDialog(null, "currentValue(after +)= " + currentValue);
						break;
					case '-':
						if( (currentPositionInFormula+1) < formulaForTheAnalysis.length() )////!!!!
						{
							if( (formulaForTheAnalysis.charAt(currentPositionInFormula+1)=='*') || 
									(formulaForTheAnalysis.charAt(currentPositionInFormula+1)=='/'))
							{
								//////////!!!!!!!!
							}
							else
							{
								valueOfTheFormula=
									valueOfTheFormula - currentValue;
								signBeforeContinuousOperation=' ';
							}
						}
						else
						{
							valueOfTheFormula=
								valueOfTheFormula - currentValue;
							signBeforeContinuousOperation=' ';
						}
						break;
					case '*':
						//JOptionPane.showMessageDialog(null, "*  firstOperand= " + firstOperand +"/n  secondOperand= "+secondOperand);
						currentValue=firstOperand=firstOperand*secondOperand;
						//JOptionPane.showMessageDialog(null, "currentValue(after *)= " + currentValue);
						break;
					case '/':
						currentValue=firstOperand=firstOperand/secondOperand;
						break;
					}
					//!!!!!!!!!!!!!
					currentOperation=' ';
				}
			}
			
		}//Cycle to parse the formula to the end
		
		//Checking for the case if formula consists of single operand:
		if(firstOperandInFormula)
		{
			if(firstSymbolIsMinus)
			{
				valueOfTheFormula=-currentValue;
			}
			else
			{
				valueOfTheFormula=currentValue;
			}
			firstOperandInFormula=false;
		}
		
		if(signBeforeContinuousOperation!=' ')/////!!!
		{
			if( signBeforeContinuousOperation=='+' )
			{
				valueOfTheFormula=valueOfTheFormula + currentValue;
			}
			else
			{
				valueOfTheFormula=valueOfTheFormula - currentValue;
			}
		}
		//JOptionPane.showMessageDialog(null, "valueOfTheFormula in brec= " + valueOfTheFormula);
		return valueOfTheFormula;
	}
	
//	public double numberReading(String contaningNumberString)
//	{
//		//JOptionPane.showMessageDialog(null, "contaningNumberString= "+contaningNumberString);
//		for(int i=0;i<contaningNumberString.length();i++)
//		{
//			if(  !(digitCollection.contains( contaningNumberString.charAt(i) ) ) )
//			{
//				//JOptionPane.showMessageDialog(null, "contaningNumberString.substring(0, i)= "+contaningNumberString.substring(0, i));
//				return Double.parseDouble( contaningNumberString.substring(0, i) );
//			}
//		}
//		return Double.parseDouble(contaningNumberString);
//	}
	
//	public double variableReading(String variableString)
//	{
//		if(variablesMap.containsKey(variableString))
//		{
//			return variablesMap.get(variableString);
//		}
//		else
//		{
//			JOptionPane.showMessageDialog(null, "Incorrect variable name: "+variableString, "Mistake window", JOptionPane.ERROR_MESSAGE);
//			return 0;
//		}		
//	}
	
	public static double specificOperationReading(String contaningSpecificOperationString, boolean itsOnlyChecking)
	{
		String specificOperationName;
		try
		{
			specificOperationName = contaningSpecificOperationString.substring( 1, contaningSpecificOperationString.indexOf("<") );
			//JOptionPane.showMessageDialog(null, "specificOperationName: "+specificOperationName, "", JOptionPane.INFORMATION_MESSAGE);
			String specificOperationContent;
			int countOfNotClosedSpecifiedBrackets=1;
			int lenghtOfSpecificOperation=0;
			specificOperationContent = contaningSpecificOperationString.substring( 
					contaningSpecificOperationString.indexOf("<")+1 , contaningSpecificOperationString.length());
			for(countOfNotClosedSpecifiedBrackets=1;countOfNotClosedSpecifiedBrackets>0;lenghtOfSpecificOperation++)
			{
				if(specificOperationContent.charAt(lenghtOfSpecificOperation)=='<')
				{
					countOfNotClosedSpecifiedBrackets++;
				}
				if(specificOperationContent.charAt(lenghtOfSpecificOperation)=='>')
				{
					countOfNotClosedSpecifiedBrackets--;
				}
			}
			specificOperationContent = specificOperationContent.substring( 0, lenghtOfSpecificOperation-1 );
			//JOptionPane.showMessageDialog(null, "specificOperationContent: "+specificOperationContent, "", JOptionPane.INFORMATION_MESSAGE);
			additionalDisplacement=specificOperationContent.length() + specificOperationName.length() + 2;
			if(specificOperationName.equals("exp"))
			{
				return Math.exp( analisisOfTheFormula( specificOperationContent , itsOnlyChecking ) );
			}
			if(specificOperationName.equals("pow"))
			{
				return Math.pow( analisisOfTheFormula( specificOperationContent.substring( 0, specificOperationContent.indexOf(",") ) , itsOnlyChecking ) 
						, analisisOfTheFormula( specificOperationContent.substring( specificOperationContent.indexOf(",")+1 , specificOperationContent.length()) , itsOnlyChecking ) );
			}
			currentFormulaIsCorrect = false;
			JOptionPane.showMessageDialog(null, "�������� � ������:  " + specificOperationName + "  �� ����������", "Mistake window", JOptionPane.ERROR_MESSAGE);
			return 0;
		}
		catch(Exception e)
		{
			currentFormulaIsCorrect = false;
			JOptionPane.showMessageDialog(null, "������ � �������� ����������� ��������", "Mistake window", JOptionPane.ERROR_MESSAGE);
			return 0;
		}
	}
	
	public static boolean isFormulaCorrect(String formulaForTheAnalysis)
	{
		//simple checking
		int simpleCheckingResult;
		simpleCheckingResult = simpleChecking(formulaForTheAnalysis);
		if( simpleCheckingResult != formulaForTheAnalysis.length() )
		{
			if( simpleCheckingResult < formulaForTheAnalysis.length() )
			{
				int position = simpleCheckingResult + 1;
				JOptionPane.showMessageDialog(null, 
						"������������ ������ '" + 
						formulaForTheAnalysis.charAt(simpleCheckingResult) + 
						"' ( ������� " + position + " � ������� )"/*,
						"Mistake window", JOptionPane.ERROR_MESSAGE*/);
			}
			else
			{
				if( simpleCheckingResult == ( formulaForTheAnalysis.length() + 1 ) )
				{
					JOptionPane.showMessageDialog(null, "������� �� ����� ���������� � ������� '" + 
							formulaForTheAnalysis.charAt(0)+ "'"/*,
							"Mistake window", JOptionPane.ERROR_MESSAGE*/);
				}
				else if( simpleCheckingResult == ( formulaForTheAnalysis.length() + 2 ) )
				{
					JOptionPane.showMessageDialog(null, "���������� ����������� � ����������� ������ '(' � ')' �� ��������� " /*,
							"Mistake window", JOptionPane.ERROR_MESSAGE*/);
				}
				else if(simpleCheckingResult == ( formulaForTheAnalysis.length() + 3 ))
				{
					JOptionPane.showMessageDialog(null, "���������� ����������� � ����������� ������ '<' � '>' �� ��������� " /*,
					"Mistake window", JOptionPane.ERROR_MESSAGE*/);
				}
			}
			return false;
		}
		//total checking
		currentFormulaIsCorrect = true;
		analisisOfTheFormula(formulaForTheAnalysis, true);
		return currentFormulaIsCorrect;
	}
	
	//simple checking did formula is correct
	private static int simpleChecking(String formulaForTheAnalysis)
	{
		int openBracketsNumber=0, openTriangleBracketsNumber=0;
		int closeBracketsNumber=0, closeTriangleBracketsNumber=0;
		char currentChar;
		
		if( ( !beginningOfTheFormulaCollection.contains( formulaForTheAnalysis.charAt(0) ) ) 
				|| formulaForTheAnalysis.charAt(0)=='.')
		{
			return formulaForTheAnalysis.length()+1;
		}
		for( int i=0; i<formulaForTheAnalysis.length(); i++ )
		{
			currentChar = formulaForTheAnalysis.charAt(i);
			if( allowedSymbolsCollection.contains(currentChar) )
			{
				if( currentChar=='<' )
				{
					openTriangleBracketsNumber++;
				}
				else if ( currentChar=='(' )
				{
					openBracketsNumber++;
				}
				else if( currentChar==')' )
				{
					closeBracketsNumber++;
				}
				else if( currentChar=='>' )
				{
					closeTriangleBracketsNumber++;
				}
			}
			else
			{
				return i;
			}
		}
		if( closeBracketsNumber != openBracketsNumber )
		{
			return formulaForTheAnalysis.length() + 2;
		}
		if( closeTriangleBracketsNumber != openTriangleBracketsNumber )
		{
			return formulaForTheAnalysis.length() + 3;
		}
		return formulaForTheAnalysis.length();
	}
	
	
	
}

