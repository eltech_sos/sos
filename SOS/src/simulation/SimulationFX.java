package simulation;

import java.util.Vector;

import Main.Main;
import Main.TreeOperations;

import javafx.application.Application;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import simulation.CheckModel;
import simulation.SimulationCountFX;

public class SimulationFX extends Application{
	private static ObservableList<SelgOrganizationMember> selgOrganizationMembersList; // = FXCollections.observableArrayList();
	private ListView<SelgOrganizationMember> listView;
	private Vector<Integer> visibleElements;
	
	@Override
	public void start(final Stage stage) throws Exception {
		stage.setTitle("����� ��������� ��� �����������");
		visibleElements = new Vector<Integer>();
		VBox root = new VBox();
    	root.setSpacing(10);
    	root.setPadding(new Insets(10, 0, 10, 10));
    	
    	//selgOrganizationMembersList.clear();
    	// TODO make correct reuse of selgOrganizationMembersList
    	// comments prevent warnings about not found styles in second run of simulation
    	// maybe there is more correct way to fix this problem
    	selgOrganizationMembersList = FXCollections.observableArrayList();
    	
    	for(int elemInd = 0; elemInd < Main.elementsNamesList.size(); elemInd++)        		
    		selgOrganizationMembersList.add(new SelgOrganizationMember(Main.elementsNamesList.elementAt(elemInd), true));        		
    	
    	Label lblCheckEl = new Label("�������� ��������, �������� ������� ������ ������������");
    	lblCheckEl.getStyleClass().add("label-as-title");
    	lblCheckEl.setMinWidth(500);            	
    	
    	listView = new ListView<SelgOrganizationMember>();
    	listView.setMaxWidth(473);
    	listView.setMaxHeight(268);
    	listView.setItems(selgOrganizationMembersList);

    	// set the cell factory
    	Callback<SelgOrganizationMember, ObservableValue<Boolean>> getProperty = new Callback<SelgOrganizationMember, ObservableValue<Boolean>>() {
    		public BooleanProperty call(SelgOrganizationMember person) {
        		return person.telecommuterProperty();
        	}
    	};
    	
    	StringConverter<SelgOrganizationMember> stringConverter = new StringConverter<SelgOrganizationMember>() {
            public String toString(SelgOrganizationMember t) {
                return t.getFirstName();
            }
 
            public SelgOrganizationMember fromString(String string) {
                for (SelgOrganizationMember tileMapLayer : selgOrganizationMembersList) {
                    if (string.equals(tileMapLayer.getFirstName())) {
                        return tileMapLayer;
                    }
                }
                return null;
            }
        };
    	Callback<ListView<SelgOrganizationMember>, ListCell<SelgOrganizationMember>> forListView = CheckBoxListCell.forListView(getProperty, stringConverter);
    	listView.setCellFactory(forListView);
    	
    	HBox hBoxButton = new HBox();
    	hBoxButton.setSpacing(5);
    	hBoxButton.setPadding(new Insets(0, 0, 0, 322));

    	Button btnOk = new Button("��");
    	btnOk.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				if(CheckModel.isModelCorrect()){
					startsimulation();
					stage.close();
				}
			}});
    	Button btnCancel = new Button("������");
    	btnCancel.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				stage.close();
			}});
    	
    	hBoxButton.getChildren().addAll(btnOk, btnCancel);
    	
    	root.getChildren().addAll(lblCheckEl, listView, hBoxButton);
        Scene scene = new Scene(root, 495, 345);	
        scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
    	stage.setScene(scene);
    	stage.show();
	}

	private boolean startsimulation()
	{
		try
		{
			for( int elemInd=0; elemInd < Main.elementsNamesList.size(); elemInd++ )
				if(listView.getItems().get(elemInd).isTelecommuter())
					visibleElements.add( TreeOperations.findElementIDInStructByName( Main.elementsNamesList.get(elemInd) ) );
	    	new SimulationCountFX(visibleElements).start(new Stage());
			//(new simulationCountFX(visibleElements)).start(new Stage());
		}
		catch(Exception e)
		{
			return false;
		}
		return true;
	}
	
	public class SelgOrganizationMember 
	{
		private final StringProperty firstNameProperty;
		public final StringProperty firstNameProperty() { return firstNameProperty; }
		public String getFirstName() { return firstNameProperty.get(); }
		public void setFirstName(String newName) { firstNameProperty.set(newName); }
		
		private final BooleanProperty telecommuterProperty;
		public final BooleanProperty telecommuterProperty() { return telecommuterProperty; }
		public boolean isTelecommuter() { return telecommuterProperty.get(); }
		public void setTelecommuter (boolean v) {telecommuterProperty().set(v);    }
		
		public SelgOrganizationMember(String firstName, boolean telecommuter) 
		{		
			this.firstNameProperty = new SimpleStringProperty(this, "firstName", firstName);
			this.telecommuterProperty = new SimpleBooleanProperty(this, "telecommuter", telecommuter);
		}
	}
}
