package simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Main.FunctionsLoader;
import Main.ParametersLoader;
import Main.TreeOperations;
import Main.TypeLoader;
import simulation.ItselfOrganizationVariables;
import simulation.Parser;

//define max for each participate (if choosed use participation for functions) function
//define max function for each type and
//define max type for each convolution function
public class FindMaxTypesValues 
{
	
	//ATENTION: in this list value saved in UNlimited form
	//for that they Translated to limited form in Parser.calculationOfFunction
	ArrayList<Double> paramValueList;
	
	ArrayList<String> paramNamesList;
	
	//ATENTION: in this list value saved in Limited form
	//for that they NO translated in Parser.calculationOfFunction
	ArrayList<Double> notVaryingParamValueList;
	
	ArrayList<String> notVaryingParamNamesList;
	
	ArrayList<Double> chengeStepList = new ArrayList<Double>();
	int paramNumber;
	int maxNumberOfCounts;
	double mas_e[][];
	String currentFunction = "0";
	
	Map< String, Double >  usedInSystemParametrsValues = null;
	
	ArrayList<String> usedInOrganizationElementaryFunctionsList = new ArrayList<String>();
	ArrayList<Double> functionsMaxValuesList = new ArrayList<Double>();
	
	Map< String, Map< String, Double >  >  paramMapsForFunctionMax = new HashMap< String, Map< String, Double >  >();
	
	double typeMaxValue = 0.0;
	
	public FindMaxTypesValues()
	{		
		TreeOperations.defineNeedsOfUpOrDownForAllElements();
		//checking:
//		if( ItselfOrganizationVariebles.notUsedInOrgElemFunctionsMeetingInModel.size() > 0 )
//		{
//			System.out.println("not_participate elem func:");
//			System.out.println();
//			for(int i=0; i < ItselfOrganizationVariebles.notUsedInOrgElemFunctionsMeetingInModel.size(); i++)
//			{
//				System.out.println( ItselfOrganizationVariebles.notUsedInOrgElemFunctionsMeetingInModel.get(i) );
//			}
//		}
//		if( ItselfOrganizationVariebles.notUsedInOrgElemTypesMeetingInModel.size() > 0 )
//		{
//			System.out.println("not_participate elem type:");
//			System.out.println();
//			for(int i=0; i < ItselfOrganizationVariebles.notUsedInOrgElemTypesMeetingInModel.size(); i++)
//			{
//				System.out.println( ItselfOrganizationVariebles.notUsedInOrgElemTypesMeetingInModel.get(i) );
//			}
//		}
//		if( ItselfOrganizationVariebles.notUsedInOrgInterTypesMeetingInModel.size() > 0 )
//		{
//			System.out.println("not_participate inter type:");
//			System.out.println();
//			for(int i=0; i < ItselfOrganizationVariebles.notUsedInOrgInterTypesMeetingInModel.size(); i++)
//			{
//				System.out.println( ItselfOrganizationVariebles.notUsedInOrgInterTypesMeetingInModel.get(i) );
//			}
//		}
		
		
//filling functionsMaxValuesMap
//and functionsMaxValuesList
//and ItselfOrganizationVariebles.notParticipateFuncsMaxValuesList
				functionsMaxValuesList.clear();
				usedInOrganizationElementaryFunctionsList.clear();
				
				usedInSystemParametrsValues = TreeOperations.findAllUsedInSystemParametrsValues();
				
				ItselfOrganizationVariables.notUsedInOrgElemFuncMeetingInModelValuesList.clear();
				
				for( int funcInd=0; funcInd < FunctionsLoader.elementaryFunctionsList.size(); funcInd++ )
				{
					currentFunction = FunctionsLoader.elementaryFunctionsList.get( funcInd );
					if( ( ! ItselfOrganizationVariables.useParticipationInFuncOrg)
							|| (FunctionsLoader.isParticipate( currentFunction ) ) )
						//if function is participate in itself - organization
						//or participation is not important 
					{
						usedInOrganizationElementaryFunctionsList.add( currentFunction );
						functionsMaxValuesList.add( findMaxForCurrentFunction() );
						
					}
					if( ItselfOrganizationVariables.notUsedInOrgElemFuncMeetingInModelList.
							contains(currentFunction) )
						//in this list contains:
					    //
						//if function organization for lists is allowed:
						//if participation is important
						//and current function not participate
						//but meeting in model in participate type
						//
						//if function organization for lists NOT allowed:
						//all functions meeting in model in participate type
					{
						if( usedInOrganizationElementaryFunctionsList.contains(currentFunction) )
						{
//							int tmpIndForFunc;
//							tmpIndForFunc = 
//								usedInOrganizationElementaryFunctionsList.
//								indexOf(currentFunction);
//							ItselfOrganizationVariebles.notUsedInOrgElemFuncMeetingInModelValuesList.
//							add( functionsMaxValuesList.get( tmpIndForFunc ) );
							ItselfOrganizationVariables.notUsedInOrgElemFuncMeetingInModelValuesList.
							add( functionsMaxValuesList.get( functionsMaxValuesList.size() - 1 ) );
						}
						else
						{
							ItselfOrganizationVariables.notUsedInOrgElemFuncMeetingInModelValuesList.
							add( findMaxForCurrentFunction() );
						}
					}
				}
//to_do				
				functionsMaxValuesList.size();
				for(int i=0;i<functionsMaxValuesList.size();i++)
				{
		        	System.out.println("MAX  "+usedInOrganizationElementaryFunctionsList.get(i) + " = " + functionsMaxValuesList.get(i) );
				}
//1:				
//define max values for each elementary type
//for types witch participate in itself - organization
//2(need if function organization for lists allowed):
//define max values for each elementary type
//for types witch not participate, but meeting in model and for
//elements in witch they selected selected participate function)
//3( need if function organization for lists allowed):
//save to ItselfOrganizationVariebles.paramMapsForFuncMax 
//max functions names and they parameters maps from paramMapsForFunctionMax
//for functions witch max for types(look upper) 
//4:
//save to ItselfOrganizationVariebles.paramMapsForFuncMax
//information about not participate functions meeting in model
//elements with participate types

				String currentTypeName;
				ArrayList<String> tmpTypeList = new ArrayList<String>();
				String maxFuncForCurrentType;
//				ArrayList<String> currentTypeFuncList;
				
//				String typeMaxFuncName = "";
//				boolean maxNotDefined = true;
//				String currentFuncInType;
//				int currentFuncInListsIndex;
				
				ItselfOrganizationVariables.usedInOrganizationElementaryTypesList.clear();
				ItselfOrganizationVariables.typesMaxValuesList.clear();
				ItselfOrganizationVariables.typesMaxFuncList.clear();
				ItselfOrganizationVariables.paramMapsForFuncMax.clear();
				
				ItselfOrganizationVariables.notUsedInOrgElemTypesMaxFuncList.clear();
				ItselfOrganizationVariables.notUsedInOrgElemTypesMaxValuesList.clear();
//1:				
				tmpTypeList.clear();
				tmpTypeList.addAll( TypeLoader.elementaryTypesList );
				for( int typeInd=0; typeInd < tmpTypeList.size(); typeInd++ )
				{
					currentTypeName = tmpTypeList.get( typeInd );
					if( ( ! ItselfOrganizationVariables.useParticipationInTypeOrg )
							|| ( TypeLoader.isParticipate( currentTypeName ) )  )
					{
						maxFuncForCurrentType = findMaxFunctionForType( currentTypeName );
						
						//checking: did this type contains any
						//participate in itself - organization 
						// function
						if( maxFuncForCurrentType != null )
						{
							ItselfOrganizationVariables.usedInOrganizationElementaryTypesList.add( currentTypeName );
							ItselfOrganizationVariables.typesMaxValuesList.add( typeMaxValue );
							ItselfOrganizationVariables.typesMaxFuncList.add( maxFuncForCurrentType );
							//3:
							if( ! ItselfOrganizationVariables.paramMapsForFuncMax.containsKey(maxFuncForCurrentType) )
							{
								ItselfOrganizationVariables.
								paramMapsForFuncMax.put
								( maxFuncForCurrentType, paramMapsForFunctionMax.get(maxFuncForCurrentType) );
							}
							
						}
						
					}
				}
//to_do begin			
				System.out.println();
				for( int i=0; i < ItselfOrganizationVariables.usedInOrganizationElementaryTypesList.size(); i++)
				{
					System.out.println(ItselfOrganizationVariables.usedInOrganizationElementaryTypesList.get(i)+"MAX  = " + ItselfOrganizationVariables.typesMaxFuncList.get(i) + " ( " + ItselfOrganizationVariables.typesMaxValuesList.get(i) + " )");
				}

//to_do end

//2:
				if( ItselfOrganizationVariables.funcLeafOrgAllowed )
				{
					tmpTypeList.clear();
					tmpTypeList.addAll(ItselfOrganizationVariables.notUsedInOrgElemTypesMeetingInModel );
					for(int typeInd=0; typeInd < tmpTypeList.size(); typeInd++)
					{
						currentTypeName = tmpTypeList.get( typeInd );
						maxFuncForCurrentType = findMaxFunctionForType( currentTypeName );
						//checking: did this type contains any
						//participate in itself - organization 
						// function
						if( maxFuncForCurrentType != null )
						{
							ItselfOrganizationVariables.notUsedInOrgElemTypesMaxValuesList.add( typeMaxValue );
							ItselfOrganizationVariables.notUsedInOrgElemTypesMaxFuncList.add( maxFuncForCurrentType );
							//3:
							if( ! ItselfOrganizationVariables.paramMapsForFuncMax.containsKey(maxFuncForCurrentType) )
							{
								ItselfOrganizationVariables.
								paramMapsForFuncMax.put
								( maxFuncForCurrentType, paramMapsForFunctionMax.get(maxFuncForCurrentType) );
							}
							
						}
					}
//to_do begin			
					System.out.println();
					System.out.println("NOT PARTICIPATE");
					for( int i=0; i < ItselfOrganizationVariables.notUsedInOrgElemTypesMeetingInModel.size(); i++)
					{
						
						System.out.println(ItselfOrganizationVariables.notUsedInOrgElemTypesMeetingInModel.get(i)+"MAX  = " + ItselfOrganizationVariables.notUsedInOrgElemTypesMaxFuncList.get(i) + " ( " + ItselfOrganizationVariables.notUsedInOrgElemTypesMaxValuesList.get(i) + " )");
					}

//to_do end
				}//end if( ItselfOrganizationVariebles.funcListOrgAllowed )
				
//4:
				String tmpFuncName;
				for(int i=0; i < ItselfOrganizationVariables.notUsedInOrgElemFuncMeetingInModelList.size(); i++)
				{
					tmpFuncName = ItselfOrganizationVariables.notUsedInOrgElemFuncMeetingInModelList.get( i );
					if( paramMapsForFunctionMax.
							containsKey( tmpFuncName ) )
					{
						ItselfOrganizationVariables.
						paramMapsForFuncMax.put( tmpFuncName,
								paramMapsForFunctionMax.
								get(tmpFuncName) );
					}
				}
				
	}
	
	//find max function for current type
	private String findMaxFunctionForType( String typeName )
	{
		ArrayList<String> currentTypeFuncList;
		double typeMax = 0.0;
		String typeMaxFuncName = "";
		boolean maxNotDefined = true;
		String currentFuncInType;
		int currentFuncInListsIndex;
		
		currentTypeFuncList = TypeLoader.getFunctionList( typeName );
		for( int funcInTypeInd = 0; funcInTypeInd < currentTypeFuncList.size(); funcInTypeInd++ )
		{
			currentFuncInType = currentTypeFuncList.get( funcInTypeInd );
			if( usedInOrganizationElementaryFunctionsList.contains(currentFuncInType) )
			{
				currentFuncInListsIndex = usedInOrganizationElementaryFunctionsList.indexOf( currentFuncInType );
				if( maxNotDefined )
				{
					typeMax = functionsMaxValuesList.get( currentFuncInListsIndex );
					typeMaxFuncName = currentFuncInType;
					maxNotDefined = false;
					if( ! ItselfOrganizationVariables.funcLeafOrgAllowed )
						//if function organization for lists not allowed we
						//make type max the first by priority this type function
						//witch participate in itself organization
					{
						return typeMaxFuncName;
					}
				}
				else
				{
					if( functionsMaxValuesList.get( currentFuncInListsIndex ) > typeMax )
					{
						typeMax = functionsMaxValuesList.get( currentFuncInListsIndex );
						typeMaxFuncName = currentFuncInType;
					}
				}
			}
		}
		
		if( maxNotDefined )
		{
			return null;
		}
		else
		{
			typeMaxValue = typeMax;
			return typeMaxFuncName;
		}
	}
	
	//find and return max value of function saved in 
	//currentFunction variable
	//and writes parameters map for this max to the
	//paramMapsForFunctionMax
	private double findMaxForCurrentFunction()
	{
		
		double funcValue;
		double tmpParamValue;
		
		paramNamesList = Parser.findAllParametrsInFunction( currentFunction );
		paramValueList = new ArrayList<Double>();
		notVaryingParamNamesList = new ArrayList<String>();
		notVaryingParamValueList = new ArrayList<Double>();
		paramNumber = paramNamesList.size();
		
//if function has no changed parameters except "t"
//its max value is not settings
		if( paramNumber < 1 )
		{
			funcValue = Parser.calculationOfFunction( currentFunction, 
					paramNamesList, paramValueList, 
					notVaryingParamNamesList, notVaryingParamValueList, false );
//now in return			functionsMaxValuesList.add( funcValue );
//to_do		
			return funcValue;
		}
		
//if parameters organization NOT allowed
//for each used parameter use value like in elements in current model
//or(if this parameter did not meets in current model)
//used it initial values
		if( ! ItselfOrganizationVariables.paramOrgAllowed )
		{
			for(int paramInd=0; paramInd < paramNumber; paramInd++ )
			{
				if( usedInSystemParametrsValues.containsKey( paramNamesList.get(paramInd) ) )
				{
					paramValueList.add( usedInSystemParametrsValues.get( paramNamesList.get(paramInd) ) );
				}
				else
				{
					tmpParamValue = ParametersLoader.getInitialValue( paramNamesList.get( paramInd ) );
					paramValueList.add( tmpParamValue );
				}
			}
			
			funcValue = Parser.calculationOfFunction( 
					currentFunction, paramNamesList, paramValueList, 
					notVaryingParamNamesList, notVaryingParamValueList,  false );
//now in return			functionsMaxValuesList.add( funcValue );
			
			//save parameters values to the 
			//paramMapsForFunctionMax
			Map< String, Double > tmpParamMap = new HashMap< String, Double >();
			for(int paramInd = 0;
			paramInd < paramNamesList.size() ;paramInd++)
			{
				tmpParamMap.put( paramNamesList.get(paramInd),
						ParametersLoader.valueTranslationToLimitedFunction( paramNamesList.get( paramInd ), paramValueList.get( paramInd ) ) );
			}
			for(int paramInd = 0;
			paramInd < notVaryingParamNamesList.size() ;paramInd++)
			{
				tmpParamMap.put( notVaryingParamNamesList.get(paramInd),
						notVaryingParamValueList.get(paramInd) );
			}
			paramMapsForFunctionMax.put( currentFunction,
					tmpParamMap );
			
			return funcValue;
		}
		
//if parameters organization is allowed
		maxNumberOfCounts = ItselfOrganizationVariables.maxNumerOfCounts;
		
		//deleting from paramNamesList names of parameters
		//witch did not participate in parameters organization
		if( ItselfOrganizationVariables.useParticipationInParamOrg )
		{
			for( int paramInd=0; paramInd < paramNamesList.size(); paramInd++ )
			{
				if( ! ParametersLoader.isParticipate( paramNamesList.get(paramInd) ) )
				{
					notVaryingParamNamesList.add( paramNamesList.get(paramInd) );
					if( usedInSystemParametrsValues.containsKey( paramNamesList.get(paramInd) ) )
					{
						notVaryingParamValueList.add( usedInSystemParametrsValues.get( paramNamesList.get(paramInd) ) );
					}
					else
					{
						tmpParamValue = ParametersLoader.getInitialValue( paramNamesList.get( paramInd ) );
						notVaryingParamValueList.add( tmpParamValue );
					}
					paramNamesList.remove( paramInd );
				}
			}
			paramNumber = paramNamesList.size();
		}
		
		
		//filling paramValueList and chengeStepList
		for(int paramInd=0; paramInd < paramNumber; paramInd++ )
		{
			chengeStepList.add( 0.5 );
			paramValueList.add( ParametersLoader.valueTranslationToUnlimitedFunction
					( paramNamesList.get( paramInd ), ParametersLoader.getInitialValue( paramNamesList.get( paramInd ) ) ) 
					);
		}
		
		
//now in return					functionsMaxValuesList.add( findCurrentFuncMax() );
		funcValue = findCurrentFuncMax();
		return funcValue;
	}
	
//	private double findCurrentFuncMin()
//	{
//		
//		
//		
//		if( ItselfOrganizationVariebles.metodRelax )
//		{
//			String realFunc = currentFunction;
//			currentFunction = "-(" + currentFunction + ")";
//			
//			
//			relaxForNotSameParameters( true,  realFunc );
//			
//			currentFunction = realFunc;
//		}
//		else
//		{
//			cpsForNotSameParameters( true );
//		}
//		
//		//save parameters values to the paramMapsForFunctionMin
//		Map< String, Double > tmpParamMap = new HashMap< String, Double >();
//		for(int paramInd = 0;
//		paramInd < paramNamesList.size() ;paramInd++)
//		{
//			tmpParamMap.put( paramNamesList.get(paramInd),
//					ParametrsLoader.valueTranslationToLimitedFunction( paramNamesList.get( paramInd ), paramValueList.get( paramInd ) ) );
//		}
//		for(int paramInd = 0;
//		paramInd < notVaryingParamNamesList.size() ;paramInd++)
//		{
//			tmpParamMap.put( notVaryingParamNamesList.get(paramInd),
//					notVaryingParamValueList.get(paramInd) );
//		}
//		paramMapsForFunctionMin.put( currentFunction,
//				tmpParamMap );
//		
//		return Parser.calculationOfFunction( currentFunction, 
//				paramNamesList, paramValueList, 
//				notVaryingParamNamesList, notVaryingParamValueList, true );
//	}
	
	private double findCurrentFuncMax()
	{
		if( ItselfOrganizationVariables.metodRelax )
		{
			relaxForNotSameParameters( false, null );
		}
		else
		{
			cpsForNotSameParameters( false );
		}
		
		//save parameters values to the paramMapsForFunctionMax
		Map< String, Double > tmpParamMap = new HashMap< String, Double >();
		for(int paramInd = 0;
		paramInd < paramNamesList.size() ;paramInd++)
		{
			tmpParamMap.put( paramNamesList.get(paramInd),
					ParametersLoader.valueTranslationToLimitedFunction
					( paramNamesList.get( paramInd ), paramValueList.get( paramInd ) ) );
		}
		for(int paramInd = 0;
		paramInd < notVaryingParamNamesList.size() ;
		paramInd++)
		{
			tmpParamMap.put( notVaryingParamNamesList.get(paramInd),
					notVaryingParamValueList.get(paramInd) );
		}
		paramMapsForFunctionMax.put( currentFunction,
				tmpParamMap );
		
		return Parser.calculationOfFunction( currentFunction,
				paramNamesList, paramValueList, 
				notVaryingParamNamesList, notVaryingParamValueList, true );
	}
	
	
	private void cpsForNotSameParameters(  boolean searchMin )
	{
		int numberOfCounts = 0;
		double oldParamValue;
		double oldFunctionValue;
		double newFunctionValue;
		oldFunctionValue = Parser.calculationOfFunction( currentFunction,
				paramNamesList, paramValueList, 
				notVaryingParamNamesList, notVaryingParamValueList, true );
		do
		{
			numberOfCounts++;
			for( int paramChangedInd=0; ( paramChangedInd< paramNamesList.size() ) 
			; paramChangedInd++ )
			{
				oldParamValue = paramValueList.get(paramChangedInd);
				paramValueList.set( paramChangedInd, oldParamValue + chengeStepList.get(paramChangedInd) );
				
				newFunctionValue = Parser.calculationOfFunction( 
						currentFunction, paramNamesList, paramValueList, 
						notVaryingParamNamesList, notVaryingParamValueList, true );
				if( searchMin )
				{
					if( newFunctionValue <= oldFunctionValue )
					{
						oldFunctionValue = newFunctionValue;
						chengeStepList.set( paramChangedInd, chengeStepList.get(paramChangedInd) * 3 );
					}
					else
					{
						paramValueList.set( paramChangedInd, oldParamValue );
						chengeStepList.set( paramChangedInd, chengeStepList.get(paramChangedInd) * (-0.5) );
					}
				}
				else
				{
					if( newFunctionValue >= oldFunctionValue )
					{
						oldFunctionValue = newFunctionValue;
						chengeStepList.set( paramChangedInd, chengeStepList.get(paramChangedInd) * 3 );
					}
					else
					{
						paramValueList.set( paramChangedInd, oldParamValue );
						chengeStepList.set( paramChangedInd, chengeStepList.get(paramChangedInd) * (-0.5) );
					}
				}
				
			}
			
			
		}while( ( numberOfCounts < maxNumberOfCounts ) );
	}
	
	private void relaxForNotSameParameters(  boolean searchMin, String realFunc )
	{
		int k_relax;
		int numberOfCounts = 0;
		double J, J1, Jt;
		double s_relax = 0.1, h0;
		double[][] mas_d = new double[ paramNumber ][ paramNumber ];
		double[][] mas_h = new double[ paramNumber ][ paramNumber ];
		mas_e = new double[ paramNumber ][ paramNumber ];
		double[][] mas_temp = new double[ paramNumber ][ paramNumber ];
		double[] vect_d = new double[ paramNumber ];
		double[] vect_2sHd = new double[ paramNumber ];
		double[] previous_step_vect = new double[ paramNumber ];
		double[]  vector_x_l;
		double determinantD;
		vector_x_l = new double[paramNumber];

		vector_x_l = getVectorX();
		J = - Parser.calculationOfFunction( currentFunction, 
				paramNamesList, paramValueList, 
				notVaryingParamNamesList, notVaryingParamValueList, true );
		J1 = J;
		numberOfCounts = 0;
		
		do
		{
			for(int i = 0; i < paramNumber; i++)
			{
				vect_d[i] = count_vect_d_i(i, s_relax);
				for(int j = 0; j < paramNumber; j++)
				{
					
					mas_d[i][j] = count_matr_Dij(i, j, s_relax);
					if(i == j)
					{
						mas_e[i][j] = 1;
					}
					else
					{
						mas_e[i][j] = 0;
					}
				}
			}
//			
			System.out.println();
			for(int i = 0; i < paramNumber; i++)
			{
				System.out.println();
				for(int j = 0; j < paramNumber; j++)
				{
					System.out.print("D ["+i+"] ["+j+"] = " + mas_d[i][j] +"   ");
				}
			}
			for(int i = 0; i < paramNumber; i++)
			{
				System.out.println("d ["+i+"] = " + vect_d[i]+"   ");
			}
//			
			determinantD = vectNorm(vect_d);//determinant(mas_d);
			if( determinantD == 0 
					|| Double.isNaN( determinantD ) 
					|| Double.isInfinite( determinantD ) )
			{
//to_do begin
				if( searchMin )
				{
					currentFunction = realFunc;
				}
				cpsForNotSameParameters( searchMin );
				return;
			}
//to_do end
			
//to_do
			
			System.out.println("determinant D == "+determinantD);
			
			
//to_do			
			
			
			h0 = 0.01 / determinantD;
			System.out.println("h0 == "+h0);
			k_relax = 0;

			// �������� mas_h
			for(int i = 0; i < paramNumber; i++)
			{
				for(int j = 0; j < paramNumber; j++)
				{
					mas_h[i][j] = 0;
				}
			}

			
			// ��������� H
			double delitel;
			for(int i = 1; i < 8; i++)
			{
				mas_temp = matr_mult_number(mas_d, (-1.0)*h0 );
				mas_temp = matr_stepen(mas_temp, i - 1);
				mas_temp = matr_mult_number(mas_temp, h0);
				delitel = 1.0 / (Double) factorial(i);
				mas_temp = matr_mult_number(mas_temp, delitel);
				mas_h = matr_sum_matr(mas_h, mas_temp);
			}
//to_do			
			
			for(int i = 0; i < paramNumber; i++)
			{
				System.out.println();
				for(int j = 0; j < paramNumber; j++)
				{
					System.out.print("mas_h [ "+i+" ]  [ "+j+" ]  =  " + mas_h[i][j]+"   "); 
				}
			}
			System.out.println();
//to_do
			
			do
			{
				// step 5:
				vect_2sHd = matr_mult_vect(mas_h, vect_d);
				vect_2sHd = vect_mult_number(vect_2sHd, 2 * s_relax);
//				
				for(int i = 0; i < paramNumber; i++)
				{
					System.out.println("vect_2sHd ["+i+"] = " + vect_2sHd[i]+"   ");
				}
//				
				
				for(int i = 0; i < paramNumber; i++)
				{
					changeVectorX(i, -vect_2sHd[i] );
//!!!
				}

//to_do begin		
				if( previous_step_vect.equals( getVectorX() ) )
				{
					numberOfCounts = maxNumberOfCounts + 1;
				}
				previous_step_vect = getVectorX();
//to_do end
				
				
				
				Jt = - Parser.calculationOfFunction( currentFunction, 
						paramNamesList, paramValueList, 
						notVaryingParamNamesList, notVaryingParamValueList, true );
				k_relax++;
				
				if( Double.isNaN( Jt ) 
						|| Double.isInfinite( Jt ) )
				{
					k_relax = 21;
				}
				
				// step 6:
				System.out.println("Jt = "+Jt);
				if(J1 > Jt)
				{
					vector_x_l = getVectorX();
					J1 = Jt;
				}
				else
				{
					setVectorX( vector_x_l );
				}

				// step 7:
				mas_h = matr_mult_matr(
									   mas_h,
									   matr_sum_matr(
													 matr_mult_number(
																	  mas_e,
																	  2.0),
													 matr_mult_number(
																	  matr_mult_matr(
																					 mas_d,
																					 mas_h),
																	  -1.0)));
			}
			while(k_relax <= 20);
			setVectorX(vector_x_l);
			J = J1;
 //////////////////////// ?????
			numberOfCounts++;
// //////////////////////// ?????
		}
		while( numberOfCounts < maxNumberOfCounts );
		if( numberOfCounts > maxNumberOfCounts )
		{
			if( searchMin )
			{
				currentFunction = realFunc;
			}
			cpsForNotSameParameters(searchMin);
		}
	}
	
	double[] getVectorX()
	{
		double[] vect_rez;
		vect_rez = new double[paramNumber];
		for(int paramInd=0; paramInd < paramNumber; paramInd++)
		{
			vect_rez[paramInd] = paramValueList.get( paramInd );
		}
		return vect_rez;
	}

	void setVectorX( double[] vect_to_set)
	{
		for(int paramInd=0; paramInd < paramNumber; paramInd++)
		{
			paramValueList.set( paramInd, vect_to_set[paramInd] );
		}	
	}

	void changeVectorX(int param_index, double s_relex)
	{
		paramValueList.set( param_index, paramValueList.get(param_index) + s_relex );
	}

	double count_matr_Dij(int i, int j, double s_relex)
	{
		double d = 0;
		changeVectorX(i, s_relex);
		changeVectorX(j, s_relex);
		d = - Parser.calculationOfFunction( currentFunction,
				paramNamesList, paramValueList, 
				notVaryingParamNamesList, notVaryingParamValueList, true );
		changeVectorX(i, (-2) * s_relex);
		d = d + Parser.calculationOfFunction( currentFunction,
				paramNamesList, paramValueList, 
				notVaryingParamNamesList, notVaryingParamValueList, true );
		changeVectorX(i, 2 * s_relex);
		changeVectorX(j, (-2) * s_relex);
		d = d + Parser.calculationOfFunction( currentFunction, 
				paramNamesList, paramValueList, 
				notVaryingParamNamesList, notVaryingParamValueList, true );
		changeVectorX(i, (-2) * s_relex);
		d = d - Parser.calculationOfFunction( currentFunction,
				paramNamesList, paramValueList, 
				notVaryingParamNamesList, notVaryingParamValueList, true );
		changeVectorX(i, s_relex);
		changeVectorX(j, s_relex);
//to_do
//		System.out.println("D ["+i+"] ["+j+"] = "+d);
//to_do
		return d;
	}

	double count_vect_d_i(int i, double s_relex)
	{
		double d;
		changeVectorX(i, s_relex);
		d = -Parser.calculationOfFunction( currentFunction,
				paramNamesList, paramValueList, 
				notVaryingParamNamesList, notVaryingParamValueList, true );
		changeVectorX(i, (-2) * s_relex);
		d = d + Parser.calculationOfFunction( currentFunction,
				paramNamesList, paramValueList, 
				notVaryingParamNamesList, notVaryingParamValueList, true );
		changeVectorX(i, s_relex);
		return d;
	}

	
	double determinant(double[][] matr_isodnik)
	{
		double determ = 1, k;
		double[][] matr;
		matr = new double[paramNumber][paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			for(int j = 0; j < paramNumber; j++)
			{
				matr[i][j] = matr_isodnik[i][j];
			}
		}

		// ������� ������� � ������������� ����
		for(int i = 0; i < (paramNumber - 1); i++)
		{
			k = matr[i + 1][i] / matr[i][i];
			for(int j = 0; j < paramNumber; j++)
			{
				matr[i + 1][j] = matr[i + 1][j] - matr[i][j] * k;
			}
		}

		for(int i = 0; i < paramNumber; i++)
		{
			determ = determ * matr[i][i];
		}

		return determ;
	}

	double[][] matr_stepen(double[][] matr_isodnik, int power)
	{
		if(power == 0)
		{
			return mas_e;
		}
		if(power == 1)
		{
			return matr_isodnik;
		}
		double[][] matr;
		matr = new double[paramNumber][paramNumber];
		double[][] matr_itog;
		matr_itog = new double[paramNumber][paramNumber];
		double[][] matr_temp;
		matr_temp = new double[paramNumber][paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			for(int j = 0; j < paramNumber; j++)
			{
				matr[i][j] = -matr_isodnik[i][j];
				matr_itog[i][j] = -matr_isodnik[i][j];
			}
		}

		for(int pow_ind = 1; pow_ind < power; pow_ind++)
		{
			for(int i = 0; i < paramNumber; i++)
			{
				for(int j = 0; j < paramNumber; j++)
				{
					matr_temp[i][j] = 0;
					for(int k = 0; k < paramNumber; k++)
					{
						matr_temp[i][j] = matr_temp[i][j] + matr_itog[i][k]
								* matr[k][j];
					}
				}
			}

			for(int i = 0; i < paramNumber; i++)
			{
				for(int j = 0; j < paramNumber; j++)
				{
					matr_itog[i][j] = matr_temp[i][j];
				}
			}
		}

		return matr_itog;

	}

	double[][] matr_sum_matr(double[][] matr_a, double[][] matr_b)
	{
		double[][] matr_itog;
		matr_itog = new double[paramNumber][paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			for(int j = 0; j < paramNumber; j++)
			{
				matr_itog[i][j] = matr_a[i][j] + matr_b[i][j];
			}
		}
		return matr_itog;
	}

	double[][] matr_mult_matr(double[][] matr_a, double[][] matr_b)
	{
		double[][] matr_itog;
		matr_itog = new double[paramNumber][paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			for(int j = 0; j < paramNumber; j++)
			{
				matr_itog[i][j] = 0;
				for(int k = 0; k < paramNumber; k++)
				{
					matr_itog[i][j] = matr_itog[i][j] + matr_a[i][k]
							* matr_b[k][j];
				}
			}
		}
		return matr_itog;
	}

	double[] matr_mult_vect(double[][] matr, double[] vect)
	{
		double[] vect_itog;
		vect_itog = new double[paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			vect_itog[i] = 0;
			for(int k = 0; k < paramNumber; k++)
			{
				vect_itog[i] = vect_itog[i] + matr[i][k] * vect[k];
			}
		}
		return vect_itog;
	}

	double[][] matr_mult_number(double[][] matr_ishodn, double number)
	{
		double[][] matr_itog;
		matr_itog = new double[paramNumber][paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			for(int j = 0; j < paramNumber; j++)
			{
				matr_itog[i][j] = matr_ishodn[i][j] * number;
			}
		}
		return matr_itog;
	}

	double[] vect_mult_number(double[] vect_ishodn, double number)
	{
		double[] vect_itog;
		vect_itog = new double[paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			vect_itog[i] = vect_ishodn[i] * number;
		}
		return vect_itog;
	}

	double factorial(int number)
	{
		if((number == 0) || (number == 1))
		{
			return 1.0;
		}
		int i, itog = 1;
		i = number;
		do
		{
			itog = itog * i;
			i--;
		}
		while(i != 1);
		return itog;
	}

	double pow(double number, int pow)
	{
		if(pow == 0)
		{
			return 1;
		}
		if(pow == 1)
		{
			return number;
		}
		double itog = 1;
		int i;
		i = pow;
		do
		{
			itog = itog * number;
			i--;
		}
		while(i != 1);
		return itog;
	}
	
	double vectNorm( double[] vect )
	{
		double rezult = 0.0;
		for( int i=0; i < paramNumber; i++ )
		{
			rezult = rezult + Math.pow( vect[i], 2.0 );
		}
		return Math.pow( rezult, 0.5 );
	}
	

}
