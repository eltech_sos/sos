package simulation;

import java.util.Vector;

import Main.Main;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import simulation.HistoryOfSelforganizations;
import simulation.SimulationCountFX;
import simulation.HistoryOfSelforganizations.AddsRecord;
import simulation.HistoryOfSelforganizations.FuncRecord;
import simulation.HistoryOfSelforganizations.ParamRecord;
import simulation.HistoryOfSelforganizations.RestRecord;
import simulation.HistoryOfSelforganizations.TypeRecord;

public class AllSoTableFX extends Application {
	HistoryOfSelforganizations History;
	private int tempSelfOrgRowIndex;
	
	public AllSoTableFX(){
		tempSelfOrgRowIndex = -1;
	}
	public AllSoTableFX(int _selfOrgRow){
		tempSelfOrgRowIndex = _selfOrgRow;
	}
	
	
	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("��� ��������������");
		VBox vBox = new VBox();
		vBox.setMinSize(300, 100);

		History = new HistoryOfSelforganizations();
		for (int k = 1; k < SimulationCountFX.t; k++){
			int j = History.indexOf(k);
			if (j == -1 && tempSelfOrgRowIndex == -1)
				continue;
			if(tempSelfOrgRowIndex != -1)
				j = tempSelfOrgRowIndex;
				
			GridPane grid = new GridPane();
			grid.setHgap(20);
			grid.setVgap(0);
			grid.setPadding(new Insets(10,10,10,10));
			grid.setAlignment(Pos.CENTER);
			grid.getStyleClass().add("allSoTableGrid");
			switch(History.getSOType(j))
			{
				case 1:
					grid.add(new Label("��� � ������ t = " + k), 0, 0);
					grid.add(new Label("�������"), 1, 0);
					grid.add(new Label("���"), 2, 0);
					grid.add(new Label("�������"), 3, 0);
					grid.add(new Label("��������"), 4, 0);
					grid.add(new Label("�� ��"), 5, 0);
					grid.add(new Label("����� ��"), 6, 0);
					Vector<ParamRecord> PR = History.getParamsVector(j);
					for (int i = 0; i < PR.size(); i++) {
						grid.add(new Label(PR.get(i).ElemName), 1, i+1);
						grid.add(new Label(PR.get(i).Type), 2, i+1);
						grid.add(new Label(PR.get(i).Func), 3, i+1);
						grid.add(new Label(PR.get(i).Param), 4, i+1);
						grid.add(new Label(PR.get(i).OldVal), 5, i+1);
						grid.add(new Label(PR.get(i).NewVal), 6, i+1);
					}
					break;
				case 2:
					grid.add(new Label("��� � ������ t = " + k), 0, 0);
					grid.add(new Label("�������"), 1, 0);
					grid.add(new Label("���"), 2, 0);
					grid.add(new Label("������� �� ��"), 3, 0);
					grid.add(new Label("��������� �� ��"), 4, 0);
					grid.add(new Label("������� ����� ��"), 5, 0);
					grid.add(new Label("��������� ����� ��"), 6, 0);
					Vector<FuncRecord> FR = History.getFuncsVector(j);
					for (int i = 0; i < FR.size(); i++) {
						grid.add(new Label(FR.get(i).ElemName), 1, i+1);
						grid.add(new Label(FR.get(i).Type), 2, i+1);
						grid.add(new Label(FR.get(i).OldFunk), 3, i+1);
						grid.add(new Label(FR.get(i).OldParams), 4, i+1);
						grid.add(new Label(FR.get(i).NewFunk), 5, i+1);
						grid.add(new Label(FR.get(i).NewParams), 6, i+1);
					}
					break;
				case 3:
					grid.add(new Label("������� ��� � ������ t = " + k), 0, 0);
					grid.add(new Label("�������"), 1, 0);
					grid.add(new Label("��� �� ��"), 2, 0);
					grid.add(new Label("��� ����� ��"), 3, 0);
					grid.add(new Label("�������"), 4, 0);
					grid.add(new Label("���������"), 5, 0);
					Vector<TypeRecord> TR = History.getTypesVector(j);
					for (int i = 0; i < TR.size(); i++) {
						grid.add(new Label(TR.get(i).ElemName), 1, i+1);
						grid.add(new Label(TR.get(i).OldType), 2, i+1);
						grid.add(new Label(TR.get(i).NewType), 3, i+1);
						grid.add(new Label(TR.get(i).Funk), 4, i+1);
						grid.add(new Label(TR.get(i).Params), 5, i+1);
					}
					break;
				case 4:
					grid.add(new Label("���������������� � ������ t = " + k), 0, 0);
					grid.add(new Label("�������"), 1, 0);
					grid.add(new Label("�������� �� ��"), 2, 0);
					grid.add(new Label("�������� ����� ��"), 3, 0);
					grid.add(new Label("���"), 4, 0);
					grid.add(new Label("�������"), 5, 0);
					grid.add(new Label("���������"), 6, 0);
					Vector<RestRecord> RR = History.getRestructsVector(j);
					for (int i = 0; i < RR.size(); i++) {
						grid.add(new Label(RR.get(i).ElemName), 1, i+1);
						grid.add(new Label(RR.get(i).OldParent), 2, i+1);
						grid.add(new Label(RR.get(i).NewParent), 3, i+1);
						grid.add(new Label(RR.get(i).Type), 4, i+1);
						grid.add(new Label(RR.get(i).Funk), 5, i+1);
						grid.add(new Label(RR.get(i).Params), 6, i+1);
					}
					break;
				case 5:
					grid.add(new Label("���������� � ������ t = " + k), 0, 0);
					grid.add(new Label("�������"), 1, 0);
					grid.add(new Label("��������"), 2, 0);
					grid.add(new Label("���"), 3, 0);
					grid.add(new Label("�������"), 4, 0);
					grid.add(new Label("���������"), 5, 0);
					Vector<AddsRecord> AR = History.getAddsVector(j);
					for (int i = 0; i < AR.size(); i++) {
						grid.add(new Label(AR.get(i).ElemName), 1, i+1);
						grid.add(new Label(AR.get(i).Parent), 2, i+1);
						grid.add(new Label(AR.get(i).Type), 3, i+1);
						grid.add(new Label(AR.get(i).Funk), 4, i+1);
						grid.add(new Label(AR.get(i).Params), 5, i+1);
					}
					break;
			}
			
			vBox.getChildren().add(grid);
			
			if(tempSelfOrgRowIndex != -1) break;
		}
		if(vBox.getChildren().size() == 0){
			VBox empty = new VBox();
			empty.setPadding(new Insets(10,10,10,10));
			empty.setMinSize(300, 150);
			Label lblEmpty = new Label("��� ���������������");
			lblEmpty.setFont(Font.font("Arial", FontWeight.BOLD, 14));
			lblEmpty.setTextFill(Color.color(0.3, 0.3, 0.3));// Dark grey
			empty.getChildren().add(lblEmpty);
			vBox.getChildren().add(empty);
		}
		
		Scene scene = new Scene(vBox);
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
		stage.setScene(scene);
		stage.show();
	}
}
