//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// �������� ������� ��������������� �������������� ������
// � ���� ���������������� ��������� ��� ������� �������� �������, 
// ����������� ��
// ������������ ����������� ���
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
package simulation; 

import java.util.ArrayList; 
import java.util.HashMap;
import java.util.Map;

import Main.Main;
import Main.TreeOperations;
import Main.TypeLoader;
import simulation.ItselfOrganizationVariables;
import object.TreeElement;

public class TypeOrganization 
{
	ArrayList<TreeElement> changedElementsList = new ArrayList<TreeElement>(); 
	//������ ���������, ���������� ������� ��
	ArrayList<TreeElement> currentChangedElementsList = new ArrayList<TreeElement>(); 
	int changedElementsCount; // ���������� ���������, ���������� ���
	
	
	ArrayList<String> convolutionFuncPriorForLists = new ArrayList<String>();
	//������ �������� ������� ������� � ������� �������� ������ �������� � 
	//��������-����� ��� ���� �������
	
	//---------------------------------------------------------------------------
	// �������-�����������
	//---------------------------------------------------------------------------
	public TypeOrganization()
	{
		changedElementsList.clear(); // �������� ������ ���������� ���������
		
		TreeElement elementInStruct; // ������� �������
		
		convolutionFuncPriorForLists.clear(); // �������� ������ ������� �������
		convolutionFuncPriorForLists.addAll //�������� ��� ������� �������
		( ItselfOrganizationVariables.convolutionFuncPriorForLeafs ); 
		
		// ��������� ������ ���������, ���������� ���
		for( int level = ( Main.structTreeElements.size() - 1 ) ; 
		level >= 0 ; level-- ) // �������� �� ���� ������� ������
		{
			for(int elemIndex=0; 
					elemIndex<Main.structTreeElements.elementAt(level).size(); 
					elemIndex++)
			// �������� �� ���� ��������� ������� ������
			{
				// ���������� ������� �������
				elementInStruct = (TreeElement)Main.structTreeElements.
									elementAt(level).elementAt(elemIndex);
				if( ( elementInStruct.type == 2 ) // ���� ��� ��
						&& ( elementInStruct.itsImportantForRootValue ) )
						//...� ������� ������ �� �������� � �����
					
				{
					if( ( ! ItselfOrganizationVariables.useParticipationInTypeOrg ) 
							// ���� ������� �������� '��������� �������� "��������� � ��" 
							//��� ������� ��)'
							|| TypeLoader.isParticipate(elementInStruct.typeName) ) 
							// � ���� ��� ��������� � �� 
					{
						changedElementsList.add( elementInStruct ); 
						// ��������� ���� ������� � ������ ���������, ���������� ���
					}
				}
			}
		}
		
		// ���� ������� ��������, ���������� ���
		if( changedElementsList.size() > 0 )
		{
			// ��������� ���������� ���������, ���������� ���
			changedElementsCount = changedElementsList.size();
			//���� ��������� ��� � ���������� �������� "��������� �������� ����������"
			if( ItselfOrganizationVariables.sameParametrsInParamOrg 
					&& ItselfOrganizationVariables.paramOrgAllowed  )
			{
				// ��������� ��� ��� ���������� �������� ����������
				typeOrganizationForSameParametrs();
			}
			else
			{
				// ����� ��������� ��� ��� ������ �������� ����������
				typeOrganizationForNOTSameParametrs();
			}
		}
	}

	//---------------------------------------------------------------------------
	// ��� ��� �������, ��� ���������� ��������� ����� ��������� ������ ��������
	//---------------------------------------------------------------------------
	private void typeOrganizationForNOTSameParametrs() 
	{

		TreeElement elementInStruct;
		// ���� ��������� ���
		if( ItselfOrganizationVariables.typeLeafOrgAllowed )
		{
//			setConvolutionFuncPriorForLists();
			// �������� �� ���� ���������, ���������� ���
			for( int elemInd=0; elemInd < changedElementsCount; elemInd++ )
			{
				elementInStruct = changedElementsList.get( elemInd ); 
				chooseElementTypeAndFunctionAndSetLists( elementInStruct );
				// ������������� ����������� ��� ��� ������� ��������
			}
		}
		else // ���� ��� �� ���������
		{
			// ������� ������ �������� ���������
			ArrayList<TreeElement> currentElemChildList = new ArrayList<TreeElement>();
			// �������� �� ���� ���������, ���������� ���
			for( int elemInd=0; elemInd < changedElementsCount; elemInd++ )
			{
				elementInStruct = changedElementsList.get( elemInd ); 
				// ������� ������ �������� ���������
				currentElemChildList.clear();
				// ��������� ��� �������� �������� �������� ��������
				currentElemChildList = 
					TreeOperations.findAllChildrenInStructByIDAndLevel(
						elementInStruct.ID,
						elementInStruct.Level);
				// ������������� ����������� ��� ��� ������� ��������
				chooseElementTypeAndFunction( currentElemChildList, elementInStruct );
			}
			
		}
	}
	
	//---------------------------------------------------------------------------
	// ��� ��� �������, ��� ���������� ��������� ������ ��������� ���� ��������
	//---------------------------------------------------------------------------
	private void typeOrganizationForSameParametrs() 
	{
		typeOrganizationForNOTSameParametrs();
	}
		
	//---------------------------------------------------------------------------
	// ����� ������������ ����, ���� ��� ��� ������ ���������
	//---------------------------------------------------------------------------
	private double chooseElementTypeAndFunction( 
			ArrayList<TreeElement> childrenList, // ������ �������� ���������
			TreeElement parentElem)				// ������������ �������
	{
		TreeElement childElem;			// ��������� �� ������� �������� �������
		double parentCountSum = 0.0; //�������� � �������� ��� �������-�����
		double parentCountSumMult = 0.0;//�������� � �������� ��� ������� � ����������
		double parentCountSumPow = 0.0;//�������� � �������� ��� ������� �� ��������
		String bestTypeName = null; // ����������� ���
		boolean elemHasListChild = false; // � �������� ���� ����-��
		boolean elemHasInterChild = false; // � �������� ���� ����-��
		
		// �������� �� ������� �������� ���������
		for(int childInd = 0; childInd < childrenList.size(); childInd++)
		{
			childElem = childrenList.get(childInd);
			// ���� ��� ��
			if( childElem.type == 2 )
			{
				elemHasInterChild = true; // � �������� ���� ����=��
			}
			else
			{
				elemHasListChild = true; // � �������� ���� ����-��
			}
			
			// ���� ������� ������ �� �������� ��������
			if( childElem.itsImportantForRootValue )
			{
				//��� �������-�����
				if( ItselfOrganizationVariables.
						interTypesContainsSumFunc.size() > 0 )
				{
					parentCountSum = parentCountSum +
					childElem.elementCount;// �������� �������� 
				}
				else
				{
					parentCountSum = Double.NEGATIVE_INFINITY;
				}
				// ��� ������� � ����������
				if( ItselfOrganizationVariables.
						interTypesContainsSumMultFunc.size() > 0 )
				{
					parentCountSumMult = parentCountSumMult + 
					childElem.elementCount * 
					TypeLoader.getFactorForSumMultFunction( 
							childElem.typeName ); // �������� ��������
				}
				else
				{
					parentCountSumMult = Double.NEGATIVE_INFINITY;
				}
				
				// ��� ������� �� ��������
				if( ItselfOrganizationVariables.
						interTypesContainsSumFunc.size() > 0 )
				{
					parentCountSumPow = parentCountSumPow + 
					Math.pow( childElem.elementCount, 
							TypeLoader.getFactorForSumPowFunction( childElem.typeName ) 
							); // �������� ���������
				}
				else
				{
					parentCountSumPow = Double.NEGATIVE_INFINITY;
				}
				
				
				
			}
		}
		
		//���������� �������
		// �������� ������ ������������������ �������
		ArrayList<String> sequenceOfFunctions = new ArrayList<String>();
		// ����������������� c sum(Xi) 
		if( ( parentCountSum >= parentCountSumMult) 
				&& ( parentCountSum >= parentCountSumPow ) )
		{
			sequenceOfFunctions.add("sum(Xi)"); // ���������
			// sum(Xi), sum(Xi*Ki), sum(Xi^Ki)
			if( parentCountSumMult >= parentCountSumPow )
			{
				sequenceOfFunctions.add("sum(Xi*Ki)");// ���������
				sequenceOfFunctions.add("sum(Xi^Ki)");// ���������
			}
			else
			// sum(Xi), sum(Xi^Ki), sum(Xi*Ki) 
			{
				sequenceOfFunctions.add("sum(Xi^Ki)");// ���������
				sequenceOfFunctions.add("sum(Xi*Ki)");// ���������
			}
		}
		else
		{
			// ����������������� c sum(Xi*Ki) 
			if( parentCountSumMult >= parentCountSumPow )
			{
				sequenceOfFunctions.add("sum(Xi*Ki)");// ���������
				//sum(Xi*Ki), sum(Xi), sum(Xi^Ki)
				if( parentCountSum >= parentCountSumPow )
				{
					sequenceOfFunctions.add("sum(Xi)");// ���������
					sequenceOfFunctions.add("sum(Xi^Ki)");// ���������
				}
				else
				//sum(Xi*Ki), sum(Xi^Ki),sum(Xi)
				{
					sequenceOfFunctions.add("sum(Xi^Ki)");// ���������
					sequenceOfFunctions.add("sum(Xi)");// ���������
				}
			}
			else
			{
				// ����������������� c sum(Xi^Ki) 
				sequenceOfFunctions.add("sum(Xi^Ki)");// ���������
				// sum(Xi^Ki), sum(Xi), sum(Xi*Ki)
				if( parentCountSum >= parentCountSumMult )
				{
					sequenceOfFunctions.add("sum(Xi)");// ���������
					sequenceOfFunctions.add("sum(Xi*Ki)");// ���������;
				}
				else
					// sum(Xi^Ki), sum(Xi*Ki), sum(Xi),
				{
					sequenceOfFunctions.add("sum(Xi*Ki)");// ���������
					sequenceOfFunctions.add("sum(Xi)");// ���������
				}
			}
		}
		
		//��� �������� ��������� ��� ������������� ������������ ���
		// 0 - ��� �������� ���������
		// 1 - ������ �������� ��
		// 2 - ������ �������� ��
		// 3 - ��� ��, ��� � �� 
		int currentChildrenType; // ��� �������� ���������
		if( elemHasInterChild && elemHasListChild )// ���� ����������� �� � ��
		{
			currentChildrenType = 3; // ��� ��, ��� � �� 
		}
		else
		{
			if( elemHasInterChild ) // ���� ��
			{
				currentChildrenType = 2; //������ �������� ��
			}
			else if( elemHasListChild ) // ���� ��
			{
				currentChildrenType = 1; //������ �������� ��
			}
			else
			{
				currentChildrenType = 0; //��� �������� ���������
			}
		}
		
		// ��� ������ ������� ������� ���� ���, 
		// ������� ����������� ���������� ������, ����� �������� �������� ��������,
		// ���� ����� ��� ������, ����� ���������������.
		// ���� ��� ����� �����������, ��� ��� ������� ������������� �� ��������
		
		// �������� �� ���� ��������
		for(int funcInd=0; funcInd < sequenceOfFunctions.size(); funcInd++ )
		{
			// ���������� ������ ��� ��� ������ ������� �������
			bestTypeName = findBestTypeForFunc( sequenceOfFunctions.get(funcInd) , 
					parentElem.iChildrenCount, currentChildrenType );
			// ���� ������ ��� ������
			if( bestTypeName != null )
			{
				parentElem.typeName = bestTypeName; // ������������� ���
				parentElem.functionName = sequenceOfFunctions.get(funcInd);// � �������
				parentElem.elementCount = 0.0; // �������� � ��������
				if( parentElem.functionName.equals("sum(Xi)") )// ���� �����
				{
					parentElem.elementCount = parentCountSum; //��������
				}
				else if( parentElem.functionName.equals("sum(Xi*Ki)") )//���������
				{
					parentElem.elementCount = parentCountSumMult; //��������
				}
				else if( parentElem.functionName.equals("sum(Xi^Ki)") )// �������
				{
					parentElem.elementCount = parentCountSumPow; //��������
				}
				
				return 1.0; // ����� �� ������� (�����)
			}
		}
		
		parentElem.elementCount = 0.0; // ���������� �������� 
		return 0.0; // ����� �� ������� (�������)
	}
	
	//---------------------------------------------------------------------------
	// ����� ������������ ����, ���� ��� ��� ������ ���������
	//---------------------------------------------------------------------------
	//use if type organization for lists is allowed
	private void chooseElementTypeAndFunctionAndSetLists(TreeElement parentElem )
	{
		TreeElement elementInStruct; // ������� �������
		// ������� ������ �������� ���������
		ArrayList<TreeElement> childrenList =  new ArrayList<TreeElement>();
		boolean elemHasImportantInterChild = false; // � �������� ���� ������ ����-�� 
		int parentLev; // ������� ��������
		int parentID; // ID ��������
		
		boolean elemHasInterChild = false, // � �������� ���� ����-��
		elemHasListChild = false; // � �������� ���� ����-��
		
		
		parentLev = parentElem.Level;  // ������� ��������
		parentID = parentElem.ID; // ID ��������
		
		// ���� ������������ ������� �� �� ��������� ������, �.� �� �������� ������
		if( Main.structTreeElements.size() > ( parentLev + 1 ) )
		{
			// �������� �� ���� ��������� ���������� ������
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt( parentLev+1 ).size(); elemIndex++)
			{
				elementInStruct = (TreeElement)Main.structTreeElements.elementAt( parentLev+1 ).elementAt(elemIndex);
				// ���� �������� �������� �������� ���������
				if( elementInStruct.ParentID == parentID)
				{
					// ���� ������� ������� - ��
					if( elementInStruct.type == 2 )
					{
					
						 elemHasInterChild = true; // ���� �������� ��
						if( elementInStruct.itsImportantForRootValue ) // ���� ������ �� ������
						{
							//���� ������ �������� ��
							elemHasImportantInterChild = true;
						}
					}
					else
					{
						// ���� �������� ��
						elemHasListChild = true;
					}
					
					childrenList.add(elementInStruct); //��������� ������� ������� � ������ �����
				}
			}
		}
		
		//��� �������� ��������� ��� ������������� ������������ ���
		// 0 - ��� �������� ���������
		// 1 - ������ �������� ��
		// 2 - ������ �������� ��
		// 3 - ��� ��, ��� � �� 
		int currentChildrenType; 
		// ���� �� � ��
		if( elemHasInterChild && elemHasListChild )
		{
			currentChildrenType = 3; // ��� ��, ��� � �� 
		}
		else
		{
			if( elemHasInterChild )// ���� ��
			{
				currentChildrenType = 2;// ������ �������� ��
			}
			else if( elemHasListChild )// ���� ��
			{
				currentChildrenType = 1;// ������ �������� ��
			}
			else
			{
				currentChildrenType = 0;// ��� �����
			}
		}
		
		if( elemHasImportantInterChild )// ���� ���� ������ ��
		{
			// �������� �� � �� - ������ ����������� �������
			interAndListsForListTypeOrgAllowed( parentElem,
					childrenList, 
					currentChildrenType);
		}
		else
		{
			// ������ �������� �� - ������ ����������� �������
			onlyListsForListTypeOrgAllowed( parentElem, 
					childrenList,
					currentChildrenType);
		}
		
		
	}
	
	//---------------------------------------------------------------------------
	// ������ ����������� ������� ��� ��, ������ � ��������� ��
	//---------------------------------------------------------------------------

	//set for choosing element best type and functions if
	//its children is only lists (or intermediate elements is zero):
	//find best possible convolution function and type
	//and set lists to the best type for selected function
	private double onlyListsForListTypeOrgAllowed( 
			TreeElement parentElem,  // ������������ �������
			ArrayList<TreeElement> childrenList, // ������ �������� ���������
			int currentChildrenType )//��� �������� ��������� 
	{
		String funcName, typeName; // ��� ������� � ����
		// �������� �� ���� ��������
		for( int funcInd=0;
		funcInd < convolutionFuncPriorForLists.size(); 
		funcInd++ )
		{
			funcName = //��� �������
				convolutionFuncPriorForLists.get(funcInd);
			// ������ ��� ��� ������ ������� �������
			typeName = findBestTypeForFunc( funcName,
					parentElem.iChildrenCount, 
					currentChildrenType); //��� ����
			//���� ��� ������
			if( typeName != null )
			{
				parentElem.typeName = typeName;//��� ����
				parentElem.functionName = funcName;//��� �������
				parentElem.elementCount = 0.0;// �������� � ��������
				setAllListMaxForFunc( funcName, childrenList );
				//������� �������� � �� � ������ ��������� �������
				intermediateCount( parentElem, childrenList );
				return 1.0;//������� ����������
			}
		}
//		parentElem.elementCount = 0.0;
		return 0.0; //��������� ����������
	}
	
	
	//---------------------------------------------------------------------------
	// ������ ������������ ���� ��� ������ ������� �������
	//---------------------------------------------------------------------------
	private void setAllListMaxForFunc(String funcName,
			ArrayList<TreeElement> childrenList) 
	{
		String listBestType=""; // ������ ��� ��� ������ ������� �������
		String listBestFunc="0"; //������ �������
		// ����������� �������� ����������
		Map<String, Double> bestParamValuesMap = new HashMap<String, Double>();
		double elemBestCount = 0.0; //������ �������� � ��������
		
		int typeInd; //������ ����
		
		//��� sum(Xi)
		if( funcName.equals("sum(Xi)") )
		{
			listBestType = ItselfOrganizationVariables.
			forSumBestElementaryChild;
		}
		//��� sum(Xi*Ki)
		else if( funcName.equals("sum(Xi*Ki)") )
		{
			listBestType = ItselfOrganizationVariables.
			forSumMultElementaryChild;
		}
		//��� sum(Xi^Ki)
		else if( funcName.equals("sum(Xi^Ki)") )
		{
			listBestType = ItselfOrganizationVariables.
			forSumPowBestElementaryChild;
		}
		
		//���� ���� ��� ��������� � ��
		if( ItselfOrganizationVariables.
				usedInOrganizationElementaryTypesList
				.contains(listBestType) )
		{
			typeInd = ItselfOrganizationVariables.
			usedInOrganizationElementaryTypesList.
			indexOf(listBestType); //������ ���
			
			listBestFunc = ItselfOrganizationVariables.
			typesMaxFuncList.get( typeInd ); //�������
			
			elemBestCount = ItselfOrganizationVariables.
			typesMaxValuesList.get( typeInd ); //�������� � �����
			
			bestParamValuesMap = ItselfOrganizationVariables.
			paramMapsForFuncMax.get( listBestFunc ); //����� ����������
			
		}
		 
		
		// ��� ������ �����, ������������ � ��
		// ���������� ����������� ���������
		// ��� ������ ������� �������
		TreeElement elementInStruct; //������� �������
		
		//�������� �� ���� �������� ���������
		for( int childInd=0; 
		childInd < childrenList.size(); 
		childInd++  )
		{
			//��������� �� ������� �������
			elementInStruct = childrenList.get( childInd );
			if( elementInStruct.type == 3 )
				//���� ��� ��
			{
				// ���� ������������ ������� � �� ��� 
				// ��� ��������� � ��
				if( ( ! ItselfOrganizationVariables.
						useParticipationInTypeOrg ) ||
						TypeLoader.isParticipate( 
								elementInStruct.typeName ) )
				{
					elementInStruct.typeName = listBestType; //������ ���
					elementInStruct.functionName = listBestFunc; //������ �������
					elementInStruct.elementCount = elemBestCount; //������ ��������
					if( bestParamValuesMap != null )
					{
						elementInStruct.parametersValuesMap 
						= bestParamValuesMap; //����� ����������
					}
					else
					{
						elementInStruct.parametersValuesMap = 
							new HashMap<String, Double>();//����� ����� ����������
					}
					TreeOperations.
					fillingParametrsWeightMap
					(elementInStruct); //����� �����
				}
			}
			
		}
		
	}
	
	
	//---------------------------------------------------------------------------
	// ������ ����������� ������� ��� ��, � ��������� �� � ��
	//---------------------------------------------------------------------------
	private double interAndListsForListTypeOrgAllowed( 
			TreeElement parentElem, //������������ �������
			ArrayList<TreeElement> childrenElemList, //������ �������� ���������
			int currentChildrenType) // ��� �������� ���������
	{
		TreeElement childElem; // ������� �������� �������
		String bestTypeName; // ��� ������� ����
		double parentCountSum = 0.0; //�������-�����
		double parentCountSumMult = 0.0; //������� � ����������
		double parentCountSumPow = 0.0; //������� �� ��������
		
		//������ �������� �� �������� ��������, ����������� � ��
		ArrayList<TreeElement> childrenListList = 
		new ArrayList<TreeElement>();


		//���� ����������� ������� � ���
		if( ItselfOrganizationVariables.useParticipationInTypeOrg )
			
		{
			//�������� �� ���� �������� ���������
			for(int childInd=0; 
			childInd < childrenElemList.size(); childInd++ )
			{
				//������� �������� �������
				childElem = childrenElemList.get( childInd );
				//���� ��� �� ��� ��, �� ���������� � ��
				if( childElem.type == 2 
						|| ( ! TypeLoader.
						isParticipate(childElem.typeName) ) )
					
				{
					//���� ����������� �������-�����
					if( ItselfOrganizationVariables.
							interTypesContainsSumFunc.size() > 0 )
					{
						parentCountSum = parentCountSum +
						childElem.elementCount; //�������� �������� � ��������
					}
					else
					{
						parentCountSum = Double.NEGATIVE_INFINITY;
					}
					// ��� ������� � �����������
					if( ItselfOrganizationVariables.
							interTypesContainsSumMultFunc.size() > 0 )
					{
						parentCountSumMult = parentCountSumMult + 
						childElem.elementCount * 
						TypeLoader.getFactorForSumMultFunction( 
								childElem.typeName ); //�������� �������� � ��������
					}
					else
					{
						parentCountSumMult = Double.NEGATIVE_INFINITY;
					}
					
					//��� �������-�������
					if( ItselfOrganizationVariables.
							interTypesContainsSumFunc.size() > 0 )
					{
						parentCountSumPow = parentCountSumPow + 
						Math.pow( childElem.elementCount, 
								TypeLoader.getFactorForSumPowFunction( childElem.typeName ) 
								); // �������� �������� � ��������
					}
					else
					{
						parentCountSumPow = Double.NEGATIVE_INFINITY;
					}
					
					
					
				}
				else
					// ���� ������� �������� ������� - ��, ����������� � ���
				{
					childrenListList.add( childElem );
				}
			}
		}
		else
			//���� ��� �������� �������� ��������� � ���
			
		{
			for(int childInd=0; 
			// �������� �� ���� ���������
			childInd < childrenElemList.size(); childInd++ )
			{
				childElem = childrenElemList.get( childInd );
				if( childElem.type == 2  )
					//���� ��� ��
					
				{
					// ��� �������-�����
					if( ItselfOrganizationVariables.
							interTypesContainsSumFunc.size() > 0 )
					{
						parentCountSum = parentCountSum +
						childElem.elementCount; // �������� �������� � ��������
					}
					else
					{
						parentCountSum = Double.NEGATIVE_INFINITY;
					}
					//��� ������� � ����������
					if( ItselfOrganizationVariables.
							interTypesContainsSumMultFunc.size() > 0 )
					{
						parentCountSumMult = parentCountSumMult + 
						childElem.elementCount * 
						TypeLoader.getFactorForSumMultFunction( 
								childElem.typeName );// �������� �������� � ��������
					}
					else
					{
						parentCountSumMult = Double.NEGATIVE_INFINITY;
					}
					
					// ��� ������� �� ��������
					if( ItselfOrganizationVariables.
							interTypesContainsSumFunc.size() > 0 )
					{
						parentCountSumPow = parentCountSumPow + 
						Math.pow( childElem.elementCount, 
								TypeLoader.getFactorForSumPowFunction( childElem.typeName ) 
								);// �������� �������� � ��������
					}
					else
					{
						parentCountSumPow = Double.NEGATIVE_INFINITY;
					}
					
					
					
				}
				else
					//���� ��� �� 
				{
					childrenListList.add( childElem );
				}
			}
		}
		
		//���������� ������� �������
		ArrayList<String> sequenceOfFunctions = new ArrayList<String>();
		// ������� � sum(Xi)
		if( ( parentCountSum >= parentCountSumMult) 
				&& ( parentCountSum >= parentCountSumPow ) )
		{
			sequenceOfFunctions.add("sum(Xi)"); //���������� ��������
			if( parentCountSumMult >= parentCountSumPow )
			{
				//sum(Xi), sum(Xi*Ki), sum(Xi^Ki) 
				sequenceOfFunctions.add("sum(Xi*Ki)");//���������� ��������
				sequenceOfFunctions.add("sum(Xi^Ki)");//���������� ��������
			}
			else
			{
				//sum(Xi), sum(Xi^Ki), sum(Xi*Ki) 
				sequenceOfFunctions.add("sum(Xi^Ki)");//���������� ��������
				sequenceOfFunctions.add("sum(Xi*Ki)");//���������� ��������
			}
		}
		else
		{
			// ������� � sum(Xi*Ki)
			if( parentCountSumMult >= parentCountSumPow )
			{
				sequenceOfFunctions.add("sum(Xi*Ki)");//���������� ��������
				if( parentCountSum >= parentCountSumPow )
				{
					//sum(Xi*Ki), sum(Xi), sum(Xi^Ki)
					sequenceOfFunctions.add("sum(Xi)");//���������� ��������
					sequenceOfFunctions.add("sum(Xi^Ki)");//���������� ��������
				}
				else
				{
					//sum(Xi*Ki), sum(Xi^Ki), sum(Xi) 
					sequenceOfFunctions.add("sum(Xi^Ki)");//���������� ��������
					sequenceOfFunctions.add("sum(Xi)");//���������� ��������
				}
			}
			else
			{
				// ������� � sum(Xi^Ki)
				sequenceOfFunctions.add("sum(Xi^Ki)");//���������� ��������
				if( parentCountSum >= parentCountSumMult )
				{
					//sum(Xi^Ki), sum(Xi), sum(Xi*Ki)
					sequenceOfFunctions.add("sum(Xi)");//���������� ��������
					sequenceOfFunctions.add("sum(Xi*Ki)");//���������� ��������
				}
				else
				{
					//sum(Xi^Ki), sum(Xi*Ki), sum(Xi)
					sequenceOfFunctions.add("sum(Xi*Ki)");//���������� ��������
					sequenceOfFunctions.add("sum(Xi)");//���������� ��������
				}
			}
		}
		
		
		//��� ����� �������-������� ������� ���
		// ������� ����������� ����������� ������, ����� ��������
		// �������� ��������
		// ���� ����� ��� ������, �� ����� ���������, �.�. ������� �������������
		
		//�������� �� ���� �������� �������
		for(int funcInd=0; funcInd < sequenceOfFunctions.size(); funcInd++ )
		{
			//������ ��� ��� ������ ������� �������
			bestTypeName = findBestTypeForFunc( sequenceOfFunctions.get(funcInd) , 
					parentElem.iChildrenCount, currentChildrenType );
			//���� ��� ������
			if( bestTypeName != null )
			{
				parentElem.typeName = bestTypeName; //��� ����
				parentElem.functionName = sequenceOfFunctions.get(funcInd); // ��� �������
				parentElem.elementCount = 0.0; //�������� � ������������ ��������
				if( childrenListList.size() > 0 )
				{
					//���������� ���������� ������� � �������� ���������
					setAllListMaxForFunc( parentElem.functionName, childrenListList );
				}
				intermediateCount( parentElem, childrenElemList ); //������� ��������� � ��
				
				return 1.0; //���������� ���������� �������
			}
		}
		
		parentElem.elementCount = 0.0;
		return 0.0; //��������� ���������� �������
		
		
	}

	//define best type contains selected function
	//with selected or more slots number
	//and not contradicting for current children type of possible children
	
	//---------------------------------------------------------------------------
	// ������ ������������ ���� ��� ������ ������� �������
	//---------------------------------------------------------------------------
	private String findBestTypeForFunc
	(String function, //������� �������
			int slotsNumber, //����� ������
			int childrenType)  //��� �������� ���������
	{
		//������ �����
		ArrayList<String> typeList = new ArrayList<String>();
		
		//���� sum(Xi), ��������� ��� ������� ������� ����
		if( function.equals("sum(Xi)") )
		{
			typeList.addAll( 
					ItselfOrganizationVariables.
					interTypesContainsSumFunc );
		}
		//���� sum(Xi*Ki), ��������� ��� ������� ������� ����
		else if( function.equals("sum(Xi*Ki)") )
		{
			typeList.addAll( 
					ItselfOrganizationVariables.
					interTypesContainsSumMultFunc );
		}
		//���� sum(Xi^Ki), ��������� ��� ������� ������� ����
		if( function.equals("sum(Xi^Ki)") )
		{
			typeList.addAll( 
					ItselfOrganizationVariables.
					interTypesContainsSumPowFunc );
		}
		
		String typeName; // ��� ����
		//�������� �� ���� �����
		for( int typeInd=0;  typeInd < typeList.size(); typeInd++ )
		{
			typeName = typeList.get( typeInd ); //������� ���
			//���� ����������� ���������� ������
			if( (TypeLoader.getSlotsNumber(typeName) >= slotsNumber) )
			{
				// ���� � �������� ��� �����, ��� ��������� ��� ��������
				// �� ����� ������, ��� ���� ���������
				if( ( childrenType == 0 )
						|| (TypeLoader.getChildrenType(typeName) == childrenType) 
						|| (TypeLoader.getChildrenType(typeName) == 3) )
				{
					return typeName; //��� ������
				}
			}
		}
		
		return null; //��������� ����������
	}
	
	
	//---------------------------------------------------------------------------
	// �������� �������� � ��
	//---------------------------------------------------------------------------
	//---------------------------------------------------------------------------
	// ������� �������� � ��
	//---------------------------------------------------------------------------
	public static double intermediateCount(
			TreeElement element, // ��
			ArrayList<TreeElement> childList) //������ �����
	{
		// ���� ������ �������� ��������� ��������
		if( childList.size() > 0)
		{
			TreeElement childElem;// ������� �������� �������
			double elemCount = 0.0; //�������� � ��������
			String convolutionFunction; // ������� �������
			convolutionFunction = element.functionName;
			childElem = childList.get( 0 ); //������ �������� �������
			if( convolutionFunction.equals( "sum(Xi)" ) )
			{
				elemCount = childElem.elementCount; //sum(Xi)
			}
			else if( convolutionFunction.equals( "sum(Xi*Ki)" ) )
			{
				elemCount = childElem.elementCount * //sum(Xi*Ki)
				TypeLoader.getFactorForIntermediateFunction( childElem.typeName, convolutionFunction);
			}
			else if( convolutionFunction.equals( "sum(Xi^Ki)" ) )
			{
				elemCount = Math.pow( childElem.elementCount, //sum(Xi^Ki)
				(double) TypeLoader.getFactorForIntermediateFunction( childElem.typeName, convolutionFunction) );
			}
			
			
			
			int childInd = 1; 
			//�������� �� ����� ������� �������� ���������
			while( childInd < childList.size() )
			{
				childElem = childList.get( childInd ); // ������� �������� �������
				if( convolutionFunction.equals( "sum(Xi)" ) )
				{
					elemCount = elemCount + childElem.elementCount;//sum(Xi)
				}
				else if( convolutionFunction.equals( "sum(Xi*Ki)" ) )
				{
					// sum(Xi*Ki)
					elemCount = elemCount + 
					childElem.elementCount * TypeLoader.getFactorForIntermediateFunction
					( childElem.typeName, convolutionFunction);
				}
				else if( convolutionFunction.equals( "sum(Xi^Ki)" ) )
				{
					//sum(Xi^Ki)
					elemCount = elemCount + Math.pow( childElem.elementCount, 
					(double) TypeLoader.getFactorForIntermediateFunction
					( childElem.typeName, convolutionFunction) );
				}
				else//pow(Xi) ��� pow(Xi*Ki)
				{
					elemCount = elemCount * childElem.elementCount * 
					TypeLoader.getFactorForIntermediateFunction( childElem.typeName, convolutionFunction);
				}
				childInd++; // ��������� � ���������� ��������
			}
			element.elementCount = elemCount;//��������� �������� � ������������ ��������
		}
		else
		{
			element.elementCount = 0; //��������� ����������
		}
//		element.itsCounted = true;
		return element.elementCount;
	}
}
