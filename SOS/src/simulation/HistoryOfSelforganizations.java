package simulation;

/******************************************************************
 * 
 * ���� �������� �� ���������� ������� ��������� ������� � ����
 * ���������� ��������������� ��� ������������ ������ ����������
 * �� ���� ���������� ������������ � ������ �� ����� ��� ���� ��.
 * 
 ******************************************************************/

import java.util.Vector;

import Main.Main;
import simulation.ItselfOrganizationVariables;

public class HistoryOfSelforganizations {
	public static Vector<SORecord> History;
	
	public class SORecord
	{ // ���������, �������� ������ ��� ���� �� �� ����� ����
		public int time; // ����� ���� (�������� �������)
		public Vector<ParamRecord> PRs; // ���
		public Vector<FuncRecord> FRs;  // ���
		public Vector<TypeRecord> TRs;  // ���
		public Vector<RestRecord> RRs;  // ����������������
		public Vector<AddsRecord> ARs;  // ���������� ���������
		
		public SORecord (int t)
		{ // �����������
			time = t;
		}
	};
	
	public class ParamRecord
	{ // ���������, �������� ���� ������ � ���
		public String 	ElemName,	// ��� ��������
						Type,		// ��� ��������
						Func,		// ������� ��-��
						Param,		// ����� � ����������
						OldVal,		// ���-�� �� ��
						NewVal;		// ���-�� ����� ��
		
		public ParamRecord(String E){
			ElemName = E;
		}
	};
	
	public class FuncRecord
	{ // ���������, �������� ���� ������ �� ���
		public String	ElemName,	// ��� ��������
						Type,		// ��� ��������
						OldFunk,	// ������� �� ��
						OldParams,	// ��������� �� ��
						NewFunk,	// ������� ����� ��
						NewParams;	// ��������� ����� ��
	};
	
	public class TypeRecord
	{ // ���������, �������� ���� ������ � ���
		public String	ElemName,	// ��� ��������
						OldType,	// ��� �� ��
						NewType,	// ��� ����� ��
						Funk,		// ������� ����� ��
						Params;		// Ÿ ���������
	};
	
	public class RestRecord
	{ // ���������, �������� ���� ������ � ����������������
		public String	ElemName,	// ��� ��������
						OldParent,	// �������� �� ��
						NewParent,	// �������� ����� ��	
						Type,		// ��� ��������
						Funk,		// ������� ��������
						Params;		// Ÿ ���������
		public int 		OldPID,		// ID �������� �� ��
						NewPID,		// ID �������� ����� ��
						ID,			// ID ��������
						level;		// ������� ��������
	};
	
	public class AddsRecord
	{ // ���������, �������� ���� ������ � ���������� ��������
		public String	ElemName,	// ��� ��������
						Parent,		// ��� ��������
						Type,		// ��� ��������
						Funk,		// �������
						Params;		// Ÿ ���������
		public int 		ID,			// ID ��������
						PID,		// ID ��������
						level;		// ������� ��������
	};
	
	public void getForParams(boolean after)
	// after - ����, �����������, ��� ��� �� ���������
	{ // ������� ������ ��������� ������� �� � ����� ���
		// ������ ������� � ���
		Vector<ParamRecord> Params = new Vector<ParamRecord>();
		int Curind; // ������� ������ ������� � ������ ����������
		String Parmap; // ������ ����������
		ParamRecord PR; // ������ � ���
		for(int i = 0; i < ItselfOrganizationVariables.leafsList.size(); i++ )
		{ // ���� �� ���� ������-�� �������
			// ������ ���������� (<��������>=<��������>)
			Parmap = ItselfOrganizationVariables.leafsList.get(i).parametersValuesMap.toString();
			while(true)
			{ // ����, ����������� ������ ������� � ���
				// ����� ������ � ���
				PR = new ParamRecord(ItselfOrganizationVariables.leafsList.get(i).elementName);
				// ���������� ��� � ������� �������
				PR.Type = ItselfOrganizationVariables.leafsList.get(i).typeName;
				PR.Func = ItselfOrganizationVariables.leafsList.get(i).functionName;
				Curind = Parmap.indexOf("="); // ������ ����� ����� ���������
				if (Curind < 2)
					break; // ���� ������ ��� ����������
				PR.Param = Parmap.substring(1, Curind); // ��� ���������
				Parmap = Parmap.substring(Curind);
				Curind = Parmap.indexOf(", "); // ������ ����� ��������
				if (Curind > -1) { // ���� ��� �� ��������� ��������
					PR.OldVal = Parmap.substring(1, Curind);
					Parmap = Parmap.substring(Curind+1);
					Params.add(PR);
				} else
					break;
			}
			Curind = Parmap.indexOf("}");
			PR.OldVal = Parmap.substring(1, Curind);
			Params.add(PR); // ��������� ������ � ���
		}
		if(! after){ // ���� ��� ��� �� ����
			History.lastElement().PRs = Params;
		} else { // ���� ��� ��� ��������� 
			for (int i = 0; i < Params.size(); i++)
			{ // ���������� �������� ������� ��������� �� � ����� ���
				if(!History.lastElement().PRs.get(i).OldVal.equals(Params.get(i).OldVal))
					History.lastElement().PRs.get(i).NewVal = Params.get(i).OldVal;
				else { // ���� ��� ����������, ������� ��.
					History.lastElement().PRs.removeElementAt(i);
					Params.removeElementAt(i);
					i--;
				}
			}
			//*******************************************************************************
			// ����� ������� PRs if it's empty. RIGHT HERE.
			//*******************************************************************************
		}
	}
	
	public void getForFuncs(boolean after)
	// after - ����, �����������, ��� ��� ��� �����������
	{ // ������� ��������� ��������� ������� �� � ����� ���
		// ������ ������� �� ���
		Vector<FuncRecord> Funcs = new Vector<FuncRecord>();
		FuncRecord FR; // ������ ���
		for (int i = 0; i < Main.structTreeElements.size(); i++)
		{ // ���� �� ������� ������� 
			for (int j = 0; j < Main.structTreeElements.elementAt(i).size(); j++)
			{ // ���� �� ��������� ������
				FR = new FuncRecord(); // ����� ������ �� ���
				// ���������� ��� ��������, ��� ��� � ������� �� ���
				FR.ElemName = Main.structTreeElements.elementAt(i).elementAt(j).elementName;
				FR.Type = Main.structTreeElements.elementAt(i).elementAt(j).typeName;
				FR.OldFunk = Main.structTreeElements.elementAt(i).elementAt(j).functionName;
				if (Main.structTreeElements.elementAt(i).elementAt(j).parametersValuesMap != null)
					// ���� � ������� ���� ���������, ���������� ��
					FR.OldParams = Main.structTreeElements.elementAt(i).elementAt(j).parametersValuesMap.toString();
				else // ���� ���������� ���, ���������� � ���
					FR.OldParams = "{}";
				if(FR.OldParams.length() > 4) // �������� ����������
					FR.OldParams = FR.OldParams.substring(1, FR.OldParams.length() - 3);
				Funcs.add(FR); // ��������� ������ �� ��� � ������
			}
		}
		if(after){ // ���� ��� ��� ���������
			for(int i = 0; i < Funcs.size(); i++){ // ���� �� ������� �� ���
				if(History.lastElement().FRs.get(i).OldFunk.equals(Funcs.get(i).OldFunk))
				{ // ���� ������� ���������, ������� ��� ������
					History.lastElement().FRs.removeElementAt(i);
					Funcs.removeElementAt(i);
					i--;
				} else { // ����� ���������� ����� �������� ������� � ���������� 
					History.lastElement().FRs.get(i).NewFunk = Funcs.get(i).OldFunk;
					History.lastElement().FRs.get(i).NewParams = Funcs.get(i).OldParams;
				}
			}
		} else { // ���� ��� ������ ���������, ���������� ������ � ������� �������� ����
			History.lastElement().FRs = Funcs;
		}
	}
	
	public void getForTypes(boolean after)
	// after - ����, �����������, ��� ��� ��� �����������.
	{ // ������� ��������� ��������� ������� �� � ����� ���
		// ������ ������� � ���
		Vector<TypeRecord> Types = new Vector<TypeRecord>();
		TypeRecord TR; // ������ � ���
		for (int i = 0; i < Main.structTreeElements.size(); i++)
		{ // ���� �� ������� �������
			for (int j = 0; j < Main.structTreeElements.elementAt(i).size(); j++)
			{ // ���� �� ��������� ������
				TR = new TypeRecord(); // ����� ������ � ���
				// ���������� ��� ��������, ��� �� �� � �������
				TR.ElemName = Main.structTreeElements.elementAt(i).elementAt(j).elementName;
				TR.OldType = Main.structTreeElements.elementAt(i).elementAt(j).typeName;
				TR.Funk = Main.structTreeElements.elementAt(i).elementAt(j).functionName;
				if (Main.structTreeElements.elementAt(i).elementAt(j).parametersValuesMap != null)
					// ���������� ������ ���������� �������, ���� ������� ����
					TR.Params = Main.structTreeElements.elementAt(i).elementAt(j).parametersValuesMap.toString();
				else // ����� ����������, ��� �� ���
					TR.Params = "{}";
				if(TR.Params.length() > 4) // �������� ����������
					TR.Params = TR.Params.substring(1, TR.Params.length() - 3);
				Types.add(TR); // ��������� ������ � ������
			}
		}
		if(after){ // ���� ��� ��� �����������
			for (int i = 0; i < Types.size(); i++){ // �������� �� �������
				if(History.lastElement().TRs.get(i).OldType.equals(Types.get(i).OldType))
				{ // ���� ��� �� ���������, ������� ������� �������
					History.lastElement().TRs.removeElementAt(i);
					Types.removeElementAt(i);
					i--;
				} else { // ����� - ���������� ��������� ����� ��
					History.lastElement().TRs.get(i).NewType = Types.get(i).OldType;
					History.lastElement().TRs.get(i).Funk = Types.get(i).Funk;
					History.lastElement().TRs.get(i).Params = Types.get(i).Params;
				}
			}
		} else { // ���� ��� ��� ������ ���������, ��������� ������
			History.lastElement().TRs = Types;
		}
	}
	
	public void getForRests(boolean after)
	// after - ����, �����������, ��� ���������������� ��� ������
	{ // ������� ��������� ��������� ������� �� � ����� ����������������
		// ������ ������� � �����������������
		Vector<RestRecord> Rests = new Vector<RestRecord>();
		RestRecord RR; // ������ � ����������������
		for(int i = 0; i < ItselfOrganizationVariables.leafsList.size(); i++ )
		{ // ���� �� ������-�� �������
			RR = new RestRecord(); // ����� ������ � ����������������
			// ���������� �������, ID, ��� ��������, ��� ��� � �������, 
			// � ����� �������� ID ������������� �� �� ����������������
			RR.level = ItselfOrganizationVariables.leafsList.get(i).Level - 1;
			RR.ID = ItselfOrganizationVariables.leafsList.get(i).ID;
			RR.OldPID = ItselfOrganizationVariables.leafsList.get(i).ParentID;
			RR.ElemName = ItselfOrganizationVariables.leafsList.get(i).elementName;
			RR.Type = ItselfOrganizationVariables.leafsList.get(i).typeName;
			RR.Funk = ItselfOrganizationVariables.leafsList.get(i).functionName;
			if (ItselfOrganizationVariables.leafsList.get(i).parametersValuesMap != null)
				// ���� � ������� ������� ���������, ���������� ������ ����������
				RR.Params = ItselfOrganizationVariables.leafsList.get(i).parametersValuesMap.toString();
			else // ����� ����������, ��� ������� ���������� �� �����
				RR.Params = "{}";
			if(RR.Params.length() > 4) // �������� ����������
				RR.Params = RR.Params.substring(1, RR.Params.length() - 3);
			Rests.add(RR); // ��������� ������� ������ � ������
		}
		if (after){ // ���� ��� ��� �����������
			int j = 0; // ������������ ����������
			for (int i = 0; i < Rests.size(); i++){ // ���� �� ������� �� ���
				j = 0;
				while (j < History.lastElement().RRs.size() - 1 && Rests.elementAt(i).ID != History.lastElement().RRs.elementAt(j).ID)
					j++; // ������� ������ ������ � ������� ��������
				if (Rests.elementAt(i).OldPID == History.lastElement().RRs.elementAt(j).OldPID)
				{ // ���� ID �������� �� ���������, ������� ������ �� �������
					Rests.removeElementAt(i--);
					History.lastElement().RRs.removeElementAt(j);
				} else { // ���� ������� ��������� (ID �������� ���������)
					//���������� ID ������ ��������
					History.lastElement().RRs.elementAt(j).NewPID = Rests.elementAt(i).OldPID;
					for (int k = 0; k < Main.structTreeElements.elementAt(Rests.elementAt(i).level).size(); k ++)
					{ // ������� ������� � ����� ������� � ��������� ��� ��� ��������
						if (Main.structTreeElements.elementAt(Rests.elementAt(i).level).elementAt(k).ID == Rests.elementAt(i).OldPID)
							History.lastElement().RRs.elementAt(j).NewParent = Main.structTreeElements.elementAt(Rests.elementAt(i).level).elementAt(k).elementName;
					}
					for (int k = 0; k < Main.structTreeElements.elementAt(History.lastElement().RRs.elementAt(j).level).size(); k ++)
					{ // ��������� ��� ������� ��������
						if (Main.structTreeElements.elementAt(History.lastElement().RRs.elementAt(j).level).elementAt(k).ID == History.lastElement().RRs.elementAt(j).OldPID)
							History.lastElement().RRs.elementAt(j).OldParent = Main.structTreeElements.elementAt(History.lastElement().RRs.elementAt(j).level).elementAt(k).elementName;
					}
					// ���������� ���, ������� � ��������� ��������
					History.lastElement().RRs.elementAt(j).Type = Rests.elementAt(i).Type;
					History.lastElement().RRs.elementAt(j).Funk = Rests.elementAt(i).Funk;
					History.lastElement().RRs.elementAt(j).Params = Rests.elementAt(i).Params;
				}
			}
		} else // ���� ��� ��� �� ����, ��������� ������ �� ���
			History.lastElement().RRs = Rests;
	}
	
	public void getForAdds(boolean after)
	// after - ����, �����������, ��� ���������� �����������
	{ // ������� ���������� � ���������� ��������� ������� �� � ����� ���������� ���������
		// ������ ������� �� ���
		Vector<AddsRecord> Adds = new Vector<AddsRecord>();
		AddsRecord AR; // ������ � ���������� ��������
		for (int i = 0; i < Main.structTreeElements.size(); i++)
		{ // ���� �� ������� �������
			for (int j = 0; j < Main.structTreeElements.elementAt(i).size(); j++)
			{ // ���� �� ��������� ������
				AR = new AddsRecord(); // ����� ������ � ����������
				// ���������� �������, ID, ������������ ID, ���, ���, ������� ��������
				AR.level = i - 1;
				AR.ID = Main.structTreeElements.elementAt(i).elementAt(j).ID;
				AR.PID = Main.structTreeElements.elementAt(i).elementAt(j).ParentID;
				AR.ElemName = Main.structTreeElements.elementAt(i).elementAt(j).elementName;
				AR.Type = Main.structTreeElements.elementAt(i).elementAt(j).typeName;
				AR.Funk = Main.structTreeElements.elementAt(i).elementAt(j).functionName;
				if (Main.structTreeElements.elementAt(i).elementAt(j).parametersValuesMap != null)
					// ���� � ������� ���� ���������, ���������� ������ ����������
					AR.Params = Main.structTreeElements.elementAt(i).elementAt(j).parametersValuesMap.toString();
				else // ���� ��� ���������� - ������ ������
					AR.Params = "{}";
				Adds.add(AR); // ��������� ������ � ������
			}
		}
		if (after){ // ���� ���������� ��� �����������
			for (int i = 0; i < Adds.size(); i++)
			{ // ������� ������ �� ����������� �������
				for (int j = 0; j < History.lastElement().ARs.size(); j++){
					if (History.lastElement().ARs.elementAt(j).ID == Adds.elementAt(i).ID){
						Adds.removeElementAt(i--);
						History.lastElement().ARs.removeElementAt(j);
						break;
					}
				}
			}// �������� ������ ����������� ��������
			for (int i = 0; i < Adds.size(); i++)
			{ // ���������� ��� ������� �������� ID � ��� ��������
				for (int j = 0; j < Main.structTreeElements.elementAt(Adds.elementAt(i).level).size(); j++){
					if (Adds.elementAt(i).PID == Main.structTreeElements.elementAt(Adds.elementAt(i).level).elementAt(j).ID)
						Adds.elementAt(i).Parent = Main.structTreeElements.elementAt(Adds.elementAt(i).level).elementAt(j).elementName;
				}
			}
		}
		History.lastElement().ARs = Adds; // ���������� ���������
	}
	
	public HistoryOfSelforganizations(){		
	} // ����������� �� ���������
	
	public void init(){ // ������� ������������� ������� ������� ��
		History = new Vector<SORecord>();
	}
	
	public void addRecord(int time)
	{ // ������� ���������� ������ �� �� �� ���� � time
		if(indexOf(time) == -1)
			History.addElement(new SORecord(time));
	}
	
	public int indexOf(int time)
	{ // ���������� ������ ������ �� �� �� ���� time
		for (int i = 0; i < History.size(); i++)
			if (History.get(i).time == time)
				return i;
		return -1;
	}
	
	public int getSOType(int index)
	{ // ������� ���������� ��� ����� ������� �� � �������� index
		if(History.size() > index && History.get(index).ARs != null)
			return 5;
		if(History.size() > index && History.get(index).RRs != null)
			return 4;
		if(History.size() > index && History.get(index).TRs != null)
			return 3;
		if(History.size() > index && History.get(index).FRs != null)
			return 2;
		if(History.size() > index && History.get(index).PRs != null)
			return 1;
		return 0;
	}
	
	public Vector<ParamRecord> getParamsVector(int index)
	{ // ������� ���������� ������ ��������� ����� ��� � index
		return History.get(index).PRs;
	}
	
	public Vector<FuncRecord> getFuncsVector(int index)
	{ // ������� ���������� ������ ��������� ����� ��� � index
		return History.get(index).FRs;
	}
	
	public Vector<TypeRecord> getTypesVector(int index)
	{ // ������� ���������� ������ ��������� ����� ��� � index
		return History.get(index).TRs;
	}
	
	public Vector<RestRecord> getRestructsVector(int index)
	{ // ������� ���������� ������ ��������� ����� ���������������� � index
		return History.get(index).RRs;
	}
	
	public Vector<AddsRecord> getAddsVector(int index)
	{ // ������� ���������� ������ ��������� ����� ���������� � index
		return History.get(index).ARs;
	}
}
