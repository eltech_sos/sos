package simulation;

import java.util.ArrayList;

import object.TreeElement;
import Main.FunctionsLoader;
import Main.Main;
import Main.TreeOperations;
import Main.TypeLoader;
import simulation.ItselfOrganizationVariables;

public class FunctionOrganization 
{
	
	ArrayList<TreeElement> changedElementsList;
	
	ArrayList<String> functionsElementsForMax = new ArrayList<String>();
	double maxModelValue = Double.NEGATIVE_INFINITY;
	int currentElementsCount;
	
	public FunctionOrganization() 
	{
		if( ItselfOrganizationVariables.sameParametrsInParamOrg 
				&& ItselfOrganizationVariables.paramOrgAllowed  )
		{
			funcOrganizationForSameParametrs();
		}
		else
		{
			funcOrganizationForNOTSameParametrs();
		}
		
	}

	private void funcOrganizationForNOTSameParametrs() 
	{
		ArrayList<String> currentTypeFunctionsList = new ArrayList<String>();
		TreeElement elementInStruct, elementChild;
		ArrayList<TreeElement> elementChildList = new ArrayList<TreeElement>();
		
		String currentFunc = "";
		double elemValueWithCurrentFunc = 0.0;
		
		String maxFunc = "";
		double maxFuncValue = 0.0;
		
		for( int level = ( Main.structTreeElements.size() - 1 ) ; 
			level >= 0 ; level-- )
		{
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
			{
				elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
				if( ( elementInStruct.type == 2 )
						&& ( elementInStruct.itsImportantForRootValue ) )//to_do
					//if element is intermediate
					//and important for root value
				{
					currentTypeFunctionsList.clear();
					currentTypeFunctionsList.addAll( TypeLoader.getAllIntermediateFunctions( elementInStruct.typeName ) );
					if( currentTypeFunctionsList.size() > 1 )
						//if we must choose function
					{
						
						elementChildList.clear();
						elementChildList.addAll(TreeOperations.
								findAllChildrenInStructByIDAndLevel
								( elementInStruct.ID, 
										elementInStruct.Level) );
						
						for( int funcInd = 0; 
							funcInd < currentTypeFunctionsList.size();
							funcInd++)
						{
							currentFunc = currentTypeFunctionsList.
							get( funcInd );
							elemValueWithCurrentFunc = 0.0;
							
							if( currentFunc.equals( "sum(Xi)" ) )
							{
								for(int childInd = 0; 
								childInd < elementChildList.size(); 
								childInd++ )
								{
									elementChild = elementChildList.get( childInd );
									elemValueWithCurrentFunc = 
										elemValueWithCurrentFunc + 
										elementChild.elementCount;
								}
							}
							else if( currentFunc.equals( "sum(Xi*Ki)" ) )
							{
								for(int childInd = 0; 
								childInd < elementChildList.size(); 
								childInd++ )
								{
									elementChild = elementChildList.get( childInd );
									elemValueWithCurrentFunc = 
										elemValueWithCurrentFunc + 
										( elementChild.elementCount 
												* TypeLoader.getFactorForSumMultFunction(elementChild.typeName) );
								}
							}
							else if( currentFunc.equals( "sum(Xi^Ki)" ) )
							{
								for(int childInd = 0; 
								childInd < elementChildList.size(); 
								childInd++ )
								{
									elementChild = elementChildList.get( childInd );
									elemValueWithCurrentFunc = 
										elemValueWithCurrentFunc + 
										Math.pow(elementChild.elementCount, 
												TypeLoader.getFactorForSumPowFunction(elementChild.typeName ) 
												);
								}
							}
							
							if( funcInd == 0 )
							{
								maxFuncValue = elemValueWithCurrentFunc;
								maxFunc = currentFunc;
							}
							else
							{
								if( elemValueWithCurrentFunc > maxFuncValue )
								{
									maxFuncValue = elemValueWithCurrentFunc;
									maxFunc = currentFunc;
								}
							}
							
						}
						
						elementInStruct.functionName = maxFunc;
						elementInStruct.elementCount = maxFuncValue;
					}
				}
			}
		}
	}

	private void funcOrganizationForSameParametrs() 
	{
		TreeElement elementInStruct;
		changedElementsList = new ArrayList<TreeElement>();
//create list of intermediate elements 
//in witch we can choose function		
		for( int level = ( Main.structTreeElements.size() - 1 ) ; 
		level >= 0 ; level-- )
		{
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
			{
				elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
				if( ( elementInStruct.type == 2 )
						&& ( elementInStruct.itsImportantForRootValue ) )//to_do
					//if element is intermediate
					//and important for root value
				{
					if( TypeLoader.getAllIntermediateFunctions( elementInStruct.typeName ).size() > 1 )
					{
						changedElementsList.add( elementInStruct );
					}
				}
			}
		}
		
		currentElementsCount = changedElementsList.size();
		
		//try for created set(changedElementsList) all possible combinations
		ArrayList<String> tmpArrayList = new ArrayList<String>();
		tryAllIntermediateFunctions( 0, tmpArrayList );
		//set best combination to the model
		if( ( ! Double.isInfinite(maxModelValue) )
				&& ( maxModelValue !=  Double.NEGATIVE_INFINITY) 
				&& ( changedElementsList.size() ==  functionsElementsForMax.size() ) )
		{
			for(int elemInd=0; elemInd < changedElementsList.size(); elemInd++ )
			{
				changedElementsList.get(elemInd).functionName = functionsElementsForMax.get(elemInd);
			}
			//TODO call to parameter organization replaced
			//new SimpleParamOrg( ItselfOrganizationVariables.rootMinimumCount );
			ItselfOrganizationVariables.getInstance().runPSO(ItselfOrganizationVariables.rootMinimumCount);
		}
		
		
	}
	
	//this method try for element ( means element with index 
	//from changedElementsList equals parameter index) 
	//all possible functions and 
	//if its element did not last in list: 
	//calling itself for next element ( index + 1 )
	//if its element last in list:
	//compare everyone root value (with different functions)
	//with maxModelValue add if current value more then maxModelValue
	//save current functions set to functionsListsForMax
	//and maxModelValue = current value
	void tryAllIntermediateFunctions( int index, ArrayList<String> currentFunctionsList )
	{
		if( ! ItselfOrganizationVariables.rootValueNotValid )
		{
			return;
		}
		int funcInd;
		String funcName = changedElementsList.get(index).functionName;
		ArrayList<String> currentFunctionsListTMP = new ArrayList<String>();
		if( FunctionsLoader.isParticipate(funcName) )
			//if function may be changed
		{
			String typeName = changedElementsList.get(index).typeName;
			ArrayList<String> currentListFunctionsList;
			currentListFunctionsList = 
				TypeLoader.getAllIntermediateFunctions( typeName );
			if( currentListFunctionsList != null )
			{
				//go through all possible functions of current element
				for( funcInd = 0; funcInd < currentListFunctionsList.size(); funcInd++ )
				{
					currentFunctionsListTMP.clear();
					currentFunctionsListTMP.addAll( currentFunctionsList );
					currentFunctionsListTMP.add( currentListFunctionsList.get(funcInd) );
					if( index < ( changedElementsList.size() - 1 )  )
						//if its element did not last in list
					{
						tryAllIntermediateFunctions( index + 1, currentFunctionsListTMP );
					}
					else
						//if its element last in list
					{
						if( ! ItselfOrganizationVariables.rootValueNotValid )
						{
							return;
						}
						//set current combination to the model
						if( changedElementsList.size() == currentFunctionsListTMP.size() )
						{
							for(int elemInd=0; elemInd < changedElementsList.size(); elemInd++ )
							{
								changedElementsList.get(elemInd).functionName = currentFunctionsListTMP.get(elemInd);
							}
							//TODO call to parameter organization replaced
							//new SimpleParamOrg( ItselfOrganizationVariables.rootMinimumCount );
							ItselfOrganizationVariables.getInstance().runPSO(ItselfOrganizationVariables.rootMinimumCount);
						}
						
						double currentRootValue = TreeOperations.rootCount();
						if( currentRootValue > maxModelValue )
						{
							maxModelValue = currentRootValue;
							functionsElementsForMax.clear();
							functionsElementsForMax.addAll( currentFunctionsListTMP );
						}
					}
						 
				}//end go through all possible functions of current element
			}
		}//end of action if function can be changed
		else
			//if function can not be changed
		{
			currentFunctionsListTMP.clear();
			currentFunctionsListTMP.addAll( currentFunctionsList );
			currentFunctionsListTMP.add( funcName );
			if( index < (currentElementsCount - 1)  )
				//if its element did not last in list
			{
				tryAllIntermediateFunctions( index + 1, currentFunctionsListTMP );
			}
			else
				//if its element last in list
			{
				if( ! ItselfOrganizationVariables.rootValueNotValid )
				{
					return;
				}
				//set current combination to the model
				if( changedElementsList.size() == currentFunctionsListTMP.size() )
				{
					for(int elemInd=0; elemInd < changedElementsList.size(); elemInd++ )
					{
						changedElementsList.get(elemInd).functionName = currentFunctionsListTMP.get(elemInd);
					}
					//TODO call to parameter organization replaced
					//new SimpleParamOrg( ItselfOrganizationVariables.rootMinimumCount );
					ItselfOrganizationVariables.getInstance().runPSO(ItselfOrganizationVariables.rootMinimumCount);
				}
				
				double currentRootValue = TreeOperations.rootCount();
				if( currentRootValue > maxModelValue )
				{
					maxModelValue = currentRootValue;
					functionsElementsForMax.clear();
					functionsElementsForMax.addAll( currentFunctionsListTMP );
					if( ItselfOrganizationVariables.rootMinimumCount >= maxModelValue )
					{
						ItselfOrganizationVariables.rootValueNotValid = false;
					}
				}
			}
		}//end of action if function can not be changed
		
		
	}
	
}
