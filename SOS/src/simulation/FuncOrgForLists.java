package simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import object.TreeElement;
import Main.FunctionsLoader;
import Main.TreeOperations;
import Main.TypeLoader;
import simulation.ItselfOrganizationVariables;

public class FuncOrgForLists 
{
	ArrayList<TreeElement> currentLeafsList;
	int currentListsCount;
	ArrayList<String> functionsListsForMax = new ArrayList<String>();
	double maxModelValue = Double.NEGATIVE_INFINITY;
//	boolean powElementsInModel = false;
	
	public FuncOrgForLists()
	{
		currentLeafsList = new ArrayList<TreeElement>();
//		TreeElement tmpTreeElem;
//		for(int i=0; i < ItselfOrganizationVariebles.leafsList.size(); i++ )
//		{
//			tmpTreeElem = ItselfOrganizationVariebles.leafsList.get( i );
//			if()
//			{
//				
//			}
//		}
		currentLeafsList.addAll( ItselfOrganizationVariables.leafsList );
		
		if( ItselfOrganizationVariables.leafsList.size() > 0 )
		{
			currentListsCount = currentLeafsList.size();
//			powElementsInModel = false;
//			powElementsInModel = TreeOperations.defineNeedsOfUpOrDownForAllElements();
			if( ItselfOrganizationVariables.sameParametrsInParamOrg 
					&& ItselfOrganizationVariables.paramOrgAllowed  )
			{
				listOrganizationForSameParametrs();
			}
			else
			{
				listOrganizationForNOTSameParametrs();
			}
		}
		
	}
	
	void listOrganizationForSameParametrs()
	{
		
		if( ItselfOrganizationVariables.paramOrgAllowed )
		{
			setAllListsToMaxForNOTSameParam();
		}
		else
		{
			//try for lists all possible combinations
			ArrayList<String> tmpArrayList = new ArrayList<String>();
			tryAllListsFunctions( 0, tmpArrayList );
			//set best combination to the model
			if( ( ! Double.isInfinite(maxModelValue) )
					&& ( maxModelValue !=  Double.NEGATIVE_INFINITY) )
			{
				TreeOperations.setFunctionsVectorToLists( functionsListsForMax );
				//TODO call to parameter organization replaced
				//new SimpleParamOrg( ItselfOrganizationVariables.rootMinimumCount );
				ItselfOrganizationVariables.getInstance().runPSO(ItselfOrganizationVariables.rootMinimumCount);
			}
		}
		
		
		
//need if done separately for POW 
//		if ( TreeOperations.defineNeedsOfUpOrDownForAllElements() )
//			//in current model NOT meets elements with POW
//			//intermediate functions, witch influence
//			//on result value (that is or not elements
//			//with POW intermediate functions 
//			//or they always null 
//			//or they parent always null)
//		{
//			ArrayList<String> tmpArrayList = new ArrayList<String>();
//			tryAllListsFunctions( 0, tmpArrayList );
//			if( ( ! Double.isInfinite(maxModelValue) )
//					&& ( maxModelValue !=  Double.NEGATIVE_INFINITY) )
//			{
//				TreeOperations.setFunctionsVectorToLists( functionsListsForMax );
//				new SimpleParamOrg( ItselfOrganizationVariebles.rootMinimumCount );
//			}
//		}
//		else
//			//in current model is meets elements with POW
//			//intermediate functions, witch influence
//			//on result value
//		{
////to_do				
//		}
	
	}
	
	
	//this method try for element ( means element with index 
	//from currentListsList equals parameter index) 
	//all possible functions and 
	//if its element did not last in list: 
	//calling itself for next element ( index + 1 )
	//if its element last in list:
	//compare everyone root value (with different functions)
	//with maxModelValue add if current value more then maxModelValue
	//save current functions set to functionsListsForMax
	//and maxModelValue = current value
	void tryAllListsFunctions( int index, ArrayList<String> currentFunctionsList )
	{
		if( ! ItselfOrganizationVariables.rootValueNotValid )
		{
			return;
		}
		int funcInd;
		String funcName = currentLeafsList.get(index).functionName;
		ArrayList<String> currentFunctionsListTMP = new ArrayList<String>();
		if( FunctionsLoader.isParticipate(funcName) )
			//if function may be changed
		{
			String typeName = currentLeafsList.get(index).typeName;
			ArrayList<String> currentListFunctionsList;
			currentListFunctionsList = 
				TypeLoader.getAllParticipateListFunctions( typeName );
			if( currentListFunctionsList != null )
			{
				//go through all possible functions of current element
				for( funcInd = 0; funcInd < currentListFunctionsList.size(); funcInd++ )
				{
					currentFunctionsListTMP.clear();
					currentFunctionsListTMP.addAll( currentFunctionsList );
					currentFunctionsListTMP.add( currentListFunctionsList.get(funcInd) );
					if( index < (currentListsCount - 1)  )
						//if its element did not last in list
					{
						tryAllListsFunctions( index + 1, currentFunctionsListTMP );
					}
					else
						//if its element last in list
					{
						if( ! ItselfOrganizationVariables.rootValueNotValid )
						{
							return;
						}						
						TreeOperations.setFunctionsVectorToLists( currentFunctionsListTMP );
						//TODO call to parameter organization replaced
						//new SimpleParamOrg( ItselfOrganizationVariables.rootMinimumCount );
						ItselfOrganizationVariables.getInstance().runPSO(ItselfOrganizationVariables.rootMinimumCount);
						
						double currentRootValue = TreeOperations.rootCount();
						if( currentRootValue > maxModelValue )
						{
							maxModelValue = currentRootValue;
							functionsListsForMax.clear();
							functionsListsForMax.addAll( currentFunctionsListTMP );
							if( ItselfOrganizationVariables.rootMinimumCount >= maxModelValue )
							{
								ItselfOrganizationVariables.rootValueNotValid = false;
							}
						}
					}
						 
				}//end go through all possible functions of current element
			}
		}//end of action if function can be changed
		else
			//if function can not be changed
		{
			currentFunctionsListTMP.clear();
			currentFunctionsListTMP.addAll( currentFunctionsList );
			currentFunctionsListTMP.add( funcName );
			if( index < (currentListsCount - 1)  )
				//if its element did not last in list
			{
				tryAllListsFunctions( index + 1, currentFunctionsListTMP );
			}
			else
				//if its element last in list
			{
				if( ! ItselfOrganizationVariables.rootValueNotValid )
				{
					return;
				}
				TreeOperations.setFunctionsVectorToLists( currentFunctionsListTMP );
				//TODO call to parameter organization replaced
				//new SimpleParamOrg( ItselfOrganizationVariables.rootMinimumCount );
				ItselfOrganizationVariables.getInstance().runPSO(ItselfOrganizationVariables.rootMinimumCount);
				double currentRootValue = TreeOperations.rootCount();
				if( currentRootValue > maxModelValue )
				{
					maxModelValue = currentRootValue;
					functionsListsForMax.clear();
					functionsListsForMax.addAll( currentFunctionsListTMP );
				}
			}
		}//end of action if function can not be changed
		
		
	}
	
	void listOrganizationForNOTSameParametrs()
	{
		setAllListsToMaxForNOTSameParam();
//need if done separately for POW 
//		if ( TreeOperations.defineNeedsOfUpOrDownForAllElements() )
//			//in current model NOT meets elements with POW
//			//intermediate functions, witch influence
//			//on result value (that is or not elements
//			//with POW intermediate functions 
//			//or they always null 
//			//or they parent always null)
//		{
//			setAllListsToMaxForNOTSameParam();
//		}
//		else
//			//in current model is meets elements with POW
//			//intermediate functions, witch influence
//			//on result value
//		{
////to_do			
//		}
	}
	
	//set to max value (by setting functions 
	//and parameters) all lists witch is influence
	//on result
	private void setAllListsToMaxForNOTSameParam()
	{
		TreeElement list;
		int index;
		String functionName = "";
		boolean needToOverwriteMaps = false;
		//go through all lists
		for(int i=0; i < currentLeafsList.size(); i++)
		{
			list = currentLeafsList.get( i );
//			if( list.itsImportantForRootValue )
//			{ //all lists are important and it in any case must be 
			//set it to max for reconfiguration
				if( FunctionsLoader.isParticipate( list.functionName )
						|| ( ! ItselfOrganizationVariables.useParticipationInFuncOrg ) )
				{
					needToOverwriteMaps = false;
					if( ItselfOrganizationVariables.
							usedInOrganizationElementaryTypesList.
							contains( list.typeName ) )
						//if type of current element is participate 
						//in itself - organization
					{
						needToOverwriteMaps = true;
						
						index = ItselfOrganizationVariables.
						usedInOrganizationElementaryTypesList.
						indexOf( list.typeName );
						
						functionName = ItselfOrganizationVariables.
						typesMaxFuncList.get( index );
						list.functionName = functionName;
						
						list.elementCount = ItselfOrganizationVariables.
						typesMaxValuesList.get( index );
						
					}
					else if( ItselfOrganizationVariables.
							notUsedInOrgElemTypesMeetingInModel.
							contains( list.typeName ) )
						//if type of current element not participate 
						//in itself - organization
					{
						needToOverwriteMaps = true;
						
						index = ItselfOrganizationVariables.
						notUsedInOrgElemTypesMeetingInModel.
						indexOf( list.typeName );
						
						functionName = ItselfOrganizationVariables.
						notUsedInOrgElemTypesMaxFuncList.get( index );
						list.functionName = functionName;
						
						list.elementCount = ItselfOrganizationVariables.
						notUsedInOrgElemTypesMaxValuesList.get( index );
						
					}
					
					if( needToOverwriteMaps )
						//if function was changed we need to change
						//list.parametrsValuesMap and list.parametrsWeightMap
					{
						if( list.parametersValuesMap != null )
						{
							list.parametersValuesMap.clear();
							list.parametrsWeightMap.clear();
						}
						
//						list.parametrsValuesMap = new HashMap<String, Double>();
//						list.parametrsWeightMap = new HashMap<String, Double>();
//						

						if( ItselfOrganizationVariables.
								paramMapsForFuncMax.containsKey( functionName ) )
						{
							list.parametersValuesMap = 
								ItselfOrganizationVariables.
								paramMapsForFuncMax.get( functionName );
							
							if( list.parametersValuesMap != null )
							{
								//overwrite .parametrsWeightMap
								if( list.parametersValuesMap.size() > 0 )
								{
									if( list.parametrsWeightMap == null )
									{
										list.parametrsWeightMap = new HashMap<String, Double> ();
									}
									double initialWeight = 1.0;
									Iterator iter = list.parametersValuesMap.entrySet().iterator();
									while( iter.hasNext() )
									{
										Entry paramInform = (Entry) iter.next();
							        	String paramName = (String) paramInform.getKey();
							        	list.parametrsWeightMap.
							        	put( paramName, initialWeight );
									}
								}
							}
							else
							{
								list.parametersValuesMap = new HashMap<String, Double> ();
								list.parametrsWeightMap = new HashMap<String, Double> ();
							}
							
							
							
						}
					}
					
				}			
//			}//end if( list.itsImportantForRootValue )
		}//end go through all lists
	}
	
	
	
	
	
}
