/**
 * 
 */
package simulation.parameter;

import java.util.ArrayList;
import java.util.Observer;


/**
 * @author User
 *
 */
public interface IParametersOrganizer extends Observer {
	
	IParametersModel getModel();
	
	ArrayList<IParameter> getParameters();
	
	void setParameterToModel(IParameter parameter);
	
	void setStrategy(ParametersOrganizerStrategy strategy);
		
	public void organize(double rootMinValue);

}
