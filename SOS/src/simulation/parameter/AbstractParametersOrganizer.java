/**
 * 
 */
package simulation.parameter;

import java.util.ArrayList;
import java.util.Observable;

/**
 * @author User
 *
 */
abstract class AbstractParametersOrganizer implements IParametersOrganizer {
	
	protected IParametersModel mModel;
	protected IParametersOrganizerStrategy mStrategy;
	
	protected ParametersOrganizerStrategy mStrategyType;
	
	
	/**
	 * flag for lazy update
	 */
	protected boolean updateNeeded = true;
	
	protected ArrayList<IParameter> mParameters;
	
	AbstractParametersOrganizer(IParametersModel model) {
		
		mModel = model;
		mParameters = new ArrayList<IParameter>();
		mStrategy = null;

	}
	
	/**
	 * Subclass must implement mParameters fill logic here
	 */
	protected abstract void updateParameters();

	/* (non-Javadoc)
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	/**
	 * Observer's update method
	 */
	@Override
	public void update(Observable arg0, Object arg1) {
		
		//check if this was our model update
		if(mModel != arg0)
			return;
		//check if this was our update
		if(arg1 == this)
			return;
		//rise lazy update flag
		updateNeeded = true;
		if(mStrategy != null) 
			mStrategy.reset();
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParametersOrganizer#getModel()
	 */
	@Override
	public IParametersModel getModel() {
		
		return mModel;
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParametersOrganizer#getParameters()
	 */
	/**
	 * This method returns a shallow copy of internal field mParameters
	 */
	@Override
	public ArrayList<IParameter> getParameters() {
		
		if(mModel == null) {
			return null;
			//TODO maybe throw some exception here
		}
		if(updateNeeded) {
			updateParameters();
			updateNeeded = false;
		}
		//create and return shallow copy
		ArrayList<IParameter> retList = new ArrayList<IParameter>(mParameters);
		return retList;
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParametersOrganizer#setParameterToModel(simulation.parameter.IParameter)
	 */
	@Override
	public abstract void setParameterToModel(IParameter parameter);

	/* (non-Javadoc)
	 * @see simulation.parameter.IParametersOrganizer#setStrategy(simulation.parameter.IParametersOrganizerStrategy)
	 */
	/**
	 * Creates and sets new strategy object corresponding to enum value
	 */
	@Override
	public void setStrategy(ParametersOrganizerStrategy strategy) {
		
		//if current strategy is the same as strategy do nothing
		if(mStrategy != null)
			if(mStrategyType == strategy)
				return;
		
		//TODO remove this line after implementing all strategies
		mStrategy = null;
		
		switch(strategy) {
		case WEIGHTED_CCS:
			mStrategy = new WeightedCCSStrategy(this);
			break;
		case CHANGE_COMPARSION_CCS:
			mStrategy = new ChangeComparsionCCSStrategy(this);
			break;
		case CHANGE_OPPORTUNITY_CCS:
			mStrategy = new ChangeOpportunityCCSStrategy(this);
			break;
		case SAVE_STEP_VECTOR_CCS:
			mStrategy = new SaveStepVectorCCSStrategy(this);
			break;
		case EXPONENTIAL_RELAX:
			mStrategy = new ExponentialRelaxStrategy(this);
			break;
		default:
			break;
		}
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParametersOrganizer#organize(double)
	 */
	@Override
	public void organize(double rootMinValue) {
		
		if( mModel == null || mStrategy == null) {
			// TODO maybe throw some exception here
			return;
		}
		if(updateNeeded) {
			updateParameters();
			updateNeeded = false;
		}
		if(mParameters.size() == 0) {
			// TODO maybe throw some exception here
			return;
		}
		mStrategy.organizeParameters(rootMinValue);
		//Notify observers that model was changed by this organizer
		mModel.modelChanged(this);
	}

}
