/**
 * 
 */
package simulation.parameter;

/**
 * This strategy sorts step vector due to parameters weights before organization
 * 
 * @author User
 *
 */
class WeightedCCSStrategy extends AbstractCCSStrategy {

	/**
	 * @param organizer
	 */
	public WeightedCCSStrategy(IParametersOrganizer organizer) {
		super(organizer);
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractCCSStrategy#startState()
	 */
	@Override
	protected void startState() {

		super.startState();
		//sort parametersVector due to parameters weights in decreasing order
		parametersVector.sort((p1, p2 )-> Double.compare(p2.getWeight(), p1.getWeight() ) );
		
		//reset stepVector
		for(int i = 0; i < stepVector.size(); ++i)
			stepVector.set(i, stepSize);
		
	}
		
}
