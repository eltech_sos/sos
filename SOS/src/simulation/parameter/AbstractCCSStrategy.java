/**
 * 
 */
package simulation.parameter;

import java.util.ArrayList;

/**
 * Abstract class to represent base Cycle Coordinate Shift strategy
 * 
 * @author User
 *
 */
abstract class AbstractCCSStrategy extends AbstractParametersOrganizerStrategy {

	//TODO stepSize init must be reorganized
	protected final double stepSize = 0.5;
	
	ArrayList<IParameter> parametersVector;
	ArrayList<Double> stepVector;	
	
	/**
	 * @param organizer
	 */
	AbstractCCSStrategy(IParametersOrganizer organizer) {
		super(organizer);

		stepVector = new ArrayList<Double>();
		initState();
	}

	protected void doOrganize(double rootMinValue) {
		
		startState();
		
		double currentRootValue = mOrganizer.getModel().getRootValue();
		
		int maxSteps = mOrganizer.getModel().getOrganizationCyclesCount();
		
		int step = 0;
		while( (currentRootValue < rootMinValue) && step < maxSteps) {
			for(int i = 0; (i < parametersVector.size()) && (currentRootValue < rootMinValue); ++i) {
				IParameter param = parametersVector.get(i);
				
				double oldParamValue = param.getUnlimitedValue();
				param.setUnlimitedValue(oldParamValue + stepVector.get(i));
				mOrganizer.setParameterToModel(param);
				
				double tmpRootValue = mOrganizer.getModel().getRootValue();
				
				if(tmpRootValue >= currentRootValue) {
					//step accepted
					currentRootValue = tmpRootValue;
					stepVector.set(i, stepVector.get(i) * 3.0);
				}
				else {
					//rejecting
					param.setUnlimitedValue(oldParamValue);
					mOrganizer.setParameterToModel(param);
					stepVector.set(i, stepVector.get(i) * -0.5);
				}
				
			}// for i
		}// while
		
		endState();
	}
	
	/**
	 *  This method initializes parametersVector by new values from mOrganizer
	 *  and sets stepVector values to stepSize. At the end of work it calls resetState method
	 *  
	 *  It is called before doOrganize method if strategy is in the resetNeeded state
	 */
	protected void doReset() {
		parametersVector = mOrganizer.getParameters();
		
		stepVector.clear();
		int paramCount = parametersVector.size();
		for(int i=0; i<paramCount; ++i)
			stepVector.add(stepSize);
		resetState();
	}
	
	/**
	 * Called just before organization
	 * 
	 * Can be overridden by successor class  if needed
	 * Method from the base class does nothing
	 */
	protected void startState() {
		
	}
	
	/**
	 * Called after organization
	 * Can be overridden by successor class  if needed
	 * Method from the base class does nothing
	 */
	protected void endState() {
		
	}
	
	/**
	 * This method is called from constructor to create all internal state data
	 * Can be overridden by successor class  if needed
	 * Method from the base class does nothing
	 */
	protected void initState() {

	}
	
	/**
	 * This method must reset all private state variables specific to concrete class
	 * It is called after doReset method
	 * Can be overridden by successor class  if needed
	 * 
	 * Method from the base class does nothing
	 */
	protected void resetState() {
		
	}

}
