/**
 * 
 */
package simulation.parameter;

/**
 * Abstract class used to set a reference to IParametersOrganizer in constructor
 * and implement simple lazy reset logic.
 * 
 * @author User
 *
 */
abstract class AbstractParametersOrganizerStrategy implements IParametersOrganizerStrategy {
	
	/**
	 * Lazy reset flag
	 */
	boolean resetNeeded = true;
	protected IParametersOrganizer mOrganizer;
	
	AbstractParametersOrganizerStrategy(IParametersOrganizer organizer) {
		mOrganizer = organizer;
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParametersOrganizerStrategy#reset()
	 */
	@Override	
	public void reset() {
		resetNeeded = true;		
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParametersOrganizerStrategy#organizeParameters(double)
	 */
	@Override
	public void organizeParameters(double rootMinValue) {
		if(resetNeeded) {
			doReset();
			resetNeeded = false;
		}
		doOrganize(rootMinValue);
	}
	
	/**
	 * Implement concrete organization in this method.
	 * It is called only after call to doReset if the strategy was reseted.
	 * @param rootMinValue
	 */
	protected abstract void doOrganize(double rootMinValue);
	/**
	 * Implement all reset procedures here. This method is called prior to
	 * doOrganize if strategy was reseted.
	 */
	protected abstract void doReset();
}
