/**
 * 
 */
package simulation.parameter;

/**
 * @author User
 *
 */
class SortParameterPair {
	
	private Double key;
	private IParameter param;

	/**
	 * 
	 */
	public SortParameterPair(Double key_, IParameter param_) {
		key = key_;
		param = param_;
	}

	/**
	 * @return the key
	 */
	public Double getKey() {
		return key;
	}

	/**
	 * @return the param
	 */
	public IParameter getParam() {
		return param;
	}


}
