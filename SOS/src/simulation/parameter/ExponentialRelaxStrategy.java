/**
 * 
 */
package simulation.parameter;

import java.util.ArrayList;

/**
 * @author User
 *
 */
final class ExponentialRelaxStrategy extends AbstractParametersOrganizerStrategy {
	
	private ArrayList<IParameter> parametersVector;
	
	int paramNumber;
	
	double[][] mas_e;

	/**
	 * @param organizer
	 */
	public ExponentialRelaxStrategy(IParametersOrganizer organizer) {
		super(organizer);
			
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractParametersOrganizerStrategy#doOrganize(double)
	 */
	@Override
	protected void doOrganize(double rootMinValue) {
		// TODO cleanup relax organization
		
		paramNumber = parametersVector.size();
		int k_relax;
		int numberOfCounts = 0;
		double J, J1, Jt;
		double s_relax = 0.1, h0;
		double[][] mas_d = new double[ paramNumber ][ paramNumber ];
		double[][] mas_h = new double[ paramNumber ][ paramNumber ];
		mas_e = new double[ paramNumber ][ paramNumber ];
		double[][] mas_temp = new double[ paramNumber ][ paramNumber ];
		double[] vect_d = new double[ paramNumber ];
		double[] vect_2sHd = new double[ paramNumber ];
		double[] previous_step_vect = new double[ paramNumber ];
		double[]  vector_x_l;
		double determinantD;
		vector_x_l = new double[paramNumber];

		vector_x_l = getVectorX();
		boolean rootValueNotValid;
		rootValueNotValid = mOrganizer.getModel().getRootValue() <= rootMinValue;
				
		J = - mOrganizer.getModel().getRootValue();
		J1 = J;
		numberOfCounts = 0;
		
		do
		{
			for(int i = 0; i < paramNumber; i++)
			{
				vect_d[i] = count_vect_d_i(i, s_relax);
				for(int j = 0; j < paramNumber; j++)
				{
					
					mas_d[i][j] = count_matr_Dij(i, j, s_relax);
					if(i == j)
					{
						mas_e[i][j] = 1;
					}
					else
					{
						mas_e[i][j] = 0;
					}
				}
			}
//			
			System.out.println();
			for(int i = 0; i < paramNumber; i++)
			{
				System.out.println();
				for(int j = 0; j < paramNumber; j++)
				{
					System.out.print("D ["+i+"] ["+j+"] = " + mas_d[i][j] +"   ");
				}				
			}
			System.out.println("");
			for(int i = 0; i < paramNumber; i++)
			{
				System.out.println("d ["+i+"] = " + vect_d[i]+"   ");
			}
//			
			// WTF determinantD must be norm of a matrix D !
			//determinantD = vectNorm(vect_d);//determinant(mas_d);
			determinantD = matrixEuclideanNorm(mas_d);
						
			if( determinantD == 0 
					|| Double.isNaN( determinantD ) 
					|| Double.isInfinite( determinantD ) )
			{
//to_do begin
				System.out.println("determinant D == 0");
				//TODO CPS call cps();
				return;
			}
//to_do end
			
//to_do
			
			System.out.println("determinant D == "+determinantD);
			
			
//to_do			
			
			//h0 = 0.1 in original algorithm
			h0 = 0.01 / determinantD;
			System.out.println("h0 == "+h0);
			k_relax = 0;

			// обнулили mas_h
			for(int i = 0; i < paramNumber; i++)
			{
				for(int j = 0; j < paramNumber; j++)
				{
					mas_h[i][j] = 0;
				}
			}

			
			// вычислили H
			double delitel;
			for(int i = 1; i < 8; i++)
			{
				mas_temp = matr_mult_number(mas_d, (-1.0)*h0 );
				mas_temp = matr_stepen(mas_temp, i - 1);
				mas_temp = matr_mult_number(mas_temp, h0);
				delitel = 1.0 / (Double) factorial(i);
				mas_temp = matr_mult_number(mas_temp, delitel);
				mas_h = matr_sum_matr(mas_h, mas_temp);
			}
//to_do			
			
			for(int i = 0; i < paramNumber; i++)
			{
				System.out.println();
				for(int j = 0; j < paramNumber; j++)
				{
					System.out.print("mas_h [ "+i+" ]  [ "+j+" ]  =  " + mas_h[i][j]+"   "); 
				}
			}
			System.out.println();
//to_do
			
			do
			{
				// step 5:
				vect_2sHd = matr_mult_vect(mas_h, vect_d);
				vect_2sHd = vect_mult_number(vect_2sHd, 2 * s_relax);
//				
				for(int i = 0; i < paramNumber; i++)
				{
					System.out.println("vect_2sHd ["+i+"] = " + vect_2sHd[i]+"   ");
				}
//				
				
				for(int i = 0; i < paramNumber; i++)
				{
					changeVectorX(i, -vect_2sHd[i] );
//!!!
				}
//to_do begin		
				if( previous_step_vect.equals( getVectorX() ) )
				{
					numberOfCounts = mOrganizer.getModel().getOrganizationCyclesCount() + 1;
				}
				previous_step_vect = getVectorX();
//to_do end
				
	
				Jt = - mOrganizer.getModel().getRootValue();
				k_relax++;
				
				if( Double.isNaN( Jt ) 
						|| Double.isInfinite( Jt ) )
				{
					k_relax = 21;
				}
				
				// step 6:
				System.out.println("Jt = "+Jt);
				if(J1 > Jt)
				{
					vector_x_l = getVectorX();
					J1 = Jt;
				}
				else
				{
					setVectorX( vector_x_l );
				}

				// step 7:
				mas_h = matr_mult_matr(
									   mas_h,
									   matr_sum_matr(
													 matr_mult_number(
																	  mas_e,
																	  2.0),
													 matr_mult_number(
																	  matr_mult_matr(
																					 mas_d,
																					 mas_h),
																	  -1.0)));
			}
			while(k_relax <= 20);
			setVectorX(vector_x_l);
			J = J1;
			if( (-J) >= rootMinValue)
			{
				rootValueNotValid = false;
			}
 //////////////////////// ?????
			numberOfCounts++;
// //////////////////////// ?????
			
		}
		while( ( numberOfCounts < mOrganizer.getModel().getOrganizationCyclesCount() )
				&& rootValueNotValid);
		if( numberOfCounts > mOrganizer.getModel().getOrganizationCyclesCount() )
		{
			//TODO CPS CALL cps();
		}
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractParametersOrganizerStrategy#doReset()
	 */
	@Override
	protected void doReset() {
	
		parametersVector = mOrganizer.getParameters();
	}
	
	//TODO cleanup all this mess (Relax matrix calls)
	//_______________old staff
	/** 
	 * returns Euclidean Norm of a matrix
	 * @param matrix
	 * @return
	 */
	double matrixEuclideanNorm(double[][] matrix) {
		double norm = 0;
		for(int i = 0; i < paramNumber; i++)
		{
			for(int j = 0; j < paramNumber; j++)
			{
				norm += Math.pow(matrix[i][j], 2);
			}
		}
		
		return Math.pow(norm, 0.5);
	}
	
	double[][] matr_stepen(double[][] matr_isodnik, int power)
	{
		if(power == 0)
		{
			return mas_e;
		}
		if(power == 1)
		{
			return matr_isodnik;
		}
		double[][] matr;
		matr = new double[paramNumber][paramNumber];
		double[][] matr_itog;
		matr_itog = new double[paramNumber][paramNumber];
		double[][] matr_temp;
		matr_temp = new double[paramNumber][paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			for(int j = 0; j < paramNumber; j++)
			{
				matr[i][j] = /*-*/matr_isodnik[i][j];
				matr_itog[i][j] = /*-*/matr_isodnik[i][j];
			}
		}

		for(int pow_ind = 1; pow_ind < power; pow_ind++)
		{
			for(int i = 0; i < paramNumber; i++)
			{
				for(int j = 0; j < paramNumber; j++)
				{
					matr_temp[i][j] = 0;
					for(int k = 0; k < paramNumber; k++)
					{
						matr_temp[i][j] = matr_temp[i][j] + matr_itog[i][k]
								* matr[k][j];
					}
				}
			}

			for(int i = 0; i < paramNumber; i++)
			{
				for(int j = 0; j < paramNumber; j++)
				{
					matr_itog[i][j] = matr_temp[i][j];
				}
			}
		}

		return matr_itog;

	}

	double[][] matr_sum_matr(double[][] matr_a, double[][] matr_b)
	{
		double[][] matr_itog;
		matr_itog = new double[paramNumber][paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			for(int j = 0; j < paramNumber; j++)
			{
				matr_itog[i][j] = matr_a[i][j] + matr_b[i][j];
			}
		}
		return matr_itog;
	}

	double[][] matr_mult_matr(double[][] matr_a, double[][] matr_b)
	{
		double[][] matr_itog;
		matr_itog = new double[paramNumber][paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			for(int j = 0; j < paramNumber; j++)
			{
				matr_itog[i][j] = 0;
				for(int k = 0; k < paramNumber; k++)
				{
					matr_itog[i][j] = matr_itog[i][j] + matr_a[i][k]
							* matr_b[k][j];
				}
			}
		}
		return matr_itog;
	}

	double[] matr_mult_vect(double[][] matr, double[] vect)
	{
		double[] vect_itog;
		vect_itog = new double[paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			vect_itog[i] = 0;
			for(int k = 0; k < paramNumber; k++)
			{
				vect_itog[i] = vect_itog[i] + matr[i][k] * vect[k];
			}
		}
		return vect_itog;
	}

	double[][] matr_mult_number(double[][] matr_ishodn, double number)
	{
		double[][] matr_itog;
		matr_itog = new double[paramNumber][paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			for(int j = 0; j < paramNumber; j++)
			{
				matr_itog[i][j] = matr_ishodn[i][j] * number;
			}
		}
		return matr_itog;
	}

	double[] vect_mult_number(double[] vect_ishodn, double number)
	{
		double[] vect_itog;
		vect_itog = new double[paramNumber];
		for(int i = 0; i < paramNumber; i++)
		{
			vect_itog[i] = vect_ishodn[i] * number;
		}
		return vect_itog;
	}

	double factorial(int number)
	{
		if((number == 0) || (number == 1))
		{
			return 1.0;
		}
		int i, itog = 1;
		i = number;
		do
		{
			itog = itog * i;
			i--;
		}
		while(i != 1);
//		double tmpForCast = itog;
		return itog;//tmpForCast;
	}
	
	void changeVectorX(int param_index, double s_relex)
	{
		IParameter param = parametersVector.get(param_index);
		param.setUnlimitedValue(param.getUnlimitedValue() + s_relex);
		
		mOrganizer.setParameterToModel(param);
	}
	
	double[] getVectorX()
	{
		double[] vect_rez;
		vect_rez = new double[paramNumber];
		
		for(int i=0; i < parametersVector.size(); ++i) {
			IParameter param = parametersVector.get(i);
			vect_rez[i] = param.getUnlimitedValue();
		}

		return vect_rez;
	}
	
	void setVectorX( double[] vect_to_set)
	{
		for(int i=0; i < parametersVector.size(); ++i) {
			IParameter param = parametersVector.get(i);
			param.setUnlimitedValue(vect_to_set[i]);
			mOrganizer.setParameterToModel(param);
		}
	}
	
	double count_vect_d_i(int i, double s_relex)
	{
		double d, oldParamValue;
		IParameter param = parametersVector.get(i);
		oldParamValue = param.getUnlimitedValue();
		
		param.setUnlimitedValue(oldParamValue + s_relex);
		mOrganizer.setParameterToModel(param);
		d = - mOrganizer.getModel().getRootValue();
		
		param.setUnlimitedValue(oldParamValue - s_relex);
		mOrganizer.setParameterToModel(param);
		d += mOrganizer.getModel().getRootValue();
		
		param.setUnlimitedValue(oldParamValue);
		mOrganizer.setParameterToModel(param);

		return d;
	}

	double count_matr_Dij(int i, int j, double s_relex)
	{
	
		double d = 0;
		changeVectorX(i, s_relex);
		changeVectorX(j, s_relex);
		d = - mOrganizer.getModel().getRootValue();
		changeVectorX(i, (-2) * s_relex);
		d = d + mOrganizer.getModel().getRootValue();
		changeVectorX(i, 2 * s_relex);
		changeVectorX(j, (-2) * s_relex);
		d = d + mOrganizer.getModel().getRootValue();
		changeVectorX(i, (-2) * s_relex);
		d = d - mOrganizer.getModel().getRootValue();
		changeVectorX(i, s_relex);
		changeVectorX(j, s_relex);
		return d;
	}
}
