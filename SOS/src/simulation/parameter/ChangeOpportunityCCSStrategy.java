/**
 * 
 */
package simulation.parameter;

/**
 * This strategy sorts parameters Vector due to parameters change opportunity in decreasing order
 * 
 * @author User
 *
 */
final class ChangeOpportunityCCSStrategy extends AbstractCCSStrategy {

	/**
	 * @param organizer
	 */
	public ChangeOpportunityCCSStrategy(IParametersOrganizer organizer) {
		super(organizer);

	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractCCSStrategy#startState()
	 */
	@Override
	protected void startState() {

		super.startState();
		//sort parametersVector by parameters change opportunity in decreasing order
		parametersVector.sort((p1, p2 )-> Double.compare(p2.getChangeOpportunity(), p1.getChangeOpportunity() ) );
		
		//reset stepVector
		for(int i = 0; i < stepVector.size(); ++i)
			stepVector.set(i, stepSize);
	}
	
}
