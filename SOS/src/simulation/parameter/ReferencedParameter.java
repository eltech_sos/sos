/**
 * 
 */
package simulation.parameter;

import java.util.Map;

/**
 * @author User
 *
 */
class ReferencedParameter extends Parameter implements IReferencedParameter {

	Map<String, Double> mReference;
	
	/* (non-Javadoc)
	 * @see simulation.parameter.IReferencedParameter#getReference()
	 */
	@Override
	public Map<String, Double> getReference() {
		return mReference;
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IReferencedParameter#setReference(java.lang.Double)
	 */
	@Override
	public void setReference(Map<String, Double> reference) {
		mReference = reference;
	}

}
