/**
 * 
 */
package simulation.parameter;

import java.util.ArrayList;

import object.TreeElement;

/**
 * @author User
 *
 */
public interface IParametersModel {
	
	ArrayList<TreeElement> getLeafs();
	
	public double getRootValue();
	
	/**
	 * If return value is true - Organazer must use "take part in organization" property from leafs and parameters.
	 * If false - all leafs and parameters will take part in organization.
	 * This parameter is fetching from model/
	 */
	public boolean getUseParticipationProperty();
	
	public int getOrganizationCyclesCount();
	
	public void setParametersModelOrganizer(ParametersOrganizer organizerType);
	
	public void setParametersModelOrganizerStrategy(ParametersOrganizerStrategy strategyType);
	
	public void modelChanged(IParametersOrganizer modelModifier);
	
	public void runPSO(double rootMinValue);
	
}
