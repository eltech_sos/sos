/**
 * 
 */
package simulation.parameter;

import Main.ParametersLoader;

/**
 * @author User
 *
 */
class Parameter implements IParameter {

	String mName;
	double mUnlimitedValue;
	double mWeight;
	
	boolean changeOpportunityValid = false;
	double changeOpportunity;
	
	/* (non-Javadoc)
	 * @see simulation.parameter.IParameter#getUnlimitedValue()
	 */
	@Override
	public double getUnlimitedValue() {
		return mUnlimitedValue;
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParameter#getLimitedValue()
	 */
	@Override
	public double getLimitedValue() {
		return ParametersLoader.valueTranslationToLimitedFunction(mName, mUnlimitedValue);
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParameter#getName()
	 */
	@Override
	public String getName() {
		return mName;
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParameter#setUnlimitedValue(double)
	 */
	@Override
	public void setUnlimitedValue(double value) {
		mUnlimitedValue = value;
		changeOpportunityValid = false;
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParameter#setLimitedValue(double)
	 */
	@Override
	public void setLimitedValue(double value) {
		mUnlimitedValue = ParametersLoader.valueTranslationToUnlimitedFunction(mName, value);
		changeOpportunityValid = false;
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParameter#setName(java.lang.String)
	 */
	@Override
	public void setName(String parameterName) {
		mName = parameterName;
	}

	@Override
	public double getWeight() {
		return mWeight;
	}

	@Override
	public void setWeight(double weight) {
		mWeight = weight;
	}

	@Override
	public double getChangeOpportunity() {
		if(!changeOpportunityValid) {
			//calculate change opportunity
			changeOpportunity = ParametersLoader.getOpportunityOfChangeForCurrentValue( mName, getLimitedValue() );
			changeOpportunityValid = true;
		}
		return changeOpportunity;
	}

}
