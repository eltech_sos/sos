/**
 * 
 */
package simulation.parameter;

/**
 * This strategy saves step vector between organize calls.
 * To do this we just don't reset stepVector every Organize call
 * 
 * @author User
 *
 */
final class SaveStepVectorCCSStrategy extends AbstractCCSStrategy {

	/**
	 * @param organizer
	 */
	public SaveStepVectorCCSStrategy(IParametersOrganizer organizer) {
		super(organizer);
	}

}
