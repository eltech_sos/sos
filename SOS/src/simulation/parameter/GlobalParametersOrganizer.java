/**
 * 
 */
package simulation.parameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import Main.ParametersLoader;
import Main.TypeLoader;
import object.TreeElement;

/**
 * This organizer assumes that all leafs parameters with the same names have equal values 
 * 
 * @author User
 *
 */
final class GlobalParametersOrganizer extends AbstractParametersOrganizer {

	/**
	 * This map will hold parameter names as key values and ArrayList of references to
	 * all maps in model leafs where parameter with corresponding name is present
	 * 
	 */
	private Map<String, ArrayList< Map<String,Double> > > mReferences;
	
	/**
	 * All parameters will be stored in this map
	 * mParameters will be created as mParameters.addAll(mParametersMap.values())
	 */
	private Map<String, IParameter> mParametersMap;
	
	/**
	 * @param model
	 */
	public GlobalParametersOrganizer(IParametersModel model) {
		super(model);
		mReferences = new HashMap<String, ArrayList< Map<String,Double>>> ();
		mParametersMap = new HashMap<String, IParameter>();
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractParametersOrganizer#updateParameters()
	 */
	@Override
	protected void updateParameters() {
		
		mParameters.clear();
		mReferences.clear();
		mParametersMap.clear();
		if(mModel == null)
			return;
		
		ArrayList<TreeElement> leafs = mModel.getLeafs();
		
		boolean useParticipationProperty = mModel.getUseParticipationProperty();
		
		for(TreeElement el : leafs) {
			//leaf has no parameters
			if(el.parametersValuesMap.size() == 0)
				continue;
			
			//leaf type is not participating in organization
			if(useParticipationProperty && (!TypeLoader.isParticipate(el.typeName)))
				continue;
			
			//get parameters names
			Set<String> namesSet = el.parametersValuesMap.keySet();
			
			for(String pName : namesSet) {
				//parameter type is not participating in organization
				if(useParticipationProperty && (!ParametersLoader.isParticipate(pName)))
						continue;
				
				ArrayList< Map<String,Double>> paramReferences;
				
				//get current references to this name
				paramReferences = mReferences.get(pName);
				
				//met this parameter first time
				if(paramReferences == null) {
					//create list of references to this param and put it into global map
					paramReferences = new ArrayList< Map<String,Double>>();
					mReferences.put(pName, paramReferences);
					
					//create parameter and push it into map
					Parameter param = new Parameter();
					param.setName(pName);
					param.setWeight(el.parametrsWeightMap.get(pName));
					param.setLimitedValue(el.parametersValuesMap.get(pName));
					
					mParametersMap.put(pName, param);					
				}
				else {
					// this parameter was met earlier
					// set it's value to value of the first such parameter
					IParameter globalParam = mParametersMap.get(pName);
					el.parametersValuesMap.replace(pName, globalParam.getLimitedValue());
				}
				//add reference to current leaf map into parameter references list
				paramReferences.add(el.parametersValuesMap);
				
			}// for parameter names				
		}// for leafs
		
		//fill mParameters
		if(!mParametersMap.isEmpty()) {
			mParameters.addAll(mParametersMap.values());
		}
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractParametersOrganizer#setParameterToModel(simulation.parameter.IParameter)
	 */
	@Override
	public void setParameterToModel(IParameter parameter) {

		String pName = parameter.getName();
		ArrayList< Map<String,Double>> referencesList = mReferences.get(pName);
		
		for( Map<String,Double> leafValuesMap : referencesList) {
			leafValuesMap.replace(pName, parameter.getLimitedValue());
		}
		
	}

}
