/**
 * 
 */
package simulation.parameter;

/**
 * Strategies used in IParametersOrganizer
 * 
 * @author User
 *
 */
public enum ParametersOrganizerStrategy {
	
	WEIGHTED_CCS,
	CHANGE_COMPARSION_CCS,
	CHANGE_OPPORTUNITY_CCS,
	SAVE_STEP_VECTOR_CCS,
	EXPONENTIAL_RELAX
}
