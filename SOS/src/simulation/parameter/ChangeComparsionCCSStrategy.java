/**
 * 
 */
package simulation.parameter;

import java.util.ArrayList;


/**
 * This strategy sorts parametersVector in decreasing order of parameters change values from the last run
 * 
 * @author User
 *
 */
final class ChangeComparsionCCSStrategy extends AbstractCCSStrategy {

	private ArrayList<Double> parameterDeltas;
	private boolean firstRun;
	
	/**
	 * @param organizer
	 */
	public ChangeComparsionCCSStrategy(IParametersOrganizer organizer) {
		super(organizer);
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractCCSStrategy#initState()
	 */
	/**
	 * create private objects and set initial values
	 */
	@Override
	protected void initState() {
		super.initState();
		parameterDeltas = new ArrayList<>();
		firstRun = true;
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractCCSStrategy#resetState()
	 */
	/**
	 * clear private data
	 */
	@Override
	protected void resetState() {
		super.resetState();
		firstRun = true;
		parameterDeltas.clear();
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractCCSStrategy#startState()
	 */
	@Override
	protected void startState() {
		super.startState();
		
		//init parameterDeltas 
		if(firstRun) {
			for(IParameter param : parametersVector) {
				parameterDeltas.add(param.getUnlimitedValue());
			}
			firstRun = false;
		}
		else {
			//sort parametersVector due to deltas in decreasing order
			
			//create and fill sort list
			ArrayList<SortParameterPair> sortList = new ArrayList<>();
			for(int i = 0; i<parametersVector.size(); ++i) {
				SortParameterPair pair = new SortParameterPair(parameterDeltas.get(i), 
												parametersVector.get(i));
				sortList.add(pair);
			}
			//sort the sort list in decreasing order
			sortList.sort((p1, p2 )-> Double.compare(p2.getKey(), p1.getKey() ));
			
			//replace parametersVector with elements in sorted order
			parametersVector.clear();
			for(SortParameterPair pair : sortList) {
				parametersVector.add(pair.getParam());
			}
		}
		
		//save initial parameter values
		for(int i =0; i < parametersVector.size(); ++i) {
			parameterDeltas.set(i, parametersVector.get(i).getUnlimitedValue());
		}
		
		//reset stepVector
		for(int i = 0; i < stepVector.size(); ++i)
			stepVector.set(i, stepSize);		
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractCCSStrategy#endState()
	 */
	/**
	 * Compute parameters deltas at the end of organization
	 */
	@Override
	protected void endState() {

		super.endState();
		
		//compute deltas
		for(int i = 0; i < parameterDeltas.size(); ++i) {
			parameterDeltas.set(i, Math.abs(parametersVector.get(i).getUnlimitedValue() - parameterDeltas.get(i)));
		}		
	}
	
}
