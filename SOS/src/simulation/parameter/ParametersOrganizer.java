/**
 * 
 */
package simulation.parameter;

/**
 * @author User
 *
 */
public enum ParametersOrganizer {
	/**
	 * All leafs parameters with the same names must have equal values
	 */
	GLOBAL_PARAMETERS_ORGANIZER,
	/**
	 * Leafs parameters with the same names may have different values
	 */
	LOCAL_PARAMETERS_ORGANIZER
}
