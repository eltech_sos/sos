/**
 * 
 */
package simulation.parameter;

/**
 * Simple factory to create IParametersOrganizer objects
 * 
 * @author User
 *
 */
public final class ParametersOrganizerFactory {

	
	public static IParametersOrganizer createOrganizer(IParametersModel model, ParametersOrganizer organizerType) {
		
		IParametersOrganizer newOrganizer;
		
		switch(organizerType) {
		case GLOBAL_PARAMETERS_ORGANIZER:
			newOrganizer = new GlobalParametersOrganizer(model);
			break;
		case LOCAL_PARAMETERS_ORGANIZER:
			newOrganizer = new LocalParametersOrganizer(model);
			break;
		default:
			newOrganizer = null;
			break;		
		};
		
		return newOrganizer;
	}
}
