/**
 * 
 */
package simulation.parameter;

import java.util.Map;

/**
 * @author User
 *
 */
interface IReferencedParameter {
	
	Map<String, Double> getReference();
	
	void setReference(Map<String, Double> reference);

}
