/**
 * 
 */
package simulation.parameter;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import Main.ParametersLoader;
import Main.TypeLoader;
import object.TreeElement;

/**
 * Parameter organization when parameters with the same names in different leafs are meant different
 * 
 * @author User
 *
 */
final class LocalParametersOrganizer extends AbstractParametersOrganizer {

	/**
	 * @param model
	 */
	public LocalParametersOrganizer(IParametersModel model) {

		super(model);

	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractParametersOrganizer#updateParameters()
	 */
	@Override
	protected void updateParameters() {

		mParameters.clear();
		if(mModel == null)
			return;
		
		ArrayList<TreeElement> leafs = mModel.getLeafs();
		
		boolean useParticipationProperty = mModel.getUseParticipationProperty();
		
		for(TreeElement el : leafs) {
			//leaf has no parameters
			if(el.parametersValuesMap.size() == 0)
				continue;
			
			//leaf type is not participating in organization
			if(useParticipationProperty && (!TypeLoader.isParticipate(el.typeName)))
				continue;
			
			//get parameters names
			Set<String> namesSet = el.parametersValuesMap.keySet();
			
			for(String pName : namesSet) {
				//parameter type is not participating in organization
				if(useParticipationProperty && (!ParametersLoader.isParticipate(pName)))
						continue;
				
				//create parameter and push it into list
				ReferencedParameter param = new ReferencedParameter();
				param.setName(pName);
				param.setWeight(el.parametrsWeightMap.get(pName));
				param.setLimitedValue(el.parametersValuesMap.get(pName));
				param.setReference(el.parametersValuesMap);
				
				mParameters.add(param);
			}// for parameter names			
		}//for leafs
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.AbstractParametersOrganizer#setParameterToModel(simulation.parameter.IParameter)
	 */
	@Override
	public void setParameterToModel(IParameter parameter) {
		if(!(parameter instanceof ReferencedParameter)) {
			//TODO maybe we should throw something here...
			return;
		}
		ReferencedParameter param = (ReferencedParameter) parameter;
		
		Map<String, Double> paramValuesMap = param.getReference();
		paramValuesMap.replace(param.getName(), param.getLimitedValue());
	}

}
