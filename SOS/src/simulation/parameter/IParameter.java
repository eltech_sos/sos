/**
 * 
 */
package simulation.parameter;

/**
 * @author User
 *
 */
interface IParameter {
	
	double getUnlimitedValue();
	
	double getLimitedValue();
	
	String getName();
	
	double getWeight();
	
	double getChangeOpportunity();
	
	void setUnlimitedValue(double value);
	
	void setLimitedValue(double value);
	
	void setName(String parametrName);
	
	void setWeight(double weight);
}
