/**
 * 
 */
package simulation.parameter;

/**
 * @author User
 *
 */
interface IParametersOrganizerStrategy {
	
	void reset();
	
	void organizeParameters(double rootMinValue);

}
