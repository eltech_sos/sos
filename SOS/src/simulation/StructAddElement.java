//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// �������� ���������� �������� ��� �������������� ������
// � ���� ���������������� ��������� �������������� ����������
// ������ �������� ����� �������, ����� ��������� �������� � ��������
// �����������
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
package simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import java.util.Map.Entry;

import Main.AddElemFromSimulation;
import Main.Main;
import Main.TreeOperations;
import Main.TypeLoader;
import simulation.FuncOrgForLists;
import simulation.FunctionOrganization;
import simulation.ItselfOrganizationVariables;
import simulation.SimulationCountFX;
import simulation.Parser;
import object.TreeElement;

public class StructAddElement 
{
	//������������ �������
	public class Parent {
		public double dJ = Double.NEGATIVE_INFINITY; //������� �����������
		public double use_coef = Double.NEGATIVE_INFINITY; //����������� ����������
		public int level = -1; //�������
		public int index = -1; //������
		public int ID = -1;//�������������
	}

	public Vector<Parent> BestParents; //������ ������ ���������
	public ArrayList<TreeElement> realLeafsList; //������ ������
	public double J; //����������
	public TreeElement CurElem; //������� �������
	public TreeElement ChildElem; //�������� �������
	public int TypeIndex; //������ ����
	public int LevelIndex; //������ ������
	public int ElemIndex;  //������ ��������
	public int ChildIndex; //������ ��������� ��������
	public Vector<Vector<TreeElement>> Backup; //����� �������
	public Vector<TreeElement> LevelList; //������ ������
	public double realMinRootValue; //����� ���������� ����������� �������� � �����
	public int TypesCount;  //���������� �����
	public int BestType; //������ ���
	public int UsefullestType;  //����� �������� ���
	public int realCount; //���������� ��������� � ������� (�����)
	public int realLevelCount; //���������� ������� � ������� (�����)
	public int free_slots; //���������� ��������� ������
	public int real_free_slots; //���������� ��������� ������ (�����)

	//---------------------------------------------------------------------------
	// �������-�����������  (�������� ������)
	//---------------------------------------------------------------------------
	public StructAddElement() 
	{
		//��������� ����������� ���������� ��������
		realMinRootValue = ItselfOrganizationVariables.rootMinimumCount;
		//��������� ��������, ���� �� ������������� �������
		do 
		{
		
			J = TreeOperations.rootCount(); //����������
			free_slots = 0; //�������������� ���������� �������� ������
			ChildElem = null; //������� �������
			realCount = Main.count; //����� ���������� ���������
			realLevelCount = Main.maxLevelsCount; //����� ����� �������
			realLeafsList = new ArrayList<TreeElement>(); //����� ������ ������
			realLeafsList.addAll(ItselfOrganizationVariables.leafsList);
			//�������� ������� ����������� �� ����������� ������ ��������� �����������
			ItselfOrganizationVariables.rootMinimumCount = Double.POSITIVE_INFINITY;
			
			//������� ����� �������
			Backup = new Vector<Vector<TreeElement>>();
			//�������� �� ���� �������
			for (int i = 0; i < Main.structTreeElements.size(); i++) 
			{
				//������� ����� �������
				Backup.add(new Vector<TreeElement>());
				//�������� �� ���� ��������� ������
				for (int j = 0; j < Main.structTreeElements.elementAt(i).size(); j ++)
				{
					//������� �������
					Backup.lastElement().add((TreeElement) 
							Main.structTreeElements.elementAt(i).get(j).clone());
					//������������ ����� �������� ������
					free_slots += TypeLoader.getSlotsNumber
						(Main.structTreeElements.elementAt(i).elementAt(j).typeName) - 
						Main.structTreeElements.elementAt(i).elementAt(j).iChildrenCount;
				}
			}
			
			//���� ��������� ������ ��� - ��������� ������
			if (free_slots<1) 
				return;
			
			//�������� ���������� ��������� ������ (��� ��������������)
			real_free_slots = free_slots;
			//������� ����� ����� �����
			TypesCount = TypeLoader.elementaryTypesList.size() + 
						 TypeLoader.intermediaryTypesList.size();
			
			//���� ������ ���������
			BestParents = new Vector<Parent>(TypesCount);
			//�������� �������� ������
			LevelList = new Vector<TreeElement>();
			//�������� �� ���� �����
			for (TypeIndex = 0; TypeIndex < TypesCount; TypeIndex++) 
			{
				//������� ���������
				BestParents.add(TypeIndex, new Parent());
				//���� ��� ��� ��
				if (TypeIndex >= TypeLoader.elementaryTypesList.size())
				{
					//������� ����������� ����������
					BestParents.elementAt(TypeIndex).use_coef = 
						(free_slots-1+TypeLoader.getSlotsNumber
						(TypeLoader.intermediaryTypesList.get
						(TypeIndex - TypeLoader.elementaryTypesList.size())))/
						(free_slots*TypeLoader.getPrice(TypeLoader.intermediaryTypesList.get
						(TypeIndex - TypeLoader.elementaryTypesList.size())));
				} 
				else
				//���� ��� ��� ��	
				{
					//�������� �� ���� ������� �������
					for (LevelIndex = 0; LevelIndex < Main.structTreeElements.size(); LevelIndex++) 
					{
						//������� �������
						LevelList.addAll(Main.structTreeElements.get(LevelIndex));
						//�������� �� ���� ��������� ������� ������
						for (ElemIndex = 0; ElemIndex < LevelList.size(); ElemIndex++) 
						{
							//������� �������
							CurElem = LevelList.get(ElemIndex);
							// ����������,������� �� ���� ������� ��
							if (TypeLoader.intermediaryTypesList.contains(CurElem.typeName) 
									&& CurElem.iChildrenCount < TypeLoader.getSlotsNumber(CurElem.typeName)) 
							{
								// ���� � ����� �� ������ ������������ �� ��� ��
								// ��������������, ���������� ���.
								if (TypeLoader.getChildrenType(CurElem.typeName) == 1 && 
										TypeIndex >= TypeLoader.elementaryTypesList.size())
									continue;
								if (TypeLoader.getChildrenType(CurElem.typeName) == 2 && 
										TypeIndex < TypeLoader.elementaryTypesList.size())
									continue;
								//��������� ������� ������� ����
								tmpAdd(LevelIndex, ElemIndex, TypeIndex);
								Optimize_A(); // ��������� ���
								double dJ = TreeOperations.rootCount() - J; // ��������� �������� � �����
								//������������ ����������� ����������
								//after tmpAdd, free_slots was increased by 1, we must take it into account
								double usec = ((double)(free_slots - 2)) * dJ / 
										(((double)free_slots - 1) * (double)TypeLoader.getPrice(CurElem.typeName));
								//���� ��������� ������
								if (BestParents.elementAt(TypeIndex).use_coef < usec) 
								{
									//���������� ���������� � ������� ��������
									BestParents.elementAt(TypeIndex).dJ = dJ; //������� �����������
									BestParents.elementAt(TypeIndex).use_coef = usec; //����������� ����������
									BestParents.elementAt(TypeIndex).level = LevelIndex;//������ ������
									BestParents.elementAt(TypeIndex).index = ElemIndex;//������ ��������
									BestParents.elementAt(TypeIndex).ID = CurElem.ID;//������������� ��������
								}
								restoreSystem(); //��������������� �������
							}
						}// for �� ElemIndex
						LevelList.clear();
					}// for �� LevelIndex
				}
			}// for �� TypeIndex
			
			
			// ��������� ������ ������� ����������� ������ �����
			BestType = 0; //������ ���
			UsefullestType = 0; //��� � ������������ ������������� ����������
			//�������� �� ���� �����
			for (TypeIndex = 1; TypeIndex < TypesCount; TypeIndex++)
				//���� ����������� ���������� ������
				if (BestParents.elementAt(UsefullestType).use_coef 
						< BestParents.elementAt(TypeIndex).use_coef)
					//��������� ���
					UsefullestType = TypeIndex;
			//���� ����� �������� ��� - ��� �� - ���������� ������� ��������
			if (UsefullestType >= TypeLoader.elementaryTypesList.size()){
				//�������� �� ���� �����
				for (TypeIndex = 1; TypeIndex < TypesCount; TypeIndex++)
					//���� ������� ��������� ����������� ������
					if (BestParents.elementAt(BestType).dJ < BestParents.elementAt(TypeIndex).dJ)
						//��������� ���
						BestType = TypeIndex;
			} else
				//������ ��� = ����� ��������� ���
				BestType = UsefullestType;
			//��������� ������� � �������
			FinallyAdd(BestParents.elementAt(BestType).level, BestParents.elementAt(BestType).index, UsefullestType);
			Optimize_A(); // ��������� ���
		} while (TreeOperations.rootCount() < realMinRootValue); //���� ������� �� ���������������
		//��������������� ����������� �� ����������� �������� �����������
		ItselfOrganizationVariables.rootMinimumCount = realMinRootValue;
	}

	//---------------------------------------------------------------------------
	// �������������� �������
	//---------------------------------------------------------------------------
	public void restoreSystem() 
	{
		Main.count = realCount;//���������� ���������
		Main.maxLevelsCount = realLevelCount; //���������� �������
		ItselfOrganizationVariables.leafsList.clear(); //������� ������ ������
		ItselfOrganizationVariables.leafsList.addAll(realLeafsList);//������ ������
		//������� ������
		Main.structTreeElements.clear();
		//�������� �� ���� �������
		for (int i = 0; i < Backup.size(); i++) {
			//������� ����� �������
			Main.structTreeElements.add(new Vector<TreeElement>());
			//�������� �� ���� ���������
			for (int j = 0; j < Backup.elementAt(i).size(); j++){
				//�������� �������
				Main.structTreeElements.lastElement().add((TreeElement) Backup.elementAt(i).get(j).clone());
			}
		}
		
		free_slots = real_free_slots;//����� ��������� ������
		//������� ����������, ������� ���������� ���������� ���
		ItselfOrganizationVariables.getInstance().modelChanged(null);
		//TODO cleanup
		//ItselfOrganizationVariables.isFirstParamOrg = true;
		//������� ������ ������
		ItselfOrganizationVariables.leafsList.clear();
		TreeElement elementInStruct;
		//�������� �� ���� �������
		for(int level=0; level<Main.structTreeElements.size(); level++)
		{
			//�������� �� ���� ��������� ������
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
			{
				//������� �������
				elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
				//���� ��� ��
				if( elementInStruct.type == 3 )
				{
					//���������
					ItselfOrganizationVariables.leafsList.add( elementInStruct );
				}
			}
		}
	}

	//---------------------------------------------------------------------------
	// ��������� ���������� ��������
	//---------------------------------------------------------------------------
	public void tmpAdd(int lvl, int ind, int type) 
	{
		int ID = -1, num = 1; //������������� � ����� ��������
		Vector<TreeElement> tmpV;
		//���������� ������������� ��������
		//�������� �� ���� �������
		for (int i = 0; i < Main.structTreeElements.size(); i++) 
		{
			//������� �������
			tmpV = Main.structTreeElements.get(i);
			//�������� �� ���� ��������� ������
			for (int j = 0; j < tmpV.size(); j++) 
			{
				//���������� ������������ �������������
				if (tmpV.get(j).ID > ID)
					ID = tmpV.get(j).ID;
			}
		}
		//���� ��� �������� �������
		if (Main.structTreeElements.size() - 1 == lvl) 
		{
			//������� ����� �������
			Main.structTreeElements.add(new Vector<TreeElement>());
			Main.maxLevelsCount++; //����������� ���������� ���������
		}
		//������� ����� �������
		TreeElement tmp = new TreeElement(ID + 1, 
				Main.structTreeElements.get(lvl).get(ind).ID, 
				(type < TypeLoader.elementaryTypesList.size()) ? 3 : 2);
		tmp.elementName = "Temporary"; // ��� ��������
		tmp.elementCount = 0;//�������� � ��������
		tmp.iChildrenCount = 0; //���������� ����� ��������
		tmp.Level = lvl + 1; //������� ��������
		//���� ��� ��
		if (type < TypeLoader.elementaryTypesList.size()) 
		{
			//���
			tmp.typeName = TypeLoader.elementaryTypesList.get(type);
		} else 
		// ���� ��� ��
		{
			//���
			tmp.typeName = TypeLoader.intermediaryTypesList.get
			(type - TypeLoader.elementaryTypesList.size());
		}
		//��������� ����������
		if (tmp.typeName == null || tmp.typeName.length() < 1)
			return;
		
		//������������� ���������� ��������� ������
		free_slots += TypeLoader.getSlotsNumber(tmp.typeName) - 1;
		tmp.functionName = TypeLoader.getFunctionList(tmp.typeName).get(0);//�������
		//����� ����������
		tmp.parametersValuesMap = Parser.fillingParametersValuesMap(tmp.functionName);
		//����� �����
		tmp.parametrsWeightMap = new HashMap<String, Double>();
		//��������� ���
		double initialWeight = 1.0;
		//���� ����� ���������� ����������
		if (tmp.parametersValuesMap != null) {
			if (tmp.parametersValuesMap.size() > 0) {
				//��������� �� ��������� �������
				Iterator iter = tmp.parametersValuesMap.entrySet().iterator();
				while (iter.hasNext()) {
					Entry paramInform = (Entry) iter.next();
					//������������� ��������� ���
					String paramName = (String) paramInform.getKey();
					//��������� ������
					tmp.parametrsWeightMap.put(paramName, initialWeight);
				}
			}
		}
		//��������� �������
		Main.structTreeElements.get(lvl + 1).add(tmp);
		//����������� ���������� �������� ��������� ������� ��������
		Main.structTreeElements.get(lvl).get(ind).iChildrenCount++;
		//��������� ������� � ������ ������
		ItselfOrganizationVariables.leafsList.add(tmp);
		// ��������� ����� ���������� �������
		Main.count++; // ���������� ��������� � �������
	}

	//---------------------------------------------------------------------------
	// ���������� ��������
	//---------------------------------------------------------------------------
	public void FinallyAdd(int lvl, int ind, int type) 
	{
		int ID = -1, num = 1; //������������� � ����� ��������
		Vector<TreeElement> tmpV; //������� �������
		//�������� �� ���� �������
		for (int i = 0; i < Main.structTreeElements.size(); i++) 
		{
			//������� �������
			tmpV = Main.structTreeElements.get(i);
			//�������� �� ���� ��������� ������� ������
			for (int j = 0; j < tmpV.size(); j++) 
			{
				//���������� ������������ �������������
				if (tmpV.get(j).ID > ID)
					ID = tmpV.get(j).ID;
				//������� ����������� ��������
				if (tmpV.get(j).elementName.startsWith("Autoadded"))
					num++;
			}
		}
		//���� ��� ��������� �������
		if (Main.structTreeElements.size() - 1 == lvl) 
		{
			//������� ����� �������
			Main.structTreeElements.add(new Vector<TreeElement>());
			Main.maxLevelsCount++; //����������� ���������� �������
		}
		//������� ����� �������
		TreeElement tmp = new TreeElement(ID + 1,  
				Main.structTreeElements.get(lvl).get(ind).ID, 
				(type < TypeLoader.elementaryTypesList.size()) ? 3 : 2);
		tmp.elementName = "Autoadded " + num; // ��� ��������
		tmp.elementCount = 0; //�������� � ��������
		tmp.iChildrenCount = 0; //���������� ����� ��������
		tmp.Level = lvl + 1; //������� ��������
		//���� ��� ��
		if (type < TypeLoader.elementaryTypesList.size()) 
		{
			//���
			tmp.typeName = TypeLoader.elementaryTypesList.get(type);
		} else 
		//���� ��� ��
		{
			//���
			tmp.typeName = TypeLoader.intermediaryTypesList.get(type - TypeLoader.elementaryTypesList.size());
		}
		tmp.functionName = TypeLoader.getFunctionList(tmp.typeName).get(0); //�������
		
		//����� ����������
		tmp.parametersValuesMap = Parser.fillingParametersValuesMap(tmp.functionName);
		//����� �����
		tmp.parametrsWeightMap = new HashMap<String, Double>();
		//��������� ���
		double initialWeight = 1.0;
		//���� ����� ����� �������
		if (tmp.parametersValuesMap != null) 
		{
			if (tmp.parametersValuesMap.size() > 0) 
			{
				//��������
				Iterator iter = tmp.parametersValuesMap.entrySet().iterator();
				while (iter.hasNext()) 
				{
					//��������� �� ��������� ������
					Entry paramInform = (Entry) iter.next();
					//������� ����� ������
					String paramName = (String) paramInform.getKey();
					//��������� �������� � ��������� �����
					tmp.parametrsWeightMap.put(paramName, initialWeight);
				}
			}
		}
		//��������� ������� � ���������� ����������
		new AddElemFromSimulation(tmp, Main.structTreeElements.
				get(BestParents.get(BestType).level).get(BestParents.get(BestType).index));
		//��������� ������� � ������ ������
		ItselfOrganizationVariables.leafsList.add(tmp);
		//��������� ������� � ���������� ����������
		ItselfOrganizationVariables.QControl.add(5, SimulationCountFX.t, TreeOperations.rootCount() - J, TypeLoader.getPrice(tmp.typeName));
		// Main.structTreeElements.get(lvl + 1).add(tmp);
		// Main.structTreeElements.get(lvl).get(ind).iChildrenCount++;
		// ��������� ����� ���������� �������
		// Main.count++; //���������� ��������� � �������
	}
	//---------------------------------------------------------------------------
	// ���������� � ��� � ����� ��� 
	//---------------------------------------------------------------------------
	public void Optimize_A() 
	{
		//���������� ���������� ���
		ItselfOrganizationVariables.getInstance().modelChanged(null);
		//TODO cleanup
		//ItselfOrganizationVariables.isFirstParamOrg = true;		
		//������� ������ ������
		ItselfOrganizationVariables.leafsList.clear();
		TreeElement elementInStruct;
		//�������� �� ���� ������� �������
		for(int level=0; level<Main.structTreeElements.size(); level++)
		{
			//�������� �� ���� ��������� �������
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
			{
				//������� �������
				elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
				//���� ��� ��
				if( elementInStruct.type == 3 )
				{
					//��������� ��� � ������ ������
					ItselfOrganizationVariables.leafsList.add( elementInStruct );
				}
			}
		}
		//���� ��������� ��� ��� ��
		if (ItselfOrganizationVariables.funcIntermediateOrgAllowed) 
		{
			new FunctionOrganization();//��� ��� ��
			new FuncOrgForLists(); //��� ��� ��
			//TODO call to parameter organization replaced
			//new SimpleParamOrg(realMinRootValue); //���
			ItselfOrganizationVariables.getInstance().runPSO(realMinRootValue);
		} else 
		{ 
			//���� ��������� ��� ��� ��
			if (ItselfOrganizationVariables.funcLeafOrgAllowed) 
			{
				new FuncOrgForLists();//��� ��� ��
				//TODO call to parameter organization replaced
				//new SimpleParamOrg(realMinRootValue);//���
				ItselfOrganizationVariables.getInstance().runPSO(realMinRootValue);
			} 
			//���� ��������� ���
			else if (ItselfOrganizationVariables.paramOrgAllowed) 
			{
				//TODO call to parameter organization replaced
				//new SimpleParamOrg(realMinRootValue);//���
				ItselfOrganizationVariables.getInstance().runPSO(realMinRootValue);
			}
		}
	}
}
