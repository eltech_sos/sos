package simulation;

import java.util.Vector;

import simulation.ItselfOrganizationVariables;

public class QualityControlVariables
{
	public class ExpirienceRecord 
	{
		// ����� "������ ������������ �����", 
		// ������ ����� ������ �������� �������
		// �� ����� �� ��������� ���������������.
		// ������ ������ ������ ������:
		public int t;			// ������ �������� �������
		public double deltaJ;	// ������� ���������� ��������� �����������
		public float price;		// ��������� ���������������
		
		// ��� ��������������, �� ������� ������������� ���������
		ExpirienceRecord(int t_current, double new_delta_J) 
		{
			t=t_current;		// ���������� ���� �������
			deltaJ=new_delta_J;	// ���������� ���� ���������� �����������
			price=0;			// ��������� ��������������� ����� ����
		}
		
		// ��� ���������������, ������� ������������� ���������
		ExpirienceRecord(int t_current, double new_delta_J, float new_price)
		{
			t=t_current;		// ���������� ���� �������
			deltaJ=new_delta_J;	// ���������� ���� ���������� �����������
			price=new_price;	// ���������� ���� ��������� ���������������
		}
	};
	
	// ������, �������� ������ � ��������������� ����������������
	public Vector<ExpirienceRecord> param;
	// ������, �������� ������ � �������������� ����������������
	public Vector<ExpirienceRecord> func;
	// ������, �������� ������ � ������� ����������������
	public Vector<ExpirienceRecord> type;
	// ������, �������� ������ � ����������� ��������������
	public Vector<ExpirienceRecord> reorg;
	// ������, �������� ������ � ��������������� ����������������
	public Vector<ExpirienceRecord> adding;
	// ����������, �������� �������� ����������� �� ������ ����
	public float start_t_value; 
	
	public QualityControlVariables()
	{
		// ����������� ������.
		init();
	}
	
	public void init(){
		// ���������� ����� �������� �������:
		param=new Vector<ExpirienceRecord>();
		func=new Vector<ExpirienceRecord>();
		type=new Vector<ExpirienceRecord>();
		reorg=new Vector<ExpirienceRecord>();
		adding=new Vector<ExpirienceRecord>();
		start_t_value=(float)0.0;
	}
	
	public void add(int level, int t, double delta_J, float price)
	{	// ������� ���������� ����� ������
		ExpirienceRecord temp = new ExpirienceRecord(t, delta_J, price);
		switch (level){
		case 1:		// ���������� ������ � �������������� ���������������
		{
			if(param.size()>0 && param.get(param.size()-1).t==t)
			{		// ���� ������ � ����� ��������� ������� ��� ����������,
					// ��������� �������� ����� �� �������� ��������.
				param.get(param.size()-1).deltaJ+=delta_J;
				param.get(param.size()-1).price+=price;
			}
			else	// ����� - �������� ����� ������ � ��������� ���������� �����
				param.add(temp);
			break;
		}
		case 2:		// ���������� ������ � �������������� ���������������
		{
			if(func.size()>0 && func.get(func.size()-1).t==t)
			{		// ���� ������ � ����� ��������� ������� ��� ����������,
					// ��������� �������� ����� �� �������� ��������.
				func.get(func.size()-1).deltaJ+=delta_J;
				func.get(func.size()-1).price+=price;
			}
			else	// ����� - �������� ����� ������ � ��������� ���������� �����
				func.add(temp);
			break;
		}
		case 3:		// ���������� ������ � ������� ���������������
		{
			if(type.size()>0 && type.get(type.size()-1).t==t)
			{		// ���� ������ � ����� ��������� ������� ��� ����������,
					// ��������� �������� ����� �� �������� ��������.
				type.get(type.size()-1).deltaJ+=delta_J;
				type.get(type.size()-1).price+=price;
			}
			else	// ����� - �������� ����� ������ � ��������� ���������� �����
				type.add(temp);
			break;
		}
		case 4:		// ���������� ������ � ����������� �������������
		{
			if(reorg.size()>0 && reorg.get(reorg.size()-1).t==t)
			{		// ���� ������ � ����� ��������� ������� ��� ����������,
					// ��������� �������� ����� �� �������� ��������.
				reorg.get(reorg.size()-1).deltaJ+=delta_J;
				reorg.get(reorg.size()-1).price+=price;
			}		
			else	// ����� - �������� ����� ������ � ��������� ���������� �����
				reorg.add(temp);
			break;
		}
		case 5:		// ���������� ������ � ����������� ���������������
		{
			if(adding.size()>0 && adding.get(adding.size()-1).t==t)
			{		// ���� ������ � ����� ��������� ������� ��� ����������,
					// ��������� �������� ����� �� �������� ��������.
				adding.get(adding.size()-1).deltaJ+=delta_J;
				adding.get(adding.size()-1).price+=price;
			}
			else	// ����� - �������� ����� ������ � ��������� ���������� �����
				adding.add(temp);
			break;
		}
		default:
			break;
		}
	}
	
	public int GetTime(int t_begin)
	{ // ������� ���������� �����, ��������� � ���������� ��������������� �� t_begin
		int time=30000;
		// ����� ����� ������� � ���� ����� ��������������� ��� ������,
		// ������� ����� �������� �������, ����������� ������� � t_begin �����
		for (int i=0;i<param.size();i++)
		{	
			if (param.get(i).t<=t_begin)
				if(t_begin-param.get(i).t<time)
					time=t_begin-param.get(i).t;
		}
		for (int i=0;i<func.size();i++)
		{
			if (func.get(i).t<=t_begin)
				if(t_begin-func.get(i).t<time)
					time=t_begin-func.get(i).t;
		}
		for (int i=0;i<type.size();i++)
		{
			if (type.get(i).t<=t_begin)
				if(t_begin-type.get(i).t<time)
					time=t_begin-type.get(i).t;
		}
		for (int i=0;i<reorg.size();i++)
		{
			if (reorg.get(i).t<=t_begin)
				if(t_begin-reorg.get(i).t<time)
					time=t_begin-reorg.get(i).t;
		}
		for (int i=0;i<adding.size();i++)
		{
			if (adding.get(i).t<=t_begin)
				if(t_begin-adding.get(i).t<time)
					time=t_begin-adding.get(i).t;
		}
		if (time==30000)	// ���� ��������������� ��� �� ���� ��������,
			return 0; 		// ������� ������� �������� �������.
		return (t_begin-time);
	}
	
	public int GetLast(int t_begin)
	{
		// ������ ����������, ����� ��������������� ���� ��������� ���������
		int time=30000;
		int level=-1;
		// ����� ����� ������� � ���� ����� ��������������� ��� ������,
		// ������� ����� �������� �������, ����������� ������� � t_begin �����
		// � ������������ � ������.
		for (int i=0;i<param.size();i++)
		{
			if (param.get(i).t<=t_begin)
				if(t_begin-param.get(i).t<=time)
				{
					time=t_begin-param.get(i).t;
					level=1;
				}
		}
		for (int i=0;i<func.size();i++)
		{
			if (func.get(i).t<=t_begin)
				if(t_begin-func.get(i).t<=time)
				{
					time=t_begin-func.get(i).t;
					level=2;
				}
		}
		for (int i=0;i<type.size();i++)
		{
			if (type.get(i).t<=t_begin)
				if(t_begin-type.get(i).t<=time)
				{
					time=t_begin-type.get(i).t;
					level=3;
				}
		}
		for (int i=0;i<reorg.size();i++)
		{
			if (reorg.get(i).t<=t_begin)
				if(t_begin-reorg.get(i).t<=time)
				{
					time=t_begin-reorg.get(i).t;
					level=4;
				}
		}
		for (int i=0;i<adding.size();i++)
		{
			if (adding.get(i).t<=t_begin)
				if(t_begin-adding.get(i).t<=time)
				{
					time=t_begin-adding.get(i).t;
					level=5;
				}
		}
		if (level==-1)		// ���� ��������������� ��� �� ���� ��������
			level=0; 		// ������� ��������������� ���������������.
		return Return(level);
	}
	
	public float GetDifference(int t)
	{
		// ������� ���������� ��������� ���������� ����������� �� ����
		// ���������������, ���������� � ������ ������� t
		float Diff=(float)-1.0;
		for (int i=0;i<param.size();i++)
			if(param.get(i).t==t)
				Diff+=param.get(i).deltaJ;
		for (int i=0;i<func.size();i++)
			if(func.get(i).t==t)
				Diff+=func.get(i).deltaJ;
		for (int i=0;i<type.size();i++)
			if(type.get(i).t==t)
				Diff+=type.get(i).deltaJ;
		for (int i=0;i<reorg.size();i++)
			if(reorg.get(i).t==t)
				Diff+=reorg.get(i).deltaJ;
		for (int i=0;i<adding.size();i++)
			if(adding.get(i).t==t)
				Diff+=adding.get(i).deltaJ;
		if(Diff==(float)-1.0)
			return start_t_value;
		return Diff;
	}
	
	public int GetBest_emperical(int cur_time)
	{
		// ������� ���������� ��� �������� ����������� ���������������
		// �� ������ ������������ �����
		int last=GetTime(cur_time);				// ����� ��������� ���������������
		int forlast=GetTime(last-1);			// ����� ������������� ���������������
												
		if(last==cur_time)
			return Return((GetLast(cur_time+1)+1));
		switch (GetLast(cur_time))
		{
		case 1:
		{
			// ���� ��������� ��������������� ���� ���������������
			if((GetDifference(last)/(float)(cur_time-last))>(GetDifference(forlast)/(float)(last-forlast)))
			{
				// ���� ����������� ��������� � ��������� �������� ��������������
				return Return(2); // ����� ����������� ����� �������������� ���������������
			}
			// ����� ���������� ����� ��������� ��������������� ���������������
			return Return(1);
		}
		case 2:
		{
			// ���� ��������� ��������������� ���� ��������������
			if((GetDifference(last)/(float)(cur_time-last))>(GetDifference(forlast)/(float)(last-forlast)))
			{
				// ���� ����������� ��������� � ��������� �������� ��������������
				return Return(3); // ����� ��������� ��������� ��������������� ���������� ������
			}
			// ����� ���������� ����� ��������� �������������� ��� ��������������� ���������������
			if(last>forlast)
				return Return(1);
			return Return(2);
		}
		case 3:
		{
			// ���� ��������� ��������������� ���� ���������� �����
			if((GetDifference(last)/(float)(cur_time-last))>(GetDifference(forlast)/(float)(last-forlast)))
			{
				// ���� ����������� ��������� � ��������� �������� ��������������
				return Return(4); // ����� ��������� ��������� ��������������� ���������� ������
			}
			// ����� ���������� ����� ��������� �������������� ���������������
			if(last>forlast)
				return Return(2);
			return Return(3);
		}
		case 4:
		{
			// ���� ��������� ��������������� ���� �����������������
			if((GetDifference(last)/(float)(cur_time-last))>(GetDifference(forlast)/(float)(last-forlast)))
			{
				// ���� ����������� ��������� � ��������� �������� ��������������
				return Return(5); // ����� ��������� ��������� ��������������� ���������� ������
			}
			// ����� ���������� ����� ��������� ���������������� �����
			if(last>forlast)
				return Return(3);
			return Return(4);
		}
		case 5:
		{
			// ���� ��������� ��������������� ���� �����������
			if((GetDifference(last)/(float)(cur_time-last))>(GetDifference(forlast)/(float)(last-forlast)))
			{
				// ���� ����������� ��������� � ��������� �������� ��������������
				return Return(5); // ����� ��������� ����� ��������� ���������� ���������
			}
			// ����� ���������� ����� ��������� ������� ��������������� ��� ����������������
			if((cur_time-last)>(last-forlast))
				return Return(3);
			return Return(4);
		}
		default:
			break;
		}
		return Return(1); // ���� ��������������� ��� �� ���� �������� �� ����, ��������� ���������������
	}
	
	public int GetBest_neural(int cur_time)
	{
		// ������� ���������� ��� �������� ����������� ���������������
		// �� ������ ������������ �����
		int last=GetTime(cur_time);				// ����� ��������� ���������������
		int forlast=GetTime(last-1);			// ����� ������������� ���������������
												
		if(last==cur_time)
			return Return((GetLast(cur_time+1)+1));
		switch (GetLast(cur_time))
		{
		case 1:
		{
			// ���� ��������� ��������������� ���� ���������������

			if((GetDifference(last)) >= (GetDifference(forlast)+(cur_time - 2*last + forlast)*(GetDifference(last)/(float)(cur_time-last)-(GetDifference(forlast)/(float)(last-forlast)))))
			{
				// ���� ����������� ��������� � ��������� �������� ��������������
				return Return(2); // ����� ����������� ����� �������������� ���������������
			}
			// ����� ���������� ����� ��������� ��������������� ���������������
			return Return(1);
		}
		case 2:
		{
			// ���� ��������� ��������������� ���� ��������������
			if((GetDifference(last)) >= (GetDifference(forlast)+(cur_time - 2*last + forlast)*(GetDifference(last)/(float)(cur_time-last)-(GetDifference(forlast)/(float)(last-forlast)))))
			{
				// ���� ����������� ��������� � ��������� �������� ��������������
				return Return(3); // ����� ��������� ��������� ��������������� ���������� ������
			}
			// ����� ���������� ����� ��������� �������������� ��� ��������������� ���������������
			if(last>forlast)
				return Return(1);
			return Return(2);
		}
		case 3:
		{
			// ���� ��������� ��������������� ���� ���������� �����
			if((GetDifference(last)) >= (GetDifference(forlast)+(cur_time - 2*last + forlast)*(GetDifference(last)/(float)(cur_time-last)-(GetDifference(forlast)/(float)(last-forlast)))))
			{
				// ���� ����������� ��������� � ��������� �������� ��������������
				return Return(4); // ����� ��������� ��������� ��������������� ���������� ������
			}
			// ����� ���������� ����� ��������� �������������� ���������������
			if(last>forlast)
				return Return(2);
			return Return(3);
		}
		case 4:
		{
			// ���� ��������� ��������������� ���� �����������������
			if((GetDifference(last)) >= (GetDifference(forlast)+(cur_time - 2*last + forlast)*(GetDifference(last)/(float)(cur_time-last)-(GetDifference(forlast)/(float)(last-forlast)))))
			{
				// ���� ����������� ��������� � ��������� �������� ��������������
				return Return(5); // ����� ��������� ��������� ��������������� ���������� ������
			}
			// ����� ���������� ����� ��������� ���������������� �����
			if(last>forlast)
				return Return(3);
			return Return(4);
		}
		case 5:
		{
			// ���� ��������� ��������������� ���� �����������
			if((GetDifference(last)) >= (GetDifference(forlast)+(cur_time - 2*last + forlast)*(GetDifference(last)/(float)(cur_time-last)-(GetDifference(forlast)/(float)(last-forlast)))))
			{
				// ���� ����������� ��������� � ��������� �������� ��������������
				return Return(5); // ����� ��������� ����� ��������� ���������� ���������
			}
			// ����� ���������� ����� ��������� ������� ��������������� ��� ����������������
			if((cur_time-last)>(last-forlast))
				return Return(3);
			return Return(4);
		}
		default:
			break;
		}
		return Return(1); // ���� ��������������� ��� �� ���� �������� �� ����, ��������� ���������������
	}
	
	public int Return(int arg)
	{ // ������� ���������� ���������� �� ����������� ����� ��, ������� �� ������ ���������
		switch(arg){
		case 1:{
			if (ItselfOrganizationVariables.paramOrgAllowed)
				return 1;
		}
		case 2:{
			if (ItselfOrganizationVariables.funcLeafOrgAllowed || ItselfOrganizationVariables.funcIntermediateOrgAllowed)
				return 2;
		}
		case 3:{
			if (ItselfOrganizationVariables.typeLeafOrgAllowed || ItselfOrganizationVariables.typeIntermediateOrgAllowed)
				return 3;
		}
		case 4:{
			if (ItselfOrganizationVariables.reconfigurationOrgAllowed)
				return 4;
		}
		case 5:{
			if (ItselfOrganizationVariables.addNewElementsOrgAllowed)
				return 5;
			if (ItselfOrganizationVariables.reconfigurationOrgAllowed)
				return 4;
			else {
				if (ItselfOrganizationVariables.typeLeafOrgAllowed || ItselfOrganizationVariables.typeIntermediateOrgAllowed)
					return 3;
				else {
					if (ItselfOrganizationVariables.funcLeafOrgAllowed || ItselfOrganizationVariables.funcIntermediateOrgAllowed)
						return 2;
					else {
						if (ItselfOrganizationVariables.paramOrgAllowed)
							return 1;
						else
							return 0; // ��� ���� �� ���������!
					}
				}
			}
		}
		}
		return arg;
	}
}
