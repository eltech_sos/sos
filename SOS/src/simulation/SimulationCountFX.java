package simulation;

import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import tabs.FXDialog;
import tabs.Message;

import Main.Main;
import Main.TreeOperations;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import simulation.AllSoTableFX;
import simulation.CheckModel;
import simulation.HistoryOfSelforganizations;
import simulation.ItselfOrganizationVariables;
import simulation.SimulationStep;
import simulation.TGFX;

public class SimulationCountFX extends Application {
	public static ObservableList<SelfOrgRow> data = FXCollections.observableArrayList();
	public static TableView<SelfOrgRow> tableView;
	public static SimpleStringProperty[] tempProperty;
	public static Vector<Integer> visibleElementsIDList;
	private Button btnStartStop, btnShowSO, btnRootChart;
	private HistoryOfSelforganizations History = new HistoryOfSelforganizations();
	
	public static int t = 1; 	
	private Timer timer;
	public static boolean stopsimulation = false;
	
	public SimulationCountFX(Vector<Integer> _visibleElements)
	{
		History.init();
		timer = new Timer();
		visibleElementsIDList = _visibleElements;
		t = ItselfOrganizationVariables.t;
		ItselfOrganizationVariables.getInstance().modelChanged(null);
		//TODO cleanup
		//ItselfOrganizationVariables.isFirstParamOrg = true;
		Main.simulationBegin();
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("�������������");
		
    	VBox root = new VBox();
    	root.setSpacing(10);
    	root.setPadding(new Insets(10, 10, 10, 10));
    	data.clear();
    	tableView = new TableView<SelfOrgRow>();
    	tableView.getItems().clear();
    	
    	// ������������ �������
    	ObservableList<String> list = FXCollections.observableArrayList();
    	list.add("�����");
		for(int i = 0; i < visibleElementsIDList.size(); i++)
			list.add(TreeOperations.findElementInStructByID(visibleElementsIDList.get(i)).elementName);
    	list.add("��� ��");
    	

    	// ���������� �������
    	for(int i = 0; i < list.size(); i++)
    	{
    		TableColumn<SelfOrgRow, String> column = new TableColumn<SelfOrgRow, String>(list.get(i));
    		//column.setMinWidth(160);
    		final int j = i;
    		column.setCellValueFactory(new Callback<CellDataFeatures<SelfOrgRow, String>, ObservableValue<String>>() {
    			public ObservableValue<String> call(CellDataFeatures<SelfOrgRow, String> p) {
    		         return new SimpleStringProperty(p.getValue().GetPropertyValue(j));
    		     }
    		  });
    		tableView.getColumns().add(column);            		
    	}
    	
    	HBox hBoxButton = new HBox();
    	hBoxButton.setSpacing(5);
    	btnStartStop = new Button("���������� �������������");
    	btnStartStop.setMinWidth(230);
    	btnShowSO = new Button("�������� ��� ���������������");
    	btnRootChart = new Button("������ �������� � �����");
    	btnShowSO.setDisable(true);
		btnRootChart.setDisable(true);

		btnStartStop.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(javafx.scene.input.MouseEvent arg0) {
				if(btnShowSO.isDisable())
				{
					stopsimulation = true;
					btnStartStop.setText("���������� �������������");
					btnShowSO.setDisable(false);
					btnRootChart.setDisable(false);
					ItselfOrganizationVariables.getInstance().modelChanged(null);
					//TODO cleanup
					//ItselfOrganizationVariables.isFirstParamOrg = true;
					Main.mainFrame.setEnabled(true);
				}
				else
				{
					if( Main.allow_simulation )
					{
						if( Main.count > 1 )
						{
							if( CheckModel.isModelCorrect() )
							{
								stopsimulation = false;
								btnStartStop.setText("���������� �������������");
								btnShowSO.setDisable(true);
								btnRootChart.setDisable(true);
								Main.mainFrame.setEnabled(false);
								timer.schedule(new RemindTask(), 700);
							}
						}
						else 
						{
							FXDialog.showMessageDialog("��������, �� ������������� �� ����� ���� ������������, " +
									"\n��� ��� ��� ������������� ���������� ������� ��� ��������", "������ �������������", Message.ERROR);
						}
					}
					else
					{
						FXDialog.showMessageDialog("��������, �� ������������� �� ����� ���� ������������. ��������� �������:" +
								"\n 1) �� ��������� ������ � ������������" +
								"\n 2) �� ��������� ������ � �����������", "������ �������������", Message.ERROR);
					}
				}
			}
		});
		btnRootChart.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				try {
					(new TGFX()).start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		btnShowSO.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(javafx.scene.input.MouseEvent arg0) {
				try {
					(new AllSoTableFX()).start(new Stage());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
    	});
		TableRowOnClick(tableView);
		
    	hBoxButton.getChildren().addAll(btnStartStop, btnRootChart, btnShowSO);
    	root.getChildren().addAll(tableView, hBoxButton);
        Scene scene = new Scene(new Group());
        ((Group) scene.getRoot()).getChildren().addAll(root);	
        scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));     
        stage.setScene(scene);
        stage.show();
        stage.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>(){
			@Override
			public void handle(WindowEvent arg0) {
				Main.simulationEnd();
				stopsimulation = true;
				timer.cancel();
			}
        });
        timer.schedule(new RemindTask(), 700);
	}
	
	public void TableRowOnClick(final TableView<SelfOrgRow> table)
	{
		table.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			@Override
			public void handle(MouseEvent arg0) {
				if(table.getSelectionModel().getSelectedIndex() != -1)
				{
					table.getSelectionModel().getSelectedItem();
					try {
						(new AllSoTableFX(table.getSelectionModel().getSelectedIndex())).start(new Stage());
					} catch (Exception e) {
						e.printStackTrace();
					}
					table.getSelectionModel().clearSelection();
				}
			}
			
		});
	}
	
	class RemindTask extends TimerTask {
	    public void run() {
			if(Main.allow_simulation)
			{
				SelfOrgRow temp = new SelfOrgRow(visibleElementsIDList.size() + 2);
				tempProperty = new SimpleStringProperty[visibleElementsIDList.size() + 2];
				tempProperty[0] = new SimpleStringProperty(Integer.toString(t));
				for(int i = 1; i < visibleElementsIDList.size() + 2; i++)
					tempProperty[i] = new SimpleStringProperty("");
				new SimulationStep();
				temp.Set(tempProperty);
				data.add(temp);
				tableView.setItems(data);
				t++;
				
				if(!stopsimulation) {
					timer.schedule(new RemindTask(), 700);
				} else {
					Main.mainFrame.setEnabled(true);
				}
				
			}	      
	    }
	  }
	
	public class SelfOrgRow
	{
		private int Length;
		private SimpleStringProperty[] str;
		
		public SelfOrgRow(int _length)
		{
			this.Length = _length;
			str = new SimpleStringProperty[Length];
		}
		
		public void Set(SimpleStringProperty[] _str)
		{
			for(int i = 0; i < Length; i++)
				this.str[i] = _str[i];
		}
		
		public String GetPropertyValue(int i){
			return this.str[i].get();
		}
		
		public void SetPropertyValue(int i, String str){
			this.str[i].set(str);
		}		
	}

}
