package simulation;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.stage.Stage;
import simulation.ItselfOrganizationVariables;
import simulation.SimulationCountFX;

public class TGFX extends Application{

	@Override
	public void start(final Stage stage) throws Exception {
		stage.setTitle("��������� ���������������");
        final NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("�����");
        final NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("�����������");
        final LineChart<Number,Number> lineChart = new LineChart<Number,Number>(xAxis,yAxis);       
        Scene scene  = new Scene(lineChart,800,500);
        lineChart.setData(CreateDataForChart());

        scene.setOnScroll(new EventHandler<ScrollEvent>() {
			@Override
			public void handle(ScrollEvent event) {
				if(event.getDeltaY() > 0){
					lineChart.setScaleX(lineChart.getScaleX() + 0.3);
					lineChart.setScaleY(lineChart.getScaleY() + 0.3);
		        }
				else
				{
					lineChart.setScaleX(lineChart.getScaleX() - 0.3);
					lineChart.setScaleY(lineChart.getScaleY() - 0.3);
		            
				}
			}
        });
        
        final Delta delta = new Delta();
        scene.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                if (me.getButton() != MouseButton.MIDDLE) {
                	delta.x = me.getSceneX();
                	delta.y = me.getSceneY();
                }
            }
        });

        scene.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent me) {
                if (me.getButton() != MouseButton.MIDDLE) {
                	lineChart.getScene().getWindow().setX(me.getScreenX() - delta.x);
                	lineChart.getScene().getWindow().setY(me.getScreenY() - delta.y);
                }
            }
        });
        
        stage.setScene(scene);
        stage.show();
	}

	@SuppressWarnings("unchecked")
	private ObservableList<XYChart.Series<Number, Number>> CreateDataForChart()
    {
    	ObservableList<XYChart.Series<Number, Number>> seriesList = FXCollections.observableArrayList();
    	XYChart.Series<Number, Number> series1 = new XYChart.Series<Number, Number>();
    	series1.setName("���");
    	XYChart.Series<Number, Number> series2 = new XYChart.Series<Number, Number>();
    	series2.setName(SimulationCountFX.tableView.getColumns().get(1).getText());
    	XYChart.Series<Number, Number> series3 = new XYChart.Series<Number, Number>();
    	series3.setName("Lim");
    	XYChart.Series<Number, Number> series4 = new XYChart.Series<Number, Number>();
    	series4.setName("���");
    	XYChart.Series<Number, Number> series5 = new XYChart.Series<Number, Number>();
    	series5.setName("���");
    	XYChart.Series<Number, Number> series6 = new XYChart.Series<Number, Number>();
    	series6.setName("���");
    	XYChart.Series<Number, Number> series7 = new XYChart.Series<Number, Number>();
    	series7.setName("���");
    	
    	Double t = 0.0;
        Double max = 0.0;
      	Double dbl = 0.0;
        for (int i = 1; i < SimulationCountFX.tableView.getItems().size(); i++) {

        	t+=1.0;
        	dbl = new Double(SimulationCountFX.tableView.getItems().get(i).GetPropertyValue(1).toString());
        	if (max < dbl){
        		max = dbl;
        	}
        	series2.getData().add(new XYChart.Data<Number, Number>(t, dbl)); 
        	int ind = ItselfOrganizationVariables.History.indexOf(i);
        	if (ind < 0)
        		continue;
        	if (ItselfOrganizationVariables.History.getSOType(ind) == 2){ 
        		series1.getData().add(new XYChart.Data<Number, Number>(t,dbl));  
        	} else if (ItselfOrganizationVariables.History.getSOType(ind) == 1){ 
        		series4.getData().add(new XYChart.Data<Number, Number>(t,dbl)); 
        	} else if (ItselfOrganizationVariables.History.getSOType(ind) == 4){ 
        		series6.getData().add(new XYChart.Data<Number, Number>(t,dbl));  
        	} else if (ItselfOrganizationVariables.History.getSOType(ind) == 3){ 
        		series5.getData().add(new XYChart.Data<Number, Number>(t,dbl));  
        	} else if (ItselfOrganizationVariables.History.getSOType(ind) == 5){ 
        		series7.getData().add(new XYChart.Data<Number, Number>(t,dbl));  
        	}

        } 
        Double MinLim = ItselfOrganizationVariables.rootMinimumCount;
        series3.getData().add(new XYChart.Data<Number, Number>(1.0, MinLim));
        series3.getData().add(new XYChart.Data<Number, Number>(t, MinLim));
        seriesList.addAll(series3, series2, series1, series4, series5, series6, series7);
        
    	return seriesList;
    }
	
	public class Delta
	{
		public double x;
		public double y;
	}
}
