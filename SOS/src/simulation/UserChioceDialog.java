package simulation;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import object.TreeElement;

import com.jidesoft.dialog.ButtonPanel;
import com.jidesoft.dialog.StandardDialog;
import com.jidesoft.swing.JideTitledBorder;
import com.jidesoft.swing.PartialEtchedBorder;
import com.jidesoft.swing.PartialSide;

import Main.Main;
import Main.TreeOperations;
import Main.TypeLoader;
import simulation.ItselfOrganizationVariables;
import simulation.SelectUniformParamValue;
import Main.FunctionsLoader;

public class UserChioceDialog extends StandardDialog
{
	
	protected JButton _okButton = new JButton("OK");
	protected JButton _cancelButton = new JButton("Cancel");
	
	private int typeOfAction;
	
	private ButtonGroup choiceGroup;
	
	private JRadioButton first_rb,
	second_rb,
	third_rb,
	fourth_rb;//,
//	five_rb;//,
//	six_rb;
	
	private int height = 200, width = 650;
	
	public static boolean childDialogWasCanceled = false;
	
	ArrayList<TreeElement> elemList;
	ArrayList<String> parametersList;
	
	//typeOfProblem == 0 => price of current model
	//is more then maximum price
	//
	//typeOfProblem == 1 => in current model exists 
	//intermediate elements with intermediate types,
	//witch did not participate in itself - organization
	//
	//typeOfProblem == 2 => in current model exists lists 
	//with elementary types, 
	//witch did not participate in itself - organization
	//
	//typeOfProblem == 3 => in current model exists lists 
	//with elementary functions, 
	//witch did not participate in itself - organization
	//
	//typeOfProblem == 4 => in current model exists parameters 
	//with different values in different elements 
	public UserChioceDialog( ArrayList<TreeElement> elemForAdditionalSettingsList, int typeOfProblem, ArrayList<String> paramList)
	{
		super( (Frame) null, "����� ��������", true);
		
		elemList = elemForAdditionalSettingsList;
		parametersList = paramList;
		typeOfAction = typeOfProblem;
		
		addCancelAction();
		
		addWindowListener(new WindowAdapter() 
		{
		    public void windowClosing(WindowEvent e) 
		    {
		    	ItselfOrganizationVariables.dataNOTValidForSimulation = true;
		    	setDialogResult(RESULT_CANCELLED);
		    	setVisible(false);
		    	dispose();
		   }
		});
		
		setToCenter();
		
		setVisible(true);
	}
	
	public JComponent createBannerPanel() 
	{
		return null;
	}

	public ButtonPanel createButtonPanel()
	{
		ButtonPanel okCancelPanel = new ButtonPanel();
		_okButton.setName(OK);
		_cancelButton.setName(CANCEL);
		okCancelPanel.addButton(_okButton, ButtonPanel.AFFIRMATIVE_BUTTON);
		okCancelPanel.addButton(_cancelButton, ButtonPanel.CANCEL_BUTTON);
		
		_okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				okActionPerformed(e);
			}
		});
		_cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				cancelActionPerformed(e);
			}
		});
		setDefaultCancelAction(_cancelButton.getAction());
		setDefaultAction(_okButton.getAction());
		getRootPane().setDefaultButton(_okButton);
		okCancelPanel.setBorder( createButtonPanelBorder() );
		return okCancelPanel;
	}
	
	public static Border createButtonPanelBorder()
	{
		return BorderFactory.createCompoundBorder(new JideTitledBorder(
				new PartialEtchedBorder(PartialEtchedBorder.LOWERED,
						PartialSide.NORTH), ""), BorderFactory
				.createEmptyBorder(9, 6, 9, 6));
	}
	
	public JComponent createContentPanel() 
	{
		JPanel mainContainer = new JPanel();
		mainContainer.setLayout( new BorderLayout() );
		
		String problemInform = "",
		secondAction = "",
		thirdAction = "",
		fourthAction = "";//,
//		fiveAction = "",
//		sixAction = "";
//		problemInform1 = "";
		
		if( typeOfAction == 0 )
		{
			problemInform = "  ��������� ��������� ������� " +
					"������ ��������� ����������� ����������";
			secondAction = "������������ �������� ������������ ��������� (��������! ��� ���� ��������� ���������)";
		}
		else if( typeOfAction == 1 )
		{
			problemInform = "  � ������� ������ ������������ " +
					"�������� - ���������� � ������ �� ������������ � ���������������";
			secondAction = "��������� ����� ����� � ��������� - ����������� (��������! ��� ���� ��������� ���������)";
			thirdAction = "������� ���� ������������� � ��������������� (��������! ��� ���� ��������� ����������)";
			fourthAction = "�� ��������� �������� '���������� � ���������������' ��� ����� (��������! ��� ���� ��������� ���������)";
//			fiveAction = "�������� ���� � ���������";
//			sixAction = "�������� ������ ����, �� � ������ ��������������� ���������������� ������ - ��������";
		}
		else if( typeOfAction == 2 )
		{
			problemInform = "  � ������� ������ ������������ " +
					"�������� - ����� � ������ �� ������������ � ���������������";
			secondAction = "��������� ����� ����� � ��������� - ������ (��������! ��� ���� ��������� ���������)";
			thirdAction = "������� ���� ������������� � ��������������� (��������! ��� ���� ��������� ����������)";
			fourthAction = "�� ��������� �������� '���������� � ���������������' ��� ����� (��������! ��� ���� ��������� ���������)";
//			fiveAction = "�������� ���� � ���������";
//			sixAction = "�������� ������ ����, �� � ������ ��������������� ���������������� ������ - ��������";
		}
		else if( typeOfAction == 3 )
		{
			problemInform = "  � ������� ������ ������������ " +
					"�������� - ����� � ��������� �� ������������ � ���������������";
			secondAction = "��������� ����� ������� � ��������� - ������ (��������! ��� ���� ��������� ���������)";
			thirdAction = "������� ������� ������������� � ��������������� (��������! ��� ���� ��������� ����������)";
			fourthAction = "�� ��������� �������� '���������� � ���������������' ��� ������� (��������! ��� ���� ��������� ���������)";
//			fiveAction = "�������� ������� � ���������";
//			sixAction = "�������� ������ �������, �� � ������ ��������������� ���������������� ������ - ��������";
		}
		else if( typeOfAction == 4 )
		{
			problemInform = "  � ������� ������ ������������ " +
					"�������� - ����� � ���������� ���������� ����� � ��� �� ����������";
			secondAction = "��������� ��������� �������� ��� ���������� � ��������� ��������� (��������! ��� ���� ��������� ���������)";
			thirdAction = "���������� �������� ��������� �� ���� ��������� ����������";
		}
		
		JPanel firstPanel = new JPanel();
		firstPanel.setLayout( new BorderLayout() );
		firstPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
		first_rb = new JRadioButton("��������� � ������� ������ � �������� �");
		firstPanel.add( first_rb, BorderLayout.WEST );
		
		JPanel secondPanel = new JPanel();
		secondPanel.setLayout( new BorderLayout() );
		secondPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
		second_rb = new JRadioButton( secondAction );
		secondPanel.add( second_rb, BorderLayout.WEST );
		
		JPanel lev2_secondPanel = new JPanel();	
		lev2_secondPanel.setLayout( new BorderLayout() );
		lev2_secondPanel.add( firstPanel, BorderLayout.NORTH );
//		lev2_secondPanel.add( secondPanel, BorderLayout.CENTER );
//		lev2_secondPanel.add( thirdPanel, BorderLayout.SOUTH );		

		
		JPanel lev2_firstPanel = new JPanel();	
		lev2_firstPanel.setLayout( new BorderLayout() );
		lev2_firstPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
		
		JPanel firstPanel_l1 = new JPanel();
		firstPanel_l1.setLayout( new BorderLayout() );
		firstPanel_l1.add( new JLabel( problemInform ), BorderLayout.WEST );
		
//		JPanel secondPanel_l1 = new JPanel();
//		secondPanel_l1.setLayout( new BorderLayout() );
//		secondPanel_l1.add( new JLabel( problemInform1 ), BorderLayout.WEST );
	
		lev2_firstPanel.add( firstPanel_l1, BorderLayout.CENTER );
//		lev2_firstPanel.add( secondPanel_l1, BorderLayout.SOUTH );
		
		lev2_secondPanel.add( firstPanel, BorderLayout.NORTH );
		lev2_secondPanel.add( secondPanel, BorderLayout.CENTER );
		
		choiceGroup = new ButtonGroup();
		choiceGroup.add( first_rb );
		choiceGroup.add( second_rb );
		
		first_rb.setSelected( true );
		
		if( typeOfAction != 0 )
		{
			JPanel thirdPanel = new JPanel();
			thirdPanel.setLayout( new BorderLayout() );
			thirdPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
			third_rb = new JRadioButton( thirdAction );
			thirdPanel.add( third_rb, BorderLayout.WEST );
			choiceGroup.add( third_rb );
			
			JPanel additionalPanel = new JPanel();
			additionalPanel.setLayout( new BorderLayout() );
			
			if( typeOfAction < 4 )
			{
				JPanel fourthPanel = new JPanel();
				fourthPanel.setLayout( new BorderLayout() );
				fourthPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
				fourth_rb = new JRadioButton( fourthAction );
				fourthPanel.add( fourth_rb, BorderLayout.WEST );
				choiceGroup.add( fourth_rb );
				
//				JPanel fivePanel = new JPanel();
//				fivePanel.setLayout( new BorderLayout() );
//				fivePanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
//				five_rb = new JRadioButton( fiveAction );
//				fivePanel.add( five_rb, BorderLayout.WEST );
//				choiceGroup.add( five_rb );
				
//				JPanel sixPanel = new JPanel();
//				sixPanel.setLayout( new BorderLayout() );
//				sixPanel.add( new JLabel("                                                                                         "), BorderLayout.NORTH );
//				six_rb = new JRadioButton( sixAction );
//				sixPanel.add( six_rb, BorderLayout.WEST );
//				choiceGroup.add( six_rb );
				
				JPanel dopPanel= new JPanel();
				dopPanel.setLayout( new BorderLayout() );
				dopPanel.add( thirdPanel, BorderLayout.NORTH );
				dopPanel.add( fourthPanel, BorderLayout.CENTER );
				dopPanel.add( new JLabel("                                                                                         "), BorderLayout.SOUTH );
//				dopPanel.add( fivePanel, BorderLayout.SOUTH );
				
				additionalPanel.add( dopPanel, BorderLayout.NORTH );
//				additionalPanel.add( sixPanel, BorderLayout.SOUTH );
			}
			else
			{
				additionalPanel.add( thirdPanel, BorderLayout.NORTH );
				additionalPanel.add( new JLabel("                                                                                         "), BorderLayout.SOUTH );
//				additionalPanel.add( fourthPanel, BorderLayout.SOUTH );
			}
			lev2_secondPanel.add( additionalPanel, BorderLayout.SOUTH );
		}
		
		
		mainContainer.add( lev2_firstPanel, BorderLayout.NORTH );
		mainContainer.add( lev2_secondPanel, BorderLayout.SOUTH );
		
		if( typeOfAction != 0 )
		{
			width = 800;
			if( typeOfAction < 4 )
			{
				height = 310;
			}
			else
			{
				height = 270;
			}
		}
		
		setSize( width, height );
		setResizable(false);
		
		return mainContainer;
	}
	
	public static Border createContentPanelBorder()
	{
		return BorderFactory.createEmptyBorder(10, 10, 0, 10);
	}
	
	
	
	
	protected void okActionPerformed(ActionEvent e)
	{
		boolean exit = true;
		if( first_rb.isSelected() )
		{
			ItselfOrganizationVariables.dataNOTValidForSimulation = true;
		}
		else if( second_rb.isSelected() )
		{
			if( typeOfAction == 0 )
			{
				ItselfOrganizationVariables.useMaximumPrice = false;
			}
			else if( typeOfAction == 1 )
			{
				ItselfOrganizationVariables.typeIntermediateOrgAllowed = false;
			}
			else if( typeOfAction == 2 )
			{
				ItselfOrganizationVariables.typeLeafOrgAllowed = false;
			}
			else if( typeOfAction == 3 )
			{
				ItselfOrganizationVariables.funcLeafOrgAllowed = false;
			}
			else if( typeOfAction == 4 )
			{
				Main.setSameParametrsInParamOrg( false );
			}
		}
		else if( third_rb.isSelected() )
		{
			if( (typeOfAction == 1) || (typeOfAction == 2) )
			{
				String typeName;
				ArrayList<String> typeList = new ArrayList<String>();
				for( int i=0; i < elemList.size(); i++)
				{
					typeName = elemList.get( i ).typeName;
					if( ! typeList.contains( typeName ) )
					{
						typeList.add( typeName );
					}
				}
				
				TypeLoader.setParticipateForTypeList( typeList, true );
			}
			else if(  typeOfAction == 3 )
			{
				String funcName;
				ArrayList<String> funcList = new ArrayList<String>();
				for( int i=0; i < elemList.size(); i++)
				{
					funcName = elemList.get( i ).functionName;
					if( ! funcList.contains( funcName ) )
					{
						funcList.add( funcName );
					}
				}
				
				FunctionsLoader.setParticipateForFuncList( funcList, true );
			}
			else if(  typeOfAction == 4 )
			{
				childDialogWasCanceled = false;
				new SelectUniformParamValue( parametersList );
				if( childDialogWasCanceled )
				{
					exit = false;
				}
			}
		}
		else if( fourth_rb.isSelected() )
		{
			if( (typeOfAction == 1) || (typeOfAction == 2) )
			{
				ItselfOrganizationVariables.useParticipationInTypeOrg = false;
			}
			else if(  typeOfAction == 3 )
			{
				ItselfOrganizationVariables.useParticipationInFuncOrg = false;
			}
		}
//		else if( five_rb.isSelected() )
//		{
//			if( typeOfAction == 1 )
//			{
//				String typeName;
//				int maxNeedSlotsNumber = 0,
//				slotsNeedNumberForCurrentType;//,
////				maxSlotsNumberForParticipationTypes = 0;
//				ArrayList<String> typeList = new ArrayList<String>();
//				ArrayList<Integer> typeSlotsMinNumberList = new ArrayList<Integer>();
//				ArrayList<String> oldFuncList = new ArrayList<String>();
//				//define necessary slots number for each type
//				//and max of this values
//				for( int i=0; i < elemList.size(); i++)
//				{
//					typeName = elemList.get( i ).typeName;
//					if( ! typeList.contains( typeName ) )
//					{
//						typeList.add( typeName );
//						slotsNeedNumberForCurrentType = TreeOperations.findMaxUsedSlotsForType(typeName);
//						typeSlotsMinNumberList.add( slotsNeedNumberForCurrentType );
//						if( slotsNeedNumberForCurrentType > maxNeedSlotsNumber )
//						{
//							maxNeedSlotsNumber = slotsNeedNumberForCurrentType;
//						}
//					}
//				}
//				
//				//checking: did exist participate type with
//				//enough slots number
//				if( TypeLoader.isExistParticipateTypeWithSlotsNumberMoreThen( maxNeedSlotsNumber ) )
//				{
//					childDialogWasCanceled = false;
//					new ChooseNewTypes( typeList, typeSlotsMinNumberList );
//					if( childDialogWasCanceled )
//					{
//						exit = false;
//					}
//				}
//				else
//				{
//					JOptionPane.showMessageDialog(null, "� ���������, � ���������� ����� ��� ����� ����������� � " +
//							"\n��������������� � ���������� ����������� ����������� ������");
//					exit = false;
//				}
//			}
//		}
//		else if( six_rb.isSelected() )
//		{
//			
//		}
		
		
		if( exit )
		{
			setDialogResult(RESULT_AFFIRMED);
			setVisible(false);
			dispose();
		}
		
	}
	
//	protected boolean modelIsCorrect()
//	{
//		return true;
//	}
	
	protected void addCancelAction()
	{
		Action action = new AbstractAction() 
		{
			public void actionPerformed(ActionEvent ae)
			{
				cancelActionPerformed(ae);
			}
		};

		KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		rootPane.getActionMap().put(action, action);
		rootPane.getInputMap(JComponent.WHEN_FOCUSED).put(stroke, action);
		rootPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(stroke, action);

	}
	
	protected void cancelActionPerformed(ActionEvent e)
	{
		ItselfOrganizationVariables.dataNOTValidForSimulation = true;
		setDialogResult(RESULT_CANCELLED);
		setVisible(false);
		dispose();
	}
	
	public void setToCenter() 
	{
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		screenSize.height = screenSize.height/2;
		screenSize.width = screenSize.width/2;
		int y = screenSize.height - height/2;
		int x = screenSize.width - width/2;
		this.setLocation( x, y );
	}
	
	
}
