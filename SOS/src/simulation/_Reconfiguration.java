package simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import object.TreeElement;

import Main.Main;
import Main.TmpRemoveForList;
import Main.TypeLoader;
import simulation.ItselfOrganizationVariables;

public class _Reconfiguration 
{
	//static leafs - leafs witch types can not be changed
	
	
	ArrayList<TreeElement> leafsList;
	ArrayList<TreeElement> notParticipateLeafsList;
	
	//this list contains convolution functions in decreasing order
	//of main characteristics of best leaf values for this
	//functions ( for participate in itself organization leafs types)
	ArrayList<String> convolutionFuncPriorForLeafs = new ArrayList<String>();
	
	//average slots number for intermediate elements with children
	//are leafs ( minSlotsNumber = "number of intermediate elements 
	//with children are leafs" / "number of leafs" )
	int minSlotsNumber = 2;
	
	//minimum parent level from that lists can be add to it
	//( defined by
	//level of upper element in model witch has child leaf element )
	int minLeafParentLevel = 0;
	
	//each element at this list contains list of convolutions functions
	//in decreasing order defined by value of corresponding ( its 
	//index in notParticipateLeafsList) leaf value with each 
	//convolution function
	ArrayList< ArrayList<String> > convFuncsPriorityForStaticLeafs = new ArrayList<ArrayList<String>>(); 
	
	int parentListSize;
	ArrayList<TreeElement> leafsParentsList;
	
	//list of elements witch can have children leafs but they
	//type can not be change
	ArrayList<TreeElement> typeStatLeafsParentsList;
	
	//list of elements witch can have children leafs but they
	//type and function can not be change
	ArrayList<TreeElement> fullStatLeafsParentsList;
	
	// ...CurParentInd - index of intermediate in leafsParentsList with selected
	//corresponding convolution function with some free slots( if index 
	//equals -1 it means that not selected so elements)
	int sumCurParentInd,
	sumMultCurParentInd,
	sumPowCurParentInd;
	//...CurParentFreeSlots - number of free slots in corresponding intermediate
	//element ( element witch index is pointed in  ...CurParentInd of 
	//corresponding convolution function)
	int sumCurParentFreeSlots,
	sumMultCurParentFreeSlots,
	sumPowCurParentFreeSlots;
	
	
	public _Reconfiguration( boolean goFromDownToUp )
	{
		
		if( ItselfOrganizationVariables.leafsList.size() <= 0 )
		{
			return;
		}
		
		ArrayList<Integer> leafsParentsIDList;
		
		leafsParentsList = new ArrayList<TreeElement>();
		
		convolutionFuncPriorForLeafs.clear();
		convolutionFuncPriorForLeafs.addAll( 
				ItselfOrganizationVariables.convolutionFuncPriorForLeafs );
		
		TreeElement currentElem;
		int currentID;
		
		Main.panelGraphic.getGraph().clearSelection();
		
		leafsList = new ArrayList<TreeElement>();
		leafsParentsIDList = new ArrayList<Integer>();
		notParticipateLeafsList = new ArrayList<TreeElement>();
//filling leafsParentsIDList
		leafsList.addAll( ItselfOrganizationVariables.leafsList );
		for(int i=0; i < leafsList.size(); i++)
		{
			currentID = leafsList.get(i).ParentID;
			if( ! leafsParentsIDList.contains( currentID ) )
				//excluding duplicate values
			{
				leafsParentsIDList.add( currentID );
			}
		}
		leafsList.clear();


//filling leafsParentsList by the order of decreasing of its level
//and put where all elements witch has no children(they will be 
//checked after)
		minLeafParentLevel = 0;
		boolean anyElemAtLevelHasLeafChild;
		for(int level = ( Main.structTreeElements.size() - 1 ) 
				; level >= 0; level--)
		{
			anyElemAtLevelHasLeafChild = false;
			
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
			{
				currentElem = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
				currentID = currentElem.ID;
				if( leafsParentsIDList.contains( currentID ) )
				{
					leafsParentsList.add( currentElem );
					anyElemAtLevelHasLeafChild = true;
				}
				if( currentElem.type == 2 &&
						currentElem.iChildrenCount == 0 )
				{
					if( ItselfOrganizationVariables.
							typeIntermediateOrgAllowed 
							&& ( ItselfOrganizationVariables.interTypesMayHaveLeafChild.size() > 0 ) )
						//if allowed type organization for intermediate elements and
						//exists participate in itself organization intermediate types
						//witch can have leaf children
					{
						if( TypeLoader.isParticipate
								(currentElem.typeName) || 
								(! ItselfOrganizationVariables.
								useParticipationInTypeOrg) )
						{
							leafsParentsList.add( currentElem );
						}
					}
					else if( 
							( TypeLoader.
							getChildrenType(currentElem.typeName) == 1 ) 
							|| ( TypeLoader.
									getChildrenType(currentElem.typeName) == 3 ) )
					{
						leafsParentsList.add( currentElem );
					}
					
				}
			}
			
			if( ! anyElemAtLevelHasLeafChild )
			{
				if( minLeafParentLevel == level )
				{
					minLeafParentLevel++;
				}
			}
			
		}
		
//remove from leafsParentsList elements witch level less then
//minLeafParentLevel
		for( int i=0; i < leafsParentsList.size() ; i++ )
		{
			currentElem = leafsParentsList.get( i );
			if( currentElem.Level < minLeafParentLevel )
			{
				leafsParentsList.remove( i );
			}
		}
		

//1:		
//delete all leafs elements from model
//2:
//separate lists to leafsList(leafs witch types participate in itself
//organization) and notParticipateLeafsList(leafs witch types NOT 
//participate in itself organization)
//3:
//define for all static leafs priority of convolution functions
//and put it to convFuncsPriorityForStaticLeafs
		//2:
		if( ItselfOrganizationVariables.typeLeafOrgAllowed )
			//if type organization for leafs is allowed
		{
			leafsList.addAll( ItselfOrganizationVariables.leafsList );
			
			
			for(int i=0; i < leafsList.size(); i++)
			{
				currentElem = leafsList.get(i);
				if( ! TypeLoader.isParticipate( currentElem.typeName ) )
				{
					notParticipateLeafsList.add( currentElem );
					
					//3:
					convFuncsPriorityForStaticLeafs.add( 
							defineConvolutionPriorityForLeaf( 
									currentElem.ID,
									currentElem.typeName, 
									currentElem.elementCount ) );
//------------------------------------------------------------- added "--" operator -------					
					leafsList.remove( i-- );
				}
				
				//1:
				new TmpRemoveForList( currentElem );
				
			}
		}
		else
			//if type organization for leafs not allowed
		{
			notParticipateLeafsList.addAll( ItselfOrganizationVariables.leafsList );
			
			
			for(int i=0; i < notParticipateLeafsList.size(); i++)
			{
				currentElem = notParticipateLeafsList.get(i);
				
				//3:
				convFuncsPriorityForStaticLeafs.add( 
						defineConvolutionPriorityForLeaf( 
								currentElem.ID,
								currentElem.typeName, 
								currentElem.elementCount ) );
				
				//1:
				new TmpRemoveForList( currentElem );
				
			}
		}		
		
		
				
//filling typeStatLeafsParentsList
		typeStatLeafsParentsList = new ArrayList<TreeElement>();
		if( ( ! ItselfOrganizationVariables.typeLeafOrgAllowed ) || 
				( ItselfOrganizationVariables.interTypesMayHaveLeafChild.size() < 2 ) )
		{
			typeStatLeafsParentsList.addAll( leafsParentsList );
//			leafsParentsList.clear();
		}
		else
		{
			for( int i=0; i < leafsParentsList.size() ; i++ )
			{
				currentElem = leafsParentsList.get( i );
				if( currentElem.iChildrenCount > 0 )
				{
					typeStatLeafsParentsList.add( currentElem );
//					leafsParentsList.remove( currentElem );
				}
				else if( ! TypeLoader.isParticipate( currentElem.typeName ) )
				{
					typeStatLeafsParentsList.add( currentElem );
//					leafsParentsList.remove( currentElem );
				}
			}
		}
		
//filling 	fullStatLeafsParentsList	
		fullStatLeafsParentsList = new ArrayList<TreeElement>();
		if( (convolutionFuncPriorForLeafs.size() < 2) ||
				( ! ItselfOrganizationVariables.
						funcIntermediateOrgAllowed ) )
		{
			fullStatLeafsParentsList.addAll( typeStatLeafsParentsList );
//			typeStatLeafsParentsList.clear();
		}
		else
		{
			for( int i=0; i < typeStatLeafsParentsList.size() ; i++ )
			{
				currentElem = typeStatLeafsParentsList.get( i );
				if( currentElem.iChildrenCount > 0 )
				{
					fullStatLeafsParentsList.add( currentElem );
//					typeStatLeafsParentsList.remove( currentElem );
				}
				else if( TypeLoader.getAllIntermediateFunctions
						( currentElem.typeName ).size() < 2 )
				{
					fullStatLeafsParentsList.add( currentElem );
//					typeStatLeafsParentsList.remove( currentElem );
				}
			}

		}
				
		
		parentListSize = leafsParentsList.size();
		
		
		
//----------------------------------------------------------- WTF? dividing x/x. Is it sometimes not 1?		
		int totalListsCount = notParticipateLeafsList.size() + 
		leafsList.size();
		minSlotsNumber = ( notParticipateLeafsList.size() + 
				leafsList.size() ) / totalListsCount;
		
		if( goFromDownToUp )
		{
			goFromDownToUp();
		}
		else
		{
			goDownFromRoot();
		}
		
//set for all intermediate elements optimal type and function	
//		
//		if( Main.count == 1 )
//			//if after deleting all leafs in system 
//			//stay only root
//		{
//			if( ( notParticipateLeafsList.size() > 0 ) 
//					&& ( leafsList.size() > 0 ) )
//			{
//				
//			}
//			else
//			{
//				if( leafsList.size() > 0 )
//					//if in system was only participate in
//					//itself organization leafs
//				{
//					
//				}
//				else
//					//if in system was only NOT participate
//					// in itself organization leafs
//				{
//					
//				}
//			}
//		}
//		else
//		{
//			//if after deleting all leafs in system 
//			//stay NOT only root
//			goDownFromRoot();
//		}
		
		
		
	}
	
	private void goFromDownToUp() 
	{
		if( ItselfOrganizationVariables.typeIntermediateOrgAllowed )
		{
			for( int statInd = 0; 
			 statInd < notParticipateLeafsList.size() ; statInd++ )
			{
				
			}
		}
		else
		{
//			jkyt
		}
	}

	//sort list of convolution functions in decreasing order
	//of leaf values for each
	//convolution function
	private ArrayList<String> defineConvolutionPriorityForLeaf(
			int id, 
			String typeName,
			double value) 
	{
		ArrayList<String> convFuncList = new ArrayList<String>();
		
		double 
		parentCountSum = value;
		double parentCountSumMult = value * 
		TypeLoader.getFactorForSumMultFunction(typeName); 
		double parentCountSumPow = Math.pow( value, 
				TypeLoader.getFactorForSumMultFunction(typeName) );
		
		if( ItselfOrganizationVariables.
				interTypesContainsSumFunc.size() < 1 )
		{
			parentCountSum = Double.NEGATIVE_INFINITY;
		}
		if( ItselfOrganizationVariables.
				interTypesContainsSumMultFunc.size() < 1 )
		{
			parentCountSumMult = Double.NEGATIVE_INFINITY;
		}
		if( ItselfOrganizationVariables.
				interTypesContainsSumPowFunc.size() < 1 )
		{
			parentCountSumPow = Double.NEGATIVE_INFINITY;
		}
		
		//choosing sequence in witch we will trying functions
		if( ( parentCountSum >= parentCountSumMult) 
				&& ( parentCountSum >= parentCountSumPow ) )
		{
			convFuncList.add("sum(Xi)");
			if( parentCountSumMult >= parentCountSumPow )
			{
				convFuncList.add("sum(Xi*Ki)");
				convFuncList.add("sum(Xi^Ki)");
			}
			else
			{
				convFuncList.add("sum(Xi^Ki)");
				convFuncList.add("sum(Xi*Ki)");
			}
		}
		else
		{
			if( parentCountSumMult >= parentCountSumPow )
			{
				convFuncList.add("sum(Xi*Ki)");
				if( parentCountSum >= parentCountSumPow )
				{
					convFuncList.add("sum(Xi)");
					convFuncList.add("sum(Xi^Ki)");
				}
				else
				{
					convFuncList.add("sum(Xi^Ki)");
					convFuncList.add("sum(Xi)");
				}
			}
			else
			{
				convFuncList.add("sum(Xi^Ki)");
				if( parentCountSum >= parentCountSumMult )
				{
					convFuncList.add("sum(Xi)");
					convFuncList.add("sum(Xi*Ki)");
				}
				else
				{
					convFuncList.add("sum(Xi*Ki)");
					convFuncList.add("sum(Xi)");
				}
			}
		}
		
		return convFuncList;
		
	}//end of defineConvolutionPriorityForList

	//go from root to down and set optimal types for 
	//intermediate elements
	private void goDownFromRoot() 
	{
		TreeElement elementInStruct;
		for(int level=0; level<Main.structTreeElements.size(); level++)
		{
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
			{
				elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
				if( elementInStruct.iChildrenCount > 0 )
					//if current element has intermediate 
					//children (lists was deleted)
				{
					
				}
				else
					//if current element has no children
				{
					
				}
			}
		}
	}
	
	//define best type contains selected function
	//with selected or more slots number
	//and not contradicting for current children type of possible children
	//current children type
	// 0 - no children
	// 1 - only list children
	// 2 - only intermediate children
	// 3 - list and intermediate children
	private String findBestTypeForFunc
	(String function,
			int slotsNumber,
			int childrenType) 
	{
		ArrayList<String> typeList = new ArrayList<String>();
		
		if( function.equals("sum(Xi)") )
		{
			typeList.addAll( 
					ItselfOrganizationVariables.
					interTypesContainsSumFunc );
		}
		else if( function.equals("sum(Xi*Ki)") )
		{
			typeList.addAll( 
					ItselfOrganizationVariables.
					interTypesContainsSumMultFunc );
		}
		if( function.equals("sum(Xi^Ki)") )
		{
			typeList.addAll( 
					ItselfOrganizationVariables.
					interTypesContainsSumPowFunc );
		}
		
		String typeName;
		
		for( int typeInd=0;  typeInd < typeList.size(); typeInd++ )
		{
			typeName = typeList.get( typeInd );
			if( (TypeLoader.getSlotsNumber(typeName) >= slotsNumber) )
			{
				if( ( childrenType == 0 )
						|| (TypeLoader.getChildrenType(typeName) == childrenType) 
						|| (TypeLoader.getChildrenType(typeName) == 3) )
				{
					return typeName;
				}
			}
		}
		
		return null;
	}
	
	//define for parent element best type for current list set(lists
	//types not changed) 
	private void findTypeAndFuncForStaticLists( TreeElement parentElem,
			ArrayList<TreeElement> listsSet)
	{
		String funcName, typeName = null;
		if( ItselfOrganizationVariables.typeIntermediateOrgAllowed )
		{
			for( int funcInd=0;
			funcInd < convolutionFuncPriorForLeafs.size(); 
			funcInd++ )
			{
				funcName = 
					convolutionFuncPriorForLeafs.get(funcInd);
				typeName = findBestTypeForFunc( funcName,
						minSlotsNumber, 
						1//list children must be supported
						);
				if( typeName != null )
				{
//					drthdr
				}
				else
				{
					
				}
			}
		}
		else
		{
//			hoho
		}
	}
	
	
	private int chekingMinSlotsNumber( int slotsNumber )
	{
		
		return 0;
	}
	
	
	
	private void addListsToModel()
	{
//		k;jk;
	}
	
	
	
}
