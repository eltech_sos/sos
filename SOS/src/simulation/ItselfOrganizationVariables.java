package simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

import Main.TreeOperations;
import Main.TypeLoader;
import simulation.HistoryOfSelforganizations;
import simulation.QualityControlVariables;
import object.TreeElement;

import simulation.parameter.IParametersModel;
import simulation.parameter.IParametersOrganizer;
import simulation.parameter.ParametersOrganizer;
import simulation.parameter.ParametersOrganizerFactory;
import simulation.parameter.ParametersOrganizerStrategy;

public class ItselfOrganizationVariables extends Observable implements IParametersModel
{
	public static QualityControlVariables QControl = new QualityControlVariables();
	public static double rootMinimumCount = 80;
	public static double maximumDevialation = 2;
	public static double maximumPrice = 100000;
	public static double priceInflusionOnMainInterChar = 0.5;
	public static ArrayList<TreeElement> leafsList = new ArrayList<TreeElement>();
	public static boolean rootValueNotValid = true;
	//TODO t was changed from 1 by Evgen, may be it must be changed back...
	public static int t = 2;


	public static boolean useMaximumPrice = true;
	
	public static boolean dataNOTValidForSimulation = true;
	
//for parameters itself - organization
	public static boolean paramOrgAllowed = true;
	public static boolean metodRelax = false;
	
	/**
	 * Max count of cycles in parameter organization
	 */
	public static int maxNumerOfCounts = 20;
	//1
	
	
	public static int paramOrgStrategy = 4;//replaced with next line
	/**
	 * Holds current parameters organizer strategy type
	 */	
	private ParametersOrganizerStrategy parametersOrganizerStrategyType = ParametersOrganizerStrategy.SAVE_STEP_VECTOR_CCS;
	
	//TODO cleanup with old parameter organization classes
	public static boolean isFirstParamOrg = true;///
	
	
	public static boolean sameParametrsInParamOrg = false;///replaced with next line
	/**
	 * Holds current parameters organizer type
	 */
	private ParametersOrganizer parametersOrganizerType = ParametersOrganizer.LOCAL_PARAMETERS_ORGANIZER;
	
	
	public static boolean useParticipationInParamOrg = true;///
	public static ArrayList<Double> chengeStepListPrevious = new ArrayList<Double>();//list of the steps for change for each parameter
	public static ArrayList<Double> changeDegreeOfParametrsPrevious = new ArrayList<Double>();
//	public static ArrayList<Double> chengeParamValueList = new ArrayList<Double>();//list of degree of change for all parameters
	
	//parameters values saved in unlimited form
	public static ArrayList<Double> parametrsInitCount = new ArrayList<Double>();//list of parameters values before itself - organization
	
//for functions itself - organization
	public static boolean funcLeafOrgAllowed = true;
	public static boolean funcIntermediateOrgAllowed = true;
	public static boolean useParticipationInFuncOrg = true;
	
	public static ArrayList<String> usedInOrganizationElementaryFunctionsList = new ArrayList<String>();
//to_do
	
//	//list of not participate in itself - organization convolutions functions
//	//witch meeting in model elements
//	public static ArrayList<String> notUsedInOrgInterFunctionsMeetingInModel = new ArrayList<String>();
	//list of possible convolutions functions
	public static ArrayList<String> usedInOrganizationConvolutionFunctionsList = new ArrayList<String>();
//to_do	
	
//for struct itself - organization
	public static boolean typeLeafOrgAllowed = true;
	public static boolean typeIntermediateOrgAllowed = true;
	public static boolean reconfigurationOrgAllowed = true;
	public static boolean addNewElementsOrgAllowed = true;
	
	public static boolean useParticipationInTypeOrg = true;
	
	public static boolean UseIntelligentSOTypeChoosing = false;
	public static boolean UseNeuralSOTypeChoosing = false;
	
	public static HistoryOfSelforganizations History = new HistoryOfSelforganizations();
//?	
	//notUsedInOrgElemTypesMeetingInModel - list of not participate 
	//in itself - organization elementary types
	//witch meeting in model elements with participate functions
	//
	//notUsedInOrgElemTypesMaxFuncList - list of
	//functions name for each type for its max
	public static ArrayList<String> notUsedInOrgElemTypesMeetingInModel = new ArrayList<String>();
	public static ArrayList<String> notUsedInOrgElemTypesMaxFuncList = new ArrayList<String>();
	public static ArrayList<Double> notUsedInOrgElemTypesMaxValuesList = new ArrayList<Double>();
	
	//list of not participate in itself - organization intermediately types
	//witch meeting in model elements
	public static ArrayList<String> notUsedInOrgInterTypesMeetingInModel = new ArrayList<String>();
	
	//usedInOrganizationElementaryTypesList - contains
	//names of participate in itself - organization 
	//elementary types
	//
	//typesMaxFuncList  - list of
	//functions name for each type for its max
	//
	//paramMapsForFuncMax  - map
	//key - function name 
	//value - map of parameters(key - parameter name,
	//value - parameter value) at witch function 
	//set to it max 
	//in this map saved ONLY max for functions
	//witch is meets in typesMaxFuncList 
	//and in notUsedInOrgElemFunctionsMeetingInModel
	//and in notUsedInOrgElemTypesMaxFuncList
	//
	//typesMaxValuesList - list of
	//elements values then for them set accordingly type,
	//function and parameters values
	//
	//(order in all lists is same as in
	// usedInOrganizationElementaryTypesList, that is: for
	//type witch name is first in usedInOrganizationElementaryTypesList
	//max value will be then chosed first function in
	//typesMaxFuncList, and value of element with this
	//type and function will be as in first element in
	//typesMaxValuesList ( certainly if in element set
	//parameters values taken from paramMapsForFuncMax by
	//the key equals chosen function) )
	//
	public static ArrayList<String> usedInOrganizationElementaryTypesList = new ArrayList<String>();
	public static ArrayList<String> typesMaxFuncList = new ArrayList<String>();
	public static ArrayList<Double> typesMaxValuesList = new ArrayList<Double>();
	public static Map< String, Map< String, Double >  >  paramMapsForFuncMax = new HashMap< String, Map< String, Double >  >();

	//notUsedInOrgElemFunctionsMeetingInModel - contains:
	//
	//if function organization for lists is allowed:
	//list of not 
	//participate in itself - organization elementary functions
	//but meeting in model elements with participate type
	//
	//if function organization for lists NOT allowed:
	//all functions meeting in model in participate type
	//
	//
	//notParticipateFuncsMaxValuesList - list of
	//elements values then for them set accordingly 
	//function and parameters values ( t.e. element with
	//function "x+y" witch second in 
	//notUsedInOrgElemFunctionsMeetingInModel list, 
	//will be has max value equals second element in 
	//notParticipateFuncsMaxValuesList, when its parameters
	//will be set to the accordingly values from the
	//paramMapsForFuncMax geted by "x+y" key)
	public static ArrayList<String> notUsedInOrgElemFuncMeetingInModelList = new ArrayList<String>();
	public static ArrayList<Double> notUsedInOrgElemFuncMeetingInModelValuesList = new ArrayList<Double>();

	
	//for...BestElementaryChild - best type of elementary 
	//children for ... convolution function
	//for...BestElementaryChildValue - value ONLY FOR COMPORATION
	//(real value must be taken from typesMaxValuesList by the
	//index of for...BestElementaryChild 
	//in usedInOrganizationElementaryTypesList)of best type of
	//elementary children for ... convolution function
	//("for...BestElementaryChildValue" = "value of type" *
	//"factor of this type in selected convolution function")
	//for sum(Xi):
	public static String forSumBestElementaryChild = null; 
	public static double forSumBestElementaryChildValue = 0.0;
	//for sum(Xi*Ki):
	public static String forSumMultElementaryChild = null;
	public static double forSumMultBestElementaryChildValue = 0.0;
	//for sum(Xi^Ki):
	public static String forSumPowBestElementaryChild = null;
	public static double forSumPowBestElementaryChildValue = 0.0;
	
	
	
	
	//key - intermediate type witch participate in itself - 
	//organization
	//value - this type main characteristic
	public static Map< String, Double  >  participateInterTypeMainCharacteristics = new HashMap< String, Double  >();
	
	
	//interTypesContains...Func - contains list of types for
	//witch include ... function, in the order of decrease them
	//general characteristic 
	public static ArrayList<String> interTypesContainsSumFunc = new ArrayList<String>();
	public static ArrayList<String> interTypesContainsSumMultFunc = new ArrayList<String>();
	public static ArrayList<String> interTypesContainsSumPowFunc = new ArrayList<String>();
	
	//this list contains convolution functions in decreasing order
	//of main characteristics of best leaf values for this
	//functions
	public static ArrayList<String> convolutionFuncPriorForLeafs = new ArrayList<String>();

	
//for reconfiguration
	
	//list of participate in itself - organization types witch can
	//have leaf children (they type must be 1 or 3)
	public static ArrayList<String> interTypesMayHaveLeafChild = new ArrayList<String>();
	
	
	
	
//	
//	//in ...BestChildrenValuesList saved list of types values
//	//from max to min(on decrease)
//	//("value in list" = "value of type" *
//	//"factor of this type in selected convolution function")
//	//in ...BestChilderenListList saved list of elementary types names
//	//accordingly they values in ...BestChilderenValuesList(at the
//	//same order)
//	//for sum(Xi):
//	public static ArrayList<String> forSumBestChilderenLeafList = new ArrayList<String>();
//	public static ArrayList<Double> forSumBestChilderenValuesList = new ArrayList<Double>();
//	//for sum(Xi*Ki):
//	public static ArrayList<String> forSumMultBestChilderenLeafList = new ArrayList<String>();
//	public static ArrayList<Double> forSumMultBestChilderenValuesList = new ArrayList<Double>();
//	//for sum(Xi^Ki):
//	public static ArrayList<String> forSumPowBestChilderenLeafList = new ArrayList<String>();
//	public static ArrayList<Double> forSumPowBestChilderenValuesList = new ArrayList<Double>();
//	
//	
	
	
	
	
//	public static boolean rootValueNotValide;
//	public static boolean itsFirstParamOrg=true;
//	public static float minimumRootValue;
//	
//	public static List<Double> chengeStepList = new ArrayList<Double>();//������ ����� ��������� ��� ���������� ���� ������
//	public static List<Float> chengeParamValueList = new ArrayList<Float>();//������ �������� ��������� ���������� ���� ������
//	public static List<Float> parametrsInitCount= new ArrayList<Float>();//������ �������� ���������� �� ������ ���������������
//	public static List<String> currentTypeLibraryFunctionsList= new ArrayList<String>();//������ ������������ �������
//	public static List<String> libraryTypesList= new ArrayList<String>();//������ ������������ ����������
//	
//	public static String oldFunction = null;
//	
//	//temp:
//	public static List<Float> paramInitialValueList = new ArrayList<Float>();//������ ��������������� �������� ���������� �����
//	                                                                         //��������������� ����������������
//	
//	//try new:
//	public static List<List<String>> typeCountExperianse= new ArrayList<List<String>>();
//	public static List<String> maxTypeCharacteristics= new ArrayList<String>();//������ �������� �������������� ������� �� �����
//	public static int numberOfParamOrgAtThisStep=0;
////	public static boolean wasItselfOrganizationAtThisStep=false;
//	
//	public static int maxNumberOfAddedLists=10;//������������ ���������� �������, ������� ����� 
//	                                           //�������� ��� ����������� ���������������
//	
//	//��� ����������� ����������� ���������������:
////	public static boolean wasFunctionsOrganizationAtThisStep=false;
////	public static boolean wasStructuralOrganizationAtThisStep=false;
//	public static List<String> parametrsOldValues= new ArrayList<String>();;//������ �������� ����������
//	public static List<String> functionsOldValues= new ArrayList<String>();;//������ �������� �������
//	public static List<String> typesOldValues= new ArrayList<String>();;//������ �������� �����
//	
	
	//find best type of elementary children for each convolution function
	public static void findBestElementaryChildren()
	{
		double typeValue;
		String typeName;
		
		//for case if we use price for elements
		//(t.e. ItselfOrganizationVariebles.useMaximumPrice = true)
		double averageElementaryPrice = 0.0;
		double averageElementaryMaxCount = 0.0;
		double currentPrice;
		if( ItselfOrganizationVariables.useMaximumPrice )
		{
			averageElementaryPrice = 
				TypeLoader.getAveragePriceForLeafs();
			averageElementaryMaxCount = 
				TypeLoader.getAverageCountForLeafs();
		}
		
		String maxForSumTypeName = "",
		maxForSumMultTypeName = "",
		maxForSumPowTypeName = "";
		
		double 
		currentTypeForSumMultValue = 0.0,
		currentTypeForSumPowValue = 0.0,
		maxForSumTypeValue = 0.0,
		maxForSumMultTypeValue = 0.0,
		maxForSumPowTypeValue = 0.0;
		
		for(int typeInd=0; typeInd < usedInOrganizationElementaryTypesList.size(); typeInd++)
		{
			typeValue = typesMaxValuesList.get( typeInd );
			typeName = usedInOrganizationElementaryTypesList.get(typeInd);
			
			currentTypeForSumMultValue = typeValue *
			TypeLoader.getFactorForSumMultFunction( typeName );
			
			currentTypeForSumPowValue = 
			Math.pow( typeValue, 
					TypeLoader.getFactorForSumPowFunction( typeName ) 
					);
			
			if( ItselfOrganizationVariables
					.useMaximumPrice )
			{
				currentPrice = TypeLoader.getPrice(typeName);
				currentPrice = 
					( (currentPrice - averageElementaryPrice)
						* 100.0 ) / averageElementaryPrice;
				if( averageElementaryMaxCount != 0 )
				{
					//...Value =
					//"percent of a gain of  
					//...Value
					//rather its average value for
					//all participate elementary types" - 
					//"percent of a gain of 
					// typeName price
					//rather its average value for
					//all participate elementary types"
					typeValue = 
						( (typeValue -
								averageElementaryMaxCount)
								* 100.0 ) / 
								averageElementaryMaxCount;
					typeValue = typeValue - currentPrice;
					
					currentTypeForSumMultValue = 
						( (currentTypeForSumMultValue -
								averageElementaryMaxCount)
								* 100.0 ) / 
								averageElementaryMaxCount;
					currentTypeForSumMultValue = 
						currentTypeForSumMultValue - currentPrice;
					
					currentTypeForSumPowValue = 
						( (currentTypeForSumPowValue -
								averageElementaryMaxCount)
								* 100.0 ) / 
								averageElementaryMaxCount;
					currentTypeForSumPowValue = currentTypeForSumPowValue - currentPrice;
				}
			}
			
			if( typeInd == 0 )
			{
				maxForSumTypeName = typeName;
				maxForSumMultTypeName = typeName;
				maxForSumPowTypeName = typeName;
				maxForSumTypeValue = typeValue;  
				maxForSumMultTypeValue = currentTypeForSumMultValue;
				maxForSumPowTypeValue = currentTypeForSumPowValue;
			}
			else
			{
				if( maxForSumTypeValue < typeValue )
				{
					maxForSumTypeName = typeName;
					maxForSumTypeValue = typeValue;  
				}
				if( maxForSumMultTypeValue < currentTypeForSumMultValue )
				{
					maxForSumMultTypeName = typeName;
					maxForSumMultTypeValue = currentTypeForSumMultValue;  
				}
				if( maxForSumPowTypeValue < currentTypeForSumPowValue )
				{
					maxForSumPowTypeName = typeName;
					maxForSumPowTypeValue = currentTypeForSumPowValue;  
				}
			}
		}
		
		forSumBestElementaryChild = maxForSumTypeName; 
		forSumBestElementaryChildValue = maxForSumTypeValue;
		
		forSumMultElementaryChild = maxForSumMultTypeName;
		forSumMultBestElementaryChildValue = maxForSumMultTypeValue;
		
		forSumPowBestElementaryChild = maxForSumPowTypeName;
		forSumPowBestElementaryChildValue = maxForSumPowTypeValue;
		
	}
	
	//sort convolutionFuncPriorForLeafs in decreasing order
	//of main characteristics of best list values for each
	//convolution function
	public static void setConvolutionFuncPriorForLeafs() 
	{
		convolutionFuncPriorForLeafs.clear();
		double 
		parentCountSum = forSumBestElementaryChildValue;
		double parentCountSumMult = forSumMultBestElementaryChildValue; 
		double parentCountSumPow = forSumPowBestElementaryChildValue;
		
		int forDelFuncCount = 0;
		if( interTypesContainsSumFunc.size() < 1 )
		{
			forDelFuncCount++;
			parentCountSum = Double.NEGATIVE_INFINITY;
		}
		if( interTypesContainsSumMultFunc.size() < 1 )
		{
			forDelFuncCount++;
			parentCountSumMult = Double.NEGATIVE_INFINITY;
		}
		if( interTypesContainsSumPowFunc.size() < 1 )
		{
			forDelFuncCount++;
			parentCountSumPow = Double.NEGATIVE_INFINITY;
		}
		
		//choosing sequence in witch we will trying functions
		if( ( parentCountSum >= parentCountSumMult) 
				&& ( parentCountSum >= parentCountSumPow ) )
		{
			convolutionFuncPriorForLeafs.add("sum(Xi)");
			if( parentCountSumMult >= parentCountSumPow )
			{
				convolutionFuncPriorForLeafs.add("sum(Xi*Ki)");
				convolutionFuncPriorForLeafs.add("sum(Xi^Ki)");
			}
			else
			{
				convolutionFuncPriorForLeafs.add("sum(Xi^Ki)");
				convolutionFuncPriorForLeafs.add("sum(Xi*Ki)");
			}
		}
		else
		{
			if( parentCountSumMult >= parentCountSumPow )
			{
				convolutionFuncPriorForLeafs.add("sum(Xi*Ki)");
				if( parentCountSum >= parentCountSumPow )
				{
					convolutionFuncPriorForLeafs.add("sum(Xi)");
					convolutionFuncPriorForLeafs.add("sum(Xi^Ki)");
				}
				else
				{
					convolutionFuncPriorForLeafs.add("sum(Xi^Ki)");
					convolutionFuncPriorForLeafs.add("sum(Xi)");
				}
			}
			else
			{
				convolutionFuncPriorForLeafs.add("sum(Xi^Ki)");
				if( parentCountSum >= parentCountSumMult )
				{
					convolutionFuncPriorForLeafs.add("sum(Xi)");
					convolutionFuncPriorForLeafs.add("sum(Xi*Ki)");
				}
				else
				{
					convolutionFuncPriorForLeafs.add("sum(Xi*Ki)");
					convolutionFuncPriorForLeafs.add("sum(Xi)");
				}
			}
		}
		
		while( forDelFuncCount > 0 )
		{
			convolutionFuncPriorForLeafs.remove( 
					convolutionFuncPriorForLeafs.size() - 1 );
			forDelFuncCount--;
		}
	}
	
	//________New fields and methods
	IParametersOrganizer parametersOrganizer;
	
	/**
	 * static singleton instance
	 */
	static private ItselfOrganizationVariables instance = null;
	
	/**
	 * singleton getInstance() method
	 * 
	 * @return
	 */
	static public ItselfOrganizationVariables getInstance() {
		if( instance == null)
			instance = new ItselfOrganizationVariables();
		return instance;
	}

	private ItselfOrganizationVariables() {
		
		//init default parametersOrganizer
		parametersOrganizer = null;
		setParametersModelOrganizer(parametersOrganizerType);
		
	}
	
	@Override
	public ArrayList<TreeElement> getLeafs() {

		return leafsList;
	}

	@Override
	public double getRootValue() {

		return TreeOperations.rootCount();
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParametersModel#getOrganizationCyclesCount()
	 */
	@Override
	public int getOrganizationCyclesCount() {
		return maxNumerOfCounts;
	}

	@Override
	public boolean getUseParticipationProperty() {

		return useParticipationInParamOrg;
	}

	@Override
	public void setParametersModelOrganizer(ParametersOrganizer organizerType) {
		
		//current organizer already has this type
		if(parametersOrganizer != null && organizerType == parametersOrganizerType) {
			return;
		}
		
		//create new organizer with current strategy
		IParametersOrganizer newOrganizer = ParametersOrganizerFactory.createOrganizer(this, organizerType);
		newOrganizer.setStrategy(parametersOrganizerStrategyType);
		
		//add new organizer to observers
		addObserver(newOrganizer);
		
		//to be sure new organizer was reset
		modelChanged( null );
		
		//remove old organizer from observers
		if(parametersOrganizer != null) {
			deleteObserver(parametersOrganizer);
		}
		
		//assign new organizer and save it's type
		parametersOrganizer = newOrganizer;
		parametersOrganizerType = organizerType;
		
	}

	
	/* (non-Javadoc)
	 * @see simulation.parameter.IParametersModel#setParametersModelOrganizerStrategy(simulation.parameter.ParametersOrganizerStrategy)
	 */
	@Override
	public void setParametersModelOrganizerStrategy(ParametersOrganizerStrategy strategyType) {

		if(parametersOrganizer == null) {
			//create new organizer in this case
			parametersOrganizerStrategyType = strategyType;
			setParametersModelOrganizer(parametersOrganizerType);
			return;
		}
			
		parametersOrganizer.setStrategy(strategyType);
		parametersOrganizerStrategyType = strategyType;
	}

	@Override
	public void modelChanged(IParametersOrganizer modelModifier) {

		setChanged();
		notifyObservers(modelModifier);
	}

	/* (non-Javadoc)
	 * @see simulation.parameter.IParametersModel#runPSO(double)
	 */
	/**
	 * run parameter organization
	 */
	@Override
	public void runPSO(double rootMinValue) {
		
		parametersOrganizer.organize(rootMinValue);
	}
	
	
	
//	public static void fillAndSortBestChildLeafs()
//	{
//		forSumBestChilderenLeafList.clear();
//		forSumBestChilderenLeafList.addAll
//		( usedInOrganizationElementaryTypesList );
//		
//		forSumMultBestChilderenLeafList.clear();
//		forSumMultBestChilderenLeafList.addAll
//		( usedInOrganizationElementaryTypesList );
//		
//		forSumPowBestChilderenLeafList.clear();
//		forSumPowBestChilderenLeafList.addAll
//		( usedInOrganizationElementaryTypesList );
//		
//		for (int j = 0; j < usedInOrganizationElementaryTypesList.size() - 1; j++) 
//		{
//			for (int i = 0; i < usedInOrganizationElementaryTypesList.size() - 1 - j; i++)
//			{
//			}
//		}
//	}
	
	
}
