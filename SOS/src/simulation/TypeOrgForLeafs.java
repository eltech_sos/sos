//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
// �������� ������� ��������������� �������������� ������
// � ���� ���������������� ��������� ��� ������� �������� �������, 
// ����������� ��
// ������������ ����������� ���
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
package simulation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import Main.FunctionsLoader;
import Main.Main;
import Main.TreeOperations;
import Main.TypeLoader;
import simulation.ItselfOrganizationVariables;
import object.TreeElement;

public class TypeOrgForLeafs 
{
	ArrayList<TreeElement> currentLeafsList; //������ ��
	int currentLeafsCount; // ���������� ��
	

	
	ArrayList<String> forCurParentChildLeafsFuncList;
	
	//---------------------------------------------------------------------------
	// �������-�����������
	//---------------------------------------------------------------------------
	public TypeOrgForLeafs()
	{
		forCurParentChildLeafsFuncList = new ArrayList<String>();
		//������  �������� ���� ������� �������� ���������
		// �������� ��, ����������� � ��
		currentLeafsList = new ArrayList<TreeElement>();
		// ������� ������ ��
		currentLeafsList.addAll( ItselfOrganizationVariables.leafsList );
		
		// ���� �� ���� ��� �� ��������� � ��
		if( ItselfOrganizationVariables.
				usedInOrganizationElementaryTypesList.size() < 1 )
		{
			return; //�����
		}
		
		//���� � ���������� ���� ����������� � �� ����
		if( ItselfOrganizationVariables.leafsList.size() > 0 )
		{
			currentLeafsCount = currentLeafsList.size(); // ���������� ��
			// ���� ���������� �������� ���������� � ��� ���������
			if( ItselfOrganizationVariables.sameParametrsInParamOrg 
					&& ItselfOrganizationVariables.paramOrgAllowed )
			{
				// ��� ��� ���������� ����������
				leafOrganizationForSameParametrs();
			}
			else
			{
				// ���, ���� ��������� ����� ����������
				leafOrganizationForNOTSameParametrs();
			}
		}
	}

	//---------------------------------------------------------------------------
	// ��� ��� �� ��� �������, ��� �������� ���������� ����� ���� ����������
	//---------------------------------------------------------------------------
	private void leafOrganizationForNOTSameParametrs() 
	{
		TreeElement list; //������� ��
		String functionName = ""; //��� �������
		String typeName = ""; //��� ����
		String parentConvolutionFunction = ""; //������� ������� ��������
		
		String maxCountedTypeForParentFunction = ""; //������ ���
		double maxCountedTypeForParentFunctionValue = 0.0; //�������� ��� ������� ����
		
		double wasElementForParentFunctionValue;
		
		//�� ������, ���� �� ���������� ������� ������������ ��������� �������
		//(�.e. ItselfOrganizationVariebles.useMaximumPrice = true)
		double averageElementaryPrice = 0.0; // ������� ��������� ��������
		double averageElementaryMaxCount = 0.0; //������������ ��������� ��������
		//���� ����������� ������������ ��������� �������
		if( ItselfOrganizationVariables.useMaximumPrice )
		{
			averageElementaryPrice = 
				TypeLoader.getAveragePriceForLeafs(); //��������� ������� ���������
			averageElementaryMaxCount = 
				TypeLoader.getAverageCountForLeafs();// ��������� ������������ ���������
		}
		
		
		TreeElement elementInStruct; //������� �������
		ArrayList<TreeElement> childrenList = new ArrayList<TreeElement>();
		//������ �������� ���������
		ArrayList<String> convolutionFuncsList = new ArrayList<String>();
		//������ ������� �������
		
		if( ItselfOrganizationVariables.funcIntermediateOrgAllowed )
			//���� ��������� ��� ��� ��
		{
			
		}
		else
			//���� �� ��������� ��� ��� ��
		{
			
		}
		
		//�������� �� ���� �������
		for(int level = Main.structTreeElements.size() - 1; level >= 0  ; level--)
		{
			//�������� �� ���� ��������� ������� ������
			for(int elemIndex=0; elemIndex<Main.structTreeElements.elementAt(level).size(); elemIndex++)
			{
				elementInStruct = (TreeElement)Main.structTreeElements.elementAt(level).elementAt(elemIndex);
				if( elementInStruct.type == 2 )
					//���� ��� ��
				{
					if( elementInStruct.iChildrenCount > 0 )
						//���� � �������� �������� ���� ��������
					{
						childrenList.clear(); // ������� ������ �������� ���������
						// ��������� ������ �������� ���������
						childrenList.addAll( TreeOperations.findAllChildrenInStructByIDAndLevel( elementInStruct.ID, elementInStruct.Level) );
						convolutionFuncsList.clear(); //������� ������ �������
						convolutionFuncsList.addAll( TypeLoader.getFunctionList(elementInStruct.typeName) );
						// ��������� ������ ������� �������
						// ���� ������ �������� ��������� �������
						if( convolutionFuncsList.size() > 1 )
						{
							//������������� ������ ������� ��� ��������	� ����������� ��
							setBestParentFunc(convolutionFuncsList, childrenList, elementInStruct);
						}
						else //���� ������������ ������� �������
						{
							//������������ �������� �� ��� ������ ������� �������
							setBestForSelectedParent( childrenList, convolutionFuncsList.get(0) );
						}
						
					}//end if( elementInStruct.iChildrenCount > 0 )
				}//end if( elementInStruct.type == 2 )
			}
		}
		

		
	}
	
	
	//---------------------------------------------------------------------------
	// ����������� ������ ������� ��� ������������� �� � 
	// ����������� �������� ��������� � ������ ������ ������� �������
	//---------------------------------------------------------------------------
	private void setBestParentFunc
	 (ArrayList<String> convolutionFuncsList, //������ ������� �������
			ArrayList<TreeElement> childrenList, //������ �������� ���������
			TreeElement parentElem)  //������������ �������
	{
		String bestFunc = null, //������ �������
		currentFunc; //������� �������
		double bestParentValue = 0.0;  // ������ �������� � ��������
		double currentParentValue = 0.0; //������� �������� � ��������
		ArrayList<String> bestChildFuncList; //������ ������ ������ ��� �������� ��
		
		bestChildFuncList = forCurParentChildLeafsFuncList; //���������
		
		
		//������� ������ ������� ������� ��� �������� ��������
		//� ��������� ������ �������� ��� �������� ���������
		
		//�������� �� �������� �������
		for( int funcInd=0; 
		funcInd < convolutionFuncsList.size() ; funcInd++ )
		{
			currentFunc = convolutionFuncsList.get( funcInd );
			//������� �������
			currentParentValue = findBestParentValueForFunc( 
					currentFunc, childrenList );
			//������������ �������� � �� � ������ ��������
			//���� ��� ������ �������
			if( funcInd == 0 )
			{
				bestFunc = currentFunc; //������ �������
				bestParentValue = currentParentValue; //��������
				bestChildFuncList = forCurParentChildLeafsFuncList;
				//������ ������� ��� �������� ��
			}
			else
			{
				//���� ������ ������� �����
				if( currentParentValue > bestParentValue )
				{
					bestFunc = currentFunc; //������ �������
					bestParentValue = currentParentValue; //������ ��������
					bestChildFuncList = forCurParentChildLeafsFuncList;
					//������ ������� ��� �������� ��
				}
			}
			
		}
		
		parentElem.functionName = bestFunc; //��������� ��� �������
		parentElem.elementCount = bestParentValue; //��������� ��������
		
		//���������� ��� �������� �� � ������ ��������
		if( TypeLoader.getChildrenType(parentElem.typeName) != 2 )
			//���� ������������ ������� ����� ����� �������� ��
		{
			
			String bestListType = null, //������ ���
			bestListFunc; //������ �������
			int bestTypeIndex; //������ ������ �������
			double bestListCount; //������ �������� � ��
			if( bestFunc.equals("sum(Xi)") )
			{
				// ��� sum(Xi)
				bestListType = ItselfOrganizationVariables.
				forSumBestElementaryChild;
			}
			else if( bestFunc.equals("sum(Xi*Ki)") )
			{
				//��� sum(Xi*Ki)
				bestListType = ItselfOrganizationVariables.
				forSumMultElementaryChild;
			}
			else if( bestFunc.equals("sum(Xi^Ki)") )
			{
				//��� sum(Xi^Ki)
				bestListType = ItselfOrganizationVariables.
				forSumPowBestElementaryChild;
			}
			bestTypeIndex = ItselfOrganizationVariables.
			usedInOrganizationElementaryTypesList.
			indexOf( bestListType ); //������������� ������
			bestListFunc = ItselfOrganizationVariables.
			typesMaxFuncList.get(bestTypeIndex); //��������������� ������ ������� ��� �����
			bestListCount = ItselfOrganizationVariables.
			typesMaxValuesList.get(bestTypeIndex); //��������������� ������������ ��������
			
			boolean overwriteMaps = true; //���� ���������� ����
			
			
			int chooseFuncInd = 0; //������ � ������� BestChildFunctionList
			
			TreeElement child; //�������� �������
			
			//�������� �� ����� ������� ���������
			for( int childInd=0; 
			childInd < childrenList.size() ; childInd++ )
			{
				child = childrenList.get( childInd );
				// ���� ��� ��
				if( child.type == 3 )
				{
					//���� ��� ������� �� ��������� � ��
					if( ItselfOrganizationVariables.
							usedInOrganizationElementaryTypesList.
							contains( child.typeName ) )
					{
						//���� ������� ������� �������� ��������� � ��
						//��� �� �� ��������� ���
						if( FunctionsLoader.
								isParticipate(child.functionName) 
								|| ( ! ItselfOrganizationVariables.
										useParticipationInFuncOrg ) )
						{
							child.elementCount = bestListCount; //��������� ��������
							child.functionName = bestListFunc; //��������� �������
							child.typeName = bestListType; //��������� ��� ����
							overwriteMaps = true; //����� ������������ �����
						}
						else
						{
							//������� ��� �����
							if( bestChildFuncList.get( chooseFuncInd ).
									equals( child.functionName ) )
								
							{
								overwriteMaps = false; //�� ��������������
							}
							else
							{
								child.elementCount = bestListCount;//��������� ��������
								child.functionName = bestListFunc;//��������� �������
								child.typeName = bestListType;//��������� ��� ����
								overwriteMaps = true;//����� ������������ �����
							}
							chooseFuncInd++; //��������� ��������
						}
						
						if( overwriteMaps )
						{
							overwriteMaps( child ); //�������������� �����
						}
					}
				}
			}
			
		}//end actions if parent element can have list children
	
	}
	
	//---------------------------------------------------------------------------
	// ����������� ������������� �������� � ������������ ��
	// ��� ������ ������� �������
	//---------------------------------------------------------------------------
	private double findBestParentValueForFunc
	( String parentFunc, // ������������ �� (�������)
			ArrayList<TreeElement> childrenList) //������ �������� ���������
	{
		TreeElement currentChild; //������� �������� �������
		String childTypeName,  //��� ���� �������� ��������� ��������
		childFuncName; //��� ������� �������� ��������� ��������
		double bestCount; //������ ��������
		int setBest; //������ ������� ����
		
		double parentValue = 0.0; //�������� � ������������ ��������
		double childValue = 0.0; //�������� � �������� ��������
		
		forCurParentChildLeafsFuncList.clear(); //������� ������� �������� ���������
		
		double bestChildValue = 0.0, //������ ������� ��������� �����������
		currentChildValue = 0.0; //������� ������� ��������� �����������
		
		
		String bestType = null, //������ ��� �.�. ��� ������ ������� ������� 
		bestFunc; //������ ������� �.�. ��� ������ ������� �������
		
		if( parentFunc.equals("sum(Xi)") )
		{
			//��� sum(Xi)
			bestType = ItselfOrganizationVariables.
			forSumBestElementaryChild; //���
			bestChildValue = ItselfOrganizationVariables.
			forSumBestElementaryChildValue; //�������
		}
		else if( parentFunc.equals("sum(Xi*Ki)") )
		{
			// ��� sum(Xi*Ki)
			bestType = ItselfOrganizationVariables.
			forSumMultElementaryChild;//���
			bestChildValue = ItselfOrganizationVariables.
			forSumMultBestElementaryChildValue;//��������
		}
		else if( parentFunc.equals("sum(Xi^Ki)") )
		{
			//��� sum(Xi^Ki)
			bestType = ItselfOrganizationVariables.
			forSumPowBestElementaryChild;//���
			bestChildValue = ItselfOrganizationVariables.
			forSumPowBestElementaryChildValue;//��������
		}
		try
		{
			setBest = ItselfOrganizationVariables.
			usedInOrganizationElementaryTypesList.
			indexOf(bestType); //������ ����
			bestFunc = ItselfOrganizationVariables.
			typesMaxFuncList.get(setBest); //������ �������
			bestCount = ItselfOrganizationVariables.
			typesMaxValuesList.get(setBest); //������ ��������
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return 0.0; //��������� ����������
		}
		
		//�������� �� ���� �������� ���������
		for(int childInd=0; childInd < childrenList.size(); childInd++)
		{
			currentChild = childrenList.get(childInd); //������� �������
			childTypeName = currentChild.typeName; //��������� ��� ���
			if( currentChild.type == 3 )
				//���� ��� ��
			{
				//���� ���� ��� ��������� � ��
				if( ItselfOrganizationVariables.
						usedInOrganizationElementaryTypesList.contains
						(childTypeName) )
				{
					//��������� ��� �������
					childFuncName = currentChild.functionName;
					//���� ��� ������� ��������� � ��
					if( FunctionsLoader.isParticipate(childFuncName) 
							|| ( ! ItselfOrganizationVariables.
									useParticipationInFuncOrg ) )
					{
						childValue = bestCount; //������ ��������
					}
					else
					{
						//���� ������� �� ��������� � ��, ��� ����� ������ ������, ��� bestcount
						//�.�. bestcount - ����� ����� ���, �������� ���������
						currentChildValue = TypeLoader. //������� �����������, �-��� ���� ������ �������
						getListMainCaracteristic(
								currentChild.elementCount, 
								currentChild.typeName, 
								parentFunc);
						//���� ������ ������� �����
						if( currentChildValue > bestChildValue )
						{
							forCurParentChildLeafsFuncList.
							add( currentChild.functionName ); //��������� �������
							childValue = currentChild.elementCount; //�������� � ��������
						}
						else
						{
							forCurParentChildLeafsFuncList.
							add( bestFunc ); //��������� ������ �������
							childValue = bestCount; //������ ��������
						}
					}
				}
			}
			else
				//���� ��� ��
			{
				childValue = currentChild.elementCount; //��������� ��������� � ���� ��������
			}
			
			//��������� �������� � ������������� ��������
			if( parentFunc.equals("sum(Xi)") )
			{
				//��� sum(Xi)
				parentValue = parentValue + childValue;
			}
			else if( parentFunc.equals("sum(Xi*Ki)") )
			{
				//��� sum(Xi*Ki)
				parentValue = parentValue + 
				childValue * TypeLoader.
				getFactorForSumMultFunction(childTypeName);
			}
			else if( parentFunc.equals("sum(Xi^Ki)") )
			{
				// ��� sum(Xi^Ki)
				parentValue = parentValue + 
				Math.pow( childValue, TypeLoader.
				getFactorForSumPowFunction(childTypeName) );
			}
			
		}
		
		return parentValue; //������� �������� � ��������
	}

	//---------------------------------------------------------------------------
	// ����������� �������� ��������� � ������ ������ ������� �������
	//---------------------------------------------------------------------------
	private void setBestForSelectedParent(
			ArrayList<TreeElement> childrenList,  //������ �������� ���������
			String parentFunc) //������� ������� ��������
	{
		TreeElement currentChild; //������� �������� �������
		String childTypeName,  // ��� ���� ��������� ��������
		childFuncName; //��� ������� ��������� ��������
		double bestCount; //������ �������� � �������� ��������
		int setBest; //���� ����������
		
		double bestChildValue = 0.0, //������ ������� �����������
		currentChildValue = 0.0; //������� ������� �����������
		
		//��� ������� ������� ������� ���������� ������ ����
		//� ������� � ������
		String bestType = null,  //������ ���
		bestFunc; //������ �������
		if( parentFunc.equals("sum(Xi)") )
		{
			// ��� sum(Xi)
			bestType = ItselfOrganizationVariables.
			forSumBestElementaryChild; //������ ���
			bestChildValue = ItselfOrganizationVariables.
			forSumBestElementaryChildValue; //������ �������
		}
		else if( parentFunc.equals("sum(Xi*Ki)") )
		{
			//��� sum(Xi*Ki)
			bestType = ItselfOrganizationVariables.
			forSumMultElementaryChild; //������ ���
			bestChildValue = ItselfOrganizationVariables.
			forSumMultBestElementaryChildValue; //������ �������
		}
		else if( parentFunc.equals("sum(Xi^Ki)") )
		{
			// ��� sum(Xi^Ki)
			bestType = ItselfOrganizationVariables.
			forSumPowBestElementaryChild; //������ ���
			bestChildValue = ItselfOrganizationVariables.
			forSumPowBestElementaryChildValue; //������ �������
		}
		try
		{
			setBest = ItselfOrganizationVariables.
			usedInOrganizationElementaryTypesList.
			indexOf(bestType); //������ ������� ����
			bestFunc = ItselfOrganizationVariables.
			typesMaxFuncList.get(setBest); //������ �������
			bestCount = ItselfOrganizationVariables.
			typesMaxValuesList.get(setBest); //������ ��������
		}
		catch(Exception e)
		{
			e.printStackTrace(); // ��������� ����������
			return;
		}
		
		// �������� �� ���� �������� ���������
		for(int childInd=0; childInd < childrenList.size(); childInd++)
		{
			currentChild = childrenList.get(childInd); //������� �.�.
			if( currentChild.type == 3 )
				//���� ��� ��
			{
				childTypeName = currentChild.typeName; //��� �������� �.�.
				//���� ��� ��������� � ��
				if( ItselfOrganizationVariables.
						usedInOrganizationElementaryTypesList.contains
						(childTypeName) )
				{
					childFuncName = currentChild.functionName; //�������
					//���� ������� ��������� � ��, ��� ��� �������
					if( FunctionsLoader.isParticipate(childFuncName) 
							|| ( ! ItselfOrganizationVariables.
									useParticipationInFuncOrg ) )
					{
						currentChild.typeName = bestType; //�������������� ��� �.�.
						currentChild.functionName = bestFunc;//�������
						currentChild.elementCount = bestCount;//� ��������
						setBest = 1;
					}
					else
					{
						//���� ������� �� ��������� � ��, ��� ��� ��� ������ �������������
						currentChildValue = TypeLoader.
						getListMainCaracteristic(
								currentChild.elementCount, 
								currentChild.typeName, 
								parentFunc); //������� �������� � ��������
						if( currentChildValue > bestChildValue ) //���� ��� �����
						{
							setBest = 0; //�������������� �� ����
						}
						else
						{
							currentChild.typeName = bestType; //�������������� ���
							currentChild.functionName = bestFunc;//�������������� ��� �������
							currentChild.elementCount = bestCount;//�������������� ��������
							setBest = 1; //���� ��������������
						}
					}
					
					if( setBest == 1 )
						//���� ���� ��������������
					{
						//�������������� �����
						overwriteMaps( currentChild );
					}
				}
			}
		}
	}

	//---------------------------------------------------------------------------
	// ������� �������������� �������������� �������� (�����������)
	//---------------------------------------------------------------------------
	private double countMainCharacteristicForChild( 
			double childValue, //�������� � �������� ��������
			String parentConvolutionFunction, //������� ������� ��������
			String childType ) //��� �������� ���������
	{
		double mainCharacteristic; 
		double childFullValue = 0.0; //������� �����������
		
		if( parentConvolutionFunction.equals("sum(Xi)") )
		{
			//��� sum(Xi)
			childFullValue = childValue;
		}
		else if( parentConvolutionFunction.equals("sum(Xi*Ki)") )
		{
			//��� sum(Xi*Ki)
			childFullValue = childValue *
			TypeLoader.getFactorForSumMultFunction( childType );
		}
		else if( parentConvolutionFunction.equals("sum(Xi^Ki)") )
		{
			//��� sum(Xi^Ki)
			childFullValue = 
				Math.pow( childValue, TypeLoader.getFactorForSumPowFunction
						( childType ) );
		}
		
		//������� �������
		double averageElementaryMaxCount;
		averageElementaryMaxCount = TypeLoader.getAverageCountForLeafs();
		//���������� ������� �����������
		mainCharacteristic = 
			( (childFullValue -
					averageElementaryMaxCount)
					* 100.0 ) / 
					averageElementaryMaxCount;
		
		//������� ���������
		double averageElementaryPrice;
		averageElementaryPrice = TypeLoader.getAveragePriceForLeafs();
		//����� ������� ��������� � ���������
		mainCharacteristic = 
			mainCharacteristic - 
			( (TypeLoader.getPrice(childType) -
					averageElementaryPrice)
					* 100.0 ) / 
					averageElementaryPrice;
		
		return mainCharacteristic;
	}
	
	//---------------------------------------------------------------------------
	// ���������� ����
	//---------------------------------------------------------------------------
	private void overwriteMaps( TreeElement list )
	{
		String functionName; // ��� �������
		
		functionName = list.functionName; //��������� ��� �������
		
		//���� ����� ��� ������ ������� ����������
		if( ItselfOrganizationVariables.
				paramMapsForFuncMax.containsKey( functionName ) )
		{
			if( list.parametersValuesMap != null ) //���� ����� �� �����
			{
				list.parametersValuesMap.clear(); //�������� ����� ����������
				list.parametrsWeightMap.clear(); //�������� ����� �����
			}
			
			list.parametersValuesMap = 
				ItselfOrganizationVariables.
				paramMapsForFuncMax.get( functionName ); //�������������� �����
			
			//���� ����� �������
			if( list.parametersValuesMap != null )
			{
				//�������������� ����� �����
				if( list.parametersValuesMap.size() > 0 )
				{
					//���� ����� ����� ���
					if( list.parametrsWeightMap == null )
					{
						//�������� ������
						list.parametrsWeightMap = new HashMap<String, Double> ();
					}
					//��������� ���
					double initialWeight = 1.0;
					//��������
					Iterator iter = list.parametersValuesMap.entrySet().iterator();
					//�������� �� ���� �������
					while( iter.hasNext() )
					{
						//��������� ������
						Entry paramInform = (Entry) iter.next();
			        	//��� ���������
						String paramName = (String) paramInform.getKey();
			        	//��������� ��� � ��������� �����
						list.parametrsWeightMap.
			        	put( paramName, initialWeight );
					}
				}
			}
			//���� ����� ���������� �����
			else
			{
				//������� ����� ����������
				list.parametersValuesMap = new HashMap<String, Double> ();
				//������� ����� �����
				list.parametrsWeightMap = new HashMap<String, Double> ();
			}
		}
		//���� ���� ���
		else
		{
			//������� ����� ����������
			list.parametersValuesMap = new HashMap<String, Double> ();
			//������� ����� �����
			list.parametrsWeightMap = new HashMap<String, Double> ();
		}
	}
	
	//---------------------------------------------------------------------------
	// ��� ��� �� ��� �������, ��� �������� ���������� �� ����� ���� ����������
	//---------------------------------------------------------------------------
	private void leafOrganizationForSameParametrs() 
	{
//to_do		
		leafOrganizationForNOTSameParametrs();
	}
}
