package simulation;

import java.util.ArrayList;

import object.TreeElement;
import Main.TreeOperations;
import simulation.ItselfOrganizationVariables;
import simulation.UserChioceDialog;

public class CheckModel 
{
	public CheckModel()
	{
		
	}
	
	public static boolean isModelCorrect()
	{
		ArrayList<TreeElement> elemForAdditionalSettingsList;
		ItselfOrganizationVariables.dataNOTValidForSimulation = false;
		
		//checking did current model cost not more then 
		//max allowed
		if( ItselfOrganizationVariables.useMaximumPrice )
		{
			if( ItselfOrganizationVariables.maximumPrice < TreeOperations.countCurrentModelCost() )
			{
				new UserChioceDialog( null, 0, null );
				if( ItselfOrganizationVariables.dataNOTValidForSimulation )
				{
					return false;
				}
			}
		}
		
//		if( ItselfOrganizationVariebles.useParticipationInTypeOrg )
//		{
//			
//			if( ItselfOrganizationVariebles.typeIntermediateOrgAllowed )
//			{
//				elemForAdditionalSettingsList = TreeOperations.findAllIntermediateWithNotParticipateType();
//				if( elemForAdditionalSettingsList.size() > 0 )
//				{
//					new UserChioceDialog( elemForAdditionalSettingsList, 1, null );
//					if( ItselfOrganizationVariebles.dataNOTValidForSimulation )
//					{
//						return false;
//					}
//				}
//			}
//			
//			if( ItselfOrganizationVariebles.typeListOrgAllowed )
//			{
//				elemForAdditionalSettingsList = TreeOperations.findAllListsWithNotParticipateType();
//				if( elemForAdditionalSettingsList.size() > 0 )
//				{
//					new UserChioceDialog( elemForAdditionalSettingsList, 2, null );
//					if( ItselfOrganizationVariebles.dataNOTValidForSimulation )
//					{
//						return false;
//					}
//				}
//			}
//			
//		}
//		
//		if( ItselfOrganizationVariebles.funcListOrgAllowed )
//		{
//			if( ItselfOrganizationVariebles.useParticipationInFuncOrg )
//			{
//				elemForAdditionalSettingsList = TreeOperations.findAllListsWithNotParticipateFunction();
//				if( elemForAdditionalSettingsList.size() > 0 )
//				{
//					new UserChioceDialog( elemForAdditionalSettingsList, 3, null );
//					if( ItselfOrganizationVariebles.dataNOTValidForSimulation )
//					{
//						return false;
//					}
//				}
//			}
//		}
		if( ItselfOrganizationVariables.sameParametrsInParamOrg )
		{
			ArrayList<String> paramList = TreeOperations.findAllParametrsWithNotSameValues();
			if( paramList.size() > 0 )
			{
				new UserChioceDialog( null, 4, paramList );
				if( ItselfOrganizationVariables.dataNOTValidForSimulation )
				{
					return false;
				}
			}
		}
		return true;
	}
	
	
}
