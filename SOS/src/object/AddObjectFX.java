package object;

import java.util.ArrayList;

import tabs.FXDialog;
import tabs.Message;
import Main.Main;
import Main.TypeLoader;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class AddObjectFX extends Application{
	private ComboBox<String> cbType ,cbFunc;
	private TextField tfName;
	
	@Override
	public void start(final Stage stage) throws Exception {
		stage.setTitle("���������� �������� ����������");
		
		VBox root = new VBox();
    	root.setSpacing(10);
    	root.setPadding(new Insets(10, 10, 10, 10));
    	
    	GridPane grid = new GridPane();
    	grid.setHgap(20);
    	grid.setVgap(10);
    	
    	Label txtName = new Label("��������");
    	txtName.getStyleClass().add("label-as-title");
    	Label txtType = new Label("��� �������");
    	txtType.getStyleClass().add("label-as-title");
    	Label txtFunc = new Label("������� �������");
    	txtFunc.getStyleClass().add("label-as-title");
    	
    	tfName = new TextField();
    	cbType = new ComboBox<String>(); 
    	cbType.setMinWidth(200);
    	try {
			for(int i=0; i < TypeLoader.intermediaryTypesList.size(); i++)    				
				cbType.getItems().add(TypeLoader.intermediaryTypesList.get(i));    				    		
	    } catch (Exception ef) { ef.printStackTrace(); }
    	if(!cbType.getItems().isEmpty()) cbType.getSelectionModel().select(0);
    	
    	cbType.valueProperty().addListener(new ChangeListener<String>() {
            public void changed(@SuppressWarnings("rawtypes") ObservableValue ov, String t, String t1) {                
            	try {
            		cbFunc.getItems().clear();
					ArrayList<String> tmpList = TypeLoader.getFunctionList(cbType.getSelectionModel().getSelectedItem());
					for(int i=0; i < tmpList.size(); i++)
						cbFunc.getItems().add(tmpList.get(i));
				} 
				catch (Exception ef) { ef.printStackTrace(); }        
            }    
        });
    	
    	cbFunc = new ComboBox<String>();
    	cbFunc.setMinWidth(200);
    	try {
			ArrayList<String> tmpList = TypeLoader.getFunctionList(cbType.getSelectionModel().getSelectedItem());
			for(int i=0; i<tmpList.size(); i++)
				cbFunc.getItems().add(tmpList.get(i));
	    } catch (Exception ef) { ef.printStackTrace(); }
    	if(!cbFunc.getItems().isEmpty()) cbFunc.getSelectionModel().select(0);
    	
    	grid.add(txtName, 0, 0);
    	grid.add(txtType, 0, 1);
    	grid.add(txtFunc, 0, 2);
    	
    	grid.add(tfName, 1, 0);
    	grid.add(cbType, 1, 1);
    	grid.add(cbFunc, 1, 2);
    	
    	HBox hBoxButton = new HBox();
    	hBoxButton.setSpacing(5);           
    	hBoxButton.setPadding(new Insets(0, 0, 0, 173));
    	Button btnOk = new Button("��");
    	Button btnCancel = new Button("������");
    	btnOk.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				if(adding())
					stage.close();
			}});
    	btnCancel.setOnAction(new EventHandler<javafx.event.ActionEvent>(){
			public void handle(javafx.event.ActionEvent arg0) {
				stage.close();
			}});
    	
    	stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent arg0) {
				Main.user_enter_data_of_adding_object = false;
			}
		});
    	
    	hBoxButton.getChildren().addAll(btnOk, btnCancel);
    	
    	root.getChildren().addAll(grid, hBoxButton);
        Scene scene = new Scene(root);
        scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
		stage.setScene(scene);
		stage.show();
	}
	
	private boolean adding()
	{
		if(!(tfName.getText().equals("")))
		{
			if(tfName.getText().length() > 20)
			{
				FXDialog.showMessageDialog("��������, �� ����� ����� ��� �������� �� ����� ��������� 20 ��������", "������ ����������", Message.WARNING);
			}
			if( Main.elementsNamesList.contains( tfName.getText() ) )
			{
				FXDialog.showMessageDialog("��������, �� ������� � ����� ������ ��� ����������", "������ ����������", Message.WARNING);
			}
			else
			{
				Main.addingElemName = new String(tfName.getText());
				Main.addingElemTypeName = new String(cbType.getSelectionModel().getSelectedItem());
				Main.addingElemFuncName = new String(cbFunc.getSelectionModel().getSelectedItem());
				Main.addingElemIsElementary = false;
				Main.user_enter_data_of_adding_object=true;
				return true;
			}
        }
		else
		{
			FXDialog.showMessageDialog("����������, ������� �������� �������", "������ ����������", Message.WARNING);
		}
		return false;
	}	
}
