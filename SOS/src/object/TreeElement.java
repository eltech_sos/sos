package object;

import java.util.Map;
import java.util.Vector;

import javax.swing.tree.DefaultMutableTreeNode;

import org.jgraph.graph.DefaultGraphCell;

import simulation.SimulationCountFX;


public class TreeElement implements Cloneable
{
	
	public DefaultMutableTreeNode element;
	public DefaultGraphCell cell = null; // "���������" �� ���� ������
	public DefaultGraphCell cellLink = null;
	public DefaultGraphCell parentCell = null; //"������" �� ��������
	public String typeName;
	public String functionName;
	public int iChildrenCount = 0;
	public int ID;
	public int ParentID;
	public int Level = 0;
	public String elementName;
	public Map<String, Double> parametersValuesMap = null;
	public Map<String, Double> parametrsWeightMap = null;
	public double TimeOfCreation;
	public double elementCount=0;
//	public boolean itsCounted=false;
	
	//for functional itself - organization
	public boolean isZeroByStructure = false;
	public boolean itsNeedUpForMax = true;
	public boolean itsImportantForRootValue = true;

	public final static int T_CONT = 2;	// container
	public final static int T_CHILD = 3;	// end
	
	public int type;
	
	public TreeElement( int id,int parentID) 
	{
		this(id, parentID, TreeElement.T_CONT);
		TimeOfCreation = (double)(SimulationCountFX.t - 1);
	}
	
	public TreeElement( int id,int parentID, int type)
	{
		ID = id;
		ParentID=parentID;
//		itsCounted=false;
		this.type = type;
		TimeOfCreation = (double)(SimulationCountFX.t - 1);
	}
	
	public Object clone()
    {
        try
    {
            return super.clone();
        }
    catch( CloneNotSupportedException e )
    {
            return null;
        }
    } 
}