package object;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import Main.GraphicPanel;
import Main.Main;
import Main.TreeOperations;
import Main.TypeLoader;

public class ChangeObject extends JFrame
{
	int compare = -1;
	private Container add_object_container;
	private JTextField inf_type_object, inf_func_object;
	private JButton add_object_but;
	private JComboBox type_object, func_object;
	
	public ChangeObject()
	{
		super("��������� �������");
		addWindowListener(new WindowAdapter() 
		{
		    public void windowClosing(WindowEvent e) 
		    {
		    	setVisible(false);
		    	dispose();
		   }
		});
		add_object_container=getContentPane();
		add_object_container.setLayout(new FlowLayout(FlowLayout.CENTER, 500, 10));
		setSize(250,200);
		TreePath treePath = Main.tree.getPathForRow(1);
		inf_type_object= new JTextField("�������� ��� �������:");
		inf_type_object.setEditable(false);
		inf_type_object.setBorder(null);
		inf_type_object.setFocusable(false);
		add_object_container.add(inf_type_object);
		type_object = new JComboBox();
		compare = (treePath.toString().compareToIgnoreCase(Main.tree.getSelectionPath().toString()));
		
		
		treePath = GraphicPanel.tree.getSelectionPath();
		DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
		final TreeElement treeElem = TreeOperations.findElementInStruct(selectedNode);
		
		//create ComboBox for type selection
//		type_object.removeAllItems();
//		if( treeElem.ID == -1 )//root element
//		{
//			for(int i=0; i<TypeLoader.rootTypesList.size(); i++)
// 			{
// 				type_object.addItem( TypeLoader.rootTypesList.elementAt(i) );
// 			}
//		}
//		else
//		{
			if( treeElem.type == 2 )//intermediary element
			{
				for(int i=0; i<TypeLoader.intermediaryTypesList.size(); i++)
	 			{
	 				type_object.addItem( TypeLoader.intermediaryTypesList.get(i) );
	 			}
			}
			else//elementary element
			{
				for(int i=0; i<TypeLoader.elementaryTypesList.size(); i++)
	 			{
	 				type_object.addItem( TypeLoader.elementaryTypesList.get(i) );
	 			}
			}
//		}
		add_object_container.add(type_object);
		
        //create ComboBox for function selection
		inf_func_object= new JTextField("�������� ������� �������:");
		inf_func_object.setBorder(null);
		inf_func_object.setEditable(false);
		inf_func_object.setFocusable(false);
		add_object_container.add(inf_func_object);
		func_object = new JComboBox();
		add_object_container.add(func_object);

		func_object.removeAllItems();
//		if( treeElem.ID == -1 )//root element
//		{
//			func_object.setEnabled(false);
//			func_object.addItem("root_function");
//		}
//		else
//		{
			if( treeElem.type == 2 )//intermediary element
			{
//				Vector<String> tmpList = TypeLoader.intermediaryTypesMapForFunctions.get( type_object.getSelectedItem().toString() );
				ArrayList<String> tmpList = TypeLoader.getFunctionList( type_object.getSelectedItem().toString() );
				for(int i=0; i<tmpList.size(); i++)
				{
					func_object.addItem(tmpList.get(i));
				}   
			}
			else//elementary element
			{
//				Vector<String> tmpList = TypeLoader.elementaryTypesMapForFunctions.get( type_object.getSelectedItem().toString() );
				ArrayList<String> tmpList = TypeLoader.getFunctionList( type_object.getSelectedItem().toString() );
				for(int i=0; i<tmpList.size(); i++)
				{
					func_object.addItem(tmpList.get(i));
				}
			}
//		}
		
		type_object.addItemListener(new ItemListener()
		{
			public void itemStateChanged(ItemEvent event) 
			{
				func_object.removeAllItems();
				if( treeElem.type == 2 )//intermediary element
				{
//					Vector<String> tmpList = TypeLoader.intermediaryTypesMapForFunctions.get( type_object.getSelectedItem().toString() );
					ArrayList<String> tmpList = TypeLoader.getFunctionList( type_object.getSelectedItem().toString() );
					for(int i=0; i<tmpList.size(); i++)
					{
						func_object.addItem(tmpList.get(i));
					}   
				}
				else//elementary element
				{
//					Vector<String> tmpList = TypeLoader.elementaryTypesMapForFunctions.get( type_object.getSelectedItem().toString() );
					ArrayList<String> tmpList = TypeLoader.getFunctionList( type_object.getSelectedItem().toString() );
					for(int i=0; i<tmpList.size(); i++)
					{
						func_object.addItem(tmpList.get(i));
					}
				}
			}
		}
		);
        //�������� ������ ��� ���������� ������� � ����������
		add_object_but= new JButton("�������� ������");
		add_object_but.setFocusable(false);
		add_object_container.add(add_object_but);
		  
        //�������� ���������� ����������� ������ ButtonHandler,
		//��������������� ������� ������ add_pole_but
		ButtonHandler add_object_hand= new ButtonHandler();
		add_object_but.addActionListener(add_object_hand);
		setVisible(true);
	}
	
	
	
	private class ButtonHandler implements ActionListener
	{

		public void actionPerformed(ActionEvent event) 
		    {
			
			if(event.getSource()==add_object_but)
			{
//					JOptionPane.showMessageDialog(null,"�� �������� ������ � ������� ");
					GraphicPanel.newTypeOfObjectAfterChange = new String(type_object.getSelectedItem().toString());
					GraphicPanel.newFuncOfObjectAfterChange = new String(func_object.getSelectedItem().toString());
					setVisible(false);
					dispose();
					
			}
	    }
	}
}
