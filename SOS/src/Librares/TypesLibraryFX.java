package Librares;

import tabs.ConfirmationType;
import tabs.FXDialog;
import tabs.Message;

import Main.Main;
import Main.TreeOperations;
import Main.TypeLoader;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class TypesLibraryFX extends Application{
	private TableView<ElementaryObject> table = new TableView<ElementaryObject>();
	private ObservableList<ElementaryObject> dataTypes;
	private ObservableList<ElementaryObject> dataObjects;
	
	public static boolean libraryWasChanged = false;
	private boolean isElementaryTypes = true;
	private String Title;
	
	public TypesLibraryFX(String _title, boolean _isElementary)
	{
		isElementaryTypes = _isElementary;
		Title = _title;
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle(Title);
    	final VBox vBox = new VBox();
    	
    	int colWidth = 250;
    	if(!isElementaryTypes) colWidth = 150;
    	
    	Label inf_type_list = new Label("������ ��� ������������ �����");
    	inf_type_list.setFont(Font.font("Arial", FontWeight.BOLD, 12));
    	inf_type_list.setMinWidth(979);
    	inf_type_list.setAlignment(Pos.CENTER);
    	inf_type_list.setTextFill(Color.color(0.3, 0.3, 0.3));// Dark grey
    	        
    	// ���
    	TableColumn<ElementaryObject, String> colType = new TableColumn<ElementaryObject, String>("���");
    	colType.setCellValueFactory(new PropertyValueFactory<ElementaryObject, String>("Type"));
    	colType.setMinWidth(colWidth);
    	table.getColumns().add(colType);
    	// �������
    	TableColumn<ElementaryObject, String> colFunction = new TableColumn<ElementaryObject, String>("�������");
        colFunction.setCellValueFactory(new PropertyValueFactory<ElementaryObject, String>("Functions"));
        colFunction.setMinWidth(colWidth);
    	table.getColumns().add(colFunction);
		// ���������� ������ (�����������)
    	if(!isElementaryTypes)
    	{
    		TableColumn<ElementaryObject, String> colSlotCount = new TableColumn<ElementaryObject, String>("���������� ������");
    		colSlotCount.setCellValueFactory(new PropertyValueFactory<ElementaryObject, String>("SlotCount"));
    		colSlotCount.setMinWidth(colWidth);
    		table.getColumns().add(colSlotCount);
    	}
    	// ���������
    	TableColumn<ElementaryObject, String> colWeight = new TableColumn<ElementaryObject, String>("���������");
        colWeight.setCellValueFactory(new PropertyValueFactory<ElementaryObject, String>("Weight"));
        colWeight.setMinWidth(colWidth);
    	table.getColumns().add(colWeight);
    	// ��������� � ���������������
    	TableColumn<ElementaryObject, String> colSOEl = new TableColumn<ElementaryObject, String>("��������� � ���������������");
        colSOEl.setCellValueFactory(new PropertyValueFactory<ElementaryObject, String>("ElementInRecovery"));
        colSOEl.setMinWidth(200);
    	table.getColumns().add(colSOEl);
    	// ���� (�����������)
		if(!isElementaryTypes)
		{ 
			TableColumn<ElementaryObject, String> colChildren = new TableColumn<ElementaryObject, String>("����");
            colChildren.setCellValueFactory(new PropertyValueFactory<ElementaryObject, String>("Children"));	
            colChildren.setMinWidth(colWidth);
			table.getColumns().add(colChildren);
		}
		
		LoadTableFx();

		table.setMaxHeight(290);
        table.setMaxWidth(952);
		
        HBox hBox = new HBox();
        Button buttonCreate = new Button("������� ���");
        ButtonOnClick(buttonCreate, ButtonAction.Create);
        Button buttonRemove = new Button("������� ���");
        ButtonOnClick(buttonRemove, ButtonAction.Remove);
        Button buttonChange = new Button("�������� ���");
        ButtonOnClick(buttonChange, ButtonAction.Change);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(5, 0, 0, 0));
        hBox.getChildren().addAll(buttonCreate,buttonRemove, buttonChange);
        
        vBox.setSpacing(5);
        vBox.setPadding(new Insets(10, 0, 20, 10));
        vBox.getChildren().addAll(inf_type_list,table,hBox);
		
		Scene scene = new Scene(vBox, 972, 365);	
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
    	stage.setScene(scene);
		stage.show();
	}

	private void LoadTableFx()
	{
		String function ="";
		String price="";
		String slot="";
		String type="";
		String participation = "";
		String childrenType = "";
		int childrenTypeIndex;
		if( isElementaryTypes )
		{
			dataTypes = FXCollections.observableArrayList();
			for(int typeInd=0; typeInd<TypeLoader.elementaryTypesList.size(); typeInd++)
			{
				type = TypeLoader.elementaryTypesList.get(typeInd);
				price = String.valueOf( TypeLoader.getPrice(type) );
				participation = "false";
				if(TypeLoader.isParticipate(type)) participation = "true";
				for(int funcInd=0; funcInd<TypeLoader.typesMapForFunctions.get(type).size(); funcInd++)
				{
					function = TypeLoader.getFunctionList(type).get( funcInd );
					dataTypes.add(new ElementaryObject(type, function, price, participation));
				}
			}
		}
		else
		{
			dataObjects = FXCollections.observableArrayList();
			for(int typeInd=0; typeInd<TypeLoader.intermediaryTypesList.size(); typeInd++)
			{
				type = TypeLoader.intermediaryTypesList.get(typeInd);
				price = String.valueOf( TypeLoader.getPrice(type) );
				slot = String.valueOf( TypeLoader.getSlotsNumber(type) );
				participation = "false";
				if(TypeLoader.isParticipate(type))	participation = "true";
				childrenTypeIndex = TypeLoader.getChildrenType( type );
				if(childrenTypeIndex == 3) childrenType = "�����";
				else if(childrenTypeIndex == 1) childrenType = "�����";
				else if(childrenTypeIndex == 2) childrenType = "����������";
				for(int funcInd=0; funcInd<TypeLoader.typesMapForFunctions.get(type).size(); funcInd++)
				{
					function = TypeLoader.getFunctionList(type).get(funcInd);
					dataObjects.add(new ElementaryObject(type,function, slot, price, participation, childrenType));
				}
			}
		}
		
		if(isElementaryTypes)
			table.setItems(dataTypes);
		else
			table.setItems(dataObjects);
	}
	
	private void ButtonOnClick(Button button, final ButtonAction action){
 		button.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, new EventHandler<javafx.scene.input.MouseEvent>() {
 	        public void handle(javafx.scene.input.MouseEvent mc) {  
 	        	if(action == ButtonAction.Create)
 	        	{
 	        		Stage stage = new Stage();
 	        		try {
						(new ChangesInTypesLibraryFX("���������� ����", null, isElementaryTypes)).start(stage);
					} catch (Exception e) {
						e.printStackTrace();
					}
 	        		stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
						public void handle(javafx.stage.WindowEvent event) {
							RefreshFX();
						}
 	        		});
 	           	}
 	        	else if(action == ButtonAction.Remove)
 	        	{
 	        		int selectedIdx = table.getSelectionModel().getSelectedIndex();
 	        		if(selectedIdx >= 0)
 	        		{
 	 	        		String typeForDelete = table.getItems().get(selectedIdx).Type.getValue().toString();
 	 	        		if(!TreeOperations.findAnyElementWithType(typeForDelete))
 	 	        		{
 	 	        			boolean confirm = FXDialog.showConfirmDialog("�� ������������� ������ ������� ��� \""+typeForDelete+"\"?", "�������� ����", ConfirmationType.DELETE_OPTION);
 	 	        			if(confirm)
 	 	        			{
 	 	        				table.getItems().remove(selectedIdx);
 	 	        				TypeLoader.typesMapForFunctions.remove(typeForDelete);
 								TypeLoader.typesParametersMap.remove(typeForDelete);
 								if( isElementaryTypes )
 								{
 									TypeLoader.elementaryTypesList.remove( typeForDelete );
 									TypeLoader.saveCurrentElementaryTypeSet();
 									if( TypeLoader.elementaryTypesList.size() == 0 )
 										Main.changeButtonsEnableForCurrentSelection();
 								}
 								else
 								{
 									TypeLoader.intermediaryTypesList.remove( typeForDelete );
 									TypeLoader.saveCurrentIntermediaryTypeSet();
 									if( TypeLoader.intermediaryTypesList.size() == 0 )
 										Main.changeButtonsEnableForCurrentSelection();
 								}
 	 	        			}
 	 	        			RefreshFX();
 	 	        		}
 	 	        		else
 	 	        		{
 	 	        			FXDialog.showMessageDialog("��������, �� ������ ��� �� ����� ���� ����� \n��� ��� �� ������������ � �������� ������", "��� ������������", Message.ERROR);
 	 	        		}
 	        		}
 	        		else
 	        		{
 	        			FXDialog.showMessageDialog("����������, �������� ��� ��� ��������", "��� ���������� ����", Message.WARNING);
 	        		}
 	        	}
 	        	else if(action == ButtonAction.Change)
 	        	{
 	        		int selectedIdx = table.getSelectionModel().getSelectedIndex();
 	        		if( selectedIdx >= 0 )
 					{
 	        			String typeForEdit = table.getItems().get(selectedIdx).Type.getValue().toString();
 	        			Stage stage = new Stage();
 	 	        		try {
 							(new ChangesInTypesLibraryFX("���������� ����", typeForEdit, isElementaryTypes)).start(stage);
 						} catch (Exception e) {
 							e.printStackTrace();
 						}
 	 	        		stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
 							public void handle(javafx.stage.WindowEvent event) {
 								RefreshFX();
 							}
 	 	        		});
 					}
 					else
 						FXDialog.showMessageDialog("����������, �������� ��� ��� ���������", "��� ���������� ����", Message.WARNING);
 	        	}
 	        }
         });
 	}
	
	private void RefreshFX()
	{
		while (!table.getItems().isEmpty())	table.getItems().remove(0);
		LoadTableFx();
		libraryWasChanged = false;
	}
	
	enum ButtonAction
	{
		Create,
		Change,
		Remove
	}
	
	public class ElementaryObject{
		private SimpleStringProperty Type;
		private SimpleStringProperty Functions;
		private SimpleStringProperty SlotCount;
		private SimpleStringProperty Weight;
		private SimpleStringProperty ElementInRecovery;
		private SimpleStringProperty Children;
		
		public ElementaryObject(String _type, String _functions, String _slotCount, String _weight, String _elementInRecovery, String _children)
		{
			this.Type = new SimpleStringProperty(_type);
			this.Functions = new SimpleStringProperty(_functions);
			this.SlotCount = new SimpleStringProperty(_slotCount);
			this.Weight = new SimpleStringProperty(_weight);
			this.ElementInRecovery = new SimpleStringProperty(_elementInRecovery);
			this.Children = new SimpleStringProperty(_children);
		}
		
		public ElementaryObject(String _type, String _functions, String _weight, String _elementInRecovery)
		{
			this.Type = new SimpleStringProperty(_type);
			this.Functions = new SimpleStringProperty(_functions);
			this.Weight = new SimpleStringProperty(_weight);
			this.ElementInRecovery = new SimpleStringProperty(_elementInRecovery);
		}
		
		public String getType(){
			return Type.get();
		}
		public void setType(String _type){
			Type.set(_type);
		}
		public String getFunctions(){
			return Functions.get();
		}
		public void setFunctions(String _functions){
			Functions.set(_functions);
		}
		public String getSlotCount(){
			return SlotCount.get();
		}
		public void setSlotCount(String _slotCount){
			SlotCount.set(_slotCount);
		}
		public String getWeight(){
			return Weight.get();
		}
		public void setWeight(String _weight){
			Weight.set(_weight);
		}
		public String getElementInRecovery(){
			return ElementInRecovery.get();
		}
		public void setElementInRecovery(String _elementInRecovery){
			ElementInRecovery.set(_elementInRecovery);
		}
		public String getChildren(){
			return Children.get();
		}
		public void setChildren(String _children){
			Children.set(_children);
		}
	}

}
