package Librares;

import tabs.FXDialog;
import tabs.Message;

import Librares.TypesLibraryFX.ButtonAction;
import Main.FunctionsLoader;
import Main.Main;
import Main.ParametersLoader;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import simulation.Parser;

public class ChangesInFunctionsLibraryFX extends Application {
	private TextField tfFunc;
	private CheckBox chbIsRecovery;
	private String oldName = null, newName = null;
	private String addingParamName = null;	
	AddParametr addParametrFrame = null;
	
	public ChangesInFunctionsLibraryFX(String frameName, String functionOldName){
		newName = frameName;
		oldName = functionOldName;
	}
	
	@Override
	public void start(final Stage stage) throws Exception {
		stage.setTitle(newName);
    	VBox vBox = new VBox();
    	
    	HBox hBox = new HBox();
    	hBox.setPadding(new Insets(10, 0, 5, 5));
    	hBox.setSpacing(-2);
    	
    	String str;
    	if(oldName == null) str = "�������� �������";
    	else str = "�������� �������";
    	tfFunc = new TextField();
    	tfFunc.setMinWidth(300);
    	if(oldName != null) tfFunc.setText(oldName);
    	Button btnAddParam = new Button("+ ��������");
    	hBox.getChildren().addAll( tfFunc, btnAddParam);
    	
    	GridPane hBox2 = new GridPane();
    	hBox2.setPadding(new Insets(0,0,0,7));
    	Text txtIsRecovery = new Text("��������� � ���������������   ");
    	txtIsRecovery.setFont(Font.font("Arial", FontWeight.BOLD, 12));
    	txtIsRecovery.setFill(Color.color(0.3, 0.3, 0.3));// Dark grey
    	chbIsRecovery = new CheckBox();
    	chbIsRecovery.setSelected(true);
		if((oldName != null)&&(!FunctionsLoader.isParticipate(oldName))) chbIsRecovery.setSelected(false);
    	
		HBox hBoxBtn = new HBox();
    	if(oldName != null)	hBoxBtn.setPadding(new Insets(0,0,0,43));
    	else hBoxBtn.setPadding(new Insets(0,0,0,45));
    	Button btnAddFunc = new Button(str);
    	btnAddFunc.setMinWidth(150);
    	hBoxBtn.getChildren().add(btnAddFunc);
    	hBox2.add(txtIsRecovery, 0, 0);
    	hBox2.add(chbIsRecovery, 1, 0);
    	hBox2.add(hBoxBtn, 2, 0);
    	
    	vBox.setSpacing(5);
        vBox.setPadding(new Insets(10, 0, 0, 10));
        vBox.getChildren().addAll(hBox, hBox2);


		btnAddFunc.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			@Override
			public void handle(MouseEvent arg0) {
				if (makeChenges())
					stage.close();
			}
		});
		ButtonOnClick(btnAddParam, ButtonAction.Change);
		
		Scene scene = new Scene(vBox, 435, 90);	
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
    	stage.setScene(scene);
		stage.show();
	}

	private void ButtonOnClick(Button button, final ButtonAction action){
 		button.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, new EventHandler<javafx.scene.input.MouseEvent>() {
 	        public void handle(javafx.scene.input.MouseEvent mc) {  
 	        	if(action == ButtonAction.Change)
 	        	{
 	        		try {
						(new AddParametr()).start(new Stage());
					} catch (Exception e) {
						e.printStackTrace();
					}
 	        	}
 	        }
         });
 	}
	
	private boolean makeChenges() 
	{
		newName = tfFunc.getText();
		if((newName != null) && (newName != "")&&(newName.length() != 0))
		{
			//if we change function
			if( oldName != null)
			{
				//checking: did we change function
				if(!newName.equals(oldName))
				{
					if(FunctionsLoader.elementaryFunctionsList.contains(newName))
					{
						FXDialog.showMessageDialog("��������, �� ������� � ����� ������ ��� ������������ � ����������", "������ ���������� �������", Message.WARNING);
						return false;
					}
					if(!Parser.isFormulaCorrect(newName)) return false;
				}
			}
			//if we add function
			else
			{
				if(FunctionsLoader.elementaryFunctionsList.contains(newName))
				{
					FXDialog.showMessageDialog("��������, �� ������� � ����� ������ ��� ������������ � ����������", "������ ���������� �������", Message.WARNING);
					return false;
				}
				if(!Parser.isFormulaCorrect(newName)) return false;
			}
			saveChanges();
			FunctionsLibraryFX.libraryWasChanged = true;
			return true;
		}
		else
		{
			FXDialog.showMessageDialog("����������, ������� �������", "������� �����", Message.INFORMATION);
			return false;
		}
	}
	
	private void saveChanges()
	{
		//save to structure
		if(oldName != null)//if we change function
		{
			FunctionsLoader.functionsCharacteristicsMap.put(newName, chbIsRecovery.isSelected());
			//checking: did we change function name
			if(!newName.equals(oldName)) FunctionsLoader.changeElementaryFunctionName(oldName, newName);
		}
		else//if we add function
		{
			FunctionsLoader.elementaryFunctionsList.add(newName);
			FunctionsLoader.functionsCharacteristicsMap.put(newName, chbIsRecovery.isSelected());
		}
		//save to file
		FunctionsLoader.saveCurrentElementaryFunctionsSet();
	}
	
	class AddParametr extends Application {
		ListView<String> list;
		ObservableList<String> items;
		
		private void loadParametrsList()
		{
			items = FXCollections.observableArrayList();
			String paramInf = null;
			for(int paramInd=0;paramInd<ParametersLoader.parametrsList.size();paramInd++)
			{
				paramInf = ParametersLoader.parametrsList.get( paramInd );
				paramInf = paramInf + "(" + ParametersLoader.getLeftBorder( ParametersLoader.parametrsList.get(paramInd) ) + ";";
				paramInf = paramInf + ParametersLoader.getRightBorder( ParametersLoader.parametrsList.get(paramInd) ) + ")";
				items.add(paramInf);
			}
		}
		
		private boolean adding()
		{
			if((list.getSelectionModel().getSelectedItem() != null))
			{
				int endIndex = 0;
				endIndex = list.getSelectionModel().getSelectedItem().toString().indexOf("(");
				addingParamName = list.getSelectionModel().getSelectedItem().toString().substring( 0, endIndex );
				return true;
			}
			else
			{
				FXDialog.showMessageDialog("����������, �������� ��� ���������, ������� �� ����������� �������� � �������", "��� �� ������", Message.INFORMATION);
				return false;
			}
		}

		@Override
		public void start(final Stage stage) throws Exception {
			stage.setTitle("���������� ��������� � �������");
			VBox vBox = new VBox();	            	
        	
        	Label inf_type_list = new Label("�������� �������� ��� ������� � �������");
        	inf_type_list.setFont(Font.font("Arial", FontWeight.BOLD, 12));
        	inf_type_list.setMinWidth(447);
        	inf_type_list.setAlignment(Pos.CENTER);
        	inf_type_list.setTextFill(Color.color(0.3, 0.3, 0.3));// Dark grey
        	
        	list = new ListView<String>();
        	list.setMaxWidth(420);
        	list.setMaxHeight(315);
        	loadParametrsList();
        	list.setItems(items);
        	
        	HBox hBoxBtn = new HBox();
        	Button btnInsertParam = new Button("�������� ��������");
        	hBoxBtn.getChildren().add(btnInsertParam);
        	hBoxBtn.setPadding(new Insets(5,0,0,272));
        	btnInsertParam.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, new EventHandler<javafx.scene.input.MouseEvent>() {
	 	        public void handle(javafx.scene.input.MouseEvent mc) { 
	 	        	if (adding()){
						tfFunc.setText( tfFunc.getText()+ addingParamName);
						stage.close();
	 	        	}
	 	        }
	 		});
        	
        	vBox.setSpacing(5);
            vBox.setPadding(new Insets(10, 0, 0, 10));
        	vBox.getChildren().addAll(inf_type_list, list, hBoxBtn);
        	Scene scene = new Scene(vBox, 440, 390);	
    		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//        	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//    				new Stop(0.0, Color.rgb(75,75,75)), 
//    				new Stop(1.0, Color.rgb(25,25,25))));
    		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
    				new Stop(0.0, Color.rgb(100,100,100)), 
    				new Stop(1.0, Color.rgb(70,70,70))));
        	stage.setScene(scene);
        	stage.show();
		}
		
	}
}
