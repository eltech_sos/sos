package Librares;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map.Entry;

import tabs.ConfirmationType;
import tabs.FXDialog;
import tabs.Message;

import Main.FunctionsLoader;
import Main.Main;
import Main.TreeOperations;
import Main.TypeLoader;
import javafx.application.Application;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import simulation.Parser;

public class ChangesInTypesLibraryFX extends Application {
	private ObservableList<String> obListLibrary;
	private TableView<TypeObject> table = new TableView<TypeObject>();
	private ListView<String> libraryList;
	private TextField tfSumDiv, tfSumPow, tfPow, tfName, tfSlotCount, tfPrice;
	private CheckBox chbList, chbDist, chbIsRecovery;
	private String oldName = null, newName = null;
	private boolean isElementaryTypes = true;
	private int maxPriority = 0;
	
	public ChangesInTypesLibraryFX(String frameName, String typeOldName, boolean elementaryTypes)
	{
		isElementaryTypes = elementaryTypes;
		newName = frameName;
		oldName = typeOldName;
	}
	
	@Override
	public void start(final Stage stage) throws Exception {
    	stage.setTitle(newName);
		
    	VBox vBox = new VBox();
    	
    	//================= ������� ����� ================
    	GridPane gridTop = new GridPane();
    	gridTop.setHgap(10);
    	gridTop.setVgap(10);
    	gridTop.setPadding(new Insets(0, 10, 10, 10));
    	
    	Text txtName = new Text("��������");
    	txtName.getStyleClass().add("text-on-form");
    	Text txtSlotCount = new Text("���������� ������");
    	txtSlotCount.getStyleClass().add("text-on-form");
    	Text txtPrice = new Text("���������");
    	txtPrice.getStyleClass().add("text-on-form");
    	Text txtChildrenType = new Text("                          ��� �����");            	
    	txtChildrenType.getStyleClass().add("text-on-form");
    	Text txtList = new Text("�����");
    	txtList.getStyleClass().add("text-on-form");
    	GridPane.setHalignment(txtList, HPos.CENTER);
    	Text txtDist = new Text("����������");
    	txtDist.getStyleClass().add("text-on-form");
    	GridPane.setHalignment(txtDist, HPos.CENTER);
    	Text txtIsRecovery = new Text("��������� � ���������������");
    	txtIsRecovery.getStyleClass().add("text-on-form");
    	
    	tfName = new TextField();
    	tfSlotCount = new TextField();
    	tfPrice = new TextField();
    	chbList = new CheckBox();
    	GridPane.setHalignment(chbList, HPos.CENTER);
    	chbDist = new CheckBox();
    	GridPane.setHalignment(chbDist, HPos.CENTER);
    	chbIsRecovery = new CheckBox();
    	
    	gridTop.add(txtName, 1, 1);
    	gridTop.add(tfName, 2, 1);
    	gridTop.add(txtSlotCount, 3, 1);
    	gridTop.add(tfSlotCount, 4, 1);
    	gridTop.add(txtPrice, 1, 2);
    	gridTop.add(tfPrice, 2, 2);
    	gridTop.add(txtChildrenType, 3, 2, 4, 2);
    	
    	HBox hBoxRecovery = new HBox();    
    	hBoxRecovery.setSpacing(20);
    	hBoxRecovery.getChildren().addAll(txtIsRecovery, chbIsRecovery);
    	gridTop.add(hBoxRecovery, 1, 3, 2, 3);
    	
    	GridPane gridGhildrenType = new GridPane();
    	gridGhildrenType.setPadding(new Insets(0,0,0,57));
    	gridGhildrenType.setHgap(30);
    	gridGhildrenType.setVgap(10);
    	gridGhildrenType.add(txtList, 0, 0);
    	gridGhildrenType.add(txtDist, 1, 0);
    	gridGhildrenType.add(chbList, 0, 1);
    	gridGhildrenType.add(chbDist, 1, 1);
    	gridTop.add(gridGhildrenType, 3, 3, 4, 4);
    	//==================================================
    	
    	//================= ������� ����� ================
    	Label lblKoeff = new Label("������������ ���� ��� ������� ������");
    	lblKoeff.getStyleClass().add("label-as-title");
    	lblKoeff.setMinWidth(550);
    	
    	GridPane gridKoeff = new GridPane();
    	gridKoeff.setHgap(10);
    	gridKoeff.setVgap(10);
    	gridKoeff.setPadding(new Insets(10, 10, 10, 10));
    	
    	Text txtSumDiv = new Text("SUM(Xi*Ki)");
    	txtSumDiv.getStyleClass().add("text-on-form");
    	Text txtSumPow = new Text("               SUM(Xi^Ki)");
    	txtSumPow.getStyleClass().add("text-on-form");
    	Text txtPow = new Text("POW(Xi*Ki)");
    	txtPow.getStyleClass().add("text-on-form");
    	
    	tfSumDiv = new TextField();
    	tfSumPow = new TextField();
    	tfPow = new TextField();
    	            	
    	gridKoeff.add(txtSumDiv, 1, 0);
    	gridKoeff.add(txtSumPow, 3, 0);
    	gridKoeff.add(txtPow, 1, 1);
    	
    	gridKoeff.add(tfSumDiv, 2, 0);
    	gridKoeff.add(tfSumPow, 4, 0);
    	gridKoeff.add(tfPow, 2, 1);
    	//==================================================
    
    	//================= ������ ����� ================
    	HBox hBoxTable = new HBox();
    	hBoxTable.setPadding(new Insets(0,0,0,10));
    	
    	VBox vBoxLibraryFunc = new VBox();
    	Label lblLibraryFunc = new Label("������������ �������");
    	lblLibraryFunc.getStyleClass().add("label-as-title");
    	lblLibraryFunc.setMinWidth(200);
    	libraryList = new ListView<String>();
    	libraryList.setMaxWidth(200);
    	libraryList.setMaxHeight(290);
    	obListLibrary = FXCollections.observableArrayList();
    	vBoxLibraryFunc.getChildren().addAll(lblLibraryFunc, libraryList);
    	
    	VBox vBoxButtonThisFuncMove = new VBox();
    	vBoxButtonThisFuncMove.setPadding(new Insets(80,0,0,0));
    	vBoxButtonThisFuncMove.setSpacing(2);
    	Button btnUp = new Button("�����");
    	Button btnDown = new Button(" ���� ");
    	vBoxButtonThisFuncMove.getChildren().addAll(btnUp, btnDown);
    	
    	VBox vBoxButtonsMove = new VBox();
    	vBoxButtonsMove.setPadding(new Insets(81,2,2,2));
    	vBoxButtonsMove.setSpacing(2);
    	vBoxButtonsMove.setAlignment(Pos.CENTER);
    	Button btnSingleToRight = new Button(">"); 
    	Button btnSingleToLeft = new Button("<"); 
    	Button btnAllToRight = new Button(">>"); 
    	Button btnAllToLeft = new Button("<<"); 
    	vBoxButtonsMove.getChildren().addAll(btnSingleToRight, btnSingleToLeft, btnAllToRight, btnAllToLeft, vBoxButtonThisFuncMove);
    	
    	VBox vBoxThisTypeFunc = new VBox();
    	Label lblThisTypeFunc = new Label("������� ������� ����");
    	lblThisTypeFunc.getStyleClass().add("label-as-title");
    	lblThisTypeFunc.setMinWidth(300);
    	
    	// ���������
    	TableColumn<TypeObject, Integer> colPriority = new TableColumn<TypeObject, Integer>("���������");
    	colPriority.setCellValueFactory(new PropertyValueFactory<TypeObject, Integer>("Priority"));
    	colPriority.setMinWidth(100);
    	table.getColumns().add(colPriority);
    	
    	// �������
    	TableColumn<TypeObject, String> colFunction = new TableColumn<TypeObject, String>("�������");
        colFunction.setCellValueFactory(new PropertyValueFactory<TypeObject, String>("Function"));
        colFunction.setMinWidth(200);
    	table.getColumns().add(colFunction);
    	table.setMaxHeight(290);
    	table.setMaxWidth(302);
    	vBoxThisTypeFunc.getChildren().addAll(lblThisTypeFunc, table);
    	
    	hBoxTable.getChildren().addAll(vBoxLibraryFunc, vBoxButtonsMove, vBoxThisTypeFunc);
    	
    	Button btnAddType = new Button("�������� ���");
    	HBox hBoxAddBtn = new HBox();
    	hBoxAddBtn.setPadding(new Insets(10,0,0,474));
    	hBoxAddBtn.getChildren().add(btnAddType);
    	//==================================================
    	
    	//================= ���������� ���� ================
    	txtSlotCount.setVisible(!isElementaryTypes);
    	tfSlotCount.setVisible(!isElementaryTypes);
    	gridGhildrenType.setVisible(!isElementaryTypes);
    	txtChildrenType.setVisible(!isElementaryTypes);
    	
    	if(oldName != null)
		{
			tfName.setText(oldName);
			tfSlotCount.setText(String.valueOf(TypeLoader.getSlotsNumber(oldName)));
			tfPrice.setText(String.valueOf(TypeLoader.getPrice(oldName)));       			
			chbDist.setSelected(TypeLoader.getChildrenType(oldName) != 1);
			chbList.setSelected(TypeLoader.getChildrenType(oldName) != 2);
			chbIsRecovery.setSelected(TypeLoader.isParticipate(oldName));
   			tfSumDiv.setText(Float.toString(TypeLoader.getFactorForSumMultFunction(oldName)));
   			tfSumPow.setText(Float.toString(TypeLoader.getFactorForSumPowFunction(oldName)));
   			tfPow.setText(Float.toString(TypeLoader.getFactorForPowMultFunction(oldName)));
   			
   			int this_type_functions_count = 0;
	    	Iterator<Entry<String, Integer>> iter = TypeLoader.getFunctionPriorityMap(oldName).entrySet().iterator();
			while(iter.hasNext())
			{
				Entry<String, Integer> funcInform = (Entry<String, Integer>) iter.next();
				table.getItems().add(new TypeObject(funcInform.getValue() , funcInform.getKey()));
	        	upElementIfNeed(this_type_functions_count);
	        	this_type_functions_count++;
			}
			maxPriority = table.getItems().get(table.getItems().size() - 1).getPriority();
			
			btnAddType.setText("�������� ���");
		}
    	else
    	{
    		chbDist.setSelected(true);
			chbList.setSelected(true);
			chbIsRecovery.setSelected(true);
			tfSumDiv.setText("1");
			tfSumPow.setText("1");
			tfPow.setText("1");
    	}
    	
    	if(isElementaryTypes)
		{
			for(int i=0; i < FunctionsLoader.elementaryFunctionsList.size(); i++)
				obListLibrary.add(FunctionsLoader.elementaryFunctionsList.get(i));
		}
		else
		{
			for(int i=0; i < FunctionsLoader.intermediaryFunctionsList.size(); i++)
				obListLibrary.add(FunctionsLoader.intermediaryFunctionsList.get(i));
		}
    	libraryList.setItems(obListLibrary);
    	//==================================================
    	
    	//================= ��������� ������� ================
    	MoveButtonOnClick(btnSingleToRight, MoveButtonAction.SingleToRight);
    	MoveButtonOnClick(btnSingleToLeft, MoveButtonAction.SingleToLeft);
    	MoveButtonOnClick(btnAllToRight, MoveButtonAction.AllToRight);
    	MoveButtonOnClick(btnAllToLeft, MoveButtonAction.AllToLeft);
    	MoveButtonOnClick(btnUp, MoveButtonAction.SingleUp);
    	MoveButtonOnClick(btnDown, MoveButtonAction.SingleDown);
    	btnAddType.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent event) {
				if(makeChenges())
					stage.close();
			}    		
    	});
    	//==================================================
    	
    	vBox.getChildren().addAll(gridTop, lblKoeff, gridKoeff, hBoxTable, hBoxAddBtn);
    	Scene scene = new Scene(vBox, 605, 560);	
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
		stage.setScene(scene);
		stage.show();
	}

	private void upElementIfNeed(int rowForMovingIndex) 
	{
		boolean correctPosition = false;
		while( ! correctPosition )
		{
			if(rowForMovingIndex == 0) return;
			//up element if its need
			if(table.getItems().get(rowForMovingIndex - 1).getPriority() > table.getItems().get(rowForMovingIndex).getPriority() )
			{
				TypeObject temp = table.getItems().get(rowForMovingIndex);
				table.getItems().remove(rowForMovingIndex);
				table.getItems().add(rowForMovingIndex-1, temp);
				rowForMovingIndex--;
			}
			else return;
		}
	}
	
	private void MoveButtonOnClick(Button button, final MoveButtonAction action){
 		button.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, new EventHandler<javafx.scene.input.MouseEvent>() {
 	        public void handle(javafx.scene.input.MouseEvent mc) {  
 	        	if(action == MoveButtonAction.SingleToRight)
 	        	{
 	        		if(libraryList.getSelectionModel().getSelectedIndex() >= 0)
 					{
 						boolean this_is_new = true;
 						for(int funcInTypeInd=0; funcInTypeInd < table.getItems().size(); funcInTypeInd++)
 						{
 							if(table.getItems().get(funcInTypeInd).getFunction().equals(libraryList.getSelectionModel().getSelectedItem()))
 							{
 								this_is_new = false;
 								break;
 							}
 						}
 						if(this_is_new)
 						{
 							table.getItems().add(new TypeObject(maxPriority+1, String.valueOf(libraryList.getSelectionModel().getSelectedItem())));
 							maxPriority++;
 						}
 						else
 						{
 							FXDialog.showMessageDialog("��������, �� ��� ������� ��� ������������ � ������ ����", "������ ����������", Message.WARNING);
 						}						
 					}
 					else
 					{
 						FXDialog.showMessageDialog("����������, �������� �������", "������ ����������", Message.WARNING);		
 					}
 	           	}
 	        	else if(action == MoveButtonAction.SingleToLeft)
 	        	{
 	        		if(table.getSelectionModel().getSelectedIndex() >= 0)
 					{
	 	        		boolean confirm = FXDialog.showConfirmDialog("�� ������������� ������ ������� �������: "+table.getSelectionModel().getSelectedItem().getFunction()+"?", "�������� ����", ConfirmationType.DELETE_OPTION);
 						if(confirm)
 						{
 							int tmpInt = table.getSelectionModel().getSelectedIndex();
 							table.getItems().remove(tmpInt);
 							if(tmpInt < table.getItems().size())
 								upPriorityFromRow( tmpInt );
 							maxPriority--;
 						}
 					}
 					else
 					{
 						FXDialog.showMessageDialog("����������, �������� �������", "������ ����������", Message.WARNING);			
 					}	
 	        	}
 	        	else if(action == MoveButtonAction.AllToRight)
 	        	{
 	        		boolean this_is_new;
 					for(int libInd=0; libInd < libraryList.getItems().size(); libInd++)	
 					{
 						this_is_new=true;
 						for(int funcInTypeInd=0; funcInTypeInd < table.getItems().size(); funcInTypeInd++)
 						{
 							if(table.getItems().get(funcInTypeInd).getFunction().equals(libraryList.getItems().get(libInd)))
 							{
 								this_is_new = false;
 								break;
 							}
 						}
 						if(this_is_new)
 						{
 							table.getItems().add(new TypeObject( maxPriority+1, libraryList.getItems().get(libInd)));
 							maxPriority++;
 						}
 					}
 	        	}
 	        	else if(action == MoveButtonAction.AllToLeft)
 	        	{
 	        		boolean confirm = FXDialog.showConfirmDialog("�� ������������� ������ ������� ��� �������?", "�������� ����", ConfirmationType.DELETE_OPTION);
 					if(confirm)
 					{
 						table.getItems().clear();
 						maxPriority = 0;
 					}	
 	        	}
 	        	else if(action == MoveButtonAction.SingleUp)
 	        	{
 	        		if(table.getSelectionModel().getSelectedIndex() >= 0)
 					{
 						if(table.getSelectionModel().getSelectedIndex() > 0) 
 						{
 							int selIdx = table.getSelectionModel().getSelectedIndex();
 							TypeObject temp = table.getItems().get(selIdx);
 							temp.setPriority(temp.getPriority()-1);
 							table.getItems().remove(selIdx);
 							table.getItems().add(selIdx-1, temp);
 							table.getItems().get(selIdx).setPriority(temp.getPriority() + 1);
 							table.getSelectionModel().select(selIdx - 1);
 						}
 						else
 						{
 							FXDialog.showMessageDialog("������� ����� ������������ ���������", "������ ����������", Message.WARNING);
 						}
 					}
 					else
 					{
 						FXDialog.showMessageDialog("����������, �������� �������", "������ ����������", Message.WARNING);
 					}
 	        	}
 	        	else if(action == MoveButtonAction.SingleDown)
 	        	{
 	        		if(table.getSelectionModel().getSelectedIndex() >= 0)
 					{
 						if(table.getSelectionModel().getSelectedIndex() < table.getItems().size() - 1) 
 						{
 							int selIdx = table.getSelectionModel().getSelectedIndex();
 							TypeObject temp = table.getItems().get(selIdx);
 							temp.setPriority(temp.getPriority()+1);
 							table.getItems().remove(selIdx);
 							table.getItems().add(selIdx+1, temp);
 							table.getItems().get(selIdx).setPriority(temp.getPriority() - 1);
 							table.getSelectionModel().select(selIdx + 1);
 						}
 						else
 						{
 	 	        			FXDialog.showMessageDialog("������� ����� ����������� ���������", "������ ����������", Message.WARNING);
 						}
 					}
 					else
 					{
 						FXDialog.showMessageDialog("����������, �������� �������", "������ ����������", Message.WARNING);
 					}
 	        	}
 	        }
         });
 	}
	
	private void upPriorityFromRow(int rowInd) 
	{
		for(int ind = rowInd; ind < table.getItems().size(); ind++)	
			table.getItems().get(ind).setPriority(table.getItems().get(ind).getPriority() - 1);
	}
	
	private boolean makeChenges() 
	{
		String testedField;
		int fieldValue;

		// Convolution indexes checking for SUM(Xi*Ki)
		testedField = tfSumDiv.getText();
		if( (testedField != null) && ( testedField.length() > 0 ) )
		{
			try
			{
				if ( Float.parseFloat( testedField ) <= 0 )
				{
					FXDialog.showMessageDialog("��������, �� ����������� ������ ������ ���� ������ 0", "������ ����������", Message.WARNING);
					return false;
				}
			}
			catch(Exception e)
			{
				FXDialog.showMessageDialog("����������, ��������� ������������ ���������� ����� ������������� ������", "������ ����������", Message.WARNING);
				return false;
			}
		}
		else
		{
			FXDialog.showMessageDialog("����������, ������� ����������� ������ ��� ������� SUM(Xi*Ki)", "������ ����������", Message.WARNING);
			return false;
		}
		//for SUM(Xi^Ki)
		testedField = tfSumPow.getText();
		if( (testedField != null) &&
				( testedField.length() > 0 ) )
		{
			try
			{
				if ( Float.parseFloat( testedField ) <= 0 )
				{
					FXDialog.showMessageDialog("��������, �� ����������� ������ ������ ���� ������ 0", "������ ����������", Message.WARNING);
					return false;
				}
			}
			catch(Exception e)
			{
				FXDialog.showMessageDialog("����������, ��������� ������������ ���������� ����� ������������� ������", "������ ����������", Message.WARNING);
				return false;
			}
		}
		else
		{
			FXDialog.showMessageDialog("����������, ������� ����������� ������ ��� ������� SUM(Xi^Ki)", "������ ����������", Message.WARNING);
			return false;
		}
		//for POW(Xi*Ki)
		testedField = tfPow.getText();
		if( (testedField != null) && ( testedField.length() > 0 ) )
		{
			try
			{
				if ( Float.parseFloat( testedField ) <= 0 )
				{
					FXDialog.showMessageDialog("��������, �� ����������� ������ ������ ���� ������ 0", "������ ����������", Message.WARNING);
					return false;
				}
			}
			catch(Exception e)
			{
				FXDialog.showMessageDialog("����������, ��������� ������������ ���������� ����� ������������� ������", "������ ����������", Message.WARNING);
				return false;
			}
		}
		else
		{
			FXDialog.showMessageDialog("����������, ������� ����������� ������ ��� ������� POW(Xi*Ki)", "������ ����������", Message.WARNING);
			return false;
		}
		
		
		//children type selection and slots count checking	
		//(only for intermediates types)	
		if( ! isElementaryTypes )
		{
			//children type selection checking
			if( ( ! chbDist.isSelected() ) && ( ! chbList.isSelected() ) )
			{
				FXDialog.showMessageDialog("��������, �� ��� �������� ������ ���� ������ �� ������� ���� ���� ��� �����", "������ ����������", Message.WARNING);
				return false;
			}
			else
			{
				if( ( ! chbDist.isSelected() ) || ( ! chbList.isSelected() )  )
				{
					if( TreeOperations.findAnyElemNotCorrespondingSelectedChildType( oldName, chbList.isSelected() ) )
					{
						FXDialog.showMessageDialog("��������, �� � �������� ������ ���������� �������" +
								"\n������� ����� �� ����������� ���� ��� ��������� ���� ���� �����", "������ ����������", Message.WARNING);
						return false;
					}
				}
			}
			
			//slots count checking
			testedField = tfSlotCount.getText();
			if( (testedField != null) && ( testedField.length() > 0 ) )
			{
				try
				{
					fieldValue = Integer.parseInt( testedField );
					if( fieldValue > 0 )
					{
						if( fieldValue > 15 )
						{
							FXDialog.showMessageDialog("���������� ������ � ���� �� ������ ��������� 15", "������ ����������", Message.WARNING);
							return false;
						}
						if( TreeOperations.findAnyElementWithTypeAndChildrenNumberMoreThen( oldName, fieldValue ) )
						{
							FXDialog.showMessageDialog("��������, �� � �������� ������ ���������� �������" +
									"\n������������ �������, ��� ������� ���� ���������� ������", "������ ����������", Message.WARNING);
							return false;
						}
					}
					else
					{
						FXDialog.showMessageDialog("���������� ������ � ���� ������ ���� ������ ����", "������ ����������", Message.WARNING);
						return false;
					}
				}
				catch(Exception e)
				{
					FXDialog.showMessageDialog("����������, ��������� ������������ ���������� ���� '���������� ������'", "������ ����������", Message.WARNING);
					return false;
				}
			}
			else
			{
				FXDialog.showMessageDialog("����������, ��������� ���� '���������� ������'", "������ ����������", Message.WARNING);
				return false;
			}
		}
		
		
		//cost count checking		
		testedField = tfPrice.getText();
		if( (testedField != null) && ( testedField.length() > 0 ) )
		{
			try
			{
				float fieldValueFloat = Float.parseFloat( testedField );
				if( fieldValueFloat > 0 )
				{
					if( fieldValueFloat > 10000 )
					{
						FXDialog.showMessageDialog("��������� ���� �� ������ ��������� 10 000", "������ ����������", Message.WARNING);
						return false;
					}
				}
				else
				{
					FXDialog.showMessageDialog("��������� ���� ������ ���� ������ ����", "������ ����������", Message.WARNING);
					return false;
				}
			}
			catch(Exception e)
			{
				FXDialog.showMessageDialog("����������, ��������� ������������ ���������� ���� '���������'", "������ ����������", Message.WARNING);
				return false;
			}
		}
		else
		{
			FXDialog.showMessageDialog("����������, ��������� ���� '���������'", "������ ����������", Message.WARNING);
			return false;
		}
		
		//existing functions checking
		if( table.getItems().size() == 0 )
		{
			FXDialog.showMessageDialog("��������, �� ��� ������ ��������� ��� ������� ���� �������", "������ ����������", Message.WARNING);
			return false;
		}
		
		//name checking
		newName = tfName.getText();
		if( ( newName != null ) && ( newName != "" ) && ( newName.length() > 0 ))
		{
			//if we change type
			if( oldName != null)
			{				
				if( ! newName.equals( oldName ) )//checking: did we change type
				{
					if( TypeLoader.elementaryTypesList.contains( newName ) || TypeLoader.intermediaryTypesList.contains( newName ))
					{
						FXDialog.showMessageDialog("��������, �� ��� � ����� ������ ��� ������������ � ����������", "������ ����������", Message.WARNING);
						return false;
					}
					if(newName.length() > 34) 
					{
						FXDialog.showMessageDialog("����� �������� ���� �� ����� ��������� 34 �������", "������ ����������", Message.WARNING);
						return false;
					}
					for(int i=0;i<newName.length();i++)
					{
						if( ( !Parser.onlyDigitCollection.contains( newName.charAt(i) ) ) && ( !Parser.literalCollection.contains( newName.charAt(i) ) ) )
						{
							FXDialog.showMessageDialog("�������� ���� ����� ��������� ������ ����� ���������� ��������, ����� � ���� ������� �������������", "������ ����������", Message.WARNING);
							return false;
						}
					}
				}
			}
			//if we add type
			else
			{
				if( TypeLoader.elementaryTypesList.contains( newName ) || TypeLoader.intermediaryTypesList.contains( newName ))
				{
					FXDialog.showMessageDialog("��������, �� ��� � ����� ������ ��� ������������ � ����������", "������ ����������", Message.WARNING);
					return false;
				}
				if(newName.length() > 34) 
				{
					FXDialog.showMessageDialog("����� �������� ���� �� ����� ��������� 34 �������", "������ ����������", Message.WARNING);
					return false;
				}
				for(int i=0;i<newName.length();i++)
				{
					if( ( !Parser.onlyDigitCollection.contains( newName.charAt(i) ) ) && ( !Parser.literalCollection.contains( newName.charAt(i) ) ) )
					{
						FXDialog.showMessageDialog("�������� ���� ����� ��������� ������ ����� ���������� ��������, ����� � ���� ������� �������������", "������ ����������", Message.WARNING);
						return false;
					}
				}
			}
			
			saveChanges();
			TypesLibraryFX.libraryWasChanged = true;
			return true;
			
		}
		else
		{
			FXDialog.showMessageDialog("����������, ������� ��� ����", "������ ����������", Message.WARNING);
			return false;
		}
	}
	
	private void saveChanges()
	{		
		//save to structure
		//create characteristics list for current type and put it to typesParametersMap
    	ArrayList<Float> typeCharacteristicsList = new ArrayList<Float>();
    	typeCharacteristicsList.add(Float.valueOf(tfSumDiv.getText()));
    	typeCharacteristicsList.add(Float.valueOf(tfSumPow.getText()));
    	typeCharacteristicsList.add(Float.valueOf(tfPow.getText()));
    	if(!isElementaryTypes) typeCharacteristicsList.add( Float.parseFloat( tfSlotCount.getText() ) );
    	else typeCharacteristicsList.add(0f);
    	
    	typeCharacteristicsList.add(Float.parseFloat(tfPrice.getText()));
    	
    	if(chbIsRecovery.isSelected()) typeCharacteristicsList.add(1f);
    	else typeCharacteristicsList.add(0f);

    	if(chbDist.isSelected() && chbList.isSelected()) typeCharacteristicsList.add(3f);
    	else if(chbDist.isSelected()) typeCharacteristicsList.add(2f);
    	else typeCharacteristicsList.add(1f);
    	
    	TypeLoader.typesParametersMap.put( tfName.getText() , typeCharacteristicsList);
    	
    	//create function list for current type and write it to corresponding TypesMapForFunctions
    	ArrayList<String> tmpFunctionList = new ArrayList<String>();
    	for(int i=0; i< table.getItems().size(); i++)
    		tmpFunctionList.add(table.getItems().get(i).getFunction() + "|" + table.getItems().get(i).getPriority());

    	TypeLoader.typesMapForFunctions.put(newName, tmpFunctionList);
		if(oldName != null)//if we change type
		{
			if(!newName.equals(oldName))//type name was changed 
			{
				TypeLoader.typesMapForFunctions.remove( oldName );
				if( isElementaryTypes )
				{
					TypeLoader.elementaryTypesList.remove( oldName );
					TypeLoader.elementaryTypesList.add( newName );
				}
				else
				{
					TypeLoader.intermediaryTypesList.remove( oldName );
					TypeLoader.intermediaryTypesList.add( newName );
				}
				TypeLoader.typesParametersMap.remove( oldName );
				//if some model is opened - make changes in it	
				if(Main.count > 0) TreeOperations.changeTypeNameInAllElementsInTree( oldName, newName );
			}
			//if some model is opened - make changes in it		
			if(Main.count > 0) TreeOperations.checkAllFunctionsOfChangedType(newName);
		}
		else // If we add type
		{
			if(isElementaryTypes) TypeLoader.elementaryTypesList.add(newName);
			else TypeLoader.intermediaryTypesList.add(newName);
		}
		Main.changeButtonsEnableForCurrentSelection();
		// Save to file
		if(isElementaryTypes) TypeLoader.saveCurrentElementaryTypeSet();
		else TypeLoader.saveCurrentIntermediaryTypeSet();
	}

	public class TypeObject
	{
		private SimpleIntegerProperty Priority;
		private SimpleStringProperty Function;
		
		public TypeObject(Integer _prioriry, String _function)
		{
			this.Priority = new SimpleIntegerProperty(_prioriry);
			this.Function = new SimpleStringProperty(_function);
		}
		
		public Integer getPriority()
		{
			return this.Priority.get();
		}
		
		public void setPriority(Integer _priority)
		{
			this.Priority.set(_priority);
		}
		
		public String getFunction()
		{
			return this.Function.get();
		}
		
		public void setFunction(String _function)
		{
			this.Function.set(_function);
		}
	}
	
	enum MoveButtonAction
	{
		SingleToRight,
		SingleToLeft,
		AllToRight,
		AllToLeft,
		SingleUp,
		SingleDown,
		Changes
	}
}
