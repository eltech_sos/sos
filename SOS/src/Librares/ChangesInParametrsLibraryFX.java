package Librares;

import java.util.ArrayList;

import tabs.FXDialog;
import tabs.Message;

import Main.FunctionsLoader;
import Main.GraphicPanel;
import Main.Main;
import Main.ParametersLoader;
import Main.TreeOperations;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import simulation.Parser;

public class ChangesInParametrsLibraryFX extends Application {
	private CheckBox tfIsRecovery;
	private TextField tfName, tfBottomBorder, tfTopBorder, tfStartValue;
	
	private String real_min_border_value = "", real_max_border_value = "";

	private Float newLeftBorder, newRightBorder, newInitialValue;
	
	private String oldName = null, newName = null;
	
	public ChangesInParametrsLibraryFX(String frameName, String oldParamName)
	{
		newName = frameName;
		oldName = oldParamName;
	}
	@Override
	public void start(final Stage stage) throws Exception {
		stage.setTitle(newName);
		
		GridPane gPane = new GridPane();
    	gPane.setHgap(10);
    	gPane.setVgap(15);
    	gPane.setPadding(new Insets(10, 10, 0, 10));  
    	
    	Text txtName = new Text("��������");
    	txtName.getStyleClass().add("text-on-form");
    	Text txtBottomBorder = new Text("������ �������");
    	txtBottomBorder.getStyleClass().add("text-on-form");
    	Text txtTopBorder = new Text("������� �������");
    	txtTopBorder.getStyleClass().add("text-on-form");
    	Text txtStartValue = new Text("��������� ��������");
    	txtStartValue.getStyleClass().add("text-on-form");
    	Text txtIsRecovery = new Text("��������� � ���������������");
    	txtIsRecovery.getStyleClass().add("text-on-form");
    	
    	gPane.add(txtName, 0, 0);
    	gPane.add(txtBottomBorder, 0, 1);
    	gPane.add(txtTopBorder, 0, 2);
    	gPane.add(txtStartValue, 0, 3);
    	gPane.add(txtIsRecovery, 0, 4);

    	tfName = new TextField();
    	tfBottomBorder = new TextField();
    	tfTopBorder = new TextField();
    	tfStartValue = new TextField();
    
    	GridPane hBox = new GridPane();
    	hBox.setHgap(10);           	
    	tfIsRecovery = new CheckBox();
    	Button btnAddParam = new Button("�������� ��������");
    	hBox.add(tfIsRecovery, 0, 0);
    	hBox.add(btnAddParam, 1, 0);
    	if( oldName != null )
		{
    		tfName.setText(oldName);
    		btnAddParam.setText("�������� ��������");
    		tfBottomBorder.setText(ParametrsLibraryFX.newLeftBorder.toString());
    		tfTopBorder.setText(ParametrsLibraryFX.newRightBorder.toString());
    		tfStartValue.setText(ParametrsLibraryFX.newInitialValue.toString());
		}
    	else
    	{
    		tfBottomBorder.setText("-1.0");
    		tfTopBorder.setText("1.0");
    		tfStartValue.setText("0.0");
    	}
    	real_min_border_value = tfBottomBorder.getText();
    	real_max_border_value = tfTopBorder.getText();
    	
    	tfIsRecovery.setSelected(true);
		if((oldName != null)&&(!ParametrsLibraryFX.newParticipation))
		{
			tfIsRecovery.setSelected(false);
			tfTopBorder.setText(tfStartValue.getText());
			tfBottomBorder.setText(tfStartValue.getText());			
			tfTopBorder.setDisable(true);
			tfBottomBorder.setDisable(true);
		}
		
		tfIsRecovery.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent evt) {
				if(tfIsRecovery.isSelected())
				{
					tfTopBorder.setText(real_max_border_value);
        			tfBottomBorder.setText(real_min_border_value);			
        			tfTopBorder.setDisable(false);
        			tfBottomBorder.setDisable(false);
				}
				else
				{
					tfTopBorder.setText(tfStartValue.getText());
					tfBottomBorder.setText(tfStartValue.getText());			
        			tfTopBorder.setDisable(true);
        			tfBottomBorder.setDisable(true);
				}
			}
		});
		
		
		tfStartValue.setOnKeyReleased(new EventHandler<javafx.scene.input.KeyEvent>(){
			public void handle(javafx.scene.input.KeyEvent evt) {
				if(!tfIsRecovery.isSelected())
				{
					tfTopBorder.setText(tfStartValue.getText());
					tfBottomBorder.setText(tfStartValue.getText());
				}
			}
		});
		
		btnAddParam.setOnMouseClicked(new EventHandler<javafx.scene.input.MouseEvent>(){
			public void handle(MouseEvent arg0) {
				if (makeChenges())
					stage.close();
			}
		});
		
    	gPane.add(tfName, 1, 0);
    	gPane.add(tfBottomBorder, 1, 1);
    	gPane.add(tfTopBorder, 1, 2);
    	gPane.add(tfStartValue, 1, 3);
    	gPane.add(hBox, 1, 4);
	
		Scene scene = new Scene(gPane, 400, 195);	
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
		stage.setScene(scene);
		stage.show();
	}

	private boolean makeChenges()
	{
		String name, left_border, right_border, init_value;
		name = tfName.getText();
		left_border = tfBottomBorder.getText();
		right_border = tfTopBorder.getText();
		init_value = tfStartValue.getText();
		if( ( ! ( name.equals("") ) )
				&& ( ! ( left_border.equals("") ) )
				&& ( ! ( right_border.equals("") ) ) )
		{
			//name checking
			if( ( ( oldName != null ) && ( !oldName.equals(name) ) ) || ( oldName == null ) )
			{
				if( ParametersLoader.parametrsList.contains(name) )
				{
					FXDialog.showMessageDialog("�������� � ����� ������ ��� ������������ � ����������", "������ ����������", Message.WARNING);	 
					return false;
				}
			}
			if(name.length() > 20) 
			{
				FXDialog.showMessageDialog("����� �������� ��������� �� ����� ��������� 20 ��������", "������ ����������", Message.WARNING);	 
				return false;
			}
			if( name.equals("t") ) 
			{
				FXDialog.showMessageDialog("��� t ��������������� ��� ����������� �������������", "������ ����������", Message.WARNING);	 
				return false;
			}
			if( Parser.onlyDigitCollection.contains( name.charAt(0) ) ) 
			{
				FXDialog.showMessageDialog("�������� ��������� �� ����� ���������� � �����", "������ ����������", Message.WARNING);	 
				return false;
			}
			for(int i=0;i<name.length();i++)
			{
				if( ( !Parser.onlyDigitCollection.contains( name.charAt(i) ) ) &&
						( !Parser.literalCollection.contains( name.charAt(i) ) ) )
				{
					FXDialog.showMessageDialog("�������� ��������� ����� ��������� ������ ����� ���������� ��������,\n����� � ���� ������� �������������", "������ ����������", Message.WARNING);	 
					return false;
				}
			}
			
			//borders and initial value checking
			try
			{
				newLeftBorder = Float.parseFloat( left_border );
				newRightBorder = Float.parseFloat( right_border );
				newInitialValue = Float.parseFloat( init_value );
				if ((newRightBorder - newLeftBorder) > 0 || !tfIsRecovery.isSelected())
				{
					if( tfIsRecovery.isSelected() && ( ( newInitialValue<newLeftBorder ) || ( newInitialValue>newRightBorder ) ) )
					{
						FXDialog.showMessageDialog("��������� �������� �� �������� � ��������", "������ ����������", Message.WARNING);	 
						return false;
					}
					else
					{
						if( name.equals(ParametrsLibraryFX.newName) &&
								newLeftBorder == ParametrsLibraryFX.newLeftBorder &&
								newRightBorder == ParametrsLibraryFX.newRightBorder &&
								newInitialValue == ParametrsLibraryFX.newInitialValue &&
										tfIsRecovery.isSelected() == ParametrsLibraryFX.newParticipation )
						{
							ParametrsLibraryFX.libraryWasChanged = false;
							return false;
						}
                		ParametrsLibraryFX.libraryWasChanged = true;
                		ParametrsLibraryFX.newInitialValue = newInitialValue;
                		ParametrsLibraryFX.newLeftBorder = tfIsRecovery.isSelected()? newLeftBorder : Float.parseFloat(real_min_border_value);
                		ParametrsLibraryFX.newRightBorder = tfIsRecovery.isSelected()? newRightBorder : Float.parseFloat(real_max_border_value);
                		ParametrsLibraryFX.newName = name;
                		ParametrsLibraryFX.oldName = oldName;
                		ParametrsLibraryFX.newParticipation = tfIsRecovery.isSelected();
						
                		saveChanges();
                		
                		return true;
					}
				}
				else
				{
					FXDialog.showMessageDialog("������� ������� ������ ���� ������ ������", "������ ����������", Message.WARNING);	 
					return false;
				}
			}
			catch(Exception e)
			{
				FXDialog.showMessageDialog("����������, ��������� ������������ ���������� �������� �����", "������ ����������", Message.WARNING);	 
				return false;
			}
			
		}
		else
		{
			FXDialog.showMessageDialog("����������, ������� �������� ���������, ��� ������� � ��������� ��������", "������ ����������", Message.WARNING);	 
			return false;
		}
		
	}
	
	private void saveChanges()
	{
		boolean parameterBordersWasChanged = false;
		if( ParametrsLibraryFX.oldName != null )
		{
			if( ParametersLoader.getLeftBorder(oldName) != ParametrsLibraryFX.newLeftBorder
					|| ParametersLoader.getRightBorder(oldName) != ParametrsLibraryFX.newRightBorder )
			{
				parameterBordersWasChanged = true;
			}
		}
		
		//save changes to the structure
		ArrayList<Float> parametrCharacteristicsList = new ArrayList<Float>();
		parametrCharacteristicsList.add(ParametrsLibraryFX.newLeftBorder);
		parametrCharacteristicsList.add(ParametrsLibraryFX.newRightBorder);
		parametrCharacteristicsList.add(ParametrsLibraryFX.newInitialValue);
		if( ParametrsLibraryFX.newParticipation )
		{
			parametrCharacteristicsList.add( 1f );
		}
		else
		{
			parametrCharacteristicsList.add( 0f );
		}
		
		//if adding new element
		if( ParametrsLibraryFX.oldName == null )
		{
			ParametersLoader.parametrsList.add( ParametrsLibraryFX.newName );
			ParametersLoader.parametrsCharacteristicsMap.put( ParametrsLibraryFX.newName, parametrCharacteristicsList );
		}
		else//if changing element
		{
			//if element name was NO changed
			if (ParametrsLibraryFX.oldName.equals( ParametrsLibraryFX.newName ) )
			{
				ParametersLoader.parametrsCharacteristicsMap.put( ParametrsLibraryFX.newName, parametrCharacteristicsList );
			}
			else//if element name was changed
			{
				int oldIndex = ParametersLoader.parametrsList.indexOf(ParametrsLibraryFX.oldName);
				ParametersLoader.parametrsList.set( oldIndex, ParametrsLibraryFX.newName );
				ParametersLoader.parametrsCharacteristicsMap.put( ParametrsLibraryFX.newName, parametrCharacteristicsList );
				ParametersLoader.parametrsCharacteristicsMap.remove( ParametrsLibraryFX.oldName );
				FunctionsLoader.replaceParameterNameInAllElementaryFunctions( oldName, ParametrsLibraryFX.newName );
			}
			
			if( parameterBordersWasChanged )
			{
				TreeOperations.checkAllBordersOfChangedParametr( ParametrsLibraryFX.newName );
			}
			
			if( Main.structTreeElements.size() > 0 )
			{
				GraphicPanel.refreshTable();
			}
		}
		
		//save changes to the file
		ParametersLoader.saveCurrentParametrsSet();
		
	}
	
}
