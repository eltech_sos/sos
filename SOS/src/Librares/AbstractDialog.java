package Librares;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import com.jidesoft.dialog.BannerPanel;
import com.jidesoft.dialog.ButtonPanel;
import com.jidesoft.dialog.StandardDialog;
import com.jidesoft.swing.JideSwingUtilities;
import com.jidesoft.swing.JideTitledBorder;
import com.jidesoft.swing.PartialEtchedBorder;
import com.jidesoft.swing.PartialSide;

public class AbstractDialog extends StandardDialog
{
	protected JButton _okButton = new JButton("OK");

	protected JButton _cancelButton = new JButton("Cancel");

	protected boolean isPack = false;

	public AbstractDialog(Frame parent, String title)
	{
		super(parent, title);
		 addCancelAction();
		 setResizable(false);
	}
	
	public AbstractDialog(Frame parent, String title, boolean modal)
	{
		super( parent, title, modal);
		 addCancelAction();
		 setResizable(false);
	}

	public AbstractDialog(String title)
	{
		super((Frame) null, title);
		 addCancelAction();
		 setResizable(false);
	}

	public AbstractDialog()
	{
		super();
		 addCancelAction();
		 setResizable(false);
	}

	public AbstractDialog(Frame parent)
	{
		super(parent);
		 addCancelAction();
		 setResizable(false);
	}

	public AbstractDialog(Dialog owner, String title, boolean modal)
	{
		super( owner, title, modal);
		 addCancelAction();
		 setResizable(false);
	}

	protected void addCancelAction()
	{
		Action action = new AbstractAction() {
			public void actionPerformed(ActionEvent ae)
			{
				cancelActionPerformed(ae);
			}
		};

		KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
		rootPane.getActionMap().put(action, action);
		rootPane.getInputMap(JComponent.WHEN_FOCUSED).put(stroke, action);
		rootPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
				.put(stroke, action);

	}
	
	public JComponent createBannerPanel()
	{
		return null;
	}

	public static BannerPanel createBannerPanel(String bannerTitle, String bannerText)
	{
		return null;
	}

	public static BannerPanel createBannerPanel(String bannerTitle, String bannerText, ImageIcon bannerIcon)
	{
		return null;
	}

	public static Border createButtonPanelBorder()
	{
		return BorderFactory.createCompoundBorder(new JideTitledBorder(
				new PartialEtchedBorder(PartialEtchedBorder.LOWERED,
						PartialSide.NORTH), ""), BorderFactory
				.createEmptyBorder(9, 6, 9, 6));
	}

	public static Border createContentPanelBorder()
	{
		return BorderFactory.createEmptyBorder(10, 10, 0, 10);
	}

	public void setBannerTitle(String title)
	{
	}

	public void setBannerSubTitle(String title)
	{
	}

	public JComponent createContentPanel()
	{
		JPanel panel = new JPanel(new BorderLayout(10, 10));
		panel.setBorder(createContentPanelBorder());
		return panel;
	}

	public ButtonPanel createButtonPanel()
	{
		ButtonPanel okCancelPanel = new ButtonPanel();
		_okButton.setName(OK);
		_cancelButton.setName(CANCEL);
		okCancelPanel.addButton(_okButton, ButtonPanel.AFFIRMATIVE_BUTTON);
		okCancelPanel.addButton(_cancelButton, ButtonPanel.CANCEL_BUTTON);
		
		_okButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				okActionPerformed(e);
			}
		});
		_cancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				cancelActionPerformed(e);
			}
		});
		setDefaultCancelAction(_cancelButton.getAction());
		setDefaultAction(_okButton.getAction());
		getRootPane().setDefaultButton(_okButton);
		okCancelPanel.setBorder(createButtonPanelBorder());
		return okCancelPanel;
	}

	protected void okActionPerformed(ActionEvent e)
	{
		setDialogResult(RESULT_AFFIRMED);
		setVisible(false);
		dispose();
		
	}

	protected void cancelActionPerformed(ActionEvent e)
	{
		setDialogResult(RESULT_CANCELLED);
		setVisible(false);
		dispose();
	}

	protected void standardPack()
	{
		super.pack();
		JideSwingUtilities.globalCenterWindow(this);
	}

	public void pack()
	{
		super.pack();
		if( isPack == false)
		{	
			Dimension d = getSize();
			d.height += 2;
			d.width += 2;
			if(d.width > 450)
				d.width = 400;
			setMinimumSize(d);
			d.height *= 1.2;
			d.width *= 1.5;
			setPreferredSize(d);
		}
		isPack = true;
		if(this.getOwner() != null)
			JideSwingUtilities.centerWindow(this);
		else
		JideSwingUtilities.globalCenterWindow(this);
	}
	
	public void packRename()
	{
		super.pack();
		if( isPack == false)
		{	
			Dimension d = getSize();
			d.height += 2;
			d.width += 2;
			setMinimumSize(d);
			d.height *= 1.2;
			d.width *= 1.05;
			setPreferredSize(d);
						
		}
		isPack = true;
		JideSwingUtilities.globalCenterWindow(this);
	}

	public boolean isAffirmed()
	{
		return getDialogResult() == RESULT_AFFIRMED;
	}
	
	 
	public Frame getParent()
	{
		return (Frame) super.getParent();
	}

	@SuppressWarnings("deprecation")
	@Override
	public void hide()
	{		
		super.hide();
		
		//fix modal dialogs memory leaks
		if(isModal())
		{
//			Bug6497929.fix(this);
		}
	}
}

