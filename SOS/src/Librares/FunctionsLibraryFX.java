package Librares;

import tabs.ConfirmationType;
import tabs.FXDialog;
import tabs.Message;

import Librares.TypesLibraryFX.ButtonAction;
import Main.FunctionsLoader;
import Main.Main;
import Main.TypeLoader;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class FunctionsLibraryFX extends Application{
	private TableView<FunctionObject> table = new TableView<FunctionObject>();
	private ObservableList<FunctionObject> dataTypes;
		
	//for make changes in library (and for initial values then changes)
	public static boolean libraryWasChanged = false;
	
	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("���������� �������");
		final VBox vBox = new VBox();
    	
    	Label inf_type_list = new Label("������ �������, ��������������� �����������");
    	inf_type_list.setFont(Font.font("Arial", FontWeight.BOLD, 12));
    	inf_type_list.setMinWidth(800);
    	inf_type_list.setAlignment(Pos.CENTER);
    	inf_type_list.setTextFill(Color.color(0.3, 0.3, 0.3));// Dark grey
    	        
    	// �������
    	TableColumn<FunctionObject, String> colType = new TableColumn<FunctionObject, String>("�������");
    	colType.setCellValueFactory(new PropertyValueFactory<FunctionObject, String>("Function"));
    	colType.setMinWidth(450);
    	table.getColumns().add(colType);
    	// ��������� � ���������������
    	TableColumn<FunctionObject, String> colSOEl = new TableColumn<FunctionObject, String>("��������� � ���������������");
        colSOEl.setCellValueFactory(new PropertyValueFactory<FunctionObject, String>("ElementInRecovery"));
        colSOEl.setMinWidth(350);
    	table.getColumns().add(colSOEl);        		
		
    	LoadTableFX();
    	
		table.setMaxHeight(290);
        table.setMaxWidth(802);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        HBox hBox = new HBox();
        Button buttonCreate = new Button("������� �������");
        ButtonOnClick(buttonCreate, ButtonAction.Create);
        Button buttonRemove = new Button("������� �������");
        ButtonOnClick(buttonRemove, ButtonAction.Remove);
        Button buttonChange = new Button("�������� �������");
        ButtonOnClick(buttonChange, ButtonAction.Change);
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(5, 0, 0, 0));
        hBox.getChildren().addAll(buttonCreate, buttonRemove, buttonChange);
        
        vBox.setSpacing(5);
        vBox.setPadding(new Insets(10, 10, 20, 10));
        vBox.getChildren().addAll(inf_type_list,table,hBox);

		Scene scene = new Scene(vBox);	
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
		stage.setScene(scene);
		stage.show();
	}

	private void LoadTableFX() 
	{
		String name, participation;
		dataTypes = FXCollections.observableArrayList();
		for(int funcInd=0; funcInd<FunctionsLoader.elementaryFunctionsList.size(); funcInd++)
		{
			name = FunctionsLoader.elementaryFunctionsList.get( funcInd );
			participation = "false";
			if( FunctionsLoader.isParticipate(name) )
			{
				participation = "true";
			}
			dataTypes.add(new FunctionObject(name, participation));
		}
		table.setItems(dataTypes);
		libraryWasChanged = false;
	}
	
	private void ButtonOnClick(Button button, final ButtonAction action){
 		button.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, new EventHandler<javafx.scene.input.MouseEvent>() {
 	        public void handle(javafx.scene.input.MouseEvent mc) {  
 	        	if(action == ButtonAction.Create)
 	        	{
 	        		Stage stage = new Stage();
 	        		try {
						(new ChangesInFunctionsLibraryFX("���������� ������� � ����������", null)).start(stage);
					} catch (Exception e) {
						e.printStackTrace();
					}
 	        		stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
						public void handle(javafx.stage.WindowEvent event) {
							RefreshFX();
						}
 	        		});
 	           	}
 	        	else if(action == ButtonAction.Remove)
 	        	{
 	        		String deletedElementName;
 					if(!table.getSelectionModel().getSelectedItems().isEmpty())
 					{
 						deletedElementName = table.getSelectionModel().getSelectedItem().getFunction();
 						if( ! TypeLoader.isAnyTypeContainsElementaryFunction(deletedElementName) )
 						{
 	 	        			boolean confirm = FXDialog.showConfirmDialog("�� ������������� ������ ������� �������: " + deletedElementName, "�������� �������", ConfirmationType.DELETE_OPTION);
 							if (confirm) 
 							{
 								FunctionsLoader.elementaryFunctionsList.remove(deletedElementName);
 								FunctionsLoader.functionsCharacteristicsMap.remove(deletedElementName);
 								FunctionsLoader.saveCurrentElementaryFunctionsSet();
 								RefreshFX();
 							}
 						}
 						else
 						{
 							FXDialog.showMessageDialog("��������, �� ������ ������� �� ����� ���� �������" +
 									"\n��� ��� ��� ������������ � ���������� �����", "�������� �������", Message.WARNING);	
 						}
 					}
 					else
 					{
 						FXDialog.showMessageDialog("����� ��������� ���������� ������� �������", "�������� �������", Message.INFORMATION);	
 					}
 	        	}
 	        	else if(action == ButtonAction.Change)
 	        	{
 	        		int index = table.getSelectionModel().getSelectedIndex();
 					if(index != -1)
 					{
 						Stage stage = new Stage();
 	 	        		try {
 							(new ChangesInFunctionsLibraryFX("���������� ������� � ����������", table.getSelectionModel().getSelectedItem().getFunction())).start(stage);
 						} catch (Exception e) {
 							e.printStackTrace();
 						}
 	 	        		stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
 							public void handle(javafx.stage.WindowEvent event) {
 								RefreshFX();
 							}
 	 	        		});
 					}
 					else
 					{
						FXDialog.showMessageDialog("����� ���������� ���������� ������� �������", "��������� �������", Message.INFORMATION);	
 					}
 	        	}
 	        }
         });
 	}
	
	private void RefreshFX()
	{
		while (!table.getItems().isEmpty())
		{
			table.getItems().remove(0);
		}
		LoadTableFX();
	}

	public class FunctionObject
	{
		private SimpleStringProperty Function;
		private SimpleStringProperty ElementInRecovery;
		
		public FunctionObject(String _function, String _elementInRecovery)
		{
			this.Function = new SimpleStringProperty(_function);
			this.ElementInRecovery = new SimpleStringProperty(_elementInRecovery);
		}
		
		public String getFunction(){
			return Function.get();
		}
		public void setFunction(String _function){
			Function.set(_function);
		}
		public String getElementInRecovery(){
			return ElementInRecovery.get();
		}
		public void setElementInRecovery(String _elementInRecovery){
			ElementInRecovery.set(_elementInRecovery);
		}
	}
	
}
