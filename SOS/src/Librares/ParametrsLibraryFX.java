package Librares;

import java.util.Iterator;

import javax.swing.table.DefaultTableModel;

import tabs.ConfirmationType;
import tabs.FXDialog;
import tabs.Message;

import Librares.TypesLibraryFX.ButtonAction;
import Main.FunctionsLoader;
import Main.Main;
import Main.ParametersLoader;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class ParametrsLibraryFX extends Application{
	private TableView<ParameterObject> table = new TableView<ParameterObject>();
	private ObservableList<ParameterObject> dataTypes;

	private int parametr_choose_index;
	
	public static DefaultTableModel tableModel;
	
	public static boolean libraryWasChanged = false;
	public static Float newLeftBorder = -1.0f;
	public static Float newRightBorder  = 0.0f;
	public static Float newInitialValue = 1.0f;
	public static String newName = null;
	public static String oldName = null;
	public static boolean newParticipation = false;
	
	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("���������� ����������");
		final VBox vBox = new VBox();
    	
    	Label inf_type_list = new Label("������ ����������, ��������������� �����������");
    	inf_type_list.setFont(Font.font("Arial", FontWeight.BOLD, 12));
    	inf_type_list.setMinWidth(1029);
    	inf_type_list.setAlignment(Pos.CENTER);
    	inf_type_list.setTextFill(Color.color(0.3, 0.3, 0.3));// Dark grey
    	        
    	// ������������
    	TableColumn<ParameterObject, String> colType = new TableColumn<ParameterObject, String>("������������");
    	colType.setCellValueFactory(new PropertyValueFactory<ParameterObject, String>("Name"));
    	colType.setMinWidth(350);
    	table.getColumns().add(colType);
    	// ������ �������
    	TableColumn<ParameterObject, String> colFunction = new TableColumn<ParameterObject, String>("������ �������");
        colFunction.setCellValueFactory(new PropertyValueFactory<ParameterObject, String>("BottomBorder"));
        colFunction.setMinWidth(150);
    	table.getColumns().add(colFunction);
		// ������� �������
		TableColumn<ParameterObject, String> colSlotCount = new TableColumn<ParameterObject, String>("������� �������");
		colSlotCount.setCellValueFactory(new PropertyValueFactory<ParameterObject, String>("TopBorder"));
		colSlotCount.setMinWidth(150);
		table.getColumns().add(colSlotCount);
    	// ��������� ��������
    	TableColumn<ParameterObject, String> colWeight = new TableColumn<ParameterObject, String>("��������� ��������");
        colWeight.setCellValueFactory(new PropertyValueFactory<ParameterObject, String>("StartValue"));
        colWeight.setMinWidth(150);
    	table.getColumns().add(colWeight);
    	// ��������� � ���������������
    	TableColumn<ParameterObject, String> colSOEl = new TableColumn<ParameterObject, String>("��������� � ���������������");
        colSOEl.setCellValueFactory(new PropertyValueFactory<ParameterObject, String>("ElementInRecovery"));
        colSOEl.setMinWidth(200);
    	table.getColumns().add(colSOEl);        		
		
    	LoadTableFX();
    	
		table.setMaxHeight(290);
        table.setMaxWidth(1002);
        table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.setPadding(new Insets(5, 0, 0, 0));
        Button buttonCreate = new Button("������� ��������");
        ButtonOnClick(buttonCreate, ButtonAction.Create);
        Button buttonRemove = new Button("������� ��������");
        ButtonOnClick(buttonRemove, ButtonAction.Remove);
        Button buttonChange = new Button("�������� ��������");
        ButtonOnClick(buttonChange, ButtonAction.Change);
        hBox.getChildren().addAll(buttonCreate, buttonRemove, buttonChange);
        
        vBox.setSpacing(5);
        vBox.setPadding(new Insets(10, 0, 0, 10));
        vBox.getChildren().addAll(inf_type_list,table,hBox);

		Scene scene = new Scene(vBox, 1023, 365);	
		scene.getStylesheets().add(Main.class.getResource("stylesheets.css").toExternalForm());
//    	scene.setFill(new LinearGradient(0,0,0,0.1,true,CycleMethod.NO_CYCLE, 
//				new Stop(0.0, Color.rgb(75,75,75)), 
//				new Stop(1.0, Color.rgb(25,25,25))));
		scene.setFill(new LinearGradient(0,0,0,0.3,true,CycleMethod.NO_CYCLE, 
				new Stop(0.0, Color.rgb(100,100,100)), 
				new Stop(1.0, Color.rgb(70,70,70))));
		stage.setScene(scene);
		stage.show();
	}

	private void ButtonOnClick(Button button, final ButtonAction action){
 		button.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, new EventHandler<javafx.scene.input.MouseEvent>() {
 	        public void handle(javafx.scene.input.MouseEvent mc) {  
 	        	if(action == ButtonAction.Create)
 	        	{
        			oldName = null;
        			Stage stage = new Stage();
 	        		try {
						(new ChangesInParametrsLibraryFX("���������� ���������", null)).start(stage);
					} catch (Exception e) {
						e.printStackTrace();
					}
 	        		stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
						public void handle(javafx.stage.WindowEvent event) {
							RefreshFX();
						}
 	        		});					
 	           	}
 	        	else if(action == ButtonAction.Remove)
 	        	{
 	        		if(!table.getSelectionModel().getSelectedItems().isEmpty())
 	        		{
 	        			boolean confirm = FXDialog.showConfirmDialog("�� ������������� ������ ������� ��������� ���������?", "�������� ���������", ConfirmationType.DELETE_OPTION);
 	        			if(confirm)
 	        			{
		 	        		String deletedElementName;
		 	        		Iterator<ParameterObject> it = table.getSelectionModel().getSelectedItems().iterator();
		 	        		String noDeleteParameters = "";
		 	        		while(it.hasNext())
		 	        		{
		 	        			ParameterObject tempObj = it.next();
		 	        			parametr_choose_index = table.getItems().indexOf(tempObj);
		 	        			deletedElementName = table.getItems().get(parametr_choose_index).Name.getValue().toString();
		 	        			if(FunctionsLoader.findAllElementaryFunctionsIncludingParametr( deletedElementName ) == null)
		 	 					{
		 	        				ParametersLoader.parametrsList.remove(deletedElementName);
	 								ParametersLoader.parametrsCharacteristicsMap.remove(deletedElementName);
	 								ParametersLoader.saveCurrentParametrsSet();
		 	 					}
		 	        			else
		 	        			{
		 	        				if(noDeleteParameters == "") noDeleteParameters = deletedElementName;
		 	        				else noDeleteParameters += ", " + deletedElementName;
		 	        			}
		 	        		}
		 	        		if(noDeleteParameters != "")
		 	        		{
	 	 	        			FXDialog.showMessageDialog("��������, �� ��������� \"" + noDeleteParameters + "\" �� ����� ���� �������" +
	 									"\n��� ��� ��� ������������ � ���������� �������", "�������� ���������", Message.ERROR);	 	        					 	        			
		 	        		}
		 	        		RefreshFX();
 	        			}
 	        		}
 	        		else
 	        		{
 	        			FXDialog.showMessageDialog("����������, �������� ��������� ��� ��������", "�������� ���������", Message.INFORMATION);
 	        		}
 	        	}
 	        	else if(action == ButtonAction.Change)
 	        	{
 	        		if(table.getSelectionModel().getSelectedItems().size() <= 1)
 	        		{
 	        			int index = table.getSelectionModel().getSelectedIndex();
 	        			if(index >= 0)
 	        			{
 	        				newName = table.getSelectionModel().getSelectedItem().getName();
	 	   					newLeftBorder = Float.parseFloat(table.getSelectionModel().getSelectedItem().getBottomBorder());
	 	   					newRightBorder  = Float.parseFloat(table.getSelectionModel().getSelectedItem().getTopBorder());
	 	   					newInitialValue = Float.parseFloat(table.getSelectionModel().getSelectedItem().getStartValue());
	 	   					newParticipation = false;
	 	   					oldName = table.getSelectionModel().getSelectedItem().getName();
	 	   					if(table.getSelectionModel().getSelectedItem().getElementInRecovery().equals("true") )
	 	   					{
	 	   						newParticipation = true;
	 	   					}
	 	   					
		 	   				Stage stage = new Stage();
		 	        		try {
								(new ChangesInParametrsLibraryFX("���������� ���������", oldName)).start(stage);
							} catch (Exception e) {
								e.printStackTrace();
							}
		 	        		stage.setOnHiding(new EventHandler<javafx.stage.WindowEvent>(){
								public void handle(javafx.stage.WindowEvent event) {
									RefreshFX();
								}
		 	        		});	

 	        			}
 	        			else
 	        			{
 		        			FXDialog.showMessageDialog("����� ���������� ���������� ������� ��������", "��������� ���������", Message.INFORMATION);	 	        				
 	        			}
 	        		}
 	        		else
 	        		{
 	        			FXDialog.showMessageDialog("����� �������� ������ ���� ������� �� ���", "��������� ���������", Message.WARNING);	 
 	        		}
 	        	}
 	        }
         });
 	}
	
	private void LoadTableFX()
	{
		String name, left_border, right_border, init_value, participation;
		dataTypes = FXCollections.observableArrayList();
		for(int paramInd=0; paramInd<ParametersLoader.parametrsList.size(); paramInd++)
		{
			name = ParametersLoader.parametrsList.get( paramInd );
			init_value = String.valueOf( ParametersLoader.getInitialValue( name ) );
			left_border = String.valueOf( ParametersLoader.getLeftBorder( name ) );
			right_border = String.valueOf( ParametersLoader.getRightBorder( name ) );
			participation = "false";
			if( ParametersLoader.isParticipate(name) ) participation = "true";
			dataTypes.add(new ParameterObject(name, left_border, right_border, init_value, participation));
		}
		table.setItems(dataTypes);
		libraryWasChanged = false;
	}
	
	private void RefreshFX()
	{
		while (!table.getItems().isEmpty())
		{
			table.getItems().remove(0);
		}
		LoadTableFX();
	}

	public class ParameterObject
	{
		private SimpleStringProperty Name;
		private SimpleStringProperty BottomBorder;
		private SimpleStringProperty TopBorder;
		private SimpleStringProperty StartValue;
		private SimpleStringProperty ElementInRecovery;
		
		public ParameterObject(String _name, String _bottomBorder, String _topBorder, String _startValue, String _elementInRecovery)
		{
			this.Name = new SimpleStringProperty(_name);
			this.BottomBorder = new SimpleStringProperty(_bottomBorder);
			this.TopBorder = new SimpleStringProperty(_topBorder);
			this.StartValue = new SimpleStringProperty(_startValue);
			this.ElementInRecovery = new SimpleStringProperty(_elementInRecovery);
		}
		
		public String getName(){
			return Name.get();
		}
		public void setName(String _name){
			Name.set(_name);
		}
		public String getBottomBorder(){
			return BottomBorder.get();
		}
		public void setBottomBorder(String _bottomBorder){
			BottomBorder.set(_bottomBorder);
		}
		public String getTopBorder(){
			return TopBorder.get();
		}
		public void setTopBorder(String _topBorder){
			TopBorder.set(_topBorder);
		}
		public String getStartValue(){
			return StartValue.get();
		}
		public void setStartValue(String _startValue){
			StartValue.set(_startValue);
		}
		public String getElementInRecovery(){
			return ElementInRecovery.get();
		}
		public void setElementInRecovery(String _elementInRecovery){
			ElementInRecovery.set(_elementInRecovery);
		}
	}
}
